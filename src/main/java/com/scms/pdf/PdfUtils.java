package com.scms.pdf;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.scms.model.DeliveryChallan;
import com.scms.model.Orders;
import com.scms.model.User;

@Service("pdfUtils")
public class PdfUtils {

	static Font blackFont10 = FontFactory.getFont("Helvetica", 10.0F, 0, new CMYKColor(59, 14, 0, 220));

	static Font blackFont10Bold = FontFactory.getFont("Helvetica", 10.0F, 1, new CMYKColor(59, 14, 0, 220));

	static Font blackFont15 = FontFactory.getFont("Helvetica", 12.0F, 0, new CMYKColor(59, 14, 0, 220));

	static Font blackFont20 = FontFactory.getFont("Helvetica", 20.0F, 0, new CMYKColor(59, 14, 0, 220));

	static Font whiteFont10 = FontFactory.getFont("Helvetica", 10.0F, 0, new CMYKColor(0, 0, 0, 0));

	static Font whiteFont12 = FontFactory.getFont("Helvetica", 12.0F, 0, new CMYKColor(0, 0, 0, 0));

	static Font whiteFont20 = FontFactory.getFont("Helvetica-Bold", 20.0F, 1, new CMYKColor(0, 0, 0, 0));

	public static void addEmptyLine(final Paragraph paragraph, final int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

	public static void addMetaData(final Document document, final String title, final String subject) {
		document.addCreationDate();
		document.addTitle(title);
		document.addSubject(subject);
		document.addAuthor("SCMS Application");
		document.addCreator("SCMS Application");
	}

	public static PdfPTable getDcTable(final Map<String, String> map, final float[] columnWidths)
			throws DocumentException {
		final PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100.0F);
		table.setSpacingBefore(0.0F);
		table.setSpacingAfter(5.0F);
		table.setWidths(columnWidths);
		for (final Map.Entry<String, String> entry : map.entrySet()) {
			final PdfPCell cell1 = new PdfPCell(new Paragraph(entry.getKey(), blackFont10Bold));
			final PdfPCell cell2 = new PdfPCell(new Paragraph(entry.getValue(), blackFont10));
			cell1.setPadding(5.0F);
			cell2.setPadding(5.0F);
			cell1.setBorderColor(BaseColor.WHITE);
			cell2.setBorderColor(BaseColor.WHITE);
			table.addCell(cell1);
			table.addCell(cell2);
		}
		return table;
	}

	public static PdfPTable getTable(final List<String> list) {
		final PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100.0F);
		table.setSpacingBefore(0.0F);
		table.setSpacingAfter(5.0F);
		list.stream().forEach(str -> {
			final PdfPCell cell = new PdfPCell(new Paragraph(str, blackFont10));
			cell.setPadding(2.0F);
			cell.setBorderColor(BaseColor.WHITE);
			table.addCell(cell);
		});
		return table;
	}

	public static PdfPTable getTable(final Map<String, String> map, final float[] columnWidths)
			throws DocumentException {
		final PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100.0F);
		table.setSpacingBefore(0.0F);
		table.setSpacingAfter(5.0F);
		table.setWidths(columnWidths);
		for (final Map.Entry<String, String> entry : map.entrySet()) {
			final PdfPCell cell1 = new PdfPCell(new Paragraph(entry.getKey(), blackFont10Bold));
			final PdfPCell cell2 = new PdfPCell(new Paragraph(entry.getValue(), blackFont10));
			cell1.setPadding(2.0F);
			cell2.setPadding(2.0F);
			cell1.setBorderColor(BaseColor.WHITE);
			cell2.setBorderColor(BaseColor.WHITE);
			table.addCell(cell1);
			table.addCell(cell2);
		}
		return table;
	}

	@Autowired
	private DcInvoice dcInvoice;

	@Autowired
	private OrderInvoice orderInvoice;

	public String getDcPdf(final User user, final DeliveryChallan deliveryChallan) {
		return dcInvoice.generatePdf(user, deliveryChallan);
	}

	public String getDcPdf(final User user, final Orders order) {
		return dcInvoice.generatePdf(user, order);
	}

	public String getOrderPdf(final User user, final Orders order) {
		return orderInvoice.generatePdf(user, order);
	}
}
