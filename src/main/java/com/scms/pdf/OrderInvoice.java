package com.scms.pdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.scms.constants.Constants;
import com.scms.enums.AddressType;
import com.scms.model.Address;
import com.scms.model.Bank;
import com.scms.model.Company;
import com.scms.model.Customer;
import com.scms.model.Item;
import com.scms.model.Orders;
import com.scms.model.TcPlaceOrder;
import com.scms.model.User;
import com.scms.util.Utility;

@Component
public class OrderInvoice extends PdfUtils {

	private static final Logger log = LoggerFactory.getLogger(DcInvoice.class);

	public String generatePdf(final User user, final Orders od) {
		log.info("generatePdf() Start : Order Id : {} ", od.getId());
		final String fileName = Constants.PATH_PDF_OD + od.getSid() + Constants.DOT_PDF_EXT;

		final File file = new File(fileName);
		final Document document = new Document(PageSize.A4);
		final Map<String, String> linkedHashMap = new LinkedHashMap<>();

		try {
			final Company company = user.getEmployee().getCompany();
			final Address cmpAddr = company.getAddress();

			final FileOutputStream outputFile = new FileOutputStream(file);
			log.debug("file path : {}", file.getAbsolutePath());
			final PdfWriter writer = PdfWriter.getInstance(document, outputFile);
			document.open();

			final PdfPTable mainTable = new PdfPTable(9);
			mainTable.setWidthPercentage(100.0F);
			mainTable.setSpacingBefore(10.0F);
			mainTable.setSpacingAfter(10.0F);
			final float[] mainTableWidth = { 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F };
			mainTable.setWidths(mainTableWidth);

			/****************** ROW ******************/
			PdfPCell mtCell = new PdfPCell(new Paragraph(company.getName(), whiteFont20));
			mtCell.setBackgroundColor(new BaseColor(60, 124, 145));
			mtCell.setHorizontalAlignment(1);
			mtCell.setVerticalAlignment(1);
			mtCell.setPaddingBottom(5.0F);
			mtCell.setPaddingTop(15.0F);
			mtCell.setColspan(9);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(new BaseColor(60, 124, 145));
			mainTable.addCell(mtCell);

			/****************** ROW ******************/
			final Paragraph companyAddr = new Paragraph(14.0F, "", whiteFont10);
			companyAddr.add(cmpAddr.toString() + "\n");
			companyAddr.add("Phone : " + company.getPhone() + ", Email : " + company.getEmail() + ", Website : "
					+ company.getWebsite());
			companyAddr.setAlignment(1);
			final PdfPCell companyAddrCell = new PdfPCell();
			companyAddrCell.setBackgroundColor(new BaseColor(60, 124, 145));
			companyAddrCell.setHorizontalAlignment(1);
			companyAddrCell.setVerticalAlignment(1);
			companyAddrCell.setPaddingBottom(5.0F);
			companyAddrCell.setColspan(9);
			companyAddrCell.setUseVariableBorders(true);
			companyAddrCell.setBorderColor(new BaseColor(60, 124, 145));
			companyAddrCell.addElement(companyAddr);
			companyAddrCell.addElement(new LineSeparator(2.0F, 100.0F, new BaseColor(238, 242, 243), 1, -14.0F));
			mainTable.addCell(companyAddrCell);

			/****************** ROW ******************/
			mtCell = new PdfPCell(new Paragraph("TAX INVOICE",
					FontFactory.getFont("Helvetica", 15.0F, 0, new CMYKColor(0, 0, 0, 0))));
			mtCell.setBackgroundColor(new BaseColor(60, 124, 145));
			mtCell.setHorizontalAlignment(1);
			mtCell.setVerticalAlignment(1);
			mtCell.setPaddingBottom(15.0F);
			mtCell.setPaddingTop(20.0F);
			mtCell.setPaddingRight(25.0F);
			mtCell.setColspan(9);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(new BaseColor(60, 124, 145));
			mainTable.addCell(mtCell);

			/****************** ROW ******************/
			mtCell = new PdfPCell(
					new Paragraph("Invoice Id : " + od.getSid() + "/" + od.getFinancialYear(), blackFont10));
			mtCell.setPaddingBottom(10.0F);
			mtCell.setColspan(5);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph("Date : " + Utility.getFormattedDate(Constants.DD_MM_YYYY_HH_MM_A, od.getOrderDate()), blackFont10));
			mtCell.setHorizontalAlignment(2);
			mtCell.setPaddingBottom(10.0F);
			mtCell.setColspan(4);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			/****************** ROW ******************/
			mtCell = new PdfPCell(new Paragraph("Customer Details", whiteFont12));
			mtCell.setBackgroundColor(new BaseColor(60, 124, 145));
			mtCell.setPaddingLeft(5.0F);
			mtCell.setPaddingBottom(10.0F);
			mtCell.setPaddingTop(5.0F);
			mtCell.setColspan(3);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(new BaseColor(60, 124, 145));
			mtCell.setBorderColorRight(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph("Billing Address", whiteFont12));
			mtCell.setBackgroundColor(new BaseColor(60, 124, 145));
			mtCell.setPaddingLeft(5.0F);
			mtCell.setPaddingTop(5.0F);
			mtCell.setColspan(3);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(new BaseColor(60, 124, 145));
			mtCell.setBorderColorRight(BaseColor.WHITE);
			mtCell.setBorderColorLeft(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph("Shipping Address", whiteFont12));
			mtCell.setBackgroundColor(new BaseColor(60, 124, 145));
			mtCell.setPaddingLeft(5.0F);
			mtCell.setPaddingTop(5.0F);
			mtCell.setColspan(3);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(new BaseColor(60, 124, 145));
			mtCell.setBorderColorLeft(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			/****************** ROW ******************/
			final Customer cust = od.getCustomer();
			final PdfPCell custDetailsCell = new PdfPCell();
			linkedHashMap.clear();
			linkedHashMap.put("Name", cust.getName());
			linkedHashMap.put("Mobile", cust.getPhone());
			linkedHashMap.put("Firm", cust.getFirm());
			linkedHashMap.put("Email", cust.getEmail());
			custDetailsCell.addElement(getTable(linkedHashMap, new float[] { 30.0F, 70.0F }));
			custDetailsCell.setPaddingLeft(5.0F);
			custDetailsCell.setPaddingBottom(10.0F);
			custDetailsCell.setColspan(3);
			custDetailsCell.setUseVariableBorders(true);
			custDetailsCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(custDetailsCell);

			final Address billAddr = od.getCustomer().getAddress().stream()
					.filter(add -> (add.getType().compareTo(AddressType.BILLING) == 0)).findFirst().orElse(null);
			final PdfPCell billAddressCell = new PdfPCell();
			billAddressCell.addElement(getTable(Arrays.asList(new String[] { billAddr.getStreet(), billAddr.getCity(),
					billAddr.getState().getName(), billAddr.getPincode() })));
			billAddressCell.setPaddingLeft(5.0F);
			billAddressCell.setPaddingBottom(10.0F);
			billAddressCell.setColspan(3);
			billAddressCell.setUseVariableBorders(true);
			billAddressCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(billAddressCell);

			final Address shippAddr = od.getAddress();
			final PdfPCell shippAddressCell = new PdfPCell();
			shippAddressCell.addElement(getTable(Arrays.asList(new String[] { shippAddr.getStreet(),
					shippAddr.getCity(), shippAddr.getState().getName(), shippAddr.getPincode() })));
			shippAddressCell.setPaddingLeft(5.0F);
			shippAddressCell.setPaddingBottom(10.0F);
			shippAddressCell.setColspan(3);
			shippAddressCell.setUseVariableBorders(true);
			shippAddressCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(shippAddressCell);

			/****************** ROW ******************/
			final String[] headerList = { "No.", "Product", "HSN", "Quantity", "Rate", "CGST", "SGST", "Total GST",
					"SubTotal" };
			for (final String headerText : headerList) {
				mtCell = new PdfPCell(new Paragraph(headerText, whiteFont10));
				mtCell.setBackgroundColor(new BaseColor(60, 124, 145));
				mtCell.setHorizontalAlignment(1);
				mtCell.setPaddingLeft(10.0F);
				mtCell.setPaddingBottom(10.0F);
				mtCell.setPaddingTop(10.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);
			}
			double totalGstAmt = Utility.toDouble(0);
			double totalCSgstAmt = Utility.toDouble(0);
			double totalOrderAmount = Utility.toDouble(0);
			int counter = 1;
			for (final Item o : od.getItems()) {
				totalGstAmt += Utility.toDouble(o.getGstAmt());
				totalCSgstAmt += Utility.toDouble(Utility.toDouble(o.getGstAmt()) / Utility.toDouble(2));
				totalOrderAmount += Utility.toDouble(o.getSubTotal());

				final String gstAmt = String
						.valueOf(Utility.toDouble(Utility.toDouble(o.getGstAmt()) / Utility.toDouble(2)));
				final String gst = String.valueOf(Utility.toDouble(Utility.toDouble(o.getGst()) / Utility.toDouble(2)));

				final String[] dataList = { String.valueOf(counter), o.getMaterial().getName(),
						o.getMaterial().getHsn(), String.valueOf(Utility.toDouble(o.getQtyBrass())),
						String.valueOf(Utility.toDouble(o.getRate())), gstAmt + " @" + gst + "%",
						gstAmt + " @" + gst + "%",
						String.valueOf(Utility.toDouble(o.getGstAmt())) + " @" + Utility.toDouble(o.getGst()) + "%",
						String.valueOf(Utility.toDouble(o.getSubTotal())) };
				for (final String data : dataList) {
					mtCell = new PdfPCell(new Paragraph(data, blackFont10));
					mtCell.setBackgroundColor(BaseColor.WHITE);
					mtCell.setHorizontalAlignment(1);
					mtCell.setPaddingLeft(10.0F);
					mtCell.setPaddingBottom(8.0F);
					mtCell.setPaddingTop(8.0F);
					mtCell.setUseVariableBorders(true);
					mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
					mainTable.addCell(mtCell);
				}
				counter++;
			}

			/****************** ROW ******************/
			mtCell = new PdfPCell(new Paragraph("Total", blackFont10Bold));
			mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
			mtCell.setHorizontalAlignment(2);
			mtCell.setColspan(5);
			mtCell.setPaddingRight(20.0F);
			mtCell.setPaddingBottom(8.0F);
			mtCell.setPaddingTop(8.0F);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph(String.valueOf(totalCSgstAmt), blackFont10Bold));
			mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
			mtCell.setHorizontalAlignment(1);
			mtCell.setPaddingLeft(10.0F);
			mtCell.setPaddingBottom(8.0F);
			mtCell.setPaddingTop(8.0F);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph(String.valueOf(totalCSgstAmt), blackFont10Bold));
			mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
			mtCell.setHorizontalAlignment(1);
			mtCell.setPaddingLeft(10.0F);
			mtCell.setPaddingBottom(8.0F);
			mtCell.setPaddingTop(8.0F);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph(String.valueOf(totalGstAmt), blackFont10Bold));
			mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
			mtCell.setHorizontalAlignment(1);
			mtCell.setPaddingLeft(10.0F);
			mtCell.setPaddingBottom(8.0F);
			mtCell.setPaddingTop(8.0F);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
			mainTable.addCell(mtCell);
			mtCell = new PdfPCell(new Paragraph(String.valueOf(totalOrderAmount), blackFont10Bold));
			mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
			mtCell.setHorizontalAlignment(1);
			mtCell.setPaddingLeft(10.0F);
			mtCell.setPaddingBottom(8.0F);
			mtCell.setPaddingTop(8.0F);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
			mainTable.addCell(mtCell);

			/****************** ROW ******************/
			if (od.isWithGst()) {
				mtCell = new PdfPCell(new Paragraph("Company GST TIN No", blackFont10Bold));
				mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
				mtCell.setColspan(3);
				mtCell.setPaddingBottom(8.0F);
				mtCell.setPaddingTop(8.0F);
				mtCell.setPaddingLeft(5.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColorRight(new BaseColor(238, 242, 243));
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph(company.getGstNo(), blackFont10));
				mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
				mtCell.setColspan(2);
				mtCell.setPaddingBottom(8.0F);
				mtCell.setPaddingTop(8.0F);
				mtCell.setPaddingLeft(5.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColorLeft(new BaseColor(238, 242, 243));
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph("Total GST + SubTotal", blackFont10Bold));
				mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
				mtCell.setHorizontalAlignment(2);
				mtCell.setColspan(3);
				mtCell.setPaddingBottom(8.0F);
				mtCell.setPaddingTop(8.0F);
				mtCell.setPaddingRight(10.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);
			} else {
				mtCell = new PdfPCell(new Paragraph("Invoice Total (Total GST + SubTotal)", blackFont10Bold));
				mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
				mtCell.setHorizontalAlignment(2);
				mtCell.setColspan(8);
				mtCell.setPaddingBottom(8.0F);
				mtCell.setPaddingTop(8.0F);
				mtCell.setPaddingRight(10.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);
			}

			mtCell = new PdfPCell(new Paragraph(String.valueOf(od.getTotal()), blackFont10Bold));
			mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
			mtCell.setHorizontalAlignment(2);
			mtCell.setPaddingBottom(8.0F);
			mtCell.setPaddingTop(8.0F);
			mtCell.setPaddingRight(10.0F);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
			mainTable.addCell(mtCell);

			/****************** ROW ******************/
			if (od.isWithGst()) {
				mtCell = new PdfPCell(new Paragraph("PAN NO ", blackFont10Bold));
				mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
				mtCell.setColspan(3);
				mtCell.setPaddingBottom(8.0F);
				mtCell.setPaddingTop(8.0F);
				mtCell.setPaddingLeft(5.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColorRight(new BaseColor(238, 242, 243));
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph(company.getPan(), blackFont10));
				mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
				mtCell.setColspan(2);
				mtCell.setPaddingBottom(8.0F);
				mtCell.setPaddingTop(8.0F);
				mtCell.setPaddingLeft(5.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColorLeft(new BaseColor(238, 242, 243));
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph("Discount", blackFont10Bold));
				mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
				mtCell.setHorizontalAlignment(2);
				mtCell.setColspan(3);
				mtCell.setPaddingBottom(8.0F);
				mtCell.setPaddingTop(8.0F);
				mtCell.setPaddingRight(10.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);
			} else {
				mtCell = new PdfPCell(new Paragraph("Discount", blackFont10Bold));
				mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
				mtCell.setHorizontalAlignment(2);
				mtCell.setColspan(8);
				mtCell.setPaddingBottom(8.0F);
				mtCell.setPaddingTop(8.0F);
				mtCell.setPaddingRight(10.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);
			}

			mtCell = new PdfPCell(new Paragraph(String.valueOf(od.getDiscount()), blackFont10Bold));
			mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
			mtCell.setHorizontalAlignment(2);
			mtCell.setPaddingBottom(8.0F);
			mtCell.setPaddingTop(8.0F);
			mtCell.setPaddingRight(10.0F);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
			mainTable.addCell(mtCell);

			if (od.isWithGst()) {
				mtCell = new PdfPCell(new Paragraph("Buyers GST TIN No", blackFont10Bold));
				mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
				mtCell.setColspan(3);
				mtCell.setPaddingBottom(8.0F);
				mtCell.setPaddingTop(8.0F);
				mtCell.setPaddingLeft(5.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColorRight(new BaseColor(238, 242, 243));
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph(cust.getGstNo(), blackFont10));
				mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
				mtCell.setColspan(2);
				mtCell.setPaddingBottom(8.0F);
				mtCell.setPaddingTop(8.0F);
				mtCell.setPaddingLeft(5.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColorLeft(new BaseColor(238, 242, 243));
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph("Total Received Amount", blackFont10Bold));
				mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
				mtCell.setHorizontalAlignment(2);
				mtCell.setColspan(3);
				mtCell.setPaddingBottom(8.0F);
				mtCell.setPaddingTop(8.0F);
				mtCell.setPaddingRight(10.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);
			} else {
				mtCell = new PdfPCell(new Paragraph("Total Received Amount", blackFont10Bold));
				mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
				mtCell.setHorizontalAlignment(2);
				mtCell.setColspan(8);
				mtCell.setPaddingBottom(8.0F);
				mtCell.setPaddingTop(8.0F);
				mtCell.setPaddingRight(10.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
				mainTable.addCell(mtCell);
			}

			mtCell = new PdfPCell(new Paragraph(String.valueOf(od.getReceivedAmt()), blackFont10Bold));
			mtCell.setBackgroundColor(new BaseColor(238, 242, 243));
			mtCell.setHorizontalAlignment(2);
			mtCell.setPaddingBottom(8.0F);
			mtCell.setPaddingTop(8.0F);
			mtCell.setPaddingRight(10.0F);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.LIGHT_GRAY);
			mainTable.addCell(mtCell);

			/****************** ROW ******************/
			mtCell = new PdfPCell(new Paragraph(" ", whiteFont12));
			mtCell.setBackgroundColor(BaseColor.WHITE);
			mtCell.setPaddingTop(1.0F);
			mtCell.setColspan(9);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			/****************** ROW ******************/
			mtCell = new PdfPCell(new Paragraph("Bank Details", whiteFont12));
			mtCell.setBackgroundColor(new BaseColor(60, 124, 145));
			mtCell.setPaddingTop(5.0F);
			mtCell.setPaddingBottom(10.0F);
			mtCell.setPaddingLeft(5.0F);
			mtCell.setColspan(4);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(new BaseColor(60, 124, 145));
			mtCell.setBorderColorLeft(new BaseColor(60, 124, 145));
			mtCell.setBorderColorRight(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph("Terms & Conditions", whiteFont12));
			mtCell.setBackgroundColor(new BaseColor(60, 124, 145));
			mtCell.setPaddingTop(5.0F);
			mtCell.setPaddingBottom(10.0F);
			mtCell.setPaddingLeft(5.0F);
			mtCell.setColspan(5);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(new BaseColor(60, 124, 145));
			mtCell.setBorderColorRight(new BaseColor(60, 124, 145));
			mtCell.setBorderColorLeft(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			/****************** ROW ******************/
			final Bank bank = company.getBank();
			final PdfPCell bankDetailsCell = new PdfPCell();
			linkedHashMap.clear();
			linkedHashMap.put("Bank Name", bank.getName());
			linkedHashMap.put("Account Name", bank.getAccName());
			linkedHashMap.put("Account No.", bank.getAccNo());
			linkedHashMap.put("IFSC Code", bank.getIfsc());
			linkedHashMap.put("Branch", bank.getBranch());
			bankDetailsCell.addElement(getTable(linkedHashMap, new float[] { 50.0F, 50.0F }));
			bankDetailsCell.setColspan(4);
			bankDetailsCell.setUseVariableBorders(true);
			bankDetailsCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(bankDetailsCell);

			final List<TcPlaceOrder> tc = new ArrayList<>();
			tc.add(new TcPlaceOrder(Long.valueOf(1L),
					"Intrest @18% will be charge on bills not paid within one month."));
			tc.add(new TcPlaceOrder(Long.valueOf(2L), "We are not responsibility for ant damage, loss in transport."));
			tc.add(new TcPlaceOrder(Long.valueOf(3L), "Goods ones sold will not taken back."));
			final List<String> tcList = tc.stream().map(obj -> obj.getId() + ". " + obj.getTc())
					.collect(Collectors.toList());
			final PdfPCell tcDetailsCell = new PdfPCell();
			tcDetailsCell.addElement(getTable(tcList));
			tcDetailsCell.setPaddingLeft(5.0F);
			tcDetailsCell.setPaddingBottom(10.0F);
			tcDetailsCell.setColspan(5);
			tcDetailsCell.setUseVariableBorders(true);
			tcDetailsCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(tcDetailsCell);
			document.add(mainTable);

			/****************** ROW ******************/
			final Paragraph hereBy = new Paragraph();
			addEmptyLine(hereBy, 1);
			hereBy.add(new Paragraph(
					"I / We hereby certify that the information on this invoice is true and correct and that the contents of this order are as stated above.\r\n",
					blackFont10));
			addEmptyLine(hereBy, 1);
			document.add(hereBy);

			/****************** ROW ******************/
			final Paragraph signature = new Paragraph();
			addEmptyLine(signature, 1);
			signature.add(new Paragraph("Signature of Authorized Person", blackFont10));
			addEmptyLine(signature, 1);
			document.add(signature);

			addMetaData(document, "Invoice #" + od.getSid(), "Invoice copy of Order No. #" + od.getSid());

			document.close();
			writer.close();
		} catch (final DocumentException e) {
			e.printStackTrace();
		} catch (final FileNotFoundException e) {
			e.printStackTrace();
		}
		log.info("generatePdf() End : Order Id : {} ", od.getId());
		return file.getAbsolutePath();
	}
}
