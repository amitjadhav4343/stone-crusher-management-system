package com.scms.pdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.scms.constants.Constants;
import com.scms.model.Address;
import com.scms.model.Company;
import com.scms.model.Customer;
import com.scms.model.DeliveryChallan;
import com.scms.model.Orders;
import com.scms.model.User;
import com.scms.util.StringUtils;
import com.scms.util.Utility;

@Component
public class DcInvoice extends PdfUtils {

	private static final Logger log = LoggerFactory.getLogger(DcInvoice.class);

	public String generatePdf(final User user, final DeliveryChallan dc) {
		log.info("generatePdf() Start : DC Id : " + dc.getId());
		final String fileName = Constants.PATH_PDF_DC + dc.getSid() + Constants.DOT_PDF_EXT;

		final File file = new File(fileName);
		final Document document = new Document(PageSize.LETTER);
		final Map<String, String> linkedHashMap = new LinkedHashMap<>();

		try {
			final Company company = user.getEmployee().getCompany();
			final Customer customer = dc.getCustomer();
			final Address da = dc.getAddress();

			final FileOutputStream outputFile = new FileOutputStream(file);
			log.debug("file path : {}", file.getAbsolutePath());
			final PdfWriter writer = PdfWriter.getInstance(document, outputFile);
			document.open();

			Paragraph emptyLine = new Paragraph();
			addEmptyLine(emptyLine, 1);
			addEmptyLine(emptyLine, 1);
			document.add(emptyLine);

			final PdfPTable mainTable = new PdfPTable(4);
			mainTable.setWidthPercentage(100.0F);
			mainTable.setSpacingBefore(10.0F);
			mainTable.setSpacingAfter(10.0F);
			final float[] mainTableWidth = { 48.0F, 2.0F, 2.0F, 48.0F };
			mainTable.setWidths(mainTableWidth);

			PdfPCell mtCell = new PdfPCell(new Paragraph(company.getName(), blackFont20));
			mtCell.setHorizontalAlignment(1);
			mtCell.setVerticalAlignment(1);
			mtCell.setPaddingBottom(15.0F);
			mtCell.setPaddingTop(10.0F);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.BLACK);
			mtCell.setBorderColorBottom(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph(""));
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColorRight(BaseColor.BLACK);
			mtCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph(""));
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph(company.getName(), blackFont20));
			mtCell.setHorizontalAlignment(1);
			mtCell.setVerticalAlignment(1);
			mtCell.setPaddingBottom(15.0F);
			mtCell.setPaddingTop(10.0F);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.BLACK);
			mtCell.setBorderColorBottom(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			linkedHashMap.clear();
			linkedHashMap.put("DC No.", dc.getSid());
			linkedHashMap.put("Date",
					dc.getOrderDate().format(DateTimeFormatter.ofPattern(Constants.DD_MM_YYYY_HH_MM_A)));
			linkedHashMap.put("Customer Name", customer.getName());
			linkedHashMap.put("Address", da.toString());
			linkedHashMap.put("Material", dc.getMaterial().getName());
			linkedHashMap.put("Quantity", String.valueOf(dc.getQtyBrass()) + " brass");
			linkedHashMap.put("Vehicle No.", dc.getVehicle().getNumber());
			linkedHashMap.put("Driver Signature", "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _");
			linkedHashMap.put("Customer Signature", "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _");

			mtCell = new PdfPCell();
			mtCell.addElement(getDcTable(linkedHashMap, new float[] { 40.0F, 60.0F }));
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.BLACK);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph(""));
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColorRight(BaseColor.BLACK);
			mtCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph(""));
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph(""));
			mtCell.addElement(getDcTable(linkedHashMap, new float[] { 40.0F, 60.0F }));
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.BLACK);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph("Customer's Copy"));
			mtCell.setHorizontalAlignment(1);
			mtCell.setPaddingTop(10.0F);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph(""));
			mtCell.setUseVariableBorders(true);
			mtCell.setColspan(2);
			mtCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			mtCell = new PdfPCell(new Paragraph("Office Copy"));
			mtCell.setHorizontalAlignment(1);
			mtCell.setPaddingTop(10.0F);
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.WHITE);
			mainTable.addCell(mtCell);

			emptyLine = new Paragraph();
			addEmptyLine(emptyLine, 1);

			emptyLine.add(new Paragraph(
					"--------------------------------------------------------------------------------------------------------------------------------------"));
			addEmptyLine(emptyLine, 1);

			mtCell = new PdfPCell();
			mtCell.setBorderColor(BaseColor.WHITE);
			mtCell.setColspan(4);
			mtCell.addElement(emptyLine);
			mainTable.addCell(mtCell);

			document.add(mainTable);
			addMetaData(document, "Delivery Challan #" + dc.getSid(), "Delivery Challan #" + dc.getSid());
			document.close();
			writer.close();
		} catch (final DocumentException e) {
			log.error("DocumentException in DCInvoice class!!!");
			e.printStackTrace();
		} catch (final FileNotFoundException e) {
			log.error("FileNotFoundException in DCInvoice class!!!");
			e.printStackTrace();
		}
		log.info("generatePdf() End : DC Id : " + dc.getId());
		return file.getAbsolutePath();
	}

	public String generatePdf(final User user, final Orders order) {
		log.info("generatePdf() Start : OrdersDetail Id : " + order.getId());
		final String orderSid = "ODC" + StringUtils.appendZeros(order.getId(), 8);
		final String fileName = Constants.PATH_PDF_OD + order.getSid() + Constants.DOT_PDF_EXT;
		final File file = new File(fileName);
		int counter = 1;
		final Document document = new Document(PageSize.LETTER);

		final Map<String, String> linkedHashMap = new LinkedHashMap<>();
		final List<DeliveryChallan> dcList = order.getItems().stream().flatMap(o -> o.getDeliveryChallans().stream())
				.collect(Collectors.toList());
		final int dcSize = dcList.size();
		log.debug("OrdersDetail dcList size : " + dcSize);

		try {
			final Company company = user.getEmployee().getCompany();
			final FileOutputStream outputFile = new FileOutputStream(file);
			log.debug("file path : {}", file.getAbsolutePath());

			final PdfWriter writer = PdfWriter.getInstance(document, outputFile);
			document.open();
			Paragraph emptyLine = new Paragraph();
			addEmptyLine(emptyLine, 1);
			document.add(emptyLine);

			for (final DeliveryChallan dc : dcList) {
				final PdfPTable mainTable = new PdfPTable(4);
				mainTable.setWidthPercentage(100.0F);
				mainTable.setSpacingBefore(10.0F);
				mainTable.setSpacingAfter(10.0F);
				final float[] mainTableWidth = { 48.0F, 2.0F, 2.0F, 48.0F };
				mainTable.setWidths(mainTableWidth);

				PdfPCell mtCell = new PdfPCell(new Paragraph(company.getName(), blackFont20));
				mtCell.setHorizontalAlignment(1);
				mtCell.setVerticalAlignment(1);
				mtCell.setPaddingBottom(15.0F);
				mtCell.setPaddingTop(10.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.BLACK);
				mtCell.setBorderColorBottom(BaseColor.WHITE);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph(""));
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColorRight(BaseColor.BLACK);
				mtCell.setBorderColor(BaseColor.WHITE);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph(""));
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.WHITE);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph(company.getName(), blackFont20));
				mtCell.setHorizontalAlignment(1);
				mtCell.setVerticalAlignment(1);
				mtCell.setPaddingBottom(15.0F);
				mtCell.setPaddingTop(10.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.BLACK);
				mtCell.setBorderColorBottom(BaseColor.WHITE);
				mainTable.addCell(mtCell);

				final Customer customer = dc.getCustomer();
				final Address da = dc.getAddress();
				linkedHashMap.clear();
				linkedHashMap.put("DC No.", dc.getSid());
				linkedHashMap.put("Date",
						dc.getOrderDate().format(DateTimeFormatter.ofPattern(Constants.DD_MM_YYYY_HH_MM_A)));
				linkedHashMap.put("Customer Name", customer.getName());
				linkedHashMap.put("Address", da.toString());
				linkedHashMap.put("Material", dc.getMaterial().getName());
				linkedHashMap.put("Quantity", String.valueOf(Utility.toDouble(dc.getQtyBrass())) + " brass");
				linkedHashMap.put("Vehicle No.", dc.getVehicle().getNumber());
				linkedHashMap.put("Driver Signature", "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _");
				linkedHashMap.put("Customer Signature", "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _");

				mtCell = new PdfPCell();
				mtCell.addElement(getDcTable(linkedHashMap, new float[] { 40.0F, 60.0F }));
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.BLACK);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph(""));
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColorRight(BaseColor.BLACK);
				mtCell.setBorderColor(BaseColor.WHITE);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph(""));
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.WHITE);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph(""));
				mtCell.addElement(getDcTable(linkedHashMap, new float[] { 40.0F, 60.0F }));
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.BLACK);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph("Customer's Copy"));
				mtCell.setHorizontalAlignment(1);
				mtCell.setPaddingTop(10.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.WHITE);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph(""));
				mtCell.setUseVariableBorders(true);
				mtCell.setColspan(2);
				mtCell.setBorderColor(BaseColor.WHITE);
				mainTable.addCell(mtCell);

				mtCell = new PdfPCell(new Paragraph("Office Copy"));
				mtCell.setHorizontalAlignment(1);
				mtCell.setPaddingTop(10.0F);
				mtCell.setUseVariableBorders(true);
				mtCell.setBorderColor(BaseColor.WHITE);
				mainTable.addCell(mtCell);

				if (counter % 2 == 0) {
					document.add(mainTable);
					if (dcSize % 2 != 0) {
						document.newPage();
						emptyLine = new Paragraph();
						addEmptyLine(emptyLine, 1);
						addEmptyLine(emptyLine, 1);
						document.add(emptyLine);
					}
				} else {
					emptyLine = new Paragraph();
					addEmptyLine(emptyLine, 1);

					emptyLine.add(new Paragraph(
							"--------------------------------------------------------------------------------------------------------------------------------------"));
					addEmptyLine(emptyLine, 1);

					mtCell = new PdfPCell();
					mtCell.setBorderColor(BaseColor.WHITE);
					mtCell.setColspan(4);
					mtCell.addElement(emptyLine);
					mainTable.addCell(mtCell);

					document.add(mainTable);
				}
				counter++;
			}
			addMetaData(document, "Delivery Challan #" + orderSid, "Delivery Challan #" + orderSid);
			document.close();
			writer.close();
		} catch (final DocumentException e) {
			log.error("DocumentException in DCInvoice class!!!");
			e.printStackTrace();
		} catch (final FileNotFoundException e) {
			log.error("FileNotFoundException in DCInvoice class!!!");
			e.printStackTrace();
		}
		log.info("generatePdf() End : Order Id : {} ", order.getId());
		return file.getAbsolutePath();
	}
}
