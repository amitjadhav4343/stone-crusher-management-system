package com.scms.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;

import com.scms.enums.ActiveStatus;
import com.scms.model.dto.KeyValueClass;

public interface PersistanceDelegatorDao {
    <T> void delete(T paramT);

    void deleteFromTable(Class<?> paramClass);

    <T> T findEntityById(Class<T> paramClass, Long paramString);

    <T> T findEntityByName(Class<T> paramClass, String paramString);

    <T> T findEntityByNative(String paramString, Class<T> paramClass, Object... paramVarArgs);

    <T> List<T> findEntityList(String paramString, Class<T> paramClass, Object... paramVarArgs);

    List<Object []> findNativeQueryResultList(String paramString, Object... paramVarArgs);

    <T> T findNativeSingleColumnValue(String paramString, Class<T> paramClass, Object... paramVarArgs);

    <T> T findSingleColumnValue(String paramString, Class<T> paramClass, Object... paramVarArgs);

    <T> List<T> getActiveEntityList(Class<T> paramClass);

    <T> T getEntityByColVal(String paramString1, String paramString2, Class<T> paramClass);

    <T> T getEntityByName(Class<T> paramClass, String paramString);

    <T> T getEntityByNative(String paramString, Class<T> paramClass, Object... paramVarArgs);

    <T> List<T> getEntityList(Class<T> paramClass);

    <T> List<T> getEntityList(String paramString, Class<T> paramClass, Object... paramVarArgs);

    <T> List<T> getEntityList(String paramString, Map<String, Object> paramMap);

    <T> List<T> getEntityListbyCriteria(Class<T> paramClass, Map<String, String> paramMap, Object... paramVarArgs);

    EntityManager getEntityManager();

    <T> List<KeyValueClass> getKeyValueList(String paramString1, String paramString2, Class<T> paramClass);

    List<Object []> getNativeQueryResultList(String paramString, Object... paramVarArgs);

    <T> T getNativeSingleColumnValue(String paramString, Class<T> paramClass, Object... paramVarArgs);

    <T, E> Set<E> getRelation(Class<T> paramClass, Class<E> paramClass1, String paramString1, String paramString2);

    String getSequence(String paramString);

    String getSequence(String paramString, int paramInt);

    <T> T getSingleColumnValue(String paramString, Class<T> paramClass, Object... paramVarArgs);

    <T, PK> T read(Class<T> paramClass, PK paramPK);

    <T> T save(T paramT);

    <T> void saveRecordList(List<T> paramList);

    <T> int toggleStatus(Long paramLong, ActiveStatus paramActiveStatus, Class<?> paramClass);

    <T> T update(T paramT);

    int updateEntityByQuery(String paramString, Object... paramVarArgs);

    <T> List<T> getNativeEntityList(String queryString, Class<T> typeKey, Object... bindVariables);

    <T> List<T> getLimitedEntityList(String queryString, Class<T> typeKey, Integer start, Integer maxResult,
        Object... bindVariables);
}
