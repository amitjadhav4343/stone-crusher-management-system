package com.scms.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.scms.enums.ActiveStatus;
import com.scms.model.dto.KeyValueClass;

@Repository
public class PersistanceDelegatorDaoImp implements PersistanceDelegatorDao {
    private static final Logger log = LoggerFactory.getLogger(PersistanceDelegatorDaoImp.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public <T> void delete(T t) {
        t = this.entityManager.merge(t);
        this.entityManager.remove(t);
    }

    @Override
    public void deleteFromTable(final Class<?> clazz) {
        this.entityManager.createQuery("DELETE FROM " + clazz.getSimpleName())
            .executeUpdate();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T findEntityById(final Class<T> typeKey, final Long id) {
        final String queryFormated = "from " + typeKey.getSimpleName() + " tbl where tbl.id = " + id;
        log.debug("queryFormated : {}", queryFormated);
        final Query query = this.entityManager.createQuery(queryFormated);

        /*
         * query.unwrap(org.hibernate.query.NativeQuery.class) .addEntity(typeKey);
         */

        return (T) query.getSingleResult();

        /*
         * return (T) query.getResultList() .stream() .findFirst() .orElse(null);
         */
    }

    @Override
    public <T> T findEntityByName(final Class<T> typeKey, final String name) {
        final TypedQuery<
            T> query = this.entityManager.createQuery("from " + typeKey.getName() + " where name = :name", typeKey);
        query.setParameter("name", name);
        return query.getResultList()
            .stream()
            .findFirst()
            .orElse(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T findEntityByNative(final String queryString, final Class<T> typeKey, final Object... bindVariables) {
        final Query query = this.entityManager.createNativeQuery(queryString);
        for(int i = 0; i < bindVariables.length; i++){
            query.setParameter(i + 1, bindVariables [i]);
        }
        query.unwrap(org.hibernate.query.NativeQuery.class)
            .addEntity(typeKey);
        return (T) query.getResultList()
            .stream()
            .findFirst()
            .orElse(null);
    }

    @Override
    public <T> List<T> findEntityList(final String queryString, final Class<T> typeKey, final Object... bindVariables) {
        final TypedQuery<T> query = this.entityManager.createQuery(queryString, typeKey);
        for(int i = 0; i < bindVariables.length; i++){
            query.setParameter(i + 1, bindVariables [i]);
        }
        log.debug(query.toString());
        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Object []> findNativeQueryResultList(final String queryString, final Object... bindVariables) {
        final Query query = this.entityManager.createNativeQuery(queryString);
        for(int i = 0; i < bindVariables.length; i++){
            query.setParameter(i + 1, bindVariables [i]);
        }
        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T findNativeSingleColumnValue(final String queryString, final Class<T> typeKey,
        final Object... bindVariables) {
        final Query query = this.entityManager.createNativeQuery(queryString);
        for(int i = 0; i < bindVariables.length; i++){
            query.setParameter(i + 1, bindVariables [i]);
        }
        return (T) query.getResultList()
            .stream()
            .findFirst()
            .orElse(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T findSingleColumnValue(final String queryString, final Class<T> typeKey,
        final Object... bindVariables) {
        final Query query = this.entityManager.createQuery(queryString);
        for(int i = 0; i < bindVariables.length; i++){
            query.setParameter(i + 1, bindVariables [i]);
        }
        return (T) query.getResultList()
            .stream()
            .findFirst()
            .orElse(null);
    }

    @Override
    public <T> List<T> getActiveEntityList(final Class<T> typeKey) {
        final TypedQuery<
            T> query = this.entityManager.createQuery("from " + typeKey.getName() + " where isActive = '1'", typeKey);
        return query.getResultList();
    }

    @Override
    public <T> T getEntityByColVal(final String column, final String value, final Class<T> typeKey) {
        final String queryString = "from " + typeKey.getName() + " where " + column + " = '%s'";
        return getEntityList(queryString, typeKey, new Object [] { value }).stream()
            .findFirst()
            .orElse(null);
    }

    @Override
    public <T> T getEntityByName(final Class<T> typeKey, final String name) {
        final TypedQuery<T> query = this.entityManager
            .createQuery("from " + typeKey.getName() + " where name = :nameParam", typeKey);
        query.setParameter("nameParam", name);
        return query.getResultList()
            .stream()
            .findFirst()
            .orElse(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getEntityByNative(final String queryString, final Class<T> typeKey, final Object... bindVariables) {
        final String queryFormated = String.format(queryString, bindVariables);
        final Query query = this.entityManager.createNativeQuery(queryFormated);
        query.unwrap(org.hibernate.query.NativeQuery.class)
            .addEntity(typeKey);
        return (T) query.getResultList()
            .stream()
            .findFirst()
            .orElse(null);
    }

    @Override
    public <T> List<T> getEntityList(final Class<T> typeKey) {
        final TypedQuery<T> query = this.entityManager.createQuery("from " + typeKey.getName(), typeKey);
        return query.getResultList();
    }

    @Override
    public <T> List<T> getEntityList(final String queryString, final Class<T> typeKey, final Object... bindVariables) {
        final String queryFormated = String.format(queryString, bindVariables);
        final TypedQuery<T> query = this.entityManager.createQuery(queryFormated, typeKey);
        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> List<T> getEntityList(final String queryString, final Map<String, Object> queryParamValue) {
        final Query query = this.entityManager.createNativeQuery(queryString);
        for(final Map.Entry<String, Object> pair : queryParamValue.entrySet()){
            query.setParameter(pair.getKey(), pair.getValue());
        }
        return query.getResultList();
    }

    @Override
    public <T> List<T> getEntityListbyCriteria(final Class<T> typeKey, final Map<String, String> inputCriteria,
        final Object... dependencyVar) {
        final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<T> q = criteriaBuilder.createQuery(typeKey);
        final List<Predicate> predicates = new ArrayList<>();
        final Root<T> root = q.from(typeKey);

        if(dependencyVar != null){
            for(final Object element : dependencyVar){
                if(element != null){
                    root.fetch(element.toString(), JoinType.INNER);
                }
            }
        }

        for(final Map.Entry<String, String> pair : inputCriteria.entrySet()){
            predicates.add(criteriaBuilder.equal(root.get(pair.getKey()), pair.getValue()));
        }
        q.select(root)
            .where(criteriaBuilder.and(predicates.toArray(new Predicate [predicates.size()])));
        final TypedQuery<T> query = this.entityManager.createQuery(q);
        return query.getResultList();
    }

    @Override
    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    @Override
    public <T> List<KeyValueClass> getKeyValueList(final String key, final String value, final Class<T> typeKey) {
        final String query = "SELECT NEW com.scms.model.dto.KeyValueClass(" + key + " as column1, " + value
            + " as column2) FROM " + typeKey.getName() + " where isActive = '1'";
        final TypedQuery<KeyValueClass> typedQuery = this.entityManager.createQuery(query, KeyValueClass.class);
        return typedQuery.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Object []> getNativeQueryResultList(final String queryString, final Object... bindVariables) {
        final String queryFormated = String.format(queryString, bindVariables);
        final Query query = this.entityManager.createNativeQuery(queryFormated);
        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> List<T> getNativeEntityList(final String queryString, final Class<T> typeKey,
        final Object... bindVariables) {
        final String queryFormated = String.format(queryString, bindVariables);
        final Query query = this.entityManager.createNativeQuery(queryFormated);
        return (List<T>) query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getNativeSingleColumnValue(final String queryString, final Class<T> typeKey,
        final Object... bindVariables) {
        final String queryFormated = String.format(queryString, bindVariables);
        final Query query = this.entityManager.createNativeQuery(queryFormated);
        return (T) query.getResultList()
            .stream()
            .findFirst()
            .orElse(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T, E> Set<E> getRelation(final Class<T> sourceClazz, final Class<E> targetClazz, final String relationName,
        final String sourceId) {
        final String queryRaw = "SELECT a.%s FROM %s a JOIN  a.%s b WHERE a.Id=%s";
        final String queryFormated = String.format(queryRaw, relationName, sourceClazz.getSimpleName(), relationName,
            sourceId);
        final Query query = this.entityManager.createQuery(queryFormated);
        final List<E> list = query.getResultList();
        return new HashSet<>(list);
    }

    @Override
    @SuppressWarnings("unchecked")
    public String getSequence(final String sequenceName) {
        log.info("Inside getSequence with name : {}", sequenceName);
        final String nativeQuery = String.format("select  %s.NEXTVAL from dual ", new Object [] { sequenceName });
        String sequenceString = "";
        List<Object []> list = null;
        try{
            final Query query = this.entityManager.createNativeQuery(nativeQuery);
            list = query.getResultList();
            if(!list.isEmpty()){
                final Object result = list.get(0);
                if(result instanceof BigDecimal){
                    final Long sequenceId = Long.valueOf( ((BigDecimal) result).longValue());
                    sequenceString = String.valueOf(sequenceId);
                }
            }
        } catch(final Exception e){
            log.error("Exception in fetching Sequence with name : {}", sequenceName);
            log.error("Exception in fetching Sequence", e);
        }
        return sequenceString;
    }

    @Override
    @SuppressWarnings("unchecked")
    public String getSequence(final String sequenceName, final int length) {

        log.info("Inside getSequence with name : {} and length {} ", sequenceName, length);
        final String nativeQuery = String.format("select  %s.NEXTVAL from dual", sequenceName);
        String sequenceString = "";
        List<Object []> list = null;
        try{
            final Query query = this.entityManager.createNativeQuery(nativeQuery);
            list = query.getResultList();

            if(!list.isEmpty()){
                final Object result = list.get(0);
                if(result instanceof BigDecimal){
                    final Long sequenceId = ((BigDecimal) result).longValue();
                    final StringBuilder stringBuilder = new StringBuilder();
                    final String format = stringBuilder.append("%0")
                        .append(length)
                        .append("d")
                        .toString();
                    sequenceString = String.format(format, sequenceId);
                }
            }
        } catch(final Exception e){
            log.error(" Exception in fetching Sequence with name : {}", sequenceName);
            log.error(" Exception in fetching Sequence", e);

        }
        return sequenceString;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getSingleColumnValue(final String queryString, final Class<T> typeKey, final Object... bindVariables) {
        final String queryFormated = String.format(queryString, bindVariables);
        final Query query = this.entityManager.createQuery(queryFormated);
        return (T) query.getResultList()
            .stream()
            .findFirst()
            .orElse(null);
    }

    @Override
    public <T, K> T read(final Class<T> t, final K id) {
        return this.entityManager.find(t, id);
    }

    @Override
    public <T> T save(final T t) {
        this.entityManager.persist(t);
        return t;
    }

    @Override
    public <T> void saveRecordList(final List<T> recordList) {

        try{
            // checkstyle fix : magic number
            final int batchSize = 20;

            for(int i = 0; i < recordList.size(); i++){

                final T t = recordList.get(i);
                this.entityManager.persist(t);

                if(i % batchSize == 0){
                    this.entityManager.flush();
                    this.entityManager.clear();
                }
            }

        } catch(final Exception e){
            log.error("Execption in saveRecordList  " + e);
        }
    }

    @Override
    public <T> int toggleStatus(final Long id, final ActiveStatus status, final Class<?> clazz) {
        return this.entityManager
            .createQuery("UPDATE " + clazz.getSimpleName() + " SET STATUS ='" + status.name() + "' WHERE ID =" + id)
            .executeUpdate();
    }

    @Override
    public <T> T update(final T t) {
        return this.entityManager.merge(t);
    }

    @Override
    public int updateEntityByQuery(final String queryString, final Object... bindVariables) {
        final String queryFormated = String.format(queryString, bindVariables);
        final Query query = this.entityManager.createNativeQuery(queryFormated);
        return query.executeUpdate();
    }

    @Override
    public <T> List<T> getLimitedEntityList(String queryString, Class<T> typeKey, Integer start, Integer maxResult,
        Object... bindVariables) {
        final String queryFormated = String.format(queryString, bindVariables);
        final TypedQuery<T> query = this.entityManager.createQuery(queryFormated, typeKey);
        query.setFirstResult(start);
        if(maxResult > 0){
            query.setMaxResults(maxResult);
        }
        return query.getResultList();
    }
}
