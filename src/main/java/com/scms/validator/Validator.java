package com.scms.validator;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.scms.enums.ErrorConstants;
import com.scms.exception.ScmsException;
import com.scms.util.StringUtils;

@Component
public class Validator {
    private static final Logger log = LoggerFactory.getLogger(Validator.class);

    public <T, E> E validateAgainstValue(final ErrorConstants smsException, final E validatedEntity,
        final T expectedValue, final T entityValue) {
        if(!expectedValue.equals(entityValue)) throw new ScmsException(smsException, validatedEntity);
        return validatedEntity;
    }

    public void validateAllNotNull(final ErrorConstants smsException, final String... values) {
        for(final String val : values){
            if(StringUtils.isEmpty(val)) throw new ScmsException(smsException);
        }
    }

    public void validateExceptValue(final ErrorConstants smsException, final String expectedValue,
        final String entityValue) {
        if(!StringUtils.isEqual(expectedValue, entityValue)) throw new ScmsException(smsException);
    }

    public void validateExceptValue(final ErrorConstants smsException, final String validatedEntity,
        final String expectedValue, final String entityValue) {
        if(!StringUtils.isEqual(expectedValue, entityValue)) throw new ScmsException(smsException, validatedEntity);
    }

    public void validateIfEqual(final ErrorConstants smsException, final String expectedValue,
        final String entityValue) {
        if(StringUtils.isEqual(expectedValue, entityValue)) throw new ScmsException(smsException);
    }

    public void validateIfEqual(final ErrorConstants smsException, final String expectedValue, final String entityValue,
        final String attributeEntity) {
        if(StringUtils.isEqual(expectedValue, entityValue)) throw new ScmsException(smsException, attributeEntity);
    }

    public boolean validateInList(final String entityValue, final Set<String> list) {
        log.info("Start of validateInList method : entityValue : {}", entityValue);
        boolean flag = false;
        if(StringUtils.isNotEmpty(entityValue) && list.contains(entityValue)){
            flag = true;
        }
        return flag;
    }

    @SuppressWarnings("rawtypes")
    public <T> T validateIsNotNull(final ErrorConstants smsException, final T entity) {
        if(entity == null) throw new ScmsException(smsException);
        if(entity instanceof Collection){
            if(CollectionUtils.isEmpty((Collection) entity)) throw new ScmsException(smsException);
        } else if(entity instanceof String){
            if(StringUtils.isEmpty(String.valueOf(entity))) throw new ScmsException(smsException);
        } else if(entity instanceof Optional){
            if(! ((Optional) entity).isPresent()) throw new ScmsException(smsException);
        } else if(entity instanceof Map && ((Map) entity).isEmpty()) throw new ScmsException(smsException);
        return entity;
    }

    @SuppressWarnings("rawtypes")
    public <T> T validateIsNotNull(final ErrorConstants smsException, final T entity, final String attributeName) {
        if(entity == null) throw new ScmsException(smsException, attributeName);
        if(entity instanceof Collection){
            if(CollectionUtils.isEmpty((Collection) entity)) throw new ScmsException(smsException, attributeName);
        } else if(entity instanceof String){
            if(StringUtils.isEmpty(String.valueOf(entity))) throw new ScmsException(smsException, attributeName);
        } else if(entity instanceof Optional){
            if(! ((Optional) entity).isPresent()) throw new ScmsException(smsException, attributeName);
        } else if(entity instanceof Map && ((Map) entity).isEmpty())
            throw new ScmsException(smsException, attributeName);
        return entity;
    }

    @SuppressWarnings("rawtypes")
    public <T> T validateIsNotNull(final T entity) {
        if(entity == null) throw new NullPointerException();
        if(entity instanceof Collection){
            if(CollectionUtils.isEmpty((Collection) entity)) throw new NullPointerException();
        } else if(entity instanceof String){
            if(StringUtils.isEmpty(String.valueOf(entity))) throw new NullPointerException();
        } else if(entity instanceof Optional){
            if(! ((Optional) entity).isPresent()) throw new NullPointerException();
        } else if(entity instanceof Map && ((Map) entity).isEmpty()) throw new NullPointerException();
        return entity;
    }

    @SuppressWarnings("rawtypes")
    public <T> T validateIsNull(final ErrorConstants smsException, final T entity) {
        if(entity instanceof Optional){
            if( ((Optional) entity).isPresent()) throw new ScmsException(smsException);
        } else if(entity instanceof Collection){
            if(!CollectionUtils.isEmpty((Collection) entity)) throw new ScmsException(smsException);
        } else if(entity instanceof String){
            if(!StringUtils.isEmpty(String.valueOf(entity))) throw new ScmsException(smsException);
        } else if(entity != null) throw new ScmsException(smsException);
        return entity;
    }

    public <T> T validateIsNull(final ErrorConstants smsException, final T entity, final String attributeName) {
        if(entity != null) throw new ScmsException(smsException, attributeName);
        return entity;
    }

    public void validateNotInList(final ErrorConstants smsException, final String entityValue, final Set<String> list) {
        log.info("Start of validateNotInList method : entityValue : {}", entityValue);
        if(!list.contains(entityValue)) throw new ScmsException(smsException);
    }

    public void validateNotInList(final ErrorConstants smsException, final String entityValue, final Set<String> list,
        final String attributeName) {
        log.info("Start of validateNotInList method: entityValue: {}", entityValue);
        if(!list.contains(entityValue)) throw new ScmsException(smsException, attributeName);
    }

    public void validateStartWith(final ErrorConstants smsException, final String entityValue,
        final String validatedEntity, final String attributeName) {
        log.info("Start of validateStartWith method : entityValue : {}, validatedEntity : {}", entityValue,
            validatedEntity);
        if(!validatedEntity.startsWith(entityValue)) throw new ScmsException(smsException, attributeName);
    }
}
