package com.scms.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.session.ConcurrentSessionControlAuthenticationStrategy;

@Configuration
@EnableWebSecurity
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private LogoutSuccessHandler logoutSuccessHandler;

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public DaoAuthenticationProvider authProvider() {
        final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(this.userDetailsService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }

    @Bean
    public ConcurrentSessionControlAuthenticationStrategy concurrentSessionControlAuthenticationStrategy() {
        final ConcurrentSessionControlAuthenticationStrategy strategy = new ConcurrentSessionControlAuthenticationStrategy(
            sessionRegistry());
        strategy.setExceptionIfMaximumExceeded(true);
        return strategy;
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
    // @formatter:off
		http.csrf().disable()
        .authorizeRequests()
        .antMatchers("/homepage", "/test", "/swagger-ui.html").permitAll()
        .antMatchers("/**").permitAll()
        .antMatchers("/index").hasAnyRole("ORDERER", "MASTER", "CUSTOMER", "ADMIN")
        .antMatchers("/resources/**").permitAll().anyRequest().permitAll()
        .antMatchers("/invalidSession*").anonymous()
        .anyRequest().authenticated()
        .and()
        .formLogin()
        .loginPage("/login")//while login which page have to call
        .defaultSuccessUrl("/index")
        .failureUrl("/homepage?error=true")
        .successHandler(this.authenticationSuccessHandler)
        .failureHandler(this.authenticationFailureHandler)
        .permitAll()
        .and()
        .sessionManagement()
        .invalidSessionUrl("/invalidSession")
        .maximumSessions(1).sessionRegistry(sessionRegistry()).and()
        .sessionFixation().none()
        .and()
        .logout()
        .logoutSuccessHandler(this.logoutSuccessHandler)
        .invalidateHttpSession(false)
        .logoutSuccessUrl("/logout?logSucc=true")
        .deleteCookies("JSESSIONID")
        .permitAll();
		// @formatter:on
    }

    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring()
            .antMatchers(new String [] { "/resources/**", "/static/**", "/css/**", "/js/**", "/img/**", "/orderer/**",
                "/plugins/**", "/scss/**" });
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }
}
