package com.scms.config.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

@Component("authenticationFailureHandler")
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response,
        final AuthenticationException exception) throws IOException, ServletException {
        setDefaultFailureUrl("/homepage?error=true");
        super.onAuthenticationFailure(request, response, exception);
        String errorMessage = "message.badCredentials";
        if(exception.getMessage() != null) if(exception.getMessage()
            .equalsIgnoreCase("User account is locked")){
            errorMessage = "User account is locked";
        } else if(exception.getMessage()
            .equalsIgnoreCase("User account has expired")){
            errorMessage = "User account has expired";
        } else if(exception.getMessage()
            .equalsIgnoreCase("User credentials have expired")){
            errorMessage = "User credentials have expired";
        } else if(exception.getMessage()
            .equalsIgnoreCase("Bad credentials")){
            errorMessage = " Bad credentials";
        } else if(exception.getMessage()
            .equalsIgnoreCase("User is disabled")){
            errorMessage = "User is disabled";
        }
        request.getSession()
            .setAttribute("SPRING_SECURITY_LAST_EXCEPTION", errorMessage);
    }
}
