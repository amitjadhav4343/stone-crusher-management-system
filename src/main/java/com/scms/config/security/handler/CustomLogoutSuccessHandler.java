package com.scms.config.security.handler;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

@Component("logoutSuccessHandler")
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {
    @Autowired
    private SessionRegistry sessionRegistry;

    @Override
    public void onLogoutSuccess(final HttpServletRequest request, final HttpServletResponse response,
        final Authentication authentication) throws IOException, ServletException {
        final HttpSession session = request.getSession();
        if(session != null){
            session.removeAttribute("user");
        }
        request.getSession()
            .invalidate();
        response.setStatus(200);
        response.sendRedirect("/homepage");
        final List<SessionInformation> userSessions = this.sessionRegistry.getAllSessions(session, true);
        for(final SessionInformation sessionInformation : userSessions){
            this.sessionRegistry.removeSessionInformation(sessionInformation.getSessionId());
        }
    }
}
