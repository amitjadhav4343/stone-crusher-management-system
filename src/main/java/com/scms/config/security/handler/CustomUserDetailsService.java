package com.scms.config.security.handler;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.scms.model.User;
import com.scms.service.UserService;

@Service("userDetailsService")
@Transactional
public class CustomUserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(final String username) {
        final User user = this.userService.findByUsername(username);
        try{
            if(user == null) throw new UsernameNotFoundException("No user found with username: " + username);
            final Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
            grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole()
                .getName()
                .toString()));
            return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), true,
                true, true, true, grantedAuthorities);
        } catch(final Exception e){
            throw new RuntimeException(e);
        }
    }
}
