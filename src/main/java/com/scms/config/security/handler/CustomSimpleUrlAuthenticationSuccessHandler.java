package com.scms.config.security.handler;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.scms.model.User;
import com.scms.service.LookupService;

@Component("authenticationSuccessHandler")
public class CustomSimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private static final Logger log = LoggerFactory.getLogger(CustomSimpleUrlAuthenticationSuccessHandler.class);

    @Autowired
    private LookupService lookupService;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    protected void clearAuthenticationAttributes(final HttpServletRequest request) {
        final HttpSession session = request.getSession(false);
        if(session == null) return;
        session.removeAttribute("SPRING_SECURITY_LAST_EXCEPTION");
    }

    protected String determineTargetUrl(final Authentication authentication) {
        final Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        if(!authorities.isEmpty()) return "/index";
        throw new IllegalStateException();
    }

    protected RedirectStrategy getRedirectStrategy() {
        return this.redirectStrategy;
    }

    protected void handle(final HttpServletRequest request, final HttpServletResponse response,
        final Authentication authentication) throws IOException {
        final String targetUrl = determineTargetUrl(authentication);
        if(response.isCommitted()){
            log.debug("Response has already been committed. Unable to redirect to ", targetUrl);
            return;
        }
        this.redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
        final Authentication authentication) throws IOException {
        handle(request, response, authentication);
        final HttpSession session = request.getSession(false);
        if(session != null){
            final User user = this.lookupService.getEntityByColumnNameAndValue("username", authentication.getName(),
                User.class);
            if(user.isActive()){
                session.setAttribute("userDto", user.getUserDto());
            }
        }
        clearAuthenticationAttributes(request);
    }

    public void setRedirectStrategy(final RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }
}
