package com.scms.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.scms.constants.Constants;
import com.scms.enums.ActiveStatus;
import com.scms.enums.AddressType;
import com.scms.enums.Designation;
import com.scms.enums.YesNo;
import com.scms.env.config.EnvironmentConfig;
import com.scms.model.Address;
import com.scms.model.Company;
import com.scms.model.Customer;
import com.scms.model.DeliveryChallan;
import com.scms.model.Employee;
import com.scms.model.EnvironmentVariable;
import com.scms.model.Gst;
import com.scms.model.Material;
import com.scms.model.ModelView;
import com.scms.model.Orders;
import com.scms.model.ScmsJob;
import com.scms.model.Site;
import com.scms.model.TransactionHistory;
import com.scms.model.User;
import com.scms.model.Vehicle;
import com.scms.model.dto.Response;
import com.scms.model.dto.UserDto;
import com.scms.repository.service.RepositoryService;
import com.scms.service.LookupService;
import com.scms.service.PersistanceService;
import com.scms.service.UpdateService;
import com.scms.util.Utility;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ScmsController {

    @Autowired
    private LookupService lookupService;

    @Autowired
    private PersistanceService persistanceService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private UpdateService updateService;
    
    @GetMapping({ "/page/**", "/fragment/**" })
    public ModelAndView getModelView(final HttpServletRequest request) {
        log.info("getModelView() called : action : " + request.getServletPath());
        //return lookupService.getModelView(request.getServletPath());
        
        ModelAndView modelAndView = new ModelAndView(request.getServletPath());
        return modelAndView;
    }

    @GetMapping({ "/balanceCheck/customer/{id}/amount/{amount}" })
    public boolean balanceCheck(@PathVariable(Constants.ID) final Long id, @PathVariable("amount") final Double amount) {
        log.info("balanceCheck() : " + id + ", " + amount);
        return lookupService.balanceCheck(id, amount);
    }

    @GetMapping({ "/list/active/{type}" })
    public Response getActiveEntityList(@PathVariable("type") final String type)
        throws ClassNotFoundException {
        log.info("getActiveEntityList() : {}", type);
        return Utility.buildResponse(lookupService.getActiveEntityList(Class.forName(Constants.PACKAGE_MODEL + type)), HttpStatus.OK);
    }

    @GetMapping(value = { "/address/{id}" }, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response getAddress(@PathVariable(Constants.ID) final Long id) {
        log.info("getAddress() : {}", id);
        return Utility.buildResponse(lookupService.lookupById(Address.class, id), HttpStatus.OK);
    }

    @GetMapping({ "/employee/{id}" })
    public Response getEmployee(@PathVariable(Constants.ID) final Long id) {
        log.info("getEmployee() : {}", id);
        return Utility.buildResponse(lookupService.lookupById(Employee.class, id), HttpStatus.OK);
    }

    @GetMapping({ "/address/clazz/{clazz}/id/{id}" })
    public Response getAddress(@PathVariable("clazz") final String clazz, @PathVariable(Constants.ID) final Long id)
        throws ClassNotFoundException {
        log.info("getAddress() : {}, {}", clazz, id);
        return Utility.buildResponse(lookupService.lookupById(Class.forName(Constants.PACKAGE_MODEL + clazz), id), HttpStatus.OK);
    }

    @GetMapping(value = { "/company/user/{id}" }, produces = { "application/json" })
    public Response getCompany(final HttpServletRequest request) {
        log.info("getCompany from user");
        final UserDto userDto = (UserDto) request.getSession()
            .getAttribute("userDto");
        return Utility.buildResponse(this.lookupService.lookupById(Company.class, userDto.getCompany()), HttpStatus.OK);
    }

    @GetMapping(value = { "/company/{id}" }, produces = { "application/json" })
    public Response getCompanyById(@PathVariable(Constants.ID) final Long id) {
        log.info("getCompanyById : {}", id);
        return Utility.buildResponse(lookupService.lookupById(Company.class, id), HttpStatus.OK);
    }

    @GetMapping({ "/customer/{id}" })
    public Response getCustomer(@PathVariable(Constants.ID) final Long id) {
        log.info("getCustomer() : {}", id);
        return Utility.buildResponse(lookupService.lookupById(Customer.class, id), HttpStatus.OK);
    }
    
    @GetMapping(value = { "/orders" }, consumes = { "application/json" }, produces = { "application/json" })
    public Response getOrders(@Valid @RequestBody final String paramJson) {
        log.info("getOrders : {}", paramJson);
        return Utility.buildResponse(lookupService.getOrders(paramJson), HttpStatus.OK);
    }

    /************************ GET /dataTable/* START **********************/

    @PostMapping({ "/dtble/customer" })
    public DataTablesOutput<Customer> getCustomers(@Valid @RequestBody final DataTablesInput dataTablesInput) {
        log.info("getCustomers() : {}", dataTablesInput);
        return repositoryService.getCustomers(dataTablesInput);
    }

    @PostMapping({ "/dtble/filter/orders" })
    public DataTablesOutput<Orders> getFilterOrders(@Valid @RequestBody final String paramJson) {
        log.info("getOrderss() : {}", paramJson);
        return lookupService.getFilterOrders(paramJson);
    }
    
    @PostMapping({ "/dtble/filter/deliveryChallan" })
    public DataTablesOutput<DeliveryChallan> getFilterDeliveryChallan(@Valid @RequestBody final String paramJson) {
        log.info("getDeliveryChallan() : {}", paramJson);
        return lookupService.getFilterDeliveryChallan(paramJson);
    }

    @PostMapping({ "/dtble/deliveryChallan" })
    public DataTablesOutput<DeliveryChallan> getDeliveryChallan(
        @Valid @RequestBody final DataTablesInput dataTablesInput) {
        log.info("getDeliveryChallan() : {}", dataTablesInput);
        return repositoryService.getDeliveryChallan(dataTablesInput);
    }

    @PostMapping({ "/dtble/material" })
    public DataTablesOutput<Material> getMaterials(@Valid @RequestBody final DataTablesInput dataTablesInput) {
        log.info("get datatable materials : {}", dataTablesInput);
        return repositoryService.getMaterials(dataTablesInput);
    }

    @PostMapping({ "/dtble/modelview" })
    public DataTablesOutput<ModelView> getModelView(@Valid @RequestBody final DataTablesInput dataTablesInput) {
        log.info("getModelView : {}", dataTablesInput);
        return repositoryService.findAll(dataTablesInput);
    }

    @PostMapping({ "/dtble/transactionHistory" })
    public DataTablesOutput<TransactionHistory> getTransactionHistory(
        @Valid @RequestBody final DataTablesInput dataTablesInput) {
        log.info("getTransHistory() : {}", dataTablesInput);
        return repositoryService.getTransactionHistory(dataTablesInput);
    }

    @PostMapping({ "/dtble/user" })
    public DataTablesOutput<User> getUsers(@Valid @RequestBody final DataTablesInput dataTablesInput) {
        log.info("get datatable users : {}", dataTablesInput);
        return repositoryService.getUsers(dataTablesInput);
    }

    @PostMapping({ "/dtble/vehicle" })
    public DataTablesOutput<Vehicle> getVehicles(@Valid @RequestBody final DataTablesInput dataTablesInput) {
        log.info("get datatable vehicles : {}", dataTablesInput);
        return repositoryService.getVehicles(dataTablesInput);
    }

    @PostMapping({ "/dtble/gst" })
    public DataTablesOutput<Gst> getGst(@Valid @RequestBody final DataTablesInput dataTablesInput) {
        log.info("get datatable Gst : {}", dataTablesInput);
        return repositoryService.getGst(dataTablesInput);
    }

    @PostMapping({ "/dtble/employee" })
    public DataTablesOutput<Employee> getEmployee(@Valid @RequestBody final DataTablesInput dataTablesInput) {
        log.info("get datatable Employee : {}", dataTablesInput);
        return repositoryService.getEmployee(dataTablesInput);
    }
    
    @PostMapping({ "/dtble/scms-jobs" })
    public DataTablesOutput<ScmsJob> getScmsJobs(@Valid @RequestBody final DataTablesInput dataTablesInput) {
        log.info("get datatable ScmsJobs : {}", dataTablesInput);
        return repositoryService.getScmsJobs(dataTablesInput);
    }

    /************************ GET /dataTable/* END **********************/

    @GetMapping({ "/dc/customer/{customerId}" })
    public Response getDcByCustomer(@PathVariable("customerId") final String customerId) {
        log.info("getDcByCustomer() : {}", customerId);
        return Utility.buildResponse(lookupService.getDcByCustomer(customerId), HttpStatus.OK);
    }

    @GetMapping({ "/lookupById/{type}/{id}" })
    public Response getEntity(@PathVariable("type") final String type, @PathVariable(Constants.ID) final Long id)
        throws ClassNotFoundException {
        log.info("getActiveEntityList() : {}", type);
        return Utility.buildResponse(lookupService.lookupById(Class.forName(Constants.PACKAGE_MODEL + type), id), HttpStatus.OK);
    }

    @GetMapping({ "/list/{type}" })
    public Response getEntityList(@PathVariable("type") final String type) throws ClassNotFoundException {
        log.info("getEntityList() : {}", type);
        return Utility.buildResponse(lookupService.getEntityList(Class.forName(Constants.PACKAGE_MODEL + type)), HttpStatus.OK);
    }

    @GetMapping({ "/gst/{id}" })
    public Response getGst(@PathVariable(Constants.ID) final Long id) {
        log.info("getGst() : {}", id);
        return Utility.buildResponse(lookupService.lookupById(Gst.class, id), HttpStatus.OK);
    }

    @GetMapping({ "/gst/materialId/{materialId}" })
    public Response getGstByMaterialId(@PathVariable("materialId") final Long materialId) {
        log.info("/gst/materialId/{materialId} : {}", materialId);
        return Utility.buildResponse(lookupService.getGstByMaterialId(materialId), HttpStatus.OK);
    }

    @GetMapping({ "/homepage", "/login" })
    public ModelAndView getHomepage() {
        log.info("getHomepage()");
        final ModelAndView model = new ModelAndView();
        model.setViewName("homepage");
        return model;
    }

    @GetMapping({ "/index" })
    public ModelAndView getIndex() {
        log.info("getIndex");
        final ModelAndView model = new ModelAndView();
        model.setViewName("index");
        return model;
    }

    @GetMapping({ "/keyValueList/{table}/{key}/{value}" })
    public Response getKeyValueList(@PathVariable("table") final String table,
        @PathVariable("key") final String key, @PathVariable("value") final String value) {
        log.info("keyValueList() : {}, {}, {}", new Object [] { table, key, value });
        return Utility.buildResponse(lookupService.getKeyValueList(table, key, value), HttpStatus.OK);
    }

    @GetMapping({ "/material/{id}" })
    public Response getMaterial(@PathVariable final Long id) {
        log.info("getMaterial() : " + id);
        return Utility.buildResponse(lookupService.lookupById(Material.class, id), HttpStatus.OK);
    }

    @PostMapping(value = { "/modelview2" }, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response getModelviewList(final HttpServletRequest httpRequest) {
        log.info("getModelviewList() : {}");
        final List<ModelView> list = lookupService.getEntityList(ModelView.class);
        return Utility.buildResponse(list.stream()
                .limit(10L)
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @PostMapping(value = { "/odcCalculation/{gst}" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response getOdcCalculation(@RequestBody final List<Long> dcIds,
        @PathVariable("gst") final YesNo gst) {
        log.info("getOdcCalculation() dcIds : {}, withGST : {}", dcIds, gst);
        return Utility.buildResponse(lookupService.getOdcCalculation(dcIds, gst), HttpStatus.OK);
    }

    /******************************** GET PDF START ***************************/

    @GetMapping(value = { "/pdf/deliverychallan/{id}" }, produces = { "application/pdf" })
    public ResponseEntity<?> getDcPdf(final HttpServletRequest httpRequest, @PathVariable(Constants.ID) final Long id) {
        log.info("getDcPdf() id : {}", id);
        return lookupService.getDcPdf(httpRequest, id);
    }

    @GetMapping(value = { "/pdf/order-deliverychallan/{id}" }, produces = { "application/pdf" })
    public ResponseEntity<?> getOdcPdf(final HttpServletRequest httpRequest, @PathVariable(Constants.ID) final Long id) {
        log.info("getOdcPdf() id : {}", id);
        return lookupService.getOdcPdf(httpRequest, id);
    }

    @GetMapping(value = { "/pdf/place-order/{id}" }, produces = { "application/pdf" })
    public ResponseEntity<?> getPlodPdf(final HttpServletRequest httpRequest, @PathVariable(Constants.ID) final Long id) {
        log.info("getPlodPdf() id : " + id);
        return lookupService.getPlodPdf(httpRequest, id);
    }

    /******************************** GET PDF END ***************************/

    @GetMapping({ "/items/order/{id}" })
    public Response getItemsByOrders(@PathVariable(Constants.ID) final Long id) {
        log.info("getItemsByOrders() : {}", id);
        return Utility.buildResponse(lookupService.getItemsByOrders(id), HttpStatus.OK);
    }

    @GetMapping({ "/order/{id}" })
    public Response getOrder(@PathVariable(Constants.ID) final Long id) {
        log.info("getAddress() : {}", id);
        return Utility.buildResponse(lookupService.lookupById(Orders.class, id), HttpStatus.OK);
    }

    @PostMapping(value = { "/material-wise-count" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response getMaterialWiseCount(@RequestBody final String json) {
        log.info("getReportByMaterial() : {}", json);
        return Utility.buildResponse(lookupService.getMaterialWiseCount(json), HttpStatus.OK);
    }

    @PostMapping(value = { "/report/list/odetails" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response getReportByOdList(@RequestBody final String json) {
        log.info("getReportByOdList() : {}", json);
        return Utility.buildResponse(lookupService.getReportByOdList(json), HttpStatus.OK);
    }

    @GetMapping({ "/site/{id}" })
    public Response getSite(@PathVariable(Constants.ID) final Long id) {
        log.info("getSite() : {}", id);
        return Utility.buildResponse(lookupService.lookupById(Site.class, id), HttpStatus.OK);
    }

    @GetMapping({ "/site/company/{id}" })
    public Response getSites(@PathVariable(Constants.ID) final Long id) {
        log.info("getSites using company id() : {}", id);
        return Utility.buildResponse(lookupService.lookupById(Company.class, id).getSite(), HttpStatus.OK);
    }

    @GetMapping(value = { "/transactionHistory/customer/{id}" }, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response getTransactionHistory(final HttpServletRequest httpRequest, @PathVariable final Long id) {
        log.info("getTransactionHistory() : {}", id);
        return Utility.buildResponse(repositoryService.getTransactionHistory(id), HttpStatus.OK);
    }

    @GetMapping({ "/user/{id}" })
    public Response getUser(@PathVariable(Constants.ID) final Long id) {
        log.info("getUser() : {}", id);
        return Utility.buildResponse(lookupService.lookupById(User.class, id), HttpStatus.OK);
    }

    @GetMapping({ "/vehicle/{id}" })
    public Response getVehicle(@PathVariable(Constants.ID) final Long id) {
        log.info("getVehicles() : {}", id);
        return Utility.buildResponse(lookupService.lookupById(Vehicle.class, id), HttpStatus.OK);
    }
    
    @GetMapping({ "/scmsJob/{id}" })
    public Response getScmsJob(@PathVariable(Constants.ID) final Long id) {
        log.info("getScmsJob() : {}", id);
        return Utility.buildResponse(lookupService.lookupById(ScmsJob.class, id), HttpStatus.OK);
    }

    @PatchMapping(value = { "/modifyAdvance" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
        produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response modifyAdvance(final HttpServletRequest httpRequest, @RequestBody final String json) {
        log.info("modifyAdvance() : {}", json);
        return Utility.buildResponse(updateService.modifyAdvance(json), HttpStatus.OK);
    }

    @PostMapping(value = { "/address" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response saveAddress(final HttpServletRequest request, @RequestBody final String json) {
        log.info("saveAddress() : {}", json);
        return Utility.buildResponse(persistanceService.saveAddress(request, json), HttpStatus.OK);
    }

    @PostMapping(value = { "/customer" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response saveCustomer(@RequestBody final String request) {
        log.info("saveCustomer() : {}", request);
        return Utility.buildResponse(persistanceService.saveCustomer(request), HttpStatus.OK);
    }

    @PostMapping(value = { "/deliveryChallan" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response saveDeliveryChallan(final HttpServletRequest httpRequest, @RequestBody final String json) {
        log.info("saveDeliveryChallan() : {}", json);
        return Utility.buildResponse(persistanceService.saveDeliveryChallan(httpRequest, json), HttpStatus.OK);
    }

    @PostMapping(value = { "/gst" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response saveGst(@RequestBody final String request) {
        log.info("saveGst() : {}", request);
        return Utility.buildResponse(persistanceService.saveGst(request), HttpStatus.OK);
    }

    @PostMapping(value = { "/material" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response saveMaterial(@RequestBody final String request) {
        log.info("saveMaterial() : {}", request);
        return Utility.buildResponse(persistanceService.saveMaterial(request), HttpStatus.OK);
    }

    @PostMapping(value = { "/order-dc" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response saveOrderViaDC(final HttpServletRequest httpRequest, @RequestBody final String json) {
        log.info("saveOrderViaDC() json : {}", json);
        return Utility.buildResponse(persistanceService.saveOrderViaDC(httpRequest, json), HttpStatus.OK);
    }

    @PostMapping(value = { "/order" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
        produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response saveOrder(final HttpServletRequest httpRequest, @RequestBody final String request) {
        log.info("saveOrder() : {}", request);
        return Utility.buildResponse(persistanceService.saveOrder(httpRequest, request), HttpStatus.OK);
    }

    @PostMapping(value = { "/site" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response saveSite(final HttpServletRequest request, @RequestBody final String json) {
        log.info("SaveSite() : {}", request);
        return Utility.buildResponse(persistanceService.saveSite(request, json), HttpStatus.OK);
    }

    @PostMapping(value = { "/user" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response saveUser(final HttpServletRequest request, @RequestBody final String json) {
        log.info("saveUser() : {}", json);
        return Utility.buildResponse(persistanceService.saveUser(json), HttpStatus.OK);
    }

    @PostMapping(value = { "/vehicle" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
        produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response saveVehicle(@RequestBody final String request) {
        log.info("saveVehicle() : {}", request);
        return Utility.buildResponse(persistanceService.saveVehicle(request), HttpStatus.OK);
    }

    @PostMapping(value = { "/employee" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
        produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response saveEmployee(final HttpServletRequest request, @RequestBody final String paramJson) {
        log.info("saveEmployee() : {}", paramJson);
        return Utility.buildResponse(persistanceService.saveEmployee(request, paramJson), HttpStatus.OK);
    }

    @PatchMapping({ "/session/toggleStatus/{id}/{status}/{clazz}" })
    public boolean sessionToggleStatus(final HttpServletRequest request, @PathVariable(Constants.ID) final String id,
        @PathVariable("status") final String status, @PathVariable("clazz") final String clazz)
        throws NumberFormatException, ClassNotFoundException {
        log.info("sessionToggleStatus()");
        final boolean flag = updateService.toggleStatus(Long.valueOf(id), ActiveStatus.valueOf(status),
            Class.forName(Constants.PACKAGE_MODEL + clazz));
        final Company company = (Company) request.getSession()
            .getAttribute("company");
        request.getSession()
            .setAttribute("company", lookupService.lookupById(Company.class, company.getId()));
        return flag;
    }

    @PatchMapping({ "/toggleStatus/{id}/{status}/{clazz}" })
    public boolean toggleStatus(@PathVariable(Constants.ID) final String id, @PathVariable("status") final String status,
        @PathVariable("clazz") final String clazz) throws NumberFormatException, ClassNotFoundException {
        log.info("toggleStatus()");
        return updateService.toggleStatus(Long.valueOf(id), ActiveStatus.valueOf(status),
            Class.forName(Constants.PACKAGE_MODEL + clazz));
    }

    @PatchMapping(value = { "/address" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response updateAddress(final HttpServletRequest request, @RequestBody final String json) {
        log.info("updateAddress() : {}", json);
        return Utility.buildResponse(updateService.updateAddress(request, json), HttpStatus.OK);
    }

    @PatchMapping(value = { "/bank" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response updateBank(final HttpServletRequest request, @RequestBody final String json) {
        log.info("updateBank() : {}", json);
        return Utility.buildResponse(updateService.updateBank(request, json), HttpStatus.OK);
    }

    @PatchMapping(value = { "/company" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response updateCompany(final HttpServletRequest request, @RequestBody final String json) {
        log.info("updateCompany() : {}", json);
        return Utility.buildResponse(updateService.updateCompany(request, json), HttpStatus.OK);
    }

    @PatchMapping(value = { "/customer" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response updateCustomer(@RequestBody final String request) {
        log.info("saveCustomer() : {}", request);
        return Utility.buildResponse(updateService.updateCustomer(request), HttpStatus.OK);
    }

    @PatchMapping(value = { "/deliveryChallan" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response updateDeliveryChallan(@RequestBody final String json) {
        log.info("updateDeliveryChallan() : {}", json);
        return Utility.buildResponse(updateService.updateDeliveryChallan(json), HttpStatus.OK);
    }

    @PatchMapping(value = { "/gst" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response updateGst(@RequestBody final String request) {
        log.info("updateGst() : {}", request);
        return Utility.buildResponse(updateService.updateGst(request), HttpStatus.OK);
    }

    @PatchMapping(value = { "/material" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response updateMaterial(@RequestBody final String request) {
        log.info("updateMaterial() : {}", request);
        return Utility.buildResponse(updateService.updateMaterial(request), HttpStatus.OK);
    }

    @PatchMapping({ "/modify-material-rate" })
    public Response updateMaterialRate(@RequestBody final String request) {
        log.info("updateMaterialRate() : {}", request);
        return Utility.buildResponse(updateService.updateMaterialRate(request), HttpStatus.OK);
    }

    @PatchMapping({ "/session/address" })
    public Response updateSessionAddress(final HttpServletRequest request, @RequestBody final String json) {
        log.info("updateSessionAddress() : {}", json);
        return Utility.buildResponse(updateService.updateAddress(request, json), HttpStatus.OK);
    }

    @PatchMapping(value = { "/site" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response updateSite(final HttpServletRequest request, @RequestBody final String json) {
        log.info("updateSite() : {}", request);
        return Utility.buildResponse(updateService.updateSite(request, json), HttpStatus.OK);
    }

    @PatchMapping(value = { "/user" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response updateUser(final HttpServletRequest request, @RequestBody final String json) {
        log.info("updateUser() : {}", json);
        return Utility.buildResponse(updateService.updateUser(json), HttpStatus.OK);
    }

    @PatchMapping(value = { "/vehicle" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
        produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response updateVehicle(@RequestBody final String request) {
        log.info("saveVehicle() : {}", request);
        return Utility.buildResponse(updateService.updateVehicle(request), HttpStatus.OK);
    }

    @PatchMapping(value = { "/employee" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
        produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response updateEmployee(@RequestBody final String request) {
        log.info("updateEmployee() : {}", request);
        return Utility.buildResponse(updateService.updateEmployee(request), HttpStatus.OK);
    }

    @PatchMapping("/reload-Environment-Variables")
    public Boolean loadEnvironmentVariables() {
        log.info("reloadEnvironmentVariables Before : {}", EnvironmentConfig.variables);
        EnvironmentConfig.clear();
        log.info("reloadEnvironmentVariables after clear : {}", EnvironmentConfig.variables);
        List<EnvironmentVariable> variableList = lookupService.getEntityList(EnvironmentVariable.class);
        if(CollectionUtils.isNotEmpty(variableList)){
            EnvironmentConfig.variables = variableList.stream()
                .collect(Collectors.toMap(EnvironmentVariable::getKeyName, EnvironmentVariable::getKeyValue));
        }
        log.info("reloadEnvironmentVariables After : {}", EnvironmentConfig.variables);
        return true;
    }

    @PostMapping(value = { "/total-sales-amount" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response getTotalSalesAmt(@RequestBody final String paramJson) {
        log.info("getTotalSalesAmt()" + paramJson);
        return Utility.buildResponse(lookupService.getTotalSalesAmt(paramJson), HttpStatus.OK);
    }

    @PostMapping(value = { "/total-pending-amount" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response getTotalPendingAmt(@RequestBody final String paramJson) {
        log.info("getTotalPendingAmt()" + paramJson);
        return Utility.buildResponse(lookupService.getTotalPendingAmt(paramJson), HttpStatus.OK);
    }

    @PostMapping(value = { "/total-received-amount" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response getTotalReceivedAmt(@RequestBody final String paramJson) {
        log.info("getTotalReceivedAmt()");
        return Utility.buildResponse(lookupService.getTotalReceivedAmt(paramJson), HttpStatus.OK);
    }

    @PostMapping(value = { "/total-discount-amount" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response getTotalDiscountAmt(@RequestBody final String paramJson) {
        log.info("getTotalPendingAmt()" + paramJson);
        return Utility.buildResponse(lookupService.getTotalDiscountAmt(paramJson), HttpStatus.OK);
    }

    @PostMapping(value = { "/total-orders" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response getTotalOrders(@RequestBody final String paramJson) {
        log.info("totalOrders()" + paramJson);
        return Utility.buildResponse(lookupService.getTotalOrders(paramJson), HttpStatus.OK);
    }

    @PostMapping(value = { "/total-delivery-challans" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response getTotalDeliveryChallans(@RequestBody final String paramJson) {
        log.info("getTotalDeliveryChallans()" + paramJson);
        return Utility.buildResponse(lookupService.getTotalDeliveryChallans(paramJson), HttpStatus.OK);
    }

    /******************************** GET ENUM START ***************************/

    @GetMapping({ "/employee/addressType" })
    public Response getEmpAddressType() {
        log.info("getEmpAddressType() : {}");
        return Utility.buildResponse(AddressType.employeeAddressType(), HttpStatus.OK);
    }

    @GetMapping({ "/customer/addressType" })
    public Response getCustAddressType() {
        log.info("getCustAddressType() : {}");
        return Utility.buildResponse(AddressType.customerAddressType(), HttpStatus.OK);
    }

    @GetMapping({ "/company/addressType" })
    public Response getCompAddressType() {
        log.info("getCompAddressType() : {}");
        return Utility.buildResponse(AddressType.companyAddressType(), HttpStatus.OK);
    }

    @GetMapping({ "/emoloyee/designation" })
    public Response getDesignations() {
        log.info("getDesignations()");
        return Utility.buildResponse(Designation.values(), HttpStatus.OK);
    }

    /******************************** GET ENUM END ***************************/
    
    @PostMapping(value = { "/scheduler-order" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Response saveSchedulerOrder(@RequestBody final String paramJson) {
        log.info("saveSchedulerOrder()" + paramJson);
        return Utility.buildResponse(persistanceService.saveSchedulerOrder(paramJson), HttpStatus.OK);
    }
    
    @PatchMapping(value = { "/scheduler-order" }, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE },
            produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
        public Response updateSchedulerOrder(@RequestBody final String request) {
            log.info("updateSchedulerOrder() : {}", request);
            return Utility.buildResponse(updateService.updateSchedulerOrder(request), HttpStatus.OK);
        }

    @GetMapping({ "/test/{id}" })
    public Response getTest(@PathVariable(Constants.ID) final Long id) {
        log.info("getTest() : {}", id);
        return Utility.buildResponse(lookupService.findEntityById(Company.class, id), HttpStatus.OK);
    }
}
