package com.scms.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.scms.constants.Constants;
import com.scms.model.Site;
import com.scms.service.LookupService;
import com.scms.util.Utility;

@Controller
public class MvcController {
    private static final Logger log = LoggerFactory.getLogger(MvcController.class);

    @Autowired
    private LookupService lookupService;

    

    /*
     * @GetMapping({ "/page/orderer/placeOrder" }) public ModelAndView
     * getPlaceOrder() { log.info("getPlaceOrder Page()"); final ModelAndView model
     * = new ModelAndView(); model.setViewName("/user/orderer/placeOrder");
     * model.addObject("vehicleList",
     * lookupService.getActiveEntityList(Vehicle.class));
     * model.addObject("materialList",
     * lookupService.getActiveEntityList(Material.class));
     * model.addObject("siteList", lookupService.getActiveEntityList(Site.class));
     * model.addObject("statesList",
     * lookupService.getActiveEntityList(States.class)); return model; }
     */

    /*
     * @GetMapping({ "/page/dc/generateDC" }) public ModelAndView getGenerateDC() {
     * log.info("getGenerateDC Page()"); final ModelAndView model = new
     * ModelAndView(); model.setViewName("/dc/generateDC");
     * model.addObject("vehicleList",
     * lookupService.getActiveEntityList(Vehicle.class));
     * model.addObject("materialList",
     * lookupService.getActiveEntityList(Material.class));
     * model.addObject("siteList", lookupService.getActiveEntityList(Site.class));
     * return model; }
     */

    @GetMapping({ "/page/totalView" })
    public ModelAndView getTotalView() {
        log.info("getTotalView Page()");
        final ModelAndView model = new ModelAndView();
        model.setViewName("/user/totalView");
        model.addObject("siteList", lookupService.getActiveEntityList(Site.class));

        String paramJson = "{ \"" + Constants.START_DATE + "\" : \"" + Utility.getCurrentDate() + "\", \""
            + Constants.END_DATE + "\" : \"" + Utility.getCurrentDate() + "\" }";

        model.addObject("totalSalesAmount", lookupService.getTotalSalesAmt(paramJson));
        model.addObject("totalReceivedAmount", lookupService.getTotalReceivedAmt(paramJson));
        model.addObject("totalPendingAmount", lookupService.getTotalPendingAmt(paramJson));

        model.addObject("totalOrders", lookupService.getTotalOrders(paramJson));
        model.addObject("totalDeliveryChallans", lookupService.getTotalDeliveryChallans(paramJson));
        model.addObject("totalDiscountAmount", lookupService.getTotalDiscountAmt(paramJson));
        return model;
    }
}
