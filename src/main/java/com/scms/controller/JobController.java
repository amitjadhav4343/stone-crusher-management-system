package com.scms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.scms.constants.Constants;
import com.scms.quartz.service.SchedulerService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class JobController {

    @Autowired
    private SchedulerService schedulerService;
    
    @GetMapping({ "/schedule-all-jobs" })
    public ResponseEntity<?> scheduleAllJobs() {
        log.info("scheduleAllJobs()");
        return new ResponseEntity<>(schedulerService.scheduleAllJobs(), HttpStatus.OK);
    }
    
    @GetMapping({ "/schedule-job/{id}" })
    public ResponseEntity<?> scheduleJob(@PathVariable(Constants.ID) final Long id) {
        log.info("scheduleJob()");
        return new ResponseEntity<>(schedulerService.scheduleJob(id), HttpStatus.OK);
    }
}
