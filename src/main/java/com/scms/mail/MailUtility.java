package com.scms.mail;

import com.scms.model.Address;
import com.scms.model.Customer;
import com.scms.model.DeliveryChallan;
import com.scms.model.Item;
import com.scms.model.Orders;

public abstract class MailUtility {
    public static String getBody(final DeliveryChallan dc) {
        final Customer cust = dc.getCustomer();
        final Address add = dc.getAddress();
        final StringBuffer sb = new StringBuffer("<h3 style='color : #cc6600;'>Hello ");
        sb.append(cust.getName());
        sb.append(
            ",</h3><div style='font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;font-size: 15px;'>Your Delivery Challan has been successfully placed.<br>Order will be shipped on below address.<br><br></div><div><strong style='font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;'>Shipping address :</strong><br><span style='font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;font-size: small;'>");
        sb.append(add.getStreet() + "<br>");
        sb.append(add.getCity() + ", " + add.getState()
            .getName() + "<br>");
        sb.append(add.getPincode());
        return sb.toString();
    }

    public static String getBody(final Orders od) {
        final Customer cust = od.getCustomer();
        final Address add = od.getAddress();
        final StringBuffer sb = new StringBuffer("<h4>Hi ");
        sb.append(cust.getName());
        sb.append(
            ",</h4><div style='font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;font-size: 15px;'>Your order has been successfully placed and estimated delivery  is today EOD.<br>Order will be shipped on below address.<br><br></div><div><strong style='font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;'>Shipping address :</strong><br><span style='font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;font-size: small;'>");
        sb.append(add.getStreet() + "<br>");
        sb.append(add.getCity() + ", " + add.getState()
            .getName() + "<br>");
        sb.append(add.getPincode());
        sb.append(
            "</span></div><br><table style='table-layout: auto; width: 100%; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; border-collapse: collapse;border: 1px solid #ddd;'><tr><th colspan='7' style='padding: 10px; background-color: #3C7C91!important;text-align: center;font-size: 25px;color: white!important;'>Order Details</th></tr><tr><td colspan='3' style='font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;font-size: small; text-align: left;'>Order Id : ");
        sb.append(od.getSid());
        sb.append(
            "<br><br></td><td colspan='4' style='font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: small; text-align: right;'>Order Placed on : ");
        sb.append(od.getOrderDate());
        sb.append(
            "<br><br></td></tr><tr><th style='padding: 8px; background-color: #3C7C91!important;  color: white!important;'>No</th><th style='padding: 8px; background-color: #3C7C91!important;  color: white!important;'>Product</th><th style='padding: 8px; background-color: #3C7C91!important;  color: white!important;'>Quantity</th><th style='padding: 8px; background-color: #3C7C91!important;  color: white!important;'>Rate</th><th style='padding: 8px; background-color: #3C7C91!important;  color: white!important;'>GST Perc</th>  <th style='padding: 8px; background-color: #3C7C91!important;  color: white!important;'>GST</th>        <th style='padding: 8px; background-color: #3C7C91!important;  color: white!important;'>Total</th></tr>");
        // String rStart = "<td style='padding: 8px; background-color:
        // #f2f2f2;text-align: center;'>";
        // String rEnd = "</td>";
        int srNo = 0;
        double gstAmt = 0.0D;
        double subTotal = 0.0D;
        for(final Item o : od.getItems()){
            gstAmt = o.getGstAmt()
                .doubleValue() + gstAmt;
            subTotal = o.getSubTotal()
                .doubleValue() + subTotal;
            srNo++;
            sb.append("<tr>");
            sb.append("<td style='padding: 8px; background-color: #f2f2f2;text-align: center;'>" + srNo + "</td>");
            sb.append("<td style='padding: 8px; background-color: #f2f2f2;text-align: center;'>" + o.getMaterial()
                .getName() + "</td>");
            sb.append(
                "<td style='padding: 8px; background-color: #f2f2f2;text-align: center;'>" + o.getQtyBrass() + "</td>");
            sb.append(
                "<td style='padding: 8px; background-color: #f2f2f2;text-align: center;'>" + o.getRate() + "</td>");
            sb.append("<td style='padding: 8px; background-color: #f2f2f2;text-align: center;'>" + o.getGst() + "%"
                + "</td>");
            sb.append(
                "<td style='padding: 8px; background-color: #f2f2f2;text-align: center;'>" + o.getGstAmt() + "</td>");
            sb.append(
                "<td style='padding: 8px; background-color: #f2f2f2;text-align: center;'>" + o.getSubTotal() + "</td>");
            sb.append("</tr>");
        }
        sb.append(
            "<td colspan='4' style='padding: 8px; background-color: #f2f2f2;'></td><td style='padding: 8px; background-color: #f2f2f2;text-align: center;'>Sub Total</td><td style='padding: 8px; background-color: #f2f2f2;text-align: center;'>");
        sb.append(gstAmt);
        sb.append("</td><td style='padding: 8px; background-color: #f2f2f2;text-align: center;'>");
        sb.append(subTotal);
        sb.append(
            "</td></tr><tr><td colspan='5' style='padding: 8px; background-color: #3C7C91;'></td><td style='padding: 8px; background-color: #3C7C91!important;text-align: center;font-size: large;font-weight: bold;color: white!important;'>Total</td><td style='padding: 10px; background-color: #3C7C91!important;text-align: center;font-size: large;font-weight: bold;color: white!important;'>");
        sb.append(od.getTotal());
        sb.append(
            "‬</td></tr></table><br><br><div style='font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;font-size: 14px;'>  If you would like to know the status of your order or make any changes into it, <br>please call to our customer care +91-9823378722.<br><br><br><span>Thank you for order...!!!</span></div>");
        return sb.toString();
    }
}
