package com.scms.mail;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.scms.constants.Constants;
import com.scms.env.config.EnvironmentConfig;
import com.scms.model.DeliveryChallan;
import com.scms.model.Orders;
import com.scms.repository.CommonRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("emailService")
public class EmailService {

    @Value("${spring.mail.username}")
    private String username;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private CommonRepository commonRepository;

    /**
     * @param path
     */
    public void cleanUp(final String path) {
        log.info("Going to delete file : {}", path);
        try{
            Files.deleteIfExists(Paths.get(path, new String [0]));
        } catch(final IOException e){
            log.error("getting error while deleting file : ", e);
            e.printStackTrace();
        }
    }

    /**
     * @param emailAddr
     * @return
     */
    private InternetAddress [] getInternetAddressList(final String emailAddr) {
        InternetAddress [] list = new InternetAddress [0];
        if(!StringUtils.isEmpty(emailAddr)){
            final String [] recipients = emailAddr.split(",");
            list = new InternetAddress [recipients.length];
            for(int i = 0; i < recipients.length; i++){
                try{
                    list [i] = new InternetAddress(recipients [i].trim());
                } catch(final Exception e){
                    log.error("error while generating list : {}", e);
                }
            }
        }
        return list;
    }

    /**
     * @param <T>
     * @param entity
     * @param toList
     * @param subject
     * @param body
     */
    @Async
    public <T> void sendMail(final T entity, final String toList, final String subject, final String body) {
        log.info("sendMailWithAttachment() : Start");
        try{
            final MimeMessage message = this.mailSender.createMimeMessage();
            final MimeMessageHelper helper = new MimeMessageHelper(message, true);
            setAttributes(helper, subject, body);
            setRecipients(message, toList, EnvironmentConfig.variables.get(Constants.MAIL_CC),
                EnvironmentConfig.variables.get(Constants.MAIL_BCC));
            this.mailSender.send(message);
            updateMailStatus(entity);
        } catch(final MessagingException e){
            log.debug("Exception while sending email : ", e);
        }
        log.info("sendMailWithAttachment() : End");
    }

    /**
     * @param <T>
     * @param entity
     * @param toList
     * @param subject
     * @param body
     * @param filePath
     */
    @Async
    public <T> void sendWithAttachment(final T entity, final String toList, final String subject, final String body,
        final String filePath) {
        log.info("sendWithAttachment() : Start");
        try{
            final MimeMessage message = this.mailSender.createMimeMessage();
            final MimeMessageHelper helper = new MimeMessageHelper(message, true);
            setAttributes(helper, subject, body);
            setRecipients(message, toList, EnvironmentConfig.variables.get(Constants.MAIL_CC),
                EnvironmentConfig.variables.get(Constants.MAIL_BCC));
            setAttachments(helper, new String [] { filePath });
            this.mailSender.send(message);
            cleanUp(filePath);
            updateMailStatus(entity);
        } catch(final MessagingException e){
            log.debug("Exception while sending email : ", e);
        }
        log.info("sendWithAttachment() : End");
    }

    /**
     * @param helper
     * @param filesToAttach
     * @return
     * @throws MessagingException
     */
    private MimeMessageHelper setAttachments(final MimeMessageHelper helper, final String... filesToAttach)
        throws MessagingException {
        for(final String filePath : filesToAttach){
            final FileSystemResource file = new FileSystemResource(new File(filePath));
            final String [] arr = filePath.split(Pattern.quote(File.separator));
            helper.addAttachment(arr [arr.length - 1], file);
        }
        return helper;
    }

    /**
     * @param helper
     * @param subject
     * @param body
     * @return
     * @throws MessagingException
     */
    private MimeMessageHelper setAttributes(final MimeMessageHelper helper, final String subject, final String body)
        throws MessagingException {
        helper.setFrom(this.username);
        helper.setSubject(subject);
        helper.setText(body, body);
        return helper;
    }

    /**
     * @param message
     * @param toList
     * @param ccList
     * @param bccList
     * @return
     * @throws MessagingException
     */
    private MimeMessage setRecipients(final MimeMessage message, final String toList, final String ccList,
        final String bccList) throws MessagingException {
        message.addRecipients(Message.RecipientType.TO, getInternetAddressList(toList));
        message.addRecipients(Message.RecipientType.CC, getInternetAddressList(ccList));
        message.addRecipients(Message.RecipientType.BCC, getInternetAddressList(bccList));
        return message;
    }

    /**
     * @param entity
     */
    public <T> void updateMailStatus(final T entity) {
        log.info("start updateMailStatus");
        try{
            Thread.sleep(10 * 1000);
            if(entity instanceof Orders){
                final Long id = ((Orders) entity).getId();
                this.commonRepository.updateOrdersSentMail(true, id);
            } else if(entity instanceof DeliveryChallan){
                final Long id = ((DeliveryChallan) entity).getId();
                this.commonRepository.updateDeliveryChallanSentMail(true, id);
            }else {
                log.debug("Mail update not required !!!");
            }
            log.info("end updateMailStatus");
        } catch(final InterruptedException e){
            log.error("InterruptedException occured while updateMailStatus!!! : " + entity.getClass()
                .getSimpleName());
            e.printStackTrace();
        } catch(final Exception e){
            log.error("Exception occured while updateMailStatus!!! : " + entity.getClass()
                .getSimpleName());
            e.printStackTrace();
        }
    }
}
