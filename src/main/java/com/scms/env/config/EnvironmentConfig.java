package com.scms.env.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.scms.model.EnvironmentVariable;
import com.scms.service.LookupService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class EnvironmentConfig {

    public static Map<String, String> variables = new HashMap<>();

    @Autowired
    private LookupService lookupService;

    public static void clear() {
        variables.clear();
    }

    public static void add(String key, String value) {
        variables.put(key, value);
    }

    public static void add(Map<String, String> map) {
        map.putAll(map);
    }

    @PostConstruct
    public void init() throws Exception {
        log.info("EnvironmentConfig init Start : {}", variables);
        List<EnvironmentVariable> variableList = lookupService.getActiveEntityList(EnvironmentVariable.class);
        if(CollectionUtils.isNotEmpty(variableList)){
            variables = variableList.stream()
                .collect(Collectors.toMap(EnvironmentVariable::getKeyName, EnvironmentVariable::getKeyValue));
        }
        log.info("EnvironmentConfig init End : {}", variables);
    }

    @PreDestroy
    public void cleanUp() throws Exception {
    }
}
