package com.scms.constants;

import java.util.List;

import com.scms.model.States;

public final class Constants {

    public static List<States> STATE_LIST = null;

    public static final String COMMA = ",";

    public static final String EMPTY = "";

    public static final String DOT_PDF_EXT = ".pdf";
    
    public static final String DOT_XLSX_EXT = ".xlsx";
    
    public static final String DOT_CSV_EXT = ".csv";

    public static final String ADDRESS = "address";

    public static final String CITY = "city";

    public static final String COMPANY_SMALL_CAPS = "company";

    public static final String CUSTOMER = "customer";

    public static final String CUSTOMERTYPE = "customerType";

    public static final String DB_FALSE = "0";

    public static final String DB_TRUE = "1";

    public static final String DD_MM_YYYY = "dd-MM-yyyy";

    public static final String DD_MM_YYYY_HH_MM_A = "dd-MM-yyyy HH:mm a";

    public static final String HH_MM_SS = " 00:00:00";

    public static final String HH_MM_SS_START = " 00:00:01";

    public static final String HH_MM_SS_END = " 23:59:59";

    public static final String GST = "gst";

    public static final String GSTAMT = "gstAmt";

    public static final String GSTNO = "gstNo";

    public static final String GSTS = "gsts";

    public static final String IDENTITY = "identity";

    public static final String INVOICE_PREFIX_DC = "DC";

    public static final String INVOICE_PREFIX_PLOD = "Invoice PLOD";

    public static final String ISPENDING = "isPending";

    public static final String LOWER_NAME = "name";

    public static final String MATERIAL = "material";

    public static final String MODEL = "model";

    public static final String NAME = "Name";

    public static final String NULL = "null";

    public static final String NUMBER = "number";

    public static final String ORDERDATE = "orderDate";

    public static final String ORDERTYPE = "orderType";

    public static final String OWNERNAME = "ownerName";

    public static final String PACKAGE_MODEL = "com.scms.model.";

    public static final String PACKAGE_MODEL_DTO = "com.scms.model.dto.";

    public static final String PREFIX_DC = "DC";

    public static final String PREFIX_OD = "OD";

    public static final String PATH_PDF_DC = "src/main/resources/files/pdf/deliverychallan/";

    public static final String PATH_PDF_OD = "src/main/resources/files/pdf/place-order/";
    
    public static final String PATH_SRC_FILES = "src/main/resources/files";

    public static final String PAYMODE = "payMode";

    public static final String PENDINGAMT = "pendingAmt";

    public static final String PHONE = "phone";

    public static final String PINCODE = "pincode";

    public static final String QTYBRASS = "qtyBrass";

    public static final String QTYKG = "qtyKg";

    public static final String RATE = "rate";

    public static final String RECEIVEDAMT = "receivedAmt";

    public static final String REGDATE = "regDate";

    public static final String REGNUMBER = "regNumber";

    public static final String REGUPTO = "regUpto";

    public static final String REMARKS = "remarks";

    public static final String SITE = "site";

    public static final String SMS_EXCEPTION = "SMS Exception: {} {}";

    public static final String STATE = "state";

    public static final String STATUS = "status";

    public static final String STREET = "street";

    public static final String subject = "Your Order OD{0} has been successfully placed";

    public static final String SUBTOTAL = "subTotal";

    public static final String TOTAL = "total";

    public static final String TRUE = "true";
    
    public static final boolean B_TRUE = true;
    
    public static final boolean B_FALSE = false;

    public static final String TYPE = "type";

    public static final String UNDEFINED = "undefined";

    public static final String USER_SMALL_CAPS = "user";

    public static final String USERDTO_CAMEL = "userDto";

    public static final String VEHICLE = "vehicle";

    public static final String VEHICLES = "vehicles";

    public static final String WITHGST = "withGst";

    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    public static final String MAIL_CC = "mail.cc";

    public static final String MAIL_BCC = "mail.bcc";

    public static final String SMS_OWNERS = "sms.owners";

    public static final String START_DATE = "startDate";

    public static final String END_DATE = "endDate";
    
    public static final String ID = "id";
}
