package com.scms.util;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.Document;
import com.scms.constants.Constants;
import com.scms.enums.ErrorConstants;
import com.scms.enums.Period;
import com.scms.exception.ScmsException;
import com.scms.model.dto.Response;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Utility {
	
	private Utility(){
		/*Private Constructor will prevent the instantiation of this class directly*/
	}

    /**
     * @param date
     * @return FinancialYear in String format
     */
    public static String getFinancialYear(final LocalDateTime date) {
        log.info("getFinancialYear : date : {}", date);
        final int year = date.getMonthValue() < 3 ? date.getYear() - 1 : date.getYear();
        return year + "-" + (year + 1);
    }

    /**
     * @param str
     * @return Initials of given string
     */
    public static String getInitials(final String str) {
        final StringBuilder sb = new StringBuilder();
        for(final String s : str.split(" ")){
            sb.append(s.charAt(0));
        }
        return sb.toString();
    }

    /**
     * @param <T>
     * @param t
     * @return String json of given object.
     */
    public static <T> String getJson(final T t) {
        log.info("getJson() : {}", t.getClass());
        String json = null;
        final ObjectMapper mapper = new ObjectMapper();
        try{
            json = mapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(t);
        } catch(final JsonProcessingException e){
            log.error("Exception while parsing json object to string", e);
        }
        return json;
    }

    /**
     * @param format
     * @param sDate
     * @return Date in given format.
     */
    public static Date getDate(final String format, final String sDate) {
        log.info("dateConversion : format : {}, date  : {}", format, sDate);
        Date date = null;
        try{
            date = new SimpleDateFormat(format).parse(sDate);
        } catch(final ParseException e){
            throw new ScmsException(ErrorConstants.ERROR_CODE_3003);
        }
        return date;
    }

    /**
     * @param format
     * @param date
     * @return LocalDateTime in given format.
     */
    public static LocalDateTime getLocalDateTime(final String format, final String date) {
        log.info("getLocalDateTime : format : {}, date  : {}", format, date);
        return LocalDateTime.parse(date, DateTimeFormatter.ofPattern(format));
    }

    /**
     * @param format
     * @param localDate
     * @return return the localDate in string format
     */
    public static String getFormattedDate(final String format, final LocalDateTime localDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return localDate.format(formatter);
    }

    /**
     * @param str
     * @return CamelCase String of given string.
     */
    public static String parseCamelCase(final String str) {
        final StringBuilder builder = new StringBuilder(str);
        boolean isLastSpace = true;
        for(int i = 0; i < builder.length(); i++){
            final char ch = builder.charAt(i);
            if(isLastSpace && ch >= 'a' && ch <= 'z'){
                builder.setCharAt(i, (char) (ch + -32));
                isLastSpace = false;
            } else if(ch != ' '){
                isLastSpace = false;
            } else{
                isLastSpace = true;
            }
        }
        return builder.toString();
    }

    /**
     * @param <T>
     * @param json
     * @param t
     * @return Class<T> t
     */
    public static <T> T getEntity(final String json, final Class<T> t) {
        log.info("parseJsonEntity() : {}", json);
        T entity = null;
        final ObjectMapper mapper = new ObjectMapper();
        try{
            entity = mapper.readValue(json, t);
        } catch(final IOException e){
            log.error("Exception while parsing json string to object", e);
        }
        return entity;
    }

    /**
     * @param <T>
     * @param entity
     * @return Map<String, String> map of given entity
     */
    @SuppressWarnings("unchecked")
    public static <T> Map<String, String> getMap(final T entity) {
        log.info("convertObjMap() : {}", entity);
        final ObjectMapper mapper = new ObjectMapper();
        return mapper.convertValue(entity, Map.class);
    }

    /**
     * @param json
     * @return Map<String, String> map of given json
     */
    @SuppressWarnings("unchecked")
    public static Map<String, String> getMap(final String json) {
        log.info("getMap() : {}", json);
        final ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map = new HashMap<>();
        try{
            map = mapper.readValue(json, HashMap.class);
        } catch(final IOException e){
            log.error("Exception while parsing json string to object", e);
        }
        return map;
    }

    /**
     * @param number in double format
     * @return exact double value
     */
    public static double toDouble(final Double number) {
        return new BigDecimal(number).setScale(2, RoundingMode.HALF_UP)
            .doubleValue();
    }

    /**
     * @param number in Integer format
     * @return
     */
    public static double toDouble(final Integer number) {
        return new BigDecimal(number).setScale(2, RoundingMode.HALF_UP)
            .doubleValue();
    }

    /**
     * @param number in string format
     * @return
     */
    public static double toDouble(final String number) {
        if(StringUtils.isEmpty(number))
            return 0.0d;
        else
            return new BigDecimal(number).setScale(2, RoundingMode.HALF_UP)
                .doubleValue();
    }

    /**
     * @param number
     * @param attribute
     * @return
     */
    public static Long toLong(final String number, final String attribute) {
        if(StringUtils.isEmpty(number))
            throw new ScmsException(ErrorConstants.CAN_NOT_NULL, attribute);
        else
            return Long.valueOf(number);
    }

    /**
     * Will contain at least one capital case letter. Will contain at least one
     * lower-case letter. Will contain at least one number. Will contain one of the
     * following special characters: @, $, #, !.
     * 
     * @param length
     * @return
     */
    public static String generatePassword(int length) {
        String capitalCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
        String specialCharacters = "!@#$";
        String numbers = "1234567890";
        String combinedChars = capitalCaseLetters + lowerCaseLetters + specialCharacters + numbers;
        Random random = new Random();
        char [] password = new char [length];

        password [0] = lowerCaseLetters.charAt(random.nextInt(lowerCaseLetters.length()));
        password [1] = capitalCaseLetters.charAt(random.nextInt(capitalCaseLetters.length()));
        password [2] = specialCharacters.charAt(random.nextInt(specialCharacters.length()));
        password [3] = numbers.charAt(random.nextInt(numbers.length()));

        for(int i = 4; i < length; i++){
            password [i] = combinedChars.charAt(random.nextInt(combinedChars.length()));
        }
        return new String(password);
    }

    public static String getCurrentDate() {
        return LocalDate.now()
            .toString();
    }

    public static LocalDate getLocalDate(String date, String format) {
        log.info("getLocalDate : date : {}, format  : {}", date, format);
        return LocalDate.parse(date, DateTimeFormatter.ofPattern(format));
    }

    
    //@formatter:off
    // Field Name     Mandatory   Allowed Values      Allowed Special Characters
    // Seconds        YES         0-59                , - * /
    // Minutes        YES         0-59                , - * /
    // Hours          YES         0-23                , - * /
    // Day of month   YES         1-31                , - * ? / L W
    // Month          YES         1-12 or JAN-DEC     , - * /
    // Day of week    YES         1-7 or SUN-SAT      , - * ? / L #
    // Year           NO          empty, 1970-2099    , - * /
    // Example : 0/10 * * * * ?
    //@formatter:on
    
	public static String getCronDesc(LocalDateTime startDate, Period period) {
		String cronDesc = StringUtils.EMPTY_STRING;
		
		switch(period) {
		case DAILY : cronDesc = "0 " + startDate.getMinute() + " " + startDate.getHour() + " * * ?";
			break;
		case WEEKLY : cronDesc = "0 " + startDate.getMinute() + " " + startDate.getHour() + " */7 * ?";
			break;
		case MONTHLY : cronDesc = "0 " + startDate.getMinute() + " " + startDate.getHour() + " " + startDate.getDayOfMonth() + " * ?";
			break;
		case DONOTREPEAT : cronDesc = "0 " + startDate.getMinute() + " " + startDate.getHour() + " " + startDate.getDayOfMonth() + " " + startDate.getMonthValue() + " ? " + startDate.getYear();
			break;//0 15 10 4 10 ? 2021(4th Oct 2021 10:15AM)
		default: log.warn("");
		}
		log.debug("Cron is created : {} against date : {}", cronDesc, startDate);
		return cronDesc;
	}
	
	/**
	 * Delete the file if it is already exists.
	 * 
	 * @param filePath
	 */
	public static void deleteFileIfExists(String filePath) {
		log.info("deleteFileIfExists : {}", filePath);
		
		File outputFile = new File(filePath);
        if (outputFile.exists()){
            outputFile.delete();
        }
	}
	
	/**
	 * Set 
	 * 
	 * @param document
	 * @param title
	 * @param subject
	 */
	public static void addMetaData(final Document document, final String title, final String subject) {
		document.addCreationDate();
		document.addTitle(title);
		document.addSubject(subject);
		document.addAuthor("SCMS Application");
		document.addCreator("SCMS Application");
	}

	public static <T> Response buildResponse(final T entity, HttpStatus httpStatus) {
		return new Response(httpStatus.value(), Constants.B_TRUE, httpStatus.getReasonPhrase(), entity);
	}
}
