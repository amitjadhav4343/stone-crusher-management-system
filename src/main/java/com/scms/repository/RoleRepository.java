package com.scms.repository;

import org.springframework.stereotype.Repository;

import com.scms.model.Role;

@Repository
public interface RoleRepository extends SqlRepository<Role> {
    Role findByName(String paramString);
}
