package com.scms.repository;

import org.springframework.stereotype.Repository;

import com.scms.model.DeliveryChallan;

@Repository
public interface DeliveryChallanRepository extends SqlRepository<DeliveryChallan> {
}
