package com.scms.repository;

import org.springframework.stereotype.Repository;

import com.scms.model.Employee;

@Repository
public interface EmployeeRepository extends SqlRepository<Employee> {
}
