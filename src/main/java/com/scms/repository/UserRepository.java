package com.scms.repository;

import org.springframework.stereotype.Repository;

import com.scms.model.User;

@Repository
public interface UserRepository extends SqlRepository<User> {
    User findByUsername(String paramString);
}
