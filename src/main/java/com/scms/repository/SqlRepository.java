package com.scms.repository;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface SqlRepository<T>
    extends JpaRepository<T, Long>, CrudRepository<T, Long>, DataTablesRepository<T, Long> {
}
