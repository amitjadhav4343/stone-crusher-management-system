package com.scms.repository;

import org.springframework.stereotype.Repository;

import com.scms.model.Gst;

@Repository
public interface GstRepository extends SqlRepository<Gst> {
}
