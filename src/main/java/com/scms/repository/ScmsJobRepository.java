package com.scms.repository;

import org.springframework.stereotype.Repository;

import com.scms.model.ScmsJob;

@Repository
public interface ScmsJobRepository extends SqlRepository<ScmsJob> {
}
