package com.scms.repository;

import org.springframework.stereotype.Repository;

import com.scms.model.Material;

@Repository
public interface MaterialRepository extends SqlRepository<Material> {
}
