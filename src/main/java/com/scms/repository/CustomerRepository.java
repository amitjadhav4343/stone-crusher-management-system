package com.scms.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.scms.model.Customer;

@Repository
public interface CustomerRepository extends SqlRepository<Customer> {

    List<Customer> findByNameContaining(String paramString);

    List<Customer> findByPhoneContaining(String paramString);
}
