package com.scms.repository;

import org.springframework.stereotype.Repository;

import com.scms.model.ModelView;

@Repository
public interface ModelViewRepository extends SqlRepository<ModelView> {
}
