package com.scms.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.scms.model.TransactionHistory;

@Repository
public interface TransHistoryRepository extends SqlRepository<TransactionHistory> {
    List<TransactionHistory> findByCustomer(Long paramLong);
}
