package com.scms.repository.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.scms.model.Customer;
import com.scms.model.DeliveryChallan;
import com.scms.model.Employee;
import com.scms.model.Gst;
import com.scms.model.Material;
import com.scms.model.ModelView;
import com.scms.model.Orders;
import com.scms.model.ScmsJob;
import com.scms.model.TransactionHistory;
import com.scms.model.User;
import com.scms.model.Vehicle;
import com.scms.repository.CustomerRepository;
import com.scms.repository.DeliveryChallanRepository;
import com.scms.repository.EmployeeRepository;
import com.scms.repository.GstRepository;
import com.scms.repository.MaterialRepository;
import com.scms.repository.ModelViewRepository;
import com.scms.repository.OrdersRepository;
import com.scms.repository.ScmsJobRepository;
import com.scms.repository.TransHistoryRepository;
import com.scms.repository.UserRepository;
import com.scms.repository.VehicleRepository;

import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class RepositoryServiceImpl implements RepositoryService {

    private CustomerRepository customerRepository;

    private DeliveryChallanRepository dcRepository;

    private MaterialRepository materialRepository;

    private OrdersRepository ordersRepository;

    private VehicleRepository vehicleRepository;

    private UserRepository userRepository;

    private TransHistoryRepository transHistoryRepository;

    private ModelViewRepository modelViewRepository;
    
    private GstRepository gstRepository;
    
    private EmployeeRepository empRepository;
    
    private ScmsJobRepository scmsJobRepository;

    @Override
    public DataTablesOutput<DeliveryChallan> getDeliveryChallan(final DataTablesInput input) {
        return this.dcRepository.findAll(input);
    }

    @Override
    public DataTablesOutput<Material> getMaterials(final DataTablesInput input) {
        return this.materialRepository.findAll(input);
    }

    @Override
    public DataTablesOutput<Orders> getOrders(final DataTablesInput input) {
        return this.ordersRepository.findAll(input);
    }

    @Override
    public DataTablesOutput<TransactionHistory> getTransactionHistory(final DataTablesInput input) {
        return this.transHistoryRepository.findAll(input);
    }

    @Override
    public DataTablesOutput<User> getUsers(final DataTablesInput input) {
        return this.userRepository.findAll(input);
    }

    @Override
    public DataTablesOutput<Vehicle> getVehicles(@Valid final DataTablesInput input) {
        return this.vehicleRepository.findAll(input);
    }

    @Override
    public List<TransactionHistory> getTransactionHistory(final Long id) {
        return this.transHistoryRepository.findByCustomer(id);
    }

    @Override
    public DataTablesOutput<ModelView> findAll(DataTablesInput input) {
        return modelViewRepository.findAll(input);
    }

    @Override
    public DataTablesOutput<Customer> getCustomers(DataTablesInput input) {
        return customerRepository.findAll(input);
    }

    @Override
    public DataTablesOutput<Gst> getGst(DataTablesInput input) {
        return gstRepository.findAll(input);
    }
    
    @Override
    public DataTablesOutput<Employee> getEmployee(DataTablesInput input) {
        return empRepository.findAll(input);
    }
    
    @Override
    public DataTablesOutput<ScmsJob> getScmsJobs(DataTablesInput input) {
        return scmsJobRepository.findAll(input);
    }
}
