package com.scms.repository.service;

import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.scms.model.Customer;
import com.scms.model.DeliveryChallan;
import com.scms.model.Employee;
import com.scms.model.Gst;
import com.scms.model.Material;
import com.scms.model.ModelView;
import com.scms.model.Orders;
import com.scms.model.ScmsJob;
import com.scms.model.TransactionHistory;
import com.scms.model.User;
import com.scms.model.Vehicle;

public interface RepositoryService {

    DataTablesOutput<User> getUsers(DataTablesInput paramDataTablesInput);

    DataTablesOutput<Vehicle> getVehicles(DataTablesInput paramDataTablesInput);

    DataTablesOutput<TransactionHistory> getTransactionHistory(DataTablesInput paramDataTablesInput);

    DataTablesOutput<Material> getMaterials(DataTablesInput paramDataTablesInput);

    DataTablesOutput<Orders> getOrders(DataTablesInput paramDataTablesInput);

    DataTablesOutput<DeliveryChallan> getDeliveryChallan(DataTablesInput paramDataTablesInput);

    List<TransactionHistory> getTransactionHistory(Long paramLong);

    DataTablesOutput<ModelView> findAll(DataTablesInput dataTablesInput);

    DataTablesOutput<Customer> getCustomers(DataTablesInput dataTablesInput);

    DataTablesOutput<Gst> getGst(DataTablesInput dataTablesInput);
    
    DataTablesOutput<Employee> getEmployee(DataTablesInput dataTablesInput);
    
    DataTablesOutput<ScmsJob> getScmsJobs(DataTablesInput dataTablesInput);
}
