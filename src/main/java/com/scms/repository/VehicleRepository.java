package com.scms.repository;

import org.springframework.stereotype.Repository;

import com.scms.model.Vehicle;

@Repository
public interface VehicleRepository extends SqlRepository<Vehicle> {
}
