package com.scms.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.scms.model.Orders;

@Repository
public interface OrdersRepository extends SqlRepository<Orders> {

    List<Orders> findAllByOrderDateBetween(LocalDateTime paramLocalDateTime1, LocalDateTime paramLocalDateTime2);

    List<Orders> findByOrderDateStartingWith(String paramString);

    // @formatter:off
    /*@Query(nativeQuery = true,
        value = "select count(o.total) from Orders o where o.orderDate between :startDate and :endDate;")
    Double getTotalSales(@Param("startDate")
    String paramString1, @Param("endDate")
    String paramString2);*/
    // @formatter:on
}
