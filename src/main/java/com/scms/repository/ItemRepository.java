package com.scms.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.scms.model.Item;

@Repository
public interface ItemRepository extends SqlRepository<Item> {

    List<Item> findAllByCreatedDateBetween(Date paramDate1, Date paramDate2);

 // @formatter:off
/*    @Query(nativeQuery = true,
        value = "select m.name as val1, sum(o.qtyBrass) as val2 from Orders o inner join Material m \r\non o.material = m.id where (o.createdDate between :startDate and :endDate) and o.status = 'ACTIVE' GROUP BY o.material")
    List<KeyValue> getMaterialReport(@Param("startDate")
    String paramString1, @Param("endDate")
    String paramString2);

    @Query(nativeQuery = true,
        value = "select m.name as val1, sum(oo.qtyBrass) as val2 from (select o.* from OrdersDetails od inner join Orders o on od.id = o.ordersDetails where od.customer = :customer and od.isPending = :isPending) oo inner join Material m on oo.material = m.id where (oo.createdDate between :startDate and :endDate) and oo.status = 'ACTIVE' GROUP BY oo.material;")
    List<KeyValue> getMaterialReportAndPendStatus(@Param("startDate")
    String paramString1, @Param("endDate")
    String paramString2, @Param("customer")
    String paramString3, @Param("isPending")
    String paramString4);

    @Query(nativeQuery = true,
        value = "select m.name as val1, sum(oo.qtyBrass) as val2 from (select o.* from OrdersDetails od inner join Orders o on od.id = o.ordersDetails where od.customer = :customer) oo inner join Material m on oo.material = m.id where (oo.createdDate between :startDate and :endDate) and oo.status = 'ACTIVE' GROUP BY oo.material;")
    List<KeyValue> getMatReportCustWise(@Param("startDate")
    String paramString1, @Param("endDate")
    String paramString2, @Param("customer")
    String paramString3);

    @Query(nativeQuery = true,
        value = "select m.name as val1, sum(oo.qtyBrass) as val2 from (select o.* from OrdersDetails od inner join Orders o on od.id = o.ordersDetails where od.isPending = :isPending) oo inner join Material m on oo.material = m.id where (oo.createdDate between :startDate and :endDate) and oo.status = 'ACTIVE' GROUP BY oo.material;")
    List<KeyValue> getMatReportPendStatusWise(@Param("startDate")
    String paramString1, @Param("endDate")
    String paramString2, @Param("isPending")
    String paramString3);*/
 // @formatter:on
    
    @Query(nativeQuery = true,
        value = "select count(o.id) from Orders o where o.orderDate between :startDate and :endDate")
    Long getTotalSales(@Param("startDate")
    String paramString1, @Param("endDate")
    String paramString2);
}
