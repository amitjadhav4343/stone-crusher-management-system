package com.scms.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.scms.model.Gst;

@Repository
@Transactional
public interface CommonRepository extends SqlRepository<Gst> {

    @Modifying
    @Query("UPDATE DeliveryChallan dc SET dc.isSentMail = ?1 WHERE dc.id = ?2")
    public void updateDeliveryChallanSentMail(boolean isSentMail, long id);

    @Modifying
    @Query("UPDATE Orders o SET o.isSentMail = ?1 WHERE o.id = ?2")
    public void updateOrdersSentMail(boolean isSentMail, long id);

    @Modifying
    @Query("UPDATE DeliveryChallan dc SET dc.isSentSms = ?1 WHERE dc.id = ?2")
    public void updateDeliveryChallanSentSms(boolean isSentMail, long id);

    @Modifying
    @Query("UPDATE Orders o SET o.isSentSms = ?1 WHERE o.id = ?2")
    public void updateOrdersSentSms(boolean isSentMail, long id);
}
