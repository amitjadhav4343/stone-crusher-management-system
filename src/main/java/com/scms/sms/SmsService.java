package com.scms.sms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.scms.constants.Constants;
import com.scms.env.config.EnvironmentConfig;
import com.scms.model.DeliveryChallan;
import com.scms.model.Orders;
import com.scms.repository.CommonRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("smsService")
public class SmsService {

    @Autowired
    private CommonRepository commonRepository;

    @Value("${sms.password}")
    private String password;

    @Value("${sms.sender}")
    private String sender;

    @Value("${sms.username}")
    private String username;

    public <T> boolean send(final T entity, final String phoneNumbers, final String message) {
        sendSms(phoneNumbers, message);
        updateSmsStatus(entity);
        return true;
    }

    private String sendSms(final String custNumber, final String message) {
        log.info("sendSms : {}", custNumber, message);
        String response = null;
        final String phoneNumbers = custNumber + Constants.COMMA
            + EnvironmentConfig.variables.get(Constants.SMS_OWNERS);

        try{
            String data = "user=" + URLEncoder.encode(this.username, "UTF-8");
            data = data + "&password=" + URLEncoder.encode(this.password, "UTF-8");
            data = data + "&message=" + URLEncoder.encode(message, "UTF-8");
            data = data + "&sender=" + URLEncoder.encode(this.sender, "UTF-8");
            data = data + "&mobile=" + URLEncoder.encode(phoneNumbers, "UTF-8");
            data = data + "&type=" + URLEncoder.encode("3", "UTF-8");
            final URL url = new URL("http://login.bulksmsgateway.in/sendmessage.php?" + data);
            final URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            final OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while( (line = rd.readLine()) != null){
                response = response + line + " ";
            }
            wr.close();
            rd.close();
            return response;
        } catch(final Exception e){
            System.out.println("Error SMS " + e);
            return "Error " + e;
        }
    }

    public <T> void updateSmsStatus(final T entity) {
        log.info("start updateSmsStatus");
        try{
            Thread.sleep(10 * 1000);
            if(entity instanceof Orders){
                final Long id = ((Orders) entity).getId();
                this.commonRepository.updateOrdersSentSms(true, id);
            } else if(entity instanceof DeliveryChallan){
                final Long id = ((DeliveryChallan) entity).getId();
                this.commonRepository.updateDeliveryChallanSentSms(true, id);
            }
            log.info("end updateSmsStatus");
        } catch(final InterruptedException e){
            log.error("InterruptedException occured while updateSmsStatus!!! : " + entity.getClass()
                .getSimpleName());
            e.printStackTrace();
        } catch(final Exception e){
            log.error("Exception occured while updateSmsStatus!!! : " + entity.getClass()
                .getSimpleName());
            e.printStackTrace();
        }
    }
}
