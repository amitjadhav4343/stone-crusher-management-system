package com.scms.enums;

import java.math.BigInteger;

import com.scms.model.DeliveryChallan;
import com.scms.model.dto.KeyValueClass;

@SuppressWarnings ("rawtypes")
public enum QueryConstants {
    GET_DC_BY_CUSTOMER (
        "from DeliveryChallan dc where dc.customer = '%s' and dc.isBillable = '%s'",
            DeliveryChallan.class),
    TOGGLE_STATUS ("UPDATE '%s' SET STATUS = '%s' WHERE ID = '%s'", String.class),
    TOTAL_PENDING_AMOUNT (
        "select CASE WHEN sum(tbl.pendingAmt) IS NULL THEN 0 ELSE sum(tbl.pendingAmt) END as total from Orders tbl where tbl.orderDate between '%s' and '%s'",
            Double.class),
    TOTAL_RECEIVED_AMOUNT (
        "select CASE WHEN sum(tbl.receivedAmt) IS NULL THEN 0 ELSE sum(tbl.receivedAmt) END as total from Orders tbl where tbl.orderDate between '%s' and '%s'",
            Double.class),
    TOTAL_SALES_AMOUNT (
        "select CASE WHEN sum(tbl.total) IS NULL THEN 0 ELSE sum(tbl.total) END as total from Orders tbl where tbl.orderDate between '%s' and '%s'",
            Double.class),
    TOTAL_ORDERS_COUNT (
        "select CASE WHEN count(tbl.id) IS NULL THEN 0 ELSE count(tbl.id) END as total from Orders tbl where tbl.orderDate between '%s' and '%s'",
            BigInteger.class),
    TOTAL_DC_COUNT (
        "select CASE WHEN count(tbl.id) IS NULL THEN 0 ELSE count(tbl.id) END as total from deliverychallan tbl where tbl.orderDate between '%s' and '%s'",
            BigInteger.class),
    TOTAL_DISCOUNT_AMOUNT (
        "select CASE WHEN sum(tbl.discount) IS NULL THEN 0 ELSE sum(tbl.discount) END as total from Orders tbl where tbl.orderDate between '%s' and '%s'",
            Double.class),
    MATERIAL_WISE_COUNT("select m.name as column1, sum(oo.qtyBrass) as column2 from (select o.* from Orders od \r\n" + 
        "inner join Item o on od.id = o.orders where od.customer = '%s' and od.isPending = '%s') oo \r\n" + 
        "inner join Material m on oo.material = m.id where (oo.createdDate between '%s' and '%s') and oo.isActive = '1' GROUP BY oo.material", KeyValueClass.class);

    private Class  entityCls;

    private String queryStr;

    QueryConstants (final String queryStr, final Class entityCls) {
        this.queryStr = queryStr;
        this.entityCls = entityCls;
    }

    public Class getEntityClass () {
        return this.entityCls;
    }

    public String getQuery () {
        return this.queryStr;
    }
}
