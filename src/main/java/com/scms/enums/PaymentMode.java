package com.scms.enums;

public enum PaymentMode{
    ADVANCE,
    ADVANCE_CASH,
    CASH;
}
