package com.scms.enums;

public enum JobParmKey{
	Site,
	StartDate,
	EndDate,
	Customer,
	FileType,
	ReportType,
	Recipients;
}
