package com.scms.enums;

import java.util.EnumSet;

public enum AddressType{
    PERMANENT,
    CURRENT,
    BILLING,
    SITE,
    DUMMY;

    public static EnumSet<AddressType> employeeAddressType() {
        return EnumSet.of(AddressType.PERMANENT, AddressType.CURRENT);
    }

    public static EnumSet<AddressType> customerAddressType() {
        return EnumSet.of(AddressType.BILLING, AddressType.SITE);
    }

    public static EnumSet<AddressType> companyAddressType() {
        return EnumSet.of(AddressType.BILLING, AddressType.SITE);
    }
}
