package com.scms.enums;

public enum Designation{
    STAFF,
    DRIVER,
    MANAGER,
    ACCOUNTANT,
    CEO;
}
