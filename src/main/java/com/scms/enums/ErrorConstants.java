package com.scms.enums;

public enum ErrorConstants{
    ALREADY_EXISTS(204, "{0} is already exists !!!"),
    ALREADY_PLACED(202, "{0} was already placed !!!"),
    CAN_NOT_NULL(100, "{0} can not be null !!!"),
    DOES_NOT_EXIST(404, "{0} not found !!!"),
    ERROR_CODE_3001(3001, "Unable to find requested page."),
    ERROR_CODE_3002(3002, "Unable to find list : {0}"),
    ERROR_CODE_3003(3003, "Getting error while date conversion"),
    ERROR_CODE_409(409, "Duplicate record !!!"),
    INVALID_LOGIN(3000, "Invalid credentials"),
    RECIPIENTS_EMPTY(3004, "Can't send scheduled report, due to empty recipients!!!"),
    SUCCESS(0, "Success");

    private int errorCode;

    private String errorText;

    ErrorConstants(final int errorCode, final String errorText) {
        this.errorCode = errorCode;
        this.errorText = errorText;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorText() {
        return this.errorText;
    }
}
