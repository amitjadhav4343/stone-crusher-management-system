package com.scms.enums;

public enum UserRole{
    CUSTOMER,
    MASTER,
    OFFICE,
    ORDERER;
}
