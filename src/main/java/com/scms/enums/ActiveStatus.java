package com.scms.enums;

public enum ActiveStatus{
    ACTIVE,
    INACTIVE;
}
