package com.scms.enums;

public enum Period{
	DAILY,
	WEEKLY,
	MONTHLY,
	DONOTREPEAT;
}
