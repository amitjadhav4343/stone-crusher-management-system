package com.scms.enums;

public enum OrderType{

    /* Order Using DC */
    ODC,

    /* Plain Order */
    PO;
}
