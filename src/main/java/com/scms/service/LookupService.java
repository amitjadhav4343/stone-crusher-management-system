package com.scms.service;

import java.math.BigInteger;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

import com.scms.enums.YesNo;
import com.scms.model.Address;
import com.scms.model.DeliveryChallan;
import com.scms.model.Gst;
import com.scms.model.Item;
import com.scms.model.Material;
import com.scms.model.Orders;
import com.scms.model.User;
import com.scms.model.dto.KeyValueClass;

public interface LookupService {
    boolean balanceCheck(Long paramLong, Double paramDouble);

    <T> List<T> getActiveEntityList(Class<T> paramClass);

    List<Address> getAddresses(String paramString);

    List<DeliveryChallan> getDcByCustomer(String paramString);

    ResponseEntity<?> getDcPdf(HttpServletRequest paramHttpServletRequest, Long id);

    <T> T getEntityByColumnNameAndValue(String paramString1, String paramString2, Class<T> paramClass);

    <T> List<T> getEntityList(Class<T> paramClass);

    <T> List<T> getEntityListByColumnNameAndValue(String paramString1, String paramString2, Class<T> paramClass);

    List<Gst> getGst();

    Gst getGstByMaterialId(Long paramLong);

    MultiValueMap<String, String> getHeader(String paramString1, String paramString2);

    List<KeyValueClass> getKeyValueList(String paramString1, String paramString2, String paramString3);

    List<Material> getMaterial();

    Material getMaterial(Long paramLong);

    String getModelView(String paramString);

    List<Item> getOdcCalculation(List<Long> paramList, YesNo paramYesNo);

    ResponseEntity<?> getOdcPdf(HttpServletRequest paramHttpServletRequest, Long paramLong);

    List<Item> getItemsByOrders(Long paramLong);

    ResponseEntity<?> getPlodPdf(HttpServletRequest paramHttpServletRequest, Long paramLong);

    List<KeyValueClass> getMaterialWiseCount(String paramString);

    List<Orders> getReportByOdList(String paramString);

    User getUser(HttpServletRequest paramHttpServletRequest);

    <T> T lookupById(Class<T> paramClass, Long paramLong);

    <T> T findEntityById(Class<T> typeKey, Long id);

    <T> T lookupByName(Class<T> paramClass, String paramString);

    List<Item> ordersByDate(String paramString1, String paramString2);

    void test(HttpServletRequest paramHttpServletRequest, Orders paramOrder);

    Double getTotalSalesAmt(String paramJson);

    BigInteger getTotalOrders(String paramJson);

    Double getTotalPendingAmt(String paramJson);

    Double getTotalReceivedAmt(String paramJson);

    BigInteger getTotalDeliveryChallans(String paramJson);

    Double getTotalDiscountAmt(String paramJson);

    DataTablesOutput<Orders> getFilterOrders(String paramJson);
    
    DataTablesOutput<DeliveryChallan> getFilterDeliveryChallan(String paramJson);

	List<Orders> getOrders(@Valid String paramJson);

}
