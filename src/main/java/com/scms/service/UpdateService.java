package com.scms.service;

import javax.servlet.http.HttpServletRequest;

import com.scms.enums.ActiveStatus;
import com.scms.model.Address;
import com.scms.model.Bank;
import com.scms.model.Company;
import com.scms.model.Customer;
import com.scms.model.DeliveryChallan;
import com.scms.model.Employee;
import com.scms.model.Gst;
import com.scms.model.Material;
import com.scms.model.ScmsJob;
import com.scms.model.Site;
import com.scms.model.User;
import com.scms.model.Vehicle;

public interface UpdateService {

    Double modifyAdvance(String paramString);

    boolean toggleStatus(Long paramLong, ActiveStatus paramActiveStatus, Class<?> paramClass);

    <T> T update(T paramT);

    Address updateAddress(HttpServletRequest paramHttpServletRequest, String paramString);

    Bank updateBank(HttpServletRequest paramHttpServletRequest, String paramString);

    Company updateCompany(HttpServletRequest paramHttpServletRequest, String paramString);

    Customer updateCustomer(String paramString);

    DeliveryChallan updateDeliveryChallan(String paramString);

    boolean updateEntityByQuery(String paramString, Object... paramVarArgs);

    Gst updateGst(String paramString);

    Material updateMaterial(String paramString);

    Material updateMaterialRate(String paramJson);

    Site updateSite(HttpServletRequest paramHttpServletRequest, String paramString);

    User updateUser(String paramString);

    Vehicle updateVehicle(String paramString);

    Employee updateEmployee(String paramJson);

	ScmsJob updateSchedulerOrder(String paramJson);
}
