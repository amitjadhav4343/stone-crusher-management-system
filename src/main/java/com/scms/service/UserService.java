package com.scms.service;

import com.scms.model.User;

public interface UserService {
    User findByUsername(String paramString);

    void save(User paramUser);

    void update(User paramUser);
}
