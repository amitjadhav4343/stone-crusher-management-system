package com.scms.service;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import com.scms.dao.PersistanceDelegatorDao;
import com.scms.enums.ErrorConstants;
import com.scms.enums.QueryConstants;
import com.scms.enums.YesNo;
import com.scms.exception.ScmsException;
import com.scms.model.Address;
import com.scms.model.Customer;
import com.scms.model.DeliveryChallan;
import com.scms.model.Gst;
import com.scms.model.Item;
import com.scms.model.Material;
import com.scms.model.ModelView;
import com.scms.model.Orders;
import com.scms.model.User;
import com.scms.model.dto.FilterDTO;
import com.scms.model.dto.FilterDTO.Order;
import com.scms.model.dto.KeyValueClass;
import com.scms.model.dto.UserDto;
import com.scms.pdf.PdfUtils;
import com.scms.repository.CustomerRepository;
import com.scms.repository.DeliveryChallanRepository;
import com.scms.repository.ItemRepository;
import com.scms.repository.OrdersRepository;
import com.scms.util.StringUtils;
import com.scms.util.Utility;
import com.scms.validator.Validator;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class LookupServiceImpl implements LookupService {

    private CustomerRepository customerRepository;

    private OrdersRepository ordersRepository;
    
    private DeliveryChallanRepository deliveryChallanRepository;

    private ItemRepository itemRepository;

    private PdfUtils pdfUtils;

    private PersistanceDelegatorDao persistanceDelegatorDao;

    private Validator validator;

    @Override
    public boolean balanceCheck(final Long cId, final Double amount) {
        final Customer customer = this.persistanceDelegatorDao.read(Customer.class, cId);
        return amount.doubleValue() < customer.getAdvance()
            .doubleValue();
    }

    @Override
    public <T> List<T> getActiveEntityList(final Class<T> typeKey) {
        log.info("getEntityList() - Type : {}", typeKey.getSimpleName());
        return this.persistanceDelegatorDao.getActiveEntityList(typeKey);
    }

    @Override
    public List<Address> getAddresses(final String customerId) {
        final List<Address> addList = this.persistanceDelegatorDao.read(Customer.class, Long.valueOf(customerId))
            .getAddress();
        if(addList.size() != 1) return addList.stream()
            .filter(add -> add.isActive())
            .collect(Collectors.toList());
        return addList;
    }

    @Override
    public List<DeliveryChallan> getDcByCustomer(final String customerId) {
        return this.persistanceDelegatorDao.getEntityList(QueryConstants.GET_DC_BY_CUSTOMER.getQuery(),
            DeliveryChallan.class, new Object [] { customerId, "1" });
    }

    @Override
    public ResponseEntity<?> getDcPdf(final HttpServletRequest httpRequest, final Long id) {
        log.info("getDcPdf() id : {}", id);
        final DeliveryChallan deliveryChallan = lookupById(DeliveryChallan.class, id);
        this.validator.validateIsNotNull(ErrorConstants.DOES_NOT_EXIST, deliveryChallan, "Delivery Challan");
        final InputStreamResource inputStreamResource = getFile(
            this.pdfUtils.getDcPdf(getUser(httpRequest), deliveryChallan));
        return new ResponseEntity<>(inputStreamResource,
            getHeader(deliveryChallan.getSid() + ".pdf", "application/pdf"), HttpStatus.OK);
    }

    @Override
    public <T> T getEntityByColumnNameAndValue(final String columnName, final String columnValue,
        final Class<T> typeKey) {
        log.info(" Start of findByColumn() and provided typeKey : {}", typeKey != null ? typeKey.getName() : null);
        if(typeKey != null){
            final String queryString = "from " + typeKey.getName() + " where " + columnName + " = '%s'";
            final List<T> entityList = this.persistanceDelegatorDao.getEntityList(queryString, typeKey,
                new Object [] { columnValue });
            return entityList.stream()
                .findFirst()
                .orElse(null);
        }
        return null;
    }

    @Override
    public <T> List<T> getEntityList(final Class<T> typeKey) {
        log.info("getEntityList() - Type : {}", typeKey.getSimpleName());
        return this.persistanceDelegatorDao.getEntityList(typeKey);
    }

    @Override
    public <T> List<T> getEntityListByColumnNameAndValue(final String columnName, final String columnValue,
        final Class<T> typeKey) {
        log.info("getEntityListByColumnNameAndValue() - columnName : {}, columnValue : {}", columnName, columnValue);
        final String queryString = "from " + typeKey.getName() + " where " + columnName + " = '%s'";
        return this.persistanceDelegatorDao.getEntityList(queryString, typeKey, new Object [] { columnValue });
    }

    public InputStreamResource getFile(final String path) {
        try{
            final FileSystemResource pdfFile = new FileSystemResource(new File(path));
            if(pdfFile.exists()) return new InputStreamResource(pdfFile.getInputStream());
        } catch(final IOException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Gst> getGst() {
        return this.persistanceDelegatorDao.getEntityList(Gst.class);
    }

    @Override
    public Gst getGstByMaterialId(final Long materialId) {
        final Gst gst = this.persistanceDelegatorDao.read(Material.class, materialId)
            .getGst();
        return gst.isActive() ? gst : null;
    }

    @Override
    public MultiValueMap<String, String> getHeader(final String fileName, final String fileType) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(fileType));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
        headers.add("Access-Control-Allow-Headers", "Content-Type");
        headers.add("Content-Disposition", "filename=" + fileName);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return headers;
    }

    @Override
    public List<KeyValueClass> getKeyValueList(final String table, final String key, final String value) {
        log.info("keyValueList() : {}, {}, {}", new Object [] { table, key, value });
        try{
            return this.persistanceDelegatorDao.getKeyValueList(key, value, Class.forName("com.scms.model." + table));
        } catch(final ClassNotFoundException e){
            log.error("ClassNotFoundException in getKeyValueList()");
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Material> getMaterial() {
        return this.persistanceDelegatorDao.getEntityList(Material.class);
    }

    @Override
    public Material getMaterial(final Long id) {
        return this.persistanceDelegatorDao.read(Material.class, id);
    }

    @Override
    public String getModelView(final String request) {
    	final ModelView modelView = this.persistanceDelegatorDao.getEntityByColVal("REQUEST", request, ModelView.class);
        //final ModelView modelView = lookupByName(ModelView.class, request);
        this.validator.validateIsNotNull(ErrorConstants.ERROR_CODE_3001, modelView);
        log.info("requested page path : " + modelView.getRequest());
        return modelView.getResponse();
    }

    @Override
    public List<Item> getOdcCalculation(final List<Long> dcIds, final YesNo gst) {
        final Map<Material, List<DeliveryChallan>> map = new HashMap<>();
        final List<Item> ordList = new ArrayList<>();
        dcIds.stream()
            .forEach(dcId -> {
                final DeliveryChallan dc = this.persistanceDelegatorDao.read(DeliveryChallan.class, dcId);
                final Material material = dc.getMaterial();
                if(map.containsKey(material)){
                    final List<DeliveryChallan> list = map.get(material);
                    list.add(dc);
                    map.put(material, list);
                } else{
                    final List<DeliveryChallan> list = new ArrayList<>();
                    list.add(dc);
                    map.put(material, list);
                }
            });

        Long counter = Long.valueOf(1L);
        for(final Map.Entry<Material, List<DeliveryChallan>> entry : map.entrySet()){
            final Item orders = new Item();
            final Material material = entry.getKey();
            final List<DeliveryChallan> dcList = entry.getValue();
            Double qtyBrass = Utility.toDouble(0);
            Double qtyKg = Utility.toDouble(0);
            orders.setId(counter);
            orders.setMaterial(material);
            orders.setRate(Utility.toDouble(material.getRate()));
            orders.setDeliveryChallans(dcList);
            for(final DeliveryChallan dc : dcList){
                qtyBrass = Utility.toDouble(qtyBrass + dc.getQtyBrass());
                qtyKg = Utility.toDouble(qtyKg + dc.getQtyKg());
            }
            orders.setQtyBrass(Utility.toDouble(qtyBrass));
            orders.setQtyKg(Utility.toDouble(qtyKg));
            final double subTotalTmp = qtyBrass * material.getRate();
            if(YesNo.YES.compareTo(gst) == 0){
                orders.setWithGst(true);
                orders.setGst(material.getGst()
                    .getValue());
                final double gstAmt = subTotalTmp * orders.getGst() / 100D;
                orders.setGstAmt(Utility.toDouble(gstAmt));
                orders.setSubTotal(Utility.toDouble(subTotalTmp));
                orders.setTotal(Utility.toDouble(subTotalTmp + gstAmt));
            } else{
                orders.setWithGst(false);
                orders.setGst(Utility.toDouble(0));
                orders.setGstAmt(Utility.toDouble(0));
                orders.setSubTotal(Utility.toDouble(subTotalTmp));
                orders.setTotal(Utility.toDouble(subTotalTmp));
            }
            ordList.add(orders);
            counter++;
        }

        return ordList;
    }

    public ResponseEntity<?> getOdcPdf(final HttpServletRequest httpRequest, final Long id) {
        log.info("getOdcPdf() id : {}", id);
        final Orders order = lookupById(Orders.class, id);
        this.validator.validateIsNotNull(ErrorConstants.DOES_NOT_EXIST, order, "Order Delivery Challan");
        final InputStreamResource inputStreamResource = getFile(this.pdfUtils.getDcPdf(getUser(httpRequest), order));
        return new ResponseEntity<>(inputStreamResource, getHeader(order.getSid() + ".pdf", "application/pdf"),
            HttpStatus.OK);
    }

    @Override
    public List<Item> getItemsByOrders(final Long id) {
        final Orders order = this.persistanceDelegatorDao.read(Orders.class, id);
        this.validator.validateIsNotNull(ErrorConstants.DOES_NOT_EXIST, order, "Order");
        if(order.isDelivered()) throw new ScmsException(ErrorConstants.ALREADY_PLACED, "Order");
        return order.getItems();
    }

    @Override
    public ResponseEntity<?> getPlodPdf(final HttpServletRequest httpRequest, final Long id) {
        log.info("getPlodPdf() id : {}", id);
        final Orders order = lookupById(Orders.class, id);
        this.validator.validateIsNotNull(ErrorConstants.DOES_NOT_EXIST, order, "Place Order");
        final InputStreamResource inputStreamResource = getFile(this.pdfUtils.getOrderPdf(getUser(httpRequest), order));
        return new ResponseEntity<>(inputStreamResource, getHeader(order.getSid() + ".pdf", "application/pdf"),
            HttpStatus.OK);
    }

    @Override
    public List<KeyValueClass> getMaterialWiseCount(final String paramJson) {
        FilterDTO filter = Utility.getEntity(paramJson, FilterDTO.class);

        final List<Object> list = new ArrayList<>();
        list.add(filter.getStartDateTime());
        list.add(filter.getEndDateTime());

        String query = "select m.name as column1, TRUNCATE(sum(oo.qtyBrass), 2) as column2 from (select itm.* from Orders od inner join Item itm on od.id = itm.orders where (od.orderDate between '%s' and '%s')";
        StringBuilder sb = new StringBuilder(query);

        if(StringUtils.isNotEmpty(filter.getCustomer())){
            sb.append(" and od.customer = '%s'");
            list.add(filter.getCustomer());
        }
        if(StringUtils.isNotEmpty(filter.getSite())){
            sb.append(" and od.site = '%s'");
            list.add(filter.getSite());
        }

        sb.append(") oo inner join Material m on oo.material = m.id where m.isActive is true GROUP BY oo.material");
        log.debug("query : " + sb.toString());

        return persistanceDelegatorDao.getNativeEntityList(sb.toString(), KeyValueClass.class,
                list.toArray());
    }

    @Override
    public List<Orders> getReportByOdList(final String json) {
        log.info("getReportByOdList() : {}", json);
        final Map<String, String> map = Utility.getMap(json);
        final List<Object> list = new ArrayList<>();
        final Object startDate = map.get("startDate");
        final Object endDate = map.get("endDate");
        final StringBuilder sb = new StringBuilder("from Orders od where od.orderDate between '%s' and '%s'");
        list.add(startDate);
        list.add(endDate);
        if(StringUtils.isNotEmpty(map.get("customer"))){
            sb.append(" and customer = '%s'");
            list.add(map.get("customer"));
        }
        if(StringUtils.isNotEmpty(map.get("isPending"))){
            sb.append(" and isPending = '%s'");
            list.add(map.get("isPending"));
        }
        if(StringUtils.isNotEmpty(map.get("site"))){
            sb.append(" and site = '%s'");
            list.add(map.get("site"));
        }
        return this.persistanceDelegatorDao.getEntityList(sb.toString(), Orders.class, list.toArray());
    }

    @Override
    public User getUser(final HttpServletRequest httpRequest) {
        log.info("To get logged user from httpRequest : {}", httpRequest);
        final UserDto userDto = (UserDto) httpRequest.getSession()
            .getAttribute("userDto");
        return lookupById(User.class, userDto.getId());
    }

    @Override
    public <T> T lookupById(final Class<T> typeKey, final Long id) {
    	T entity = this.persistanceDelegatorDao.read(typeKey, id);
    	validator.validateIsNotNull(ErrorConstants.DOES_NOT_EXIST, entity, typeKey.getSimpleName());
        return entity;
    }

    @Override
    public <T> T findEntityById(final Class<T> typeKey, final Long id) {
        return this.persistanceDelegatorDao.findEntityById(typeKey, id);
    }

    @Override
    public <T> T lookupByName(final Class<T> typeKey, final String name) {
        return this.persistanceDelegatorDao.findEntityByName(typeKey, name);
    }

    @Override
    public List<Item> ordersByDate(final String startDate, final String endDate) {
        return this.itemRepository.findAllByCreatedDateBetween(Utility.getDate("yyyy-MM-dd HH:mm:ss", startDate),
            Utility.getDate("yyyy-MM-dd HH:mm:ss", endDate));
    }

    @Override
    public void test(final HttpServletRequest httpRequest, final Orders order) {
        this.pdfUtils.getDcPdf(getUser(httpRequest), order);
    }

    @Override
    public Double getTotalSalesAmt(String paramJson) {
        log.info("getTotalSalesAmt : {}", paramJson);
        return getFilteredResult(Double.class, paramJson, QueryConstants.TOTAL_SALES_AMOUNT);
    }

    @Override
    public BigInteger getTotalOrders(String paramJson) {
        log.info("getTotalOrders : {}", paramJson);
        return getFilteredResult(BigInteger.class, paramJson, QueryConstants.TOTAL_ORDERS_COUNT);
    }

    @Override
    public Double getTotalPendingAmt(String paramJson) {
        log.info("getTotalPendingAmt : {}", paramJson);
        return getFilteredResult(Double.class, paramJson, QueryConstants.TOTAL_PENDING_AMOUNT);

    }

    @Override
    public Double getTotalReceivedAmt(final String paramJson) {
        log.info("getTotalReceivedAmt : {}", paramJson);
        return getFilteredResult(Double.class, paramJson, QueryConstants.TOTAL_RECEIVED_AMOUNT);
    }

    @Override
    public BigInteger getTotalDeliveryChallans(String paramJson) {
        log.info("getTotalDeliveryChallans : {}", paramJson);
        return getFilteredResult(BigInteger.class, paramJson, QueryConstants.TOTAL_DC_COUNT);
    }

    @Override
    public Double getTotalDiscountAmt(String paramJson) {
        log.info("getTotalDiscountAmt : {}", paramJson);
        return getFilteredResult(Double.class, paramJson, QueryConstants.TOTAL_DISCOUNT_AMOUNT);
    }

    /**
     * @param <T>
     * @param paramJson
     * @param queryConstant
     * @return
     */
    public <T> T getFilteredResult(final Class<T> clazz, final String paramJson, final QueryConstants queryConstant) {
        FilterDTO filter = Utility.getEntity(paramJson, FilterDTO.class);

        final List<Object> list = new ArrayList<>();
        list.add(filter.getStartDateTime());
        list.add(filter.getEndDateTime());

        StringBuilder query = new StringBuilder();

        switch(queryConstant) {
            case TOTAL_SALES_AMOUNT:
                query.append(QueryConstants.TOTAL_SALES_AMOUNT.getQuery());
                break;
            case TOTAL_ORDERS_COUNT:
                query.append(QueryConstants.TOTAL_ORDERS_COUNT.getQuery());
                break;
            case TOTAL_PENDING_AMOUNT:
                query.append(QueryConstants.TOTAL_PENDING_AMOUNT.getQuery());
                break;
            case TOTAL_RECEIVED_AMOUNT:
                query.append(QueryConstants.TOTAL_RECEIVED_AMOUNT.getQuery());
                break;
            case TOTAL_DC_COUNT:
                query.append(QueryConstants.TOTAL_DC_COUNT.getQuery());
                break;
            case TOTAL_DISCOUNT_AMOUNT:
                query.append(QueryConstants.TOTAL_DISCOUNT_AMOUNT.getQuery());
                break;

            default:
                log.error("No matching query found !!!");
        }

        if(StringUtils.isNotEmpty(filter.getCustomer())){
            query.append(" and tbl.customer = '%s'");
            list.add(filter.getCustomer());
        }

        if(StringUtils.isNotEmpty(filter.getSite())){
            query.append(" and tbl.site = '%s'");
            list.add(filter.getSite());
        }
        return this.persistanceDelegatorDao.getNativeSingleColumnValue(query.toString(), clazz, list.toArray());
    }

    @Override
    public DataTablesOutput<Orders> getFilterOrders(String paramJson) {
    	log.info("getFilterOrders : {}", paramJson);
        FilterDTO filter = Utility.getEntity(paramJson, FilterDTO.class);

        final List<Object> list = new ArrayList<>();
        list.add(filter.getStartDateTime());
        list.add(filter.getEndDateTime());

        long count = ordersRepository.count();
        StringBuilder query = new StringBuilder("from Orders tbl where tbl.orderDate between '%s' and '%s'");

        if(StringUtils.isNotEmpty(filter.getCustomer())){
            query.append(" and tbl.customer = '%s'");
            list.add(filter.getCustomer());
        }

        if(StringUtils.isNotEmpty(filter.getSite())){
            query.append(" and tbl.site = '%s'");
            list.add(filter.getSite());
        }

        if(StringUtils.isNotEmpty(filter.getPending())){
            query.append(" and tbl.isPending is %s");
            list.add(filter.getPending());
        }

        // Sorting logic
        Order order = filter.getOrder();
        if(order != null){
            switch(order.getColumn()) {
                case 0:
                    query.append(" order by orderDate %s");
                    break;
                case 2:
                    query.append(" order by id %s");
                    break;
                default:
                    log.debug("sorting has been not enabled !!!");
            }
            list.add(order.getDir());
        }

        List<Orders> oList = this.persistanceDelegatorDao.getLimitedEntityList(query.toString(), Orders.class,
            filter.getStart(), filter.getLength(), list.toArray());

        DataTablesOutput<Orders> dtOutput = new DataTablesOutput<>();
        dtOutput.setError(null);
        dtOutput.setRecordsTotal(count);
        dtOutput.setDraw(filter.getDraw() + 1);
        dtOutput.setRecordsFiltered(oList.size());
        dtOutput.setData(oList);

        return dtOutput;
    }
    
    @Override
    public DataTablesOutput<DeliveryChallan> getFilterDeliveryChallan(String paramJson) {
    	log.info("getFilterDeliveryChallan : {}", paramJson);
        FilterDTO filter = Utility.getEntity(paramJson, FilterDTO.class);

        final List<Object> list = new ArrayList<>();
        list.add(filter.getStartDateTime());
        list.add(filter.getEndDateTime());

        long count = deliveryChallanRepository.count();
        StringBuilder query = new StringBuilder("from DeliveryChallan tbl where tbl.orderDate between '%s' and '%s'");

        if(StringUtils.isNotEmpty(filter.getCustomer())){
            query.append(" and tbl.customer = '%s'");
            list.add(filter.getCustomer());
        }

        if(StringUtils.isNotEmpty(filter.getSite())){
            query.append(" and tbl.site = '%s'");
            list.add(filter.getSite());
        }

        if(StringUtils.isNotEmpty(filter.getIsBillable())){
            query.append(" and tbl.isBillable is %s");
            list.add(filter.getIsBillable());
        }

        // Sorting logic
        Order order = filter.getOrder();
        if(order != null){
            switch(order.getColumn()) {
                case 0:
                    query.append(" order by orderDate %s");
                    break;
                case 2:
                    query.append(" order by id %s");
                    break;
                default:
                    log.debug("sorting has been not enabled !!!");
            }
            list.add(order.getDir());
        }

        List<DeliveryChallan> dcList = this.persistanceDelegatorDao.getLimitedEntityList(query.toString(), DeliveryChallan.class,
            filter.getStart(), filter.getLength(), list.toArray());

        DataTablesOutput<DeliveryChallan> dtOutput = new DataTablesOutput<>();
        dtOutput.setError(null);
        dtOutput.setRecordsTotal(count);
        dtOutput.setDraw(filter.getDraw() + 1);
        dtOutput.setRecordsFiltered(dcList.size());
        dtOutput.setData(dcList);

        return dtOutput;
    }

	@Override
	public List<Orders> getOrders(String paramJson) {
		log.info("getOrders : {}", paramJson);
        FilterDTO filter = Utility.getEntity(paramJson, FilterDTO.class);

        final List<Object> list = new ArrayList<>();
        list.add(filter.getStartDateTime());
        list.add(filter.getEndDateTime());

        StringBuilder query = new StringBuilder("from Orders tbl where tbl.orderDate between '%s' and '%s'");

        if(StringUtils.isNotEmpty(filter.getCustomer())){
            query.append(" and tbl.customer = '%s'");
            list.add(filter.getCustomer());
        }

        if(StringUtils.isNotEmpty(filter.getSite())){
            query.append(" and tbl.site = '%s'");
            list.add(filter.getSite());
        }

        if(StringUtils.isNotEmpty(filter.getPending())){
            query.append(" and tbl.isPending is %s");
            list.add(filter.getPending());
        }

        return this.persistanceDelegatorDao.getEntityList(query.toString(), Orders.class, list.toArray());
    }
}
