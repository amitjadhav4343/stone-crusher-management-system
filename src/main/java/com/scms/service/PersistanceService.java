package com.scms.service;

import javax.servlet.http.HttpServletRequest;

import com.scms.model.Address;
import com.scms.model.Customer;
import com.scms.model.DeliveryChallan;
import com.scms.model.Employee;
import com.scms.model.Gst;
import com.scms.model.Material;
import com.scms.model.Orders;
import com.scms.model.ScmsJob;
import com.scms.model.Site;
import com.scms.model.User;
import com.scms.model.Vehicle;

public interface PersistanceService {
    Address saveAddress(HttpServletRequest paramHttpServletRequest, String paramString);

    Customer saveCustomer(String paramString);

    DeliveryChallan saveDeliveryChallan(HttpServletRequest paramHttpServletRequest, String paramString);

    Gst saveGst(String paramString);

    Material saveMaterial(String paramString);

    Orders saveOrderViaDC(HttpServletRequest paramHttpServletRequest, String paramString);

    Orders saveOrder(HttpServletRequest paramHttpServletRequest, String paramString);

    Site saveSite(HttpServletRequest paramHttpServletRequest, String paramString);

    User saveUser(String paramString);

    Vehicle saveVehicle(String paramString);

    Employee saveEmployee(HttpServletRequest paramHttpServletRequest, String paramJson);

	ScmsJob saveSchedulerOrder(String paramJson);
}
