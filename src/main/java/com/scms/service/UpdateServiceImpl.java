package com.scms.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.scms.constants.Constants;
import com.scms.dao.PersistanceDelegatorDao;
import com.scms.enums.ActiveStatus;
import com.scms.enums.ErrorConstants;
import com.scms.exception.ScmsException;
import com.scms.model.Address;
import com.scms.model.Bank;
import com.scms.model.Company;
import com.scms.model.Customer;
import com.scms.model.DeliveryChallan;
import com.scms.model.Employee;
import com.scms.model.Gst;
import com.scms.model.Material;
import com.scms.model.ScmsJob;
import com.scms.model.ScmsJobParms;
import com.scms.model.Site;
import com.scms.model.TransactionHistory;
import com.scms.model.User;
import com.scms.model.Vehicle;
import com.scms.util.Utility;
import com.scms.validator.Validator;

@Service
@Transactional
public class UpdateServiceImpl implements UpdateService {
	private static final Logger log = LoggerFactory.getLogger(UpdateServiceImpl.class);

	@Autowired
	private Validator validator;

	@Autowired
	private LookupService lookupService;

	@Autowired
	private PersistanceDelegatorDao persistanceDelegatorDao;

	@Override
	public Double modifyAdvance(final String request) {
		final TransactionHistory th = Utility.getEntity(request, TransactionHistory.class);
		final Customer customer = persistanceDelegatorDao.read(Customer.class, th.getCustomerId());
		final double total = th.getAmount() + customer.getAdvance();
		customer.setAdvance(Utility.toDouble(total));
		th.setClosingBalance(Utility.toDouble(total));
		persistanceDelegatorDao.save(th);
		return customer.getAdvance();
	}

	@Override
	public boolean toggleStatus(final Long id, final ActiveStatus status, final Class<?> clazz) {
		final int result = persistanceDelegatorDao.toggleStatus(id, status, clazz);
		log.info("toggleStatus result : {}", Integer.valueOf(result));
		return result != 0;
	}

	@Override
	public <T> T update(final T paramT) {
		return persistanceDelegatorDao.update(paramT);
	}

	@Override
	public Address updateAddress(final HttpServletRequest request, final String json) {
		log.info("updateAddress : {}", json);
		final Address add = Utility.getEntity(json, Address.class);
		final Address address = persistanceDelegatorDao.read(Address.class, add.getId());
		address.setStreet(add.getStreet());
		address.setCity(add.getCity());
		address.setPincode(add.getPincode());
		address.setState(add.getState());
		address.setType(add.getType());
		address.setActive(add.isActive());
		return address;
	}

	@Override
	public Bank updateBank(final HttpServletRequest request, final String json) {
		log.info("updateBank : {}", json);
		final Bank bnk = Utility.getEntity(json, Bank.class);
		final Bank bank = persistanceDelegatorDao.read(Bank.class, bnk.getId());
		bank.setName(bnk.getName());
		bank.setBranch(bnk.getBranch());
		bank.setAccName(bnk.getAccName());
		bank.setAccNo(bnk.getAccNo());
		bank.setIfsc(bnk.getIfsc());
		bank.setMicr(bnk.getMicr());
		return bank;
	}

	@Override
	public Company updateCompany(final HttpServletRequest request, final String json) {
		log.info("updateCompany : {}", json);
		final Company comp = Utility.getEntity(json, Company.class);
		final Company company = persistanceDelegatorDao.read(Company.class, comp.getId());
		company.setRegNo(comp.getRegNo());
		company.setRegDate(comp.getRegDate());
		company.setName(comp.getName());
		company.setPhone(comp.getPhone());
		company.setEmail(comp.getEmail());
		company.setFax(comp.getFax());
		company.setWebsite(comp.getWebsite());
		company.setGstNo(comp.getGstNo());
		company.setPan(comp.getPan());
		return company;
	}

	@Override
	public Customer updateCustomer(final String request) {
		final Customer c = Utility.getEntity(request, Customer.class);
		final Customer customer = persistanceDelegatorDao.read(Customer.class, c.getId());
		customer.setName(c.getName());
		customer.setPhone(c.getPhone());
		customer.setEmail(c.getEmail());
		customer.setFirm(c.getFirm());
		customer.setGstNo(c.getGstNo());
		customer.setCustomerType(c.getCustomerType());
		customer.setIdentity(c.getIdentity());
		customer.setAdvance(c.getAdvance());
		customer.setActive(c.isActive());
		return customer;
	}

	@Override
	public DeliveryChallan updateDeliveryChallan(final String json) {
		final DeliveryChallan dc = Utility.getEntity(json, DeliveryChallan.class);
		final DeliveryChallan deliveryChallan = persistanceDelegatorDao.read(DeliveryChallan.class, dc.getId());
		deliveryChallan.setQtyBrass(dc.getQtyBrass());
		deliveryChallan.setQtyKg(dc.getQtyKg());
		return deliveryChallan;
	}

	@Override
	public boolean updateEntityByQuery(final String queryString, final Object... bindVariables) {
		final int result = persistanceDelegatorDao.updateEntityByQuery(queryString, bindVariables);
		log.debug("updateEntityByQuery result : {}", Integer.valueOf(result));
		return result != 0;
	}

	@Override
	public Gst updateGst(final String request) {
		final Gst g = Utility.getEntity(request, Gst.class);
		final Gst gst = persistanceDelegatorDao.read(Gst.class, g.getId());
		gst.setActive(g.isActive());

		// if gst status is false then
		if (!gst.isActive()) {
			String query = "from Material m where m.gst = '%s'";
			List<Material> mList = persistanceDelegatorDao.getEntityList(query, Material.class, gst.getId());
			if (CollectionUtils.isNotEmpty(mList)) {
				Gst zeroGst = persistanceDelegatorDao.getEntityByColVal("VALUE", "0", Gst.class);
				mList.stream().forEach(m -> m.setGst(zeroGst));
			}
		}
		return gst;
	}

	@Override
	public Material updateMaterial(final String request) {
		log.info("updateMaterial : {}", request);
		Material m = Utility.getEntity(request, Material.class);
		final Material material = persistanceDelegatorDao.read(Material.class, m.getId());
		material.setName(m.getName());
		material.setRate(m.getRate());
		material.setHsn(m.getHsn());
		material.setActive(m.isActive());
		material.setGst(persistanceDelegatorDao.read(Gst.class, m.getGstId()));
		return material;
	}

	@Override
	public Material updateMaterialRate(final String paramJson) {
		log.info("updateMaterialRate : {}", paramJson);
		Material m = Utility.getEntity(paramJson, Material.class);
		final Material material = persistanceDelegatorDao.read(Material.class, m.getId());
		material.setRate(m.getRate());
		return material;
	}

	@Override
	public Site updateSite(final HttpServletRequest request, final String json) {
		log.info("updateSite : {}", json);
		final Site s = Utility.getEntity(json, Site.class);
		final Site site = persistanceDelegatorDao.read(Site.class, s.getId());
		site.setName(s.getName());
		site.setRegNo(s.getRegNo());
		site.setRegDate(s.getRegDate());
		site.setRegUpto(s.getRegUpto());
		site.setPhone(s.getPhone());
		site.setEmail(s.getEmail());
		return site;
	}

	@Override
	public User updateUser(final String json) {
		log.info("updateUser : {}", json);
		final User usr = Utility.getEntity(json, User.class);
		final User user = persistanceDelegatorDao.read(User.class, usr.getId());
		user.setActive(usr.isActive());
		user.setRole(usr.getRole());
		return user;
	}

	@Override
	public Vehicle updateVehicle(final String request) {
		log.info("updateVehicle : {}", request);
		final Vehicle v = Utility.getEntity(request, Vehicle.class);
		final Vehicle vehicle = persistanceDelegatorDao.read(Vehicle.class, v.getId());
		vehicle.setNumber(v.getNumber());
		vehicle.setOwnerName(v.getOwnerName());
		vehicle.setModel(v.getModel());
		vehicle.setRegNo(v.getRegNo());
		vehicle.setRegDate(v.getRegDate());
		vehicle.setRegUpto(v.getRegUpto());
		vehicle.setActive(v.isActive());
		return vehicle;
	}

	@Override
	public Employee updateEmployee(String request) {
		log.info("updateEmployee : {}", request);
		final Employee emp = Utility.getEntity(request, Employee.class);
		final Employee employee = persistanceDelegatorDao.read(Employee.class, emp.getId());

		employee.setName(emp.getName());
		employee.setPhone(emp.getPhone());
		employee.setEmail(emp.getEmail());
		employee.setDesignation(emp.getDesignation());
		employee.setResignedDate(emp.getResignedDate());
		employee.setEndDate(emp.getEndDate());
		employee.setIdentity(emp.getIdentity());
		employee.setSalary(emp.getSalary());
		employee.setActive(emp.isActive());

		return employee;
	}

	@Override
	public ScmsJob updateSchedulerOrder(String paramJson) {
		log.info("updateSchedulerOrder : {}", paramJson);
		ScmsJob sJob = Utility.getEntity(paramJson, ScmsJob.class);

		ScmsJob scmsJob = lookupService.lookupById(ScmsJob.class, sJob.getId());
		
		/* Validate Job is already exists or not */
		if(sJob.getJobName().equalsIgnoreCase(scmsJob.getJobName()) && sJob.getId() != scmsJob.getId()) {
			throw new ScmsException(ErrorConstants.ALREADY_EXISTS, "Job Name");
		}

		/* update job attributes */
		scmsJob.setJobName(sJob.getJobName());
		scmsJob.setActive(sJob.isActive());
		scmsJob.setStartDate(sJob.getStartDate());
		scmsJob.setPeriod(sJob.getPeriod());
		scmsJob.setCronDesc(Utility.getCronDesc(Utility.getLocalDateTime(Constants.YYYY_MM_DD_HH_MM_SS, scmsJob.getStartDate()), scmsJob.getPeriod()));
		
		List<ScmsJobParms> list = scmsJob.getJobParms();
		
		/* If found the key then update the value of it. */
		sJob.getJobParms().forEach(nwParm -> 
			list.forEach(exParm -> {
				if(exParm.getParmkey().equals(nwParm.getParmkey())) {
					exParm.setParmValue(nwParm.getParmValue());
				}
			})
		);
		
		/* If key not found then add into existing list. */
		sJob.getJobParms().forEach(nwPram -> {
			if(!list.contains(nwPram)) {
				nwPram.setScmsJob(scmsJob);
				list.add(nwPram);
			}
		});

		return scmsJob;
	}
}
