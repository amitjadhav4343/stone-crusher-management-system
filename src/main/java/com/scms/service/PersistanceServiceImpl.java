package com.scms.service;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.scms.constants.Constants;
import com.scms.dao.PersistanceDelegatorDao;
import com.scms.enums.AddressType;
import com.scms.enums.DateType;
import com.scms.enums.ErrorConstants;
import com.scms.enums.JobParmKey;
import com.scms.enums.OrderType;
import com.scms.enums.PaymentMode;
import com.scms.enums.TransactionType;
import com.scms.enums.YesNo;
import com.scms.exception.ScmsException;
import com.scms.mail.EmailService;
import com.scms.mail.MailUtility;
import com.scms.model.Address;
import com.scms.model.Company;
import com.scms.model.Customer;
import com.scms.model.DeliveryChallan;
import com.scms.model.Employee;
import com.scms.model.Gst;
import com.scms.model.Item;
import com.scms.model.Material;
import com.scms.model.Orders;
import com.scms.model.ScmsJob;
import com.scms.model.Site;
import com.scms.model.TransactionHistory;
import com.scms.model.User;
import com.scms.model.Vehicle;
import com.scms.model.dto.UserDto;
import com.scms.pdf.PdfUtils;
import com.scms.quartz.service.SchedulerService;
import com.scms.sms.SmsService;
import com.scms.util.StringUtils;
import com.scms.util.Utility;
import com.scms.validator.Validator;

@Service
@Transactional
public class PersistanceServiceImpl implements PersistanceService {
    private static final Logger log = LoggerFactory.getLogger(PersistanceServiceImpl.class);

    @Autowired
    private PdfUtils pdfUtils;

    @Autowired
    private Validator validator;

    @Autowired
    private SmsService smsService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private LookupService lookupService;
    
    @Autowired
    private SchedulerService schedulerService;

    @Autowired
    private PersistanceDelegatorDao persistanceDelegatorDao;

    @Override
    public Address saveAddress(final HttpServletRequest request, final String json) {
        Site site;
        Company company;
        Customer customer;
        log.info("saveAddress : {}", json);
        final Address address = Utility.getEntity(json, Address.class);
        switch(address.getAddrAgainstClass()) {
            case "Site":
                site = persistanceDelegatorDao.read(Site.class, address.getAddrAgainstId());
                site.setAddress(address);
                request.getSession()
                    .setAttribute("company", site.getCompany());
                break;
            case "Company":
                company = persistanceDelegatorDao.read(Company.class, address.getAddrAgainstId());
                company.setAddress(address);
                request.getSession()
                    .setAttribute("company", company);
                break;
            case "Customer":
                customer = persistanceDelegatorDao.read(Customer.class, address.getAddrAgainstId());
                customer.getAddress()
                    .add(address);
                break;
            default:log.warn("AddrAgainstClass is different :" + address.getAddrAgainstClass());
        }
        address.setId(null);
        return persistanceDelegatorDao.save(address);
    }

    @Override
    public Customer saveCustomer(final String request) {
        log.info("saveCustomer : {}", request);
        Customer customer = Utility.getEntity(request, Customer.class);
        if(persistanceDelegatorDao.getEntityByColVal("PHONE", customer.getPhone(), Customer.class) == null){
            customer = persistanceDelegatorDao.save(customer);
            persistanceDelegatorDao.save(new TransactionHistory(customer, customer.getAdvance(), customer.getAdvance(),
                TransactionType.Cr, "CUSTOMER CREATED"));
            return customer;
        }
        throw new ScmsException(ErrorConstants.ERROR_CODE_409);
    }

    @Override
    public DeliveryChallan saveDeliveryChallan(final HttpServletRequest httpRequest, final String json) {
        log.info("saveDeliveryChallan : {}", json);
        final Map<String, String> map = Utility.getMap(json);

        final DeliveryChallan dc = new DeliveryChallan();
        final LocalDateTime orderDate = Utility.getLocalDateTime("yyyy-MM-dd HH:mm:ss", map.get("orderDate"));
        dc.setOrderDate(orderDate);
        dc.setMaterial(persistanceDelegatorDao.read(Material.class, Utility.toLong(map.get("material"), "Material")));
        dc.setCustomer(persistanceDelegatorDao.read(Customer.class, Utility.toLong(map.get("customer"), "Customer")));
        dc.setAddress(persistanceDelegatorDao.read(Address.class, Utility.toLong(map.get("address"), "Address")));
        dc.setVehicle(persistanceDelegatorDao.read(Vehicle.class, Utility.toLong(map.get("vehicle"), "Vehicle")));

        dc.setQtyKg(Utility.toDouble(map.get("qtyKg")));
        dc.setQtyBrass(Utility.toDouble(map.get("qtyBrass")));
        dc.setSite(persistanceDelegatorDao.read(Site.class, Utility.toLong(map.get("site"), "Site")));
        dc.setRemarks(map.get("remarks"));

        dc.setActive(true);
        dc.setSentMail(false);
        dc.setSentSms(false);
        dc.setBillable(true);

        final DeliveryChallan deliveryChallan = persistanceDelegatorDao.save(dc);
        deliveryChallan.setSid(Constants.PREFIX_DC + StringUtils.appendZeros(deliveryChallan.getId(), 8));

        final String mailSubject = MessageFormat.format(
            "Your Delivery Challan {0} for {1} has been successfully placed", deliveryChallan.getSid(),
            deliveryChallan.getMaterial()
                .getName());

        /* Send SMS */
        // this.smsService.send(deliveryChallan, customer.getPhone(), mailSubject);

        emailService.sendMail(deliveryChallan, deliveryChallan.getCustomer()
            .getEmail(), mailSubject, MailUtility.getBody(deliveryChallan));

        return deliveryChallan;
    }

    @Override
    public Gst saveGst(final String request) {
        log.info("saveGst : {}", request);
        final Gst gst = Utility.getEntity(request, Gst.class);
        validator.validateIsNull(ErrorConstants.ALREADY_EXISTS,
            lookupService.getEntityByColumnNameAndValue("VALUE", String.valueOf(gst.getValue()), Gst.class), "Gst");
        return persistanceDelegatorDao.save(gst);
    }

    @Override
    public Material saveMaterial(final String request) {
        log.info("saveMaterial : {}", request);
        Material material = Utility.getEntity(request, Material.class);
        material.setGst(persistanceDelegatorDao.read(Gst.class, material.getGstId()));
        return persistanceDelegatorDao.save(material);
    }

    @Override
    public Orders saveOrderViaDC(final HttpServletRequest httpRequest, final String json) {
        log.info("Save order using DC : {}", json);
        final List<Item> itemList = new ArrayList<>();
        final Orders order = new Orders();
        /* use to store newly saved object */
        Orders nOrder = null;

        final Map<String, String> map = Utility.getMap(json);
        final YesNo gst = YesNo.valueOf(map.get("gst"));
        final List<Long> dcIds = Arrays.<String>stream(map.get("dcs")
            .split(","))
            .map(Long::valueOf)
            .collect(Collectors.toList());

        final LocalDateTime orderDate = Utility.getLocalDateTime("yyyy-MM-dd HH:mm:ss", map.get("orderDate"));
        order.setOrderDate(orderDate);
        order.setFinancialYear(Utility.getFinancialYear(orderDate));

        final Customer customer = persistanceDelegatorDao.read(Customer.class,
            Utility.toLong(map.get("customer"), "Customer"));
        order.setCustomer(customer);

        final Address billAddr = customer.getAddress()
            .stream()
            .filter(add -> (add.getType()
                .compareTo(AddressType.BILLING) == 0))
            .findFirst()
            .orElse(null);
        order.setAddress(billAddr);

        order.setPayMode(PaymentMode.valueOf(map.get("payMode")));
        order.setDateType(DateType.REGULAR);
        order.setOrderType(OrderType.ODC);
        order.setTotal(Utility.toDouble(map.get("total")));
        order.setSite(persistanceDelegatorDao.read(Site.class, Utility.toLong(map.get("site"), "Site")));
        order.setReceivedAmt(Utility.toDouble(map.get("receivedAmt")));
        order.setPending(Boolean.parseBoolean(map.get("isPending")));
        order.setPendingAmt(Utility.toDouble(map.get("pendingAmt")));
        order.setDiscount(Utility.toDouble(order.getReceivedAmt() < order.getTotal() && order.isPending()
            ? 0.0D
            : order.getTotal() - order.getReceivedAmt()));
        order.setRemarks(map.get("remarks"));

        order.setActive(true);
        order.setSentMail(false);
        order.setSentSms(false);

        lookupService.getOdcCalculation(dcIds, gst)
            .stream()
            .forEach(item -> {
                if(item.isWithGst()){
                    order.setWithGst(true);
                }
                item.setOrders(order);
                item.setId(null);
                item.setActive(true);
                final Item nItem = persistanceDelegatorDao.save(item);

                nItem.getDeliveryChallans()
                    .stream()
                    .forEach(dc -> {
                        dc.setItem(nItem);
                        dc.setBillable(false);
                    });
                itemList.add(nItem);
            });

        order.setItems(itemList);
        nOrder = persistanceDelegatorDao.save(order);
        nOrder.setSid(Constants.PREFIX_OD + StringUtils.appendZeros(nOrder.getId(), 8));

        if(PaymentMode.ADVANCE.compareTo(nOrder.getPayMode()) == 0){
            final TransactionHistory trans = new TransactionHistory();
            trans.setCustomer(customer);
            trans.setAmount(nOrder.getReceivedAmt());
            trans.setClosingBalance(customer.getAdvance() - nOrder.getReceivedAmt());
            trans.setType(TransactionType.Dr);
            trans.setRemarks("Debited for Order : " + nOrder.getSid());

            persistanceDelegatorDao.save(trans);

            customer.setAdvance(customer.getAdvance() - nOrder.getReceivedAmt());
        }

        final String filePath = pdfUtils.getOrderPdf(lookupService.getUser(httpRequest), nOrder);

        final String mailSubject = MessageFormat.format("Your Order {0} has been successfully placed", nOrder.getSid());
        emailService.sendWithAttachment(nOrder, nOrder.getCustomer()
            .getEmail(), mailSubject, MailUtility.getBody(nOrder), filePath);
        return nOrder;
    }

    public Item saveItem(final String request, final Orders order) {
        log.info("Save Item : {}", request);
        final Map<String, String> map = Utility.getMap(request);
        final Item item = new Item();
        item.setMaterial(persistanceDelegatorDao.read(Material.class, Utility.toLong(map.get("material"), "material")));
        item.setQtyKg(Utility.toDouble(map.get("qtyKg")));
        item.setQtyBrass(Utility.toDouble(map.get("qtyBrass")));
        item.setRate(Utility.toDouble(map.get("rate")));
        log.debug(map.get("withGst"));
        item.setWithGst(Boolean.parseBoolean(map.get("withGst")));
        item.setGst(Utility.toDouble(map.get("gst")));
        item.setGstAmt(Utility.toDouble(map.get("gstAmt")));
        item.setSubTotal(Utility.toDouble(map.get("subTotal")));
        item.setTotal(Utility.toDouble(item.getSubTotal() + item.getGstAmt()));
        item.setActive(true);
        if(item.isWithGst()){
            order.setWithGst(true);
        }
        final List<DeliveryChallan> dcList = new ArrayList<>();
        final String [] vehicleArr = map.get("vehicle")
            .split(",");
        for(final String vehicleId : vehicleArr){
            final DeliveryChallan dc = new DeliveryChallan();
            dc.setOrderDate(order.getOrderDate());
            dc.setCustomer(order.getCustomer());
            dc.setAddress(order.getAddress());
            dc.setVehicle(persistanceDelegatorDao.read(Vehicle.class, Utility.toLong(vehicleId, "Vehicle")));
            dc.setSite(order.getSite());
            dc.setMaterial(item.getMaterial());
            if(vehicleArr.length == 1){
                dc.setQtyKg(item.getQtyKg());
                dc.setQtyBrass(item.getQtyBrass());
            } else{
                dc.setQtyKg(Utility.toDouble(0));
                dc.setQtyBrass(Utility.toDouble(0));
            }
            dc.setSentMail(false);
            dc.setSentSms(false);
            dc.setActive(true);
            dc.setBillable(false);
            dc.setItem(item);
            dcList.add(dc);
        }
        item.setDeliveryChallans(dcList);
        return persistanceDelegatorDao.save(item);
    }

    @Override
    public Orders saveOrder(final HttpServletRequest httpRequest, final String request) {
        log.info("saveOrder : {}", request);
        final JsonNode oNode = Utility.getEntity(request, JsonNode.class);
        final LocalDateTime orderDate = Utility.getLocalDateTime("yyyy-MM-dd HH:mm:ss", oNode.get("orderDate")
            .textValue());
        final Orders order = new Orders();
        Orders nOrder = null;

        final Customer customer = persistanceDelegatorDao.read(Customer.class, Utility.toLong(oNode.get("customer")
            .asText(), "Customer"));
        order.setOrderDate(orderDate);
        order.setFinancialYear(Utility.getFinancialYear(orderDate));
        order.setCustomer(customer);
        order.setTotal(Utility.toDouble(oNode.get("total")
            .asText()));

        order.setPayMode(PaymentMode.valueOf(oNode.get("payMode")
            .textValue()));
        order.setSite(persistanceDelegatorDao.read(Site.class, Utility.toLong(oNode.get("site")
            .asText(), "Site")));
        order.setAddress(persistanceDelegatorDao.read(Address.class, Utility.toLong(oNode.get("address")
            .asText(), "Address")));
        order.setReceivedAmt(Utility.toDouble(oNode.get("receivedAmt")
            .asText()));
        order.setPending(oNode.get("isPending")
            .booleanValue());
        order.setPendingAmt(Utility.toDouble(oNode.get("pendingAmt")
            .asText()));
        order.setDiscount(Utility.toDouble(order.getReceivedAmt() < order.getTotal() && order.isPending()
            ? 0.0D
            : order.getTotal() - order.getReceivedAmt()));
        order.setRemarks(oNode.get("remarks")
            .textValue());

        /* Check order is backdated or regular ? */
        final Period period = Period.between(orderDate.toLocalDate(), LocalDate.now());
        order.setDateType(period.getDays() == 0 ? DateType.REGULAR : DateType.BACKDATED);

        order.setOrderType(OrderType.PO);
        final List<Item> itemList = new ArrayList<>();

        /* Save Orders */
        for(final JsonNode orderJson : oNode.get("items")){
            itemList.add(saveItem(orderJson.toString(), order));
        }
        log.info("itemList : {}", itemList.size());

        itemList.stream()
            .forEach(item -> {
                item.setOrders(order);
                item.getDeliveryChallans()
                    .stream()
                    .forEach(dc -> dc.setSid(Constants.PREFIX_DC + StringUtils.appendZeros(dc.getId(), 8)));
            });

        order.setItems(itemList);
        order.setActive(true);
        order.setSentMail(false);
        order.setSentSms(false);
        nOrder = persistanceDelegatorDao.save(order);
        nOrder.setSid(Constants.PREFIX_OD + StringUtils.appendZeros(nOrder.getId(), 8));

        if(PaymentMode.ADVANCE.compareTo(nOrder.getPayMode()) == 0){
            final TransactionHistory trans = new TransactionHistory();
            trans.setCustomer(customer);
            trans.setAmount(nOrder.getReceivedAmt());
            trans.setClosingBalance(customer.getAdvance() - nOrder.getReceivedAmt());
            trans.setType(TransactionType.Dr);
            trans.setRemarks("Debited for Order : " + nOrder.getSid());

            persistanceDelegatorDao.save(trans);

            customer.setAdvance(customer.getAdvance() - nOrder.getReceivedAmt());
        }

        /* Generate pdf */
        final String filePath = pdfUtils.getOrderPdf(lookupService.getUser(httpRequest), nOrder);

        /* Build Subject */
        final String mailSubject = MessageFormat.format("Your Order {0} has been successfully placed", nOrder.getSid());

        /* Send SMS */
        // this.smsService.send(nOrder, customer.getPhone(), mailSubject);

        /* Send Mail */
        emailService.sendWithAttachment(nOrder, customer.getEmail(), mailSubject, MailUtility.getBody(nOrder),
            filePath);
        return nOrder;
    }

    @Override
    public Site saveSite(final HttpServletRequest request, final String json) {
        log.info("SaveSite : {}", json);
        final Site site = Utility.getEntity(json, Site.class);
        final Company company = persistanceDelegatorDao.read(Company.class, site.getSiteAgainstId());
        site.setId(null);
        company.getSite()
            .add(site);
        site.setCompany(company);
        site.setActive(true);
        site.setIsPrimary(Boolean.valueOf(false));
        request.getSession()
            .setAttribute("company", company);
        return persistanceDelegatorDao.save(site);
    }

    @Override
    public User saveUser(final String json) {
        log.info("saveUser : {}", json);
        final User user = Utility.getEntity(json, User.class);
        String pass = Utility.generatePassword(8);
        user.setEmployee(lookupService.lookupById(Employee.class, user.getEmployeeId()));
        user.setPassword(new BCryptPasswordEncoder().encode(pass));
        User puser = persistanceDelegatorDao.save(user);
        puser.getEmployee().setUser(puser);
        
        /* Password will be sent via email & message */
        
        // String mailSubject = "Try to login with below password : \n" + pass;
        /* Send SMS */
        // this.smsService.send(nOrder, customer.getPhone(), mailSubject);
        
        return puser;
    }

    @Override
    public Vehicle saveVehicle(final String request) {
        log.info("saveVehicle : {}", request);
        final Vehicle vehicle = Utility.getEntity(request, Vehicle.class);
        validator.validateIsNull(ErrorConstants.ALREADY_EXISTS,
            lookupService.getEntityByColumnNameAndValue("NUMBER", vehicle.getNumber(), Vehicle.class), "Vehicle");
        return persistanceDelegatorDao.save(vehicle);
    }

    @Override
    public Employee saveEmployee(HttpServletRequest request, String paramJson) {
        Employee employee = Utility.getEntity(paramJson, Employee.class);
        employee.setInitials(Utility.getInitials(employee.getName()));
        employee.setAddress(Collections.emptyList());
        final UserDto userDto = (UserDto) request.getSession()
            .getAttribute("userDto");
        employee.setCompany(this.lookupService.lookupById(Company.class, userDto.getCompany()));
        return persistanceDelegatorDao.save(employee);
    }

	@Override
	public ScmsJob saveSchedulerOrder(String paramJson) {
		log.info("saveSchedulerOrder : {}", paramJson);
		ScmsJob scmsJob = Utility.getEntity(paramJson, ScmsJob.class);
		 
		 /*Validate Job is already exists or not*/
		 validator.validateIsNull(ErrorConstants.ALREADY_EXISTS,
		            lookupService.getEntityByColumnNameAndValue("JOBNAME", scmsJob.getJobName(), ScmsJob.class), "Job Name");

		 /*Set job attributes*/
		 scmsJob.setClassName("JobOrder");
		 scmsJob.setGroupName("Order Scheduler Group");
		 scmsJob.setTriggerName("Order Scheduler Trigger");
		 /* scmsJob.setCronDesc("0/10 * * * * ?"); */
		 scmsJob.setCronDesc(Utility.getCronDesc(Utility.getLocalDateTime(Constants.YYYY_MM_DD_HH_MM_SS, scmsJob.getStartDate()), scmsJob.getPeriod()));
		 
		 Customer customer = lookupService.findEntityById(Customer.class, scmsJob.getCustomerId());
		 scmsJob.getJobParms().forEach(jobParm -> {
			 if(JobParmKey.Recipients.compareTo(jobParm.getParmkey()) == 0 && StringUtils.isNotEmpty(jobParm.getParmValue())) {
				 jobParm.setParmValue(customer.getEmail() +","+jobParm.getParmValue());
			 }
			 jobParm.setScmsJob(scmsJob);
		 });
		 
		 ScmsJob svJob = persistanceDelegatorDao.save(scmsJob);
		 if(svJob.isActive()) {
			 log.info("Job[{}] Scheduled ? :{}", svJob.getJobName(), schedulerService.scheduleJob(svJob));
		 }

		 return svJob;
	}
}
