package com.scms.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scms.enums.ErrorConstants;

public class ScmsException extends RuntimeException {
    private static final Logger log = LoggerFactory.getLogger(ScmsException.class);

    private static final long serialVersionUID = 1L;

    private int errorCode;

    private String errorText;

    private ErrorConstants scmsException;

    private Object validatedEntity;

    public ScmsException(final ErrorConstants smsException) {
        super(smsException.getErrorText());
        this.scmsException = smsException;
        log.error("SMS Exception: {} {}", Integer.valueOf(smsException.getErrorCode()), smsException.getErrorText());
    }

    public <T> ScmsException(final ErrorConstants smsException, final T validatedEntity) {
        super(smsException.getErrorText());
        this.scmsException = smsException;
        this.validatedEntity = validatedEntity;
        log.error("SMS Exception: {} {}", Integer.valueOf(smsException.getErrorCode()), smsException.getErrorText());
    }

    public ScmsException(final int errorCode, final String errorText) {
        super(errorText);
        this.errorCode = errorCode;
        this.errorText = errorText;
        log.error("SMS Exception: {} {}", Integer.valueOf(errorCode), errorText);
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorText() {
        return this.errorText;
    }

    public ErrorConstants getScmsException() {
        return this.scmsException;
    }

    public Object getValidatedEntity() {
        return this.validatedEntity;
    }

    public void setErrorCode(final int errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorText(final String errorText) {
        this.errorText = errorText;
    }

    public void setScmsException(final ErrorConstants scmsException) {
        this.scmsException = scmsException;
    }

    public void setValidatedEntity(final Object validatedEntity) {
        this.validatedEntity = validatedEntity;
    }
}
