package com.scms.exception;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.scms.constants.Constants;
import com.scms.enums.ErrorConstants;
import com.scms.model.dto.Response;

@ControllerAdvice
public class ExceptionHandlerAspect extends ResponseEntityExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(ExceptionHandlerAspect.class);

    @ExceptionHandler(ScmsException.class)
    public ResponseEntity<?> handleScmsExceptions(final ScmsException smsException) {
        final ErrorConstants smsErrorConstants = smsException.getScmsException();
        String message = MessageFormat.format(smsErrorConstants.getErrorText(), smsException.getValidatedEntity());
        log.error("Inside SmsException handler aspect :{} {}", smsErrorConstants.getErrorCode(), message);        

        switch(smsErrorConstants.getErrorCode()) {
            case 401:
            	return new ResponseEntity<>(new Response(HttpStatus.FORBIDDEN.value(), Constants.B_FALSE, message, null), HttpStatus.FORBIDDEN);
            case 404:
            	return new ResponseEntity<>(new Response(HttpStatus.NOT_FOUND.value(), Constants.B_FALSE, message, null), HttpStatus.NOT_FOUND);
            case 409:
            	return new ResponseEntity<>(new Response(HttpStatus.CONFLICT.value(), Constants.B_FALSE, message, null), HttpStatus.CONFLICT);
            default:
            	return new ResponseEntity<>(new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), Constants.B_FALSE, message, null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<?> handleException(final Exception e) {
        final HttpStatus status = resolveAnnotatedResponseStatus(e);
        return new ResponseEntity<>(new Response(status.value(), Constants.B_FALSE, e.getLocalizedMessage(), null),
        		status);
    }

    private HttpStatus resolveAnnotatedResponseStatus(final Exception exception) {
        final ResponseStatus annotation = AnnotatedElementUtils.findMergedAnnotation(exception.getClass(),
            ResponseStatus.class);
        if(annotation != null) return annotation.value();
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
