package com.scms.model;

import java.util.StringJoiner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.scms.enums.AddressType;
import com.scms.util.StringUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Address")
public class Address extends RootEntity {
    private static final long serialVersionUID = -4748079676355164684L;

    private transient String addrAgainstClass;

    private transient Long addrAgainstId;

    @Column(name = "CITY")
    private String city;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "PINCODE")
    private String pincode;

    @ManyToOne
    @JoinColumn(name = "STATE")
    private States state;
    
    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

    @Column(name = "STREET")
    private String street;

    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private AddressType type;

    @Override
    public String toString() {
        final StringJoiner address = new StringJoiner(", ");
        if(!StringUtils.isEmpty(this.street)){
            address.add(this.street);
        }
        if(!StringUtils.isEmpty(this.city)){
            address.add(this.city);
        }
        if(this.state != null){
            address.add(this.state.getName());
        }
        if(!StringUtils.isEmpty(this.pincode)){
            address.add(this.pincode);
        }
        return address.toString();
    }
}
