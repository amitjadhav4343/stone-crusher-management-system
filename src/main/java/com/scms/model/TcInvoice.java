package com.scms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TcInvoice")
public class TcInvoice extends RootEntity {
    private static final long serialVersionUID = -5496592974013656748L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "TC", columnDefinition = "TEXT")
    private String tc;

    public TcInvoice() {
    }

    public TcInvoice(final long id, final String tc) {
        this.id = Long.valueOf(id);
        this.tc = tc;
    }

}
