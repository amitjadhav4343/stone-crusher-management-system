package com.scms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.scms.model.dto.UserDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "User")
public class User extends RootEntity {
    private static final long serialVersionUID = -2308446692460707736L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    
    private transient Long employeeId;

    @OneToOne(targetEntity = Employee.class)
    @JoinColumn(name = "EMPLOYEE")
    private Employee employee;

    @Column(name = "USERNAME", length = 30)
    private String username;

    @JsonIgnore
    @Column(name = "PASSWORD", columnDefinition = "TEXT")
    private String password;

    @ManyToOne
    @JoinColumn(name = "ROLE")
    private Role role;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

    public UserDto getUserDto() {
        Employee emp = this.employee;

        return new UserDto(this.id, emp.getName(), emp.getEmail(), this.username, emp.getInitials(), this.role,
            emp.getCompany()
                .getId());
    }

}
