package com.scms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.scms.enums.Period;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "ScmsJob")
public class ScmsJob extends RootEntity {

	private static final long serialVersionUID = 1850967212176618562L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
	
	private transient Long customerId;
    
	@Column(name = "CLASSNAME", length = 50)
    private String className;

    @Column(name = "CRONDESC", length = 50)
    private String cronDesc;

    @Column(name = "GROUPNAME", length = 50)
    private String groupName;

    @Column(name = "JOBNAME", length = 50)
    private String jobName;

    @Column(name = "TRIGGERNAME", length = 50)
    private String triggerName;
    
    @Column(name = "STARTDATE")
    private String startDate;

    @Column(name = "PERIOD", nullable = false)
    @Enumerated(EnumType.STRING)
    private Period period;
    
    @OneToMany(mappedBy = "scmsJob", cascade = { CascadeType.ALL })
    @Fetch(FetchMode.SUBSELECT)
    private List<ScmsJobParms> jobParms;
    
    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

	@Override
	public String toString() {
		return "ScmsJob [id=" + id + ", className=" + className + ", cronDesc=" + cronDesc + ", groupName=" + groupName
				+ ", jobName=" + jobName + ", triggerName=" + triggerName + ", jobParms=" + jobParms + ", isActive="
				+ isActive + "]";
	}
}
