package com.scms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Gst")
public class Gst extends RootEntity {
    private static final long serialVersionUID = 3739286746875883433L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

    @Column(name = "VALUE", unique = true, length = 10)
    private Double value;

}
