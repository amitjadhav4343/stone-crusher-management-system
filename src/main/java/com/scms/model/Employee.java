package com.scms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.scms.enums.Designation;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Employee")
public class Employee extends RootEntity {

    private static final long serialVersionUID = -8500699305568658933L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", length = 50)
    private String name;

    @Column(name = "INITIALS", length = 30)
    private String initials;

    @Column(name = "EMAIL", length = 30)
    private String email;

    @Column(name = "PHONE", length = 10)
    private String phone;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "IST")
    @Temporal(TemporalType.DATE)
    @Column(name = "BIRTHDATE", nullable = false)
    private Date birthDate;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "IST")
    @Temporal(TemporalType.DATE)
    @Column(name = "JOININGDATE", nullable = false)
    private Date joiningDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "IST")
    @Temporal(TemporalType.DATE)
    @Column(name = "RESIGNEDDATE")
    private Date resignedDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "IST")
    @Temporal(TemporalType.DATE)
    @Column(name = "ENDDATE")
    private Date endDate;

    @Column(name = "DESIGNATION", nullable = false)
    @Enumerated(EnumType.STRING)
    private Designation designation;

    @Column(name = "IDENTITY", length = 30)
    private String identity;

    @Column(name = "SALARY")
    private Double salary;

    @JsonIgnore
    @OneToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "USER")
    private User user;

    @OneToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "EmployeeAddress", joinColumns = { @JoinColumn(name = "EMPLOYEE", referencedColumnName = "id") },
        inverseJoinColumns = { @JoinColumn(name = "ADDRESS", referencedColumnName = "id") })
    @Fetch(FetchMode.SUBSELECT)
    private List<Address> address;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPANY")
    private Company company;
}
