
package com.scms.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.scms.enums.DateType;
import com.scms.enums.OrderType;
import com.scms.enums.PaymentMode;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Orders")
public class Orders extends RootEntity {
    private static final long serialVersionUID = -4025284058266424815L;

    @ManyToOne
    @JoinColumn(name = "ADDRESS")
    private Address address;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER")
    private Customer customer;

    @Column(name = "DATETYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private DateType dateType;

    @Column(name = "DISCOUNT", scale = 2)
    private Double discount;

    @Column(name = "FINANCIALYEAR", length = 10)
    private String financialYear;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @JsonProperty("isDelivered")
    @Column(name = "ISDELIVERED", nullable = false)
    private boolean isDelivered;

    @JsonProperty("isPending")
    @Column(name = "ISPENDING", nullable = false)
    private boolean isPending;

    @JsonProperty("isSentMail")
    @Column(name = "ISSENTMAIL", nullable = false)
    private boolean isSentMail;

    @JsonProperty("isSentSms")
    @Column(name = "ISSENTSMS", nullable = false)
    private boolean isSentSms;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd  HH:mm:ss", timezone = "IST")
    @Column(name = "ORDERDATE")
    private LocalDateTime orderDate;

    @OneToMany(mappedBy = "orders", cascade = { CascadeType.ALL })
    @Fetch(FetchMode.SUBSELECT)
    private List<Item> items;

    @Column(name = "ORDERTYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private OrderType orderType;

    @Column(name = "PAYMODE", nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentMode payMode;

    @Column(name = "PENDINGAMT", scale = 2)
    private Double pendingAmt;

    @Column(name = "RECEIVEDAMT", scale = 2)
    private Double receivedAmt;

    @Column(name = "REMARKS")
    private String remarks;

    @Column(name = "SID", length = 30)
    private String sid;

    @ManyToOne
    @JoinColumn(name = "SITE")
    private Site site;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

    @Column(name = "TOTAL", scale = 2)
    private Double total;

    @Column(name = "WITHGST", nullable = false)
    private boolean withGst;

}
