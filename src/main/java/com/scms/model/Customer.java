package com.scms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.scms.enums.CustomerType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Customer")
public class Customer extends RootEntity {
    private static final long serialVersionUID = -8191745552325490747L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "ADVANCE", scale = 2)
    private Double advance;

    @Column(name = "CUSTOMERTYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private CustomerType customerType;

    @Column(name = "EMAIL", length = 255)
    private String email;

    @Column(name = "FIRM", length = 100)
    private String firm;

    @Column(name = "GSTNO", length = 30)
    private String gstNo;

    @Column(name = "IDENTITY", length = 30)
    private String identity;

    @Column(name = "NAME", length = 255)
    private String name;

    @Column(name = "PHONE", length = 10)
    private String phone;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;
    
    @OneToMany(cascade = {CascadeType.ALL })
    @JoinTable(name = "CustomerAddress",
            joinColumns = {@JoinColumn(name = "CUSTOMER", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "ADDRESS", referencedColumnName = "id")}
    )
    @Fetch(FetchMode.SUBSELECT)
    private List<Address> address;
}
