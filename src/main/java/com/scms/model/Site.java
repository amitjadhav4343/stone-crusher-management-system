package com.scms.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Site")
public class Site extends RootEntity {
    private static final long serialVersionUID = -8191745552325490747L;

    @OneToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name = "ADDRESS")
    private Address address;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "COMPANY", nullable = false)
    private Company company;

    @Column(name = "EMAIL")
    private String email;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @JsonProperty("isPrimary")
    @Column(name = "ISPRIMARY", nullable = false)
    private Boolean isPrimary;

    @Column(name = "NAME", length = 50)
    private String name;

    @Column(name = "PHONE")
    private String phone;

    @Temporal(TemporalType.DATE)
    @Column(name = "REGDATE", nullable = false)
    private Date regDate;

    @Column(name = "REGNO", length = 50)
    private String regNo;

    @Temporal(TemporalType.DATE)
    @Column(name = "REGUPTO", nullable = false)
    private Date regUpto;

    private transient String siteAgainstClass;

    private transient Long siteAgainstId;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

}
