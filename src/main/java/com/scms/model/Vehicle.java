package com.scms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Vehicle")
public class Vehicle extends RootEntity {
    private static final long serialVersionUID = -8805708840980600698L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "MODEL", length = 30)
    private String model;

    @Column(name = "NUMBER", length = 11)
    private String number;

    @Column(name = "OWNERNAME", length = 100)
    private String ownerName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "IST")
    @Temporal(TemporalType.DATE)
    @Column(name = "REGDATE", nullable = false)
    private Date regDate;

    @Column(name = "REGNO", length = 30)
    private String regNo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "IST")
    @Temporal(TemporalType.DATE)
    @Column(name = "REGUPTO", nullable = false)
    private Date regUpto;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

    public Vehicle() {
    }

    public Vehicle(final String number, final String ownerName, final String model, final String regNumber,
        final Date regDate, final Date regUpto) {
        this.number = number;
        this.ownerName = ownerName;
        this.model = model;
        this.regNo = regNumber;
        this.regDate = regDate;
        this.regUpto = regUpto;
        this.isActive = true;
    }

}
