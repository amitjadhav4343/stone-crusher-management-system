package com.scms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Bank")
public class Bank extends RootEntity {
    private static final long serialVersionUID = -4748079676355164684L;

    @Column(name = "ACCNAME", length = 50)
    private String accName;

    @Column(name = "ACCNO", length = 30)
    private String accNo;

    @Column(name = "BRANCH", length = 50)
    private String branch;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "IFSC", length = 30)
    private String ifsc;

    @Column(name = "MICR", length = 30)
    private String micr;

    @Column(name = "NAME", length = 30)
    private String name;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

}
