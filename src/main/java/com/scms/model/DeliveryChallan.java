package com.scms.model;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DeliveryChallan")
public class DeliveryChallan extends RootEntity {
    private static final long serialVersionUID = 3879530960185845793L;

    @ManyToOne
    @JoinColumn(name = "ADDRESS")
    private Address address;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER")
    private Customer customer;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @JsonProperty("isBillable")
    @Column(name = "ISBILLABLE", nullable = false)
    private boolean isBillable;

    @JsonProperty("isDelivered")
    @Column(name = "ISDELIVERED", nullable = false)
    private boolean isDelivered;

    @JsonProperty("isSentMail")
    @Column(name = "ISSENTMAIL", nullable = false)
    private boolean isSentMail;

    @JsonProperty("isSentSms")
    @Column(name = "ISSENTSMS", nullable = false)
    private boolean isSentSms;

    @ManyToOne
    @JoinColumn(name = "MATERIAL")
    private Material material;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "IST")
    @Column(name = "ORDERDATE")
    private LocalDateTime orderDate;

    @JsonIgnore
    @ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
    @JoinColumn(name = "ITEM")
    private Item item;

    @Column(name = "QTYBRASS", scale = 2)
    private Double qtyBrass;

    @Column(name = "QTYKG")
    private Double qtyKg;

    @Column(name = "REMARKS", columnDefinition = "TEXT")
    private String remarks;

    @Column(name = "SID", length = 30)
    private String sid;

    @ManyToOne
    @JoinColumn(name = "SITE")
    private Site site;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

    @ManyToOne
    @JoinColumn(name = "VEHICLE")
    private Vehicle vehicle;

}
