package com.scms.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
@EntityListeners({ AuditingEntityListener.class })
public abstract class RootEntity implements Serializable {
    private static final long serialVersionUID = 2938324584896551015L;

    @CreatedDate
    @Column(name = "CREATEDDATE", length = 50, updatable = false)
    protected LocalDateTime createdDate;

    @CreatedBy
    @Column(name = "CREATEDUSERID", length = 50, updatable = false)
    protected String createdUserId;

    @LastModifiedDate
    @Column(name = "MODIFIEDDATE", length = 50)
    protected LocalDateTime lastModifiedDate;

    @LastModifiedBy
    @Column(name = "LASTMODIFIEDUSERID")
    protected String lastModifiedUserId;

}
