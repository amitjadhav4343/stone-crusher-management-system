package com.scms.model.dto;

public interface KeyValue {
    String getVal1();

    String getVal2();
}
