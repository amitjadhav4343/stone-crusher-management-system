package com.scms.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {
	int code;
	boolean success;
	String message;
	Object data;
	
	public Response(int code, boolean success, String message, Object data) {
		super();
		this.code = code;
		this.success = success;
		this.message = message;
		this.data = data;
	}
}
