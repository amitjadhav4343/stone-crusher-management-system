package com.scms.model.dto;

public class KeyValueClass {
    public String column1;

    public String column2;

    public KeyValueClass(final String column1, final Double column2) {
        this.column1 = column1;
        this.column2 = String.valueOf(column2);
    }

    public KeyValueClass(final String column1, final int column2) {
        this.column1 = column1;
        this.column2 = String.valueOf(column2);
    }

    public KeyValueClass(final String column1, final String column2) {
        this.column1 = column1;
        this.column2 = column2;
    }
}
