package com.scms.model.dto;

import javax.persistence.Transient;

import com.scms.constants.Constants;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FilterDTO {
    
    private Integer draw;
    
    private Integer start;
    
    private Integer length;
    
    private String startDate;

    private String endDate;
    
    @Transient
    private String startDateTime;

    @Transient
    private String endDateTime;
    
    private String customer;

	private String site;

	private boolean allRefreshed;

	private String pending;
	
	private Order order;
	
	private String isBillable;
	
	public String getStartDateTime() {
        return this.startDate + Constants.HH_MM_SS_START;
	}
	
	public String getEndDateTime() {
        return this.endDate + Constants.HH_MM_SS_END;
    }
   
    @Override
    public String toString() {
        return "FilterDTO [draw=" + draw + ", start=" + start + ", length=" + length + ", startDate=" + startDate
            + ", endDate=" + endDate + ", customer=" + customer + ", site=" + site + ", allRefreshed=" + allRefreshed
            + ", pending=" + pending + ", order=" + order.toString() + "]";
    }
    
    @Setter
    @Getter
    public class Order {
        
        private Integer column;
        
        private String dir;

        @Override
        public String toString() {
            return "Order [column=" + this.column + ", dir=" + this.dir + "]";
        }
    }
}


