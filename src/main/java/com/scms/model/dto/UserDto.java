package com.scms.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scms.model.Role;

public class UserDto {
    private String email;

    private Long id;

    private String initials;

    private String name;

    private Role role;

    private String username;

    @JsonIgnore
    private Long company;

    public UserDto(final Long id, final String name, final String email, final String username, final String initials,
        final Role role, final Long company) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.username = username;
        this.initials = initials;
        this.role = role;
        this.company = company;
    }

    public String getEmail() {
        return this.email;
    }

    public Long getId() {
        return this.id;
    }

    public String getInitials() {
        return this.initials;
    }

    public String getName() {
        return this.name;
    }

    public Role getRole() {
        return this.role;
    }

    public String getUsername() {
        return this.username;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setInitials(final String initials) {
        this.initials = initials;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public Long getCompany() {
        return company;
    }

    public void setCompany(Long company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "UserDto [email=" + email + ", id=" + id + ", initials=" + initials + ", name=" + name + ", role=" + role
            + ", username=" + username + ", company=" + company + "]";
    }
}
