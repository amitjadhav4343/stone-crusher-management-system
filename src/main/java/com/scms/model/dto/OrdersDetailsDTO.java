package com.scms.model.dto;

public class OrdersDetailsDTO {
    private String customer;

    private Long id;

    private String material;

    public OrdersDetailsDTO() {
    }

    public OrdersDetailsDTO(final Long id, final String customer, final String material) {
        this.id = id;
        this.customer = customer;
        this.material = material;
    }

    public String getCustomer() {
        return this.customer;
    }

    public Long getId() {
        return this.id;
    }

    public String getMaterial() {
        return this.material;
    }

    public void setCustomer(final String customer) {
        this.customer = customer;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setMaterial(final String material) {
        this.material = material;
    }
}
