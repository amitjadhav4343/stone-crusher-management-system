package com.scms.model.dto;

public class QuantityDTO {
    private Double qtyBrass;

    private Double qtyKg;

    public QuantityDTO(final Double qtyKg, final Double qtyBrass) {
        this.qtyKg = qtyKg;
        this.qtyBrass = qtyBrass;
    }

    @Override
    public boolean equals(final Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass()) return false;
        final QuantityDTO other = (QuantityDTO) obj;
        if(this.qtyBrass == null){
            if(other.qtyBrass != null) return false;
        } else if(!this.qtyBrass.equals(other.qtyBrass)) return false;
        if(this.qtyKg == null){
            if(other.qtyKg != null) return false;
        } else if(!this.qtyKg.equals(other.qtyKg)) return false;
        return true;
    }

    public Double getQtyBrass() {
        return this.qtyBrass;
    }

    public Double getQtyKg() {
        return this.qtyKg;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.qtyBrass == null ? 0 : this.qtyBrass.hashCode());
        result = prime * result + (this.qtyKg == null ? 0 : this.qtyKg.hashCode());
        return result;
    }

    public void setQtyBrass(final Double qtyBrass) {
        this.qtyBrass = qtyBrass;
    }

    public void setQtyKg(final Double qtyKg) {
        this.qtyKg = qtyKg;
    }
}
