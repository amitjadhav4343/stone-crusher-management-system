package com.scms.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scms.enums.JobParmKey;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "ScmsJobParms")
public class ScmsJobParms extends RootEntity {

	private static final long serialVersionUID = 2839949682789847318L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    
	@Column(name = "PARMKEY", nullable = false)
    @Enumerated(EnumType.STRING)
    private JobParmKey parmkey;

    @Column(name = "PARMVALUE", length = 50)
    private String parmValue;
    
    @JsonIgnore
    @ManyToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name = "SCMSJOB")
    private ScmsJob scmsJob;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((parmValue == null) ? 0 : parmValue.hashCode());
		result = prime * result + ((parmkey == null) ? 0 : parmkey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScmsJobParms other = (ScmsJobParms) obj;
		if (parmValue == null) {
			if (other.parmValue != null)
				return false;
		} else if (!parmValue.equals(other.parmValue))
			return false;
		if (parmkey == null) {
			if (other.parmkey != null)
				return false;
		} else if (!parmkey.equals(other.parmkey))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ScmsJobParms [id=" + id + ", parmkey=" + parmkey + ", parmValue=" + parmValue + ", scmsJob=" + scmsJob
				+ "]";
	}
}
