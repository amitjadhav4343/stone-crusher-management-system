package com.scms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "ModelView")
public class ModelView extends RootEntity {
    private static final long serialVersionUID = -7951068447376889465L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "REQUEST", length = 100)
    private String request;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

    @Column(name = "RESPONSE", length = 100)
    private String response;

}
