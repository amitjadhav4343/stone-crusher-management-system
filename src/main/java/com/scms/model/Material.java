package com.scms.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Material")
public class Material extends RootEntity {
    private static final long serialVersionUID = -8842174183721990640L;

    @ManyToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name = "GST")
    private Gst gst;

    private transient Long gstId;

    @Column(name = "HSN", length = 30)
    private String hsn;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", length = 30)
    private String name;

    @Column(name = "RATE", scale = 2)
    private Double rate;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( (gst == null) ? 0 : gst.hashCode());
        result = prime * result + ( (hsn == null) ? 0 : hsn.hashCode());
        result = prime * result + ( (id == null) ? 0 : id.hashCode());
        result = prime * result + (isActive ? 1231 : 1237);
        result = prime * result + ( (name == null) ? 0 : name.hashCode());
        result = prime * result + ( (rate == null) ? 0 : rate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass()) return false;
        Material other = (Material) obj;
        if(gst == null){
            if(other.gst != null) return false;
        } else if(!gst.equals(other.gst)) return false;
        if(hsn == null){
            if(other.hsn != null) return false;
        } else if(!hsn.equals(other.hsn)) return false;
        if(id == null){
            if(other.id != null) return false;
        } else if(!id.equals(other.id)) return false;
        if(isActive != other.isActive) return false;
        if(name == null){
            if(other.name != null) return false;
        } else if(!name.equals(other.name)) return false;
        if(rate == null){
            if(other.rate != null) return false;
        } else if(!rate.equals(other.rate)) return false;
        return true;
    }
}
