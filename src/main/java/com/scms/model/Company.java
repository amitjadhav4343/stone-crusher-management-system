package com.scms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Company")
public class Company extends RootEntity {
    private static final long serialVersionUID = -8191745552325490747L;

    @OneToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name = "ADDRESS")
    private Address address;

    @OneToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name = "BANK")
    private Bank bank;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "FAX", length = 30)
    private String fax;

    @Column(name = "GSTNO", length = 30)
    private String gstNo;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", length = 50)
    private String name;

    @Column(name = "PAN", length = 30)
    private String pan;

    @Column(name = "PHONE")
    private String phone;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "IST")
    @Temporal(TemporalType.DATE)
    @Column(name = "REGDATE")
    private Date regDate;

    @Column(name = "REGNO", length = 50)
    private String regNo;

    @OneToMany(cascade = { CascadeType.ALL }, mappedBy = "company")
    @Fetch(FetchMode.SUBSELECT)
    private List<Site> site;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

    @Column(name = "WEBSITE", length = 50)
    private String website;

}
