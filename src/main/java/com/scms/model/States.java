package com.scms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "States")
public class States extends RootEntity {
    private static final long serialVersionUID = 8750036884022284184L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", length = 30)
    private String name;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

}
