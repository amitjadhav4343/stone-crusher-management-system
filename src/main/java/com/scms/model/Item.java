package com.scms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Item")
public class Item extends RootEntity {
    private static final long serialVersionUID = -8346482331682566152L;

    @OneToMany(mappedBy = "item", cascade = { CascadeType.ALL })
    @Fetch(FetchMode.SUBSELECT)
    private List<DeliveryChallan> deliveryChallans;

    @Column(name = "GST", scale = 2)
    private Double gst;

    @Column(name = "GSTAMT", scale = 2)
    private Double gstAmt;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "MATERIAL")
    private Material material;

    @JsonIgnore
    @ManyToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name = "ORDERS")
    private Orders orders;

    @Column(name = "QTYBRASS", scale = 2)
    private Double qtyBrass;

    @Column(name = "QTYKG", scale = 2)
    private Double qtyKg;

    @Column(name = "RATE", scale = 2)
    private Double rate;

    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

    @Column(name = "SUBTOTAL", scale = 2)
    private Double subTotal;

    @Column(name = "TOTAL", scale = 2)
    private Double total;

    @Column(name = "WITHGST")
    private boolean withGst;

    public String getMaterialName() {
        return this.material.getName();
    }
}
