package com.scms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TcPlaceOrder")
public class TcPlaceOrder extends RootEntity {
    private static final long serialVersionUID = -8111368657470993068L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "TC", columnDefinition = "TEXT")
    private String tc;

    public TcPlaceOrder() {
    }

    public TcPlaceOrder(final Long id, final String tc) {
        this.id = id;
        this.tc = tc;
    }
}
