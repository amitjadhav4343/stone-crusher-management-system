package com.scms.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.scms.enums.TransactionType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TransactionHistory")
public class TransactionHistory extends RootEntity {
    private static final long serialVersionUID = -3991222916002005707L;

    @Column(name = "AMOUNT", scale = 2)
    private Double amount;

    @Column(name = "CLOSINGBALANCE", scale = 2)
    private Double closingBalance;

    @Column(name = "CUSTOMERID")
    private Long customerId;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "CUSTOMER")
    private Customer customer;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "REMARKS")
    private String remarks;

    @CreatedDate
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "IST")
    @Column(name = "TRANSACTIONDATE", updatable = false)
    private LocalDateTime transactionDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "TRANSACTIONTYPE", nullable = false)
    private TransactionType type;
    
    @JsonProperty("isActive")
    @Column(name = "ISACTIVE", nullable = false)
    private boolean isActive;

    public TransactionHistory() {
    }

    /**
     * @param customer
     * @param amount
     * @param closingBalance
     * @param type
     * @param remarks
     */
    public TransactionHistory(final Customer customer, final Double amount, final Double closingBalance,
        final TransactionType type, final String remarks) {
        this.customerId = customer.getId();
        this.customer = customer;
        this.amount = amount;
        this.closingBalance = closingBalance;
        this.type = type;
        this.remarks = remarks;
    }
}
