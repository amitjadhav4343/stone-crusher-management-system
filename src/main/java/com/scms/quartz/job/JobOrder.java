package com.scms.quartz.job;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.scms.constants.Constants;
import com.scms.enums.ErrorConstants;
import com.scms.enums.JobParmKey;
import com.scms.exception.ScmsException;
import com.scms.mail.EmailService;
import com.scms.model.Orders;
import com.scms.model.ScmsJob;
import com.scms.model.ScmsJobParms;
import com.scms.model.dto.FilterDTO;
import com.scms.quartz.export.CsvExport;
import com.scms.quartz.export.ExcelExport;
import com.scms.quartz.export.PdfExport;
import com.scms.service.LookupService;
import com.scms.util.StringUtils;
import com.scms.util.Utility;

public class JobOrder implements Job {
    private static final Logger log = LoggerFactory.getLogger(JobOrder.class);
    
    @Autowired
    private EmailService emailService;
    
    @Autowired
    private LookupService lookupService;
    
    @Autowired
    private ExcelExport excelExporter;
    
    @Autowired
    private CsvExport csvExporter;
    
    @Autowired
    private PdfExport pdfExporter;
    
    private Map<String, String> map = null;

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
    	log.info("welcomes JobOrder job");
    	JobDataMap jdMap = context.getJobDetail().getJobDataMap();
    	ScmsJob scmsJob = (ScmsJob) jdMap.get("ScmsJob");
		FilterDTO filterDTO = new FilterDTO();
		this.map = new HashMap<>();
		setAttributes(filterDTO, scmsJob);
		
		if("Data".equals(this.map.get(JobParmKey.ReportType.name()))) {
			List<Orders> list = lookupService.getOrders(Utility.getJson(filterDTO));
			String filePath = writeData(list, scmsJob.getJobName());
			String mailSubject = "Scheduled " + scmsJob.getPeriod() + " report of " + scmsJob.getJobName();
			String recipients = this.map.get(JobParmKey.Recipients.name());
			if(StringUtils.isEmpty(recipients)) {
				throw new ScmsException(ErrorConstants.RECIPIENTS_EMPTY);
			}
			emailService.sendWithAttachment(null, recipients, mailSubject, mailSubject, filePath);
			log.debug("Job[{}] executed successfully !!!", scmsJob.getJobName());
		}
    }
    
    private String writeData(List<Orders> orderList, String fileName) {
    	String filePath = null;
		switch (this.map.get(JobParmKey.FileType.name())) {
		case "CSV":
			log.debug("CSV");
			filePath = csvExporter.export(orderList, fileName);
			break;
		case "EXCEL":
			log.debug("EXCEL");
			filePath = excelExporter.export(orderList, fileName);
			break;
		case "PDF":
			log.debug("PDF");
			filePath = pdfExporter.export(orderList, fileName);
			break;
		default:
			break;
		}
		return filePath;
	}

	private void setAttributes(FilterDTO filterDTO, ScmsJob scmsJob) {
		for (ScmsJobParms jobParms : scmsJob.getJobParms()) {
    		this.map.put(jobParms.getParmkey().name(), jobParms.getParmValue());
		}
		
    	filterDTO.setSite(this.map.get("Site"));
		filterDTO.setCustomer(this.map.get("Customer"));
		
		switch (scmsJob.getPeriod()) {
			case DAILY:
				filterDTO.setEndDate(Utility.getFormattedDate(Constants.YYYY_MM_DD, LocalDateTime.now().minusDays(1)));
				filterDTO.setStartDate(Utility.getFormattedDate(Constants.YYYY_MM_DD, LocalDateTime.now().minusDays(1))); 
				break;
			case WEEKLY:
				filterDTO.setEndDate(Utility.getFormattedDate(Constants.YYYY_MM_DD, LocalDateTime.now().minusDays(1)));
				filterDTO.setStartDate(Utility.getFormattedDate(Constants.YYYY_MM_DD, LocalDateTime.now().minusDays(7)));
				break;
			case MONTHLY:
				filterDTO.setEndDate(Utility.getFormattedDate(Constants.YYYY_MM_DD, LocalDateTime.now().minusDays(1)));
				filterDTO.setStartDate(Utility.getFormattedDate(Constants.YYYY_MM_DD, LocalDateTime.now().minusMonths(1)));
				break;
		case DONOTREPEAT:
				filterDTO.setEndDate(this.map.get("EndDate"));
				filterDTO.setStartDate(this.map.get("StartDate"));
				break;
		default:
			break;
		}
	}
}
