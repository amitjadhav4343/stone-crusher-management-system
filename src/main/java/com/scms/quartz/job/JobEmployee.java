package com.scms.quartz.job;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JobEmployee implements Job {
    private static final Logger log = LoggerFactory.getLogger(JobEmployee.class);

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
    	log.info("welcomes JobEmployee job");
		JobDataMap jdMap = context.getJobDetail().getJobDataMap();
		
		log.debug("JobEmployee jobName: {}", jdMap.get("Site"));
    }
}
