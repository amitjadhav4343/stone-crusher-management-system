package com.scms.quartz.export;

import java.util.List;

import com.scms.model.Orders;

public interface ExcelExport {
	public String export(List<Orders> orderList, String fileId);
}
