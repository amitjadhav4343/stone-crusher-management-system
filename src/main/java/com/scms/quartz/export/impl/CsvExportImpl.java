package com.scms.quartz.export.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.scms.constants.Constants;
import com.scms.model.Item;
import com.scms.model.Orders;
import com.scms.quartz.export.CsvExport;
import com.scms.util.CsvWriter;
import com.scms.util.Utility;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CsvExportImpl implements CsvExport {
	private CsvWriter csvOutput = null;
       
    private void writeHeaderLine() throws IOException {
        String[] headers = {"DateTime", "Customer", "Order Id", "Material", "Quantity", "Rate", "Sub Total", "Gst %", "Gst Amt", "Total(ST+GA)", "Discount", "Received Amount", "Pending", "Pending Amount", "Payment Mode", "Site", "Remarks"};
        /* Used below loop to append CSV header part */
        for (String header : headers) {
            this.csvOutput.write(header);
        }
        this.csvOutput.endRecord();
    }
    
    private void writeDataLines(List<Orders> orderList) throws IOException {
    	for (Orders order : orderList) {
    		for (Item item : order.getItems()) {
    			this.csvOutput.write(order.getOrderDate().toString());      
        		this.csvOutput.write(order.getCustomer().getName());       
        		this.csvOutput.write(order.getSid());    
        		this.csvOutput.write(item.getMaterial().getName());
        		this.csvOutput.write(item.getQtyBrass().toString());
        		this.csvOutput.write(item.getRate().toString());
        		this.csvOutput.write(item.getSubTotal().toString());
        		this.csvOutput.write(item.getGst().toString());
        		this.csvOutput.write(item.getGstAmt().toString());
        		this.csvOutput.write(order.getTotal().toString());
        		this.csvOutput.write(order.getDiscount().toString());
        		this.csvOutput.write(order.getReceivedAmt().toString());
        		this.csvOutput.write(String.valueOf(order.isPending()));
        		this.csvOutput.write(order.getPendingAmt().toString());
        		this.csvOutput.write(order.getPayMode().name());
        		this.csvOutput.write(order.getSite().getName());
        		this.csvOutput.write(order.getRemarks());
        		this.csvOutput.endRecord();
    		}
        }
        
    }

	public String export(List<Orders> orderList, String fileName) {
		String filePath = null;
        try {
        	filePath = Constants.PATH_SRC_FILES + File.separator + "scheduler/csv" + File.separator + fileName + Constants.DOT_CSV_EXT;
        	log.debug("CSV created : " + filePath);
        	
        	/* Before creating tmp file check the if it already exists */
            Utility.deleteFileIfExists(filePath);
            
            /* Use FileWriter constructor that specifies open for appending */
            this.csvOutput = new CsvWriter(new FileWriter(filePath, true), ',');
            
            writeHeaderLine();
            writeDataLines(orderList);
            
            this.csvOutput.close();
            log.info("CSV created successfully !!!");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (this.csvOutput != null)
                this.csvOutput.close();
		}
        return filePath;
	}
}
