package com.scms.quartz.export.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.scms.constants.Constants;
import com.scms.model.Item;
import com.scms.model.Orders;
import com.scms.quartz.export.ExcelExport;
import com.scms.util.Utility;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ExcelExportImpl implements ExcelExport {
	private XSSFWorkbook workbook;
    private XSSFSheet sheet;
       
    private void writeHeaderLine() {
        sheet = workbook.createSheet("Order sheet");
        String[] headers = {"DateTime", "Customer", "Order Id", "Material", "Quantity", "Rate", "Sub Total", "Gst %", "Gst Amt", "Total(ST+GA)", "Discount", "Received Amount", "Pending", "Pending Amount", "Payment Mode", "Site", "Remarks"};
        XSSFRow row = sheet.createRow(0);
        int cellNum = 0;
        
        for(String header : headers) {
        	createCell(row, cellNum++, header);
        }
    }
    
    private void createCell(XSSFRow row, int cellNum, Object value) {
    	log.info("cellNum : {}, value : {}", cellNum, value);
        sheet.autoSizeColumn(cellNum);
        XSSFCell cell = row.createCell(cellNum);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }
    }
    
    private void writeDataLines(List<Orders> orderList) {
        int rowNum = 1;
        for (Orders order : orderList) {
        	log.info("Order Id : {}", order.getSid());

            for (Item item : order.getItems()) {
            	int cellNum = 0;
            	XSSFRow row = sheet.createRow(rowNum);
            	log.info("rowNum : {}, cellNum : {}", rowNum, cellNum);
            	
                createCell(row, cellNum++, order.getOrderDate().toString());      
                createCell(row, cellNum++, order.getCustomer().getName());       
                createCell(row, cellNum++, order.getSid());    
                createCell(row, cellNum++, item.getMaterial().getName());
                createCell(row, cellNum++, item.getQtyBrass().toString());
                createCell(row, cellNum++, item.getRate().toString());
                createCell(row, cellNum++, item.getSubTotal().toString());
                createCell(row, cellNum++, item.getGst().toString());
                createCell(row, cellNum++, item.getGstAmt().toString());
                createCell(row, cellNum++, order.getTotal().toString());
                createCell(row, cellNum++, order.getDiscount().toString());
                createCell(row, cellNum++, order.getReceivedAmt().toString());
                createCell(row, cellNum++, order.isPending());
                createCell(row, cellNum++, order.getPendingAmt().toString());
                createCell(row, cellNum++, order.getPayMode().name());
                createCell(row, cellNum++, order.getSite().getName());
                createCell(row, cellNum, order.getRemarks());
                
                rowNum++;
            }
        }
    }

	public String export(List<Orders> orderList, String fileName) {
		String filePath = null;
        try {
        	filePath = Constants.PATH_SRC_FILES + File.separator + "scheduler/excel" + File.separator + fileName + Constants.DOT_XLSX_EXT;
        	
        	/* Before creating tmp file check the if it already exists */
            Utility.deleteFileIfExists(filePath);
            
            /* Creating Workbook instances */
        	workbook = new XSSFWorkbook();
    		
            writeHeaderLine();
            writeDataLines(orderList);
            
			/* Write the output to a file */
            FileOutputStream fileOut = new FileOutputStream(filePath);
            workbook.write(fileOut);
            
            /* Closing the output stream */
            fileOut.close();

            // Closing the workbook
            workbook.close();
            log.info("Excel created successfully !!!");
		} catch (IOException e) {
			e.printStackTrace();
		}
        return filePath;
	}
}
