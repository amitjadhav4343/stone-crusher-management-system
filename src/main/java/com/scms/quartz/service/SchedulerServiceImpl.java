package com.scms.quartz.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scms.model.ScmsJob;
import com.scms.model.ScmsJobParms;
import com.scms.service.LookupService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SchedulerServiceImpl implements SchedulerService {

    @Autowired
    private Scheduler scheduler;
    
    @Autowired
    private LookupService lookupService;

    //@formatter:off
    // Field Name     Mandatory   Allowed Values      Allowed Special Characters
    // Seconds        YES         0-59                , - * /
    // Minutes        YES         0-59                , - * /
    // Hours          YES         0-23                , - * /
    // Day of month   YES         1-31                , - * ? / L W
    // Month          YES         1-12 or JAN-DEC     , - * /
    // Day of week    YES         1-7 or SUN-SAT      , - * ? / L #
    // Year           NO          empty, 1970-2099    , - * /
    //@formatter:on

    private Trigger getCronTrigger(final String jobName, final String groupName, final String cronDesc) {
        return TriggerBuilder.newTrigger()
            .withIdentity(jobName, groupName)
            .withSchedule(CronScheduleBuilder.cronSchedule(cronDesc))
            .build();
    }

    public boolean scheduleAllJobs() {
        boolean flag = false;
        final List<ScmsJob> list = this.lookupService.getActiveEntityList(ScmsJob.class);
        log.info("Number of active Order jobs : {}", Integer.valueOf(list.size()));
        if(CollectionUtils.isNotEmpty(list)){
            try{
                scheduler.clear();
                JobDetail job = null;
                Trigger trigger = null;
                for(final ScmsJob scmsJob : list){
                	job = creteJob(scmsJob);                	
                	injectJobParms(job, scmsJob);

                    trigger = getCronTrigger(scmsJob.getTriggerName(), scmsJob.getGroupName(), scmsJob.getCronDesc());
                    scheduler.scheduleJob(job, trigger);
                }
                scheduler.start();
                flag = true;
            } catch(final SchedulerException e){
                log.error("SchedulerException in SchedulerService");
                e.printStackTrace();
            }
        }
        return flag;
    }
    
    public boolean scheduleJob(ScmsJob scmsJob) {
        boolean flag = false;
        log.info("Scheduling new job : {}", scmsJob.getJobName());

        try{
			/* Schedule Job */
            JobDetail job = creteJob(scmsJob);                	
			/* injectJobParms(job, scmsJob); */
        	job.getJobDataMap().put("ScmsJob", scmsJob);

        	Trigger trigger = getCronTrigger(scmsJob.getTriggerName(), scmsJob.getGroupName(), scmsJob.getCronDesc());
            scheduler.scheduleJob(job, trigger);
            scheduler.start();
            flag = true;
        } catch(final SchedulerException e){
            log.error("SchedulerException in SchedulerService");
            e.printStackTrace();
        }
        return flag;
    }
    
    /**
     * Create a job using scmsJob object.
     * 
     * @param scmsJob
     * @return JobDetail
     */
	@SuppressWarnings("unchecked")
	private JobDetail creteJob(ScmsJob scmsJob) {
		//log.info("create JobDetail against : {}", scmsJob.getJobName());
		log.info("Created Scms job from ScmsJob[{}] group of[{}]", scmsJob.getJobName(), scmsJob.getGroupName());
		try {
			Class<?> clazz = Class.forName("com.scms.quartz.job." + scmsJob.getClassName());
			return JobBuilder.newJob((Class<? extends Job>) clazz)
            .withIdentity(scmsJob.getJobName(), scmsJob.getGroupName())
            .build();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
    
    /**
     * Inject job parms into job.
     * 
     * @param job
     * @param scmsJob
     */
    private void injectJobParms(JobDetail job, ScmsJob scmsJob) {
    	Map<String, String> map = new HashMap<>();
    	
		/*
		 * Map<String, String> map = scmsJob.getJobParms().stream()
		 * .collect(Collectors.toMap(ScmsJobParms::getParmkey,
		 * ScmsJobParms::getParmValue));
		 */
    	
    	for (ScmsJobParms jobParms : scmsJob.getJobParms()) {
    		map.put(jobParms.getParmkey().name(), jobParms.getParmValue());
		}
    	
    	log.info("Injecting jobParms : {}", map);
    	job.getJobDataMap().putAll(map);
    	job.getJobDataMap().put("ScmsJob", scmsJob);
	}

	@Override
    public boolean pauseJob(String jobName) {
        return false;
    }

	@Override
	public boolean scheduleJob(Long id) {
        boolean flag = false;
        
        ScmsJob scmsJob = lookupService.lookupById(ScmsJob.class, id);
        log.info("Scheduling new job : {}", scmsJob.getJobName());

        try{
            JobDetail job = null;
            Trigger trigger = null;
            
			/* Schedule Job */
            job = creteJob(scmsJob);                	
        	//injectJobParms(job, scmsJob);
            job.getJobDataMap().put("ScmsJob", scmsJob);

            trigger = getCronTrigger(scmsJob.getTriggerName(), scmsJob.getGroupName(), scmsJob.getCronDesc());
            scheduler.scheduleJob(job, trigger);
            scheduler.start();
            flag = true;
        } catch(final SchedulerException e){
            log.error("SchedulerException in SchedulerService");
            e.printStackTrace();
        }
        return flag;
	}
}
