package com.scms.quartz.service;

import com.scms.model.ScmsJob;

public interface SchedulerService {
	
	public boolean scheduleJob(ScmsJob scmsJob);

    public boolean scheduleAllJobs();
    
    public boolean pauseJob(String jobName);

	public boolean scheduleJob(Long id);
}
