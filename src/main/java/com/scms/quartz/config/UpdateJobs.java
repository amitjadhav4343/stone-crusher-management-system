package com.scms.quartz.config;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateJobs implements Job {
    private static final Logger log = LoggerFactory.getLogger(UpdateJobs.class);

    @Override
    public void execute(final JobExecutionContext arg0) throws JobExecutionException {
        log.info("welcomes simple job");
    }
}
