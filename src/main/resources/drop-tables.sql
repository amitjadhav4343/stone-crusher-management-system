-- MySQL Drop table dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: scms
-- ------------------------------------------------------
-- Server version	5.7.22-log

drop table deliverychallan;
drop table customer;
drop table company;
drop table bank;
drop table address;

drop table environmentvariable;
drop table gst;
drop table item;
drop table jobschedular;
drop table material;

drop table modelview;
drop table orders;
drop table role;
drop table site;
drop table states;

drop table tcinvoice;
drop table tcplaceorder;
drop table tcquotation;
drop table transactionhistory;
drop table user;

drop table vehicle;
drop table hibernate_sequence;

-- Dropped table successfully