-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: scms
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'2021-05-05 19:09:30','amit4343','2021-05-05 19:09:30','amit4343',NULL,_binary '',NULL,'dummy address for dummy emp','DUMMY',NULL,NULL),(2,'2021-05-05 19:09:30','amit4343','2021-05-05 19:09:30','amit4343','Malkapur',_binary '','415539','Gokak road, Near Star English Medium','BILLING',NULL,21);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `bank`
--

LOCK TABLES `bank` WRITE;
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
INSERT INTO `bank` VALUES (1,'2021-05-05 19:11:07','amit4343','2021-05-05 19:11:07','amit4343','Unity Builders','HDFC000034961','Karad','IFSC000486',_binary '','MICR041798','HDFC');
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'2021-05-05 19:10:38','amit4343','2021-05-05 19:10:38','amit4343','info@unity.com','FAX0616289','GST10264861',_binary '','Unity Builders','PAN5642','020-264486','2020-05-01','REG068162','www.unity.com',2,1);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,NULL,NULL,NULL,NULL,'amitjadhav4343@gmail.com',NULL,'admin',_binary '','2000-01-01 00:00:00','admin','9823378722',NULL,1,1,NULL);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `environmentvariable`
--

LOCK TABLES `environmentvariable` WRITE;
/*!40000 ALTER TABLE `environmentvariable` DISABLE KEYS */;
INSERT INTO `environmentvariable` VALUES (1,'2021-05-05 19:11:16','amit4343','2021-05-05 19:11:16','amit4343',_binary '','mail.cc','amitjadhav4343@gmail.com, prajakta.desai0001@gmail.com'),(2,'2021-05-05 19:11:16','amit4343','2021-05-05 19:11:16','amit4343',_binary '','mail.bcc','amitjadhav4343@gmail.com'),(3,'2021-05-05 19:11:16','amit4343','2021-05-05 19:11:16','amit4343',_binary '','sms.owners','9823378722');
/*!40000 ALTER TABLE `environmentvariable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `gst`
--

LOCK TABLES `gst` WRITE;
/*!40000 ALTER TABLE `gst` DISABLE KEYS */;
INSERT INTO `gst` VALUES (1,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '',0),(2,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '',9),(3,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '',18);
/*!40000 ALTER TABLE `gst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (1000),(10000);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `modelview`
--

LOCK TABLES `modelview` WRITE;
/*!40000 ALTER TABLE `modelview` DISABLE KEYS */;
INSERT INTO `modelview` VALUES (1,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/orderer/placeOrder','/user/orderer/placeOrder'),(2,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/orderer/placeInOrder','/user/orderer/placeInOrder'),(3,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/orderer/adminPanel','/user/orderer/adminPanel'),(4,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/orderer/reportList','/user/orderer/reportList'),(5,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/aboutUs','/aboutUs'),(7,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/orderer/dashboard','/user/orderer/dashboard'),(8,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/widgets/page-not-found','/widgets/page-not-found'),(9,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/orderer/reportChart','/user/orderer/reportChart'),(10,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/homepage','/homepage'),(11,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/admin/material','/admin/material'),(12,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/admin/company','/admin/company'),(13,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/admin/gst','/admin/gst'),(14,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/admin/customer','/admin/customer'),(15,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/admin/users','/admin/users'),(16,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/admin/vehicle','/admin/vehicle'),(17,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/dc/generateDC','/dc/generateDC'),(18,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/master/dashboard','/user/master/dashboard'),(19,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/dc/generateODC','/dc/generateODC'),(20,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/admin/transactionHistory','/admin/transactionHistory'),(21,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/admin/test','/admin/test'),(22,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/orderer/updatePLDC','/user/orderer/updatePLDC'),(23,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/totalView','/user/totalView'),(24,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/admin/order','/admin/order'),(25,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/contactUs','/includes/contactUs'),(26,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/privacyPolicy','/includes/privacyPolicy'),(27,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','/page/termsConditions','/includes/termsConditions');
/*!40000 ALTER TABLE `modelview` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,_binary '','ORDERER'),(2,_binary '','MASTER'),(3,_binary '','CUSTOMER'),(4,_binary '','OFFICE');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `site`
--

LOCK TABLES `site` WRITE;
/*!40000 ALTER TABLE `site` DISABLE KEYS */;
INSERT INTO `site` VALUES (1,'2020-06-26 11:22:10','amit4343','2020-06-26 11:22:10','amit4343','surlighat@gmail.com',_binary '',_binary '\0','Surli Ghat','9988776655','2020-02-29','REG64841','2020-10-31',NULL,1);
/*!40000 ALTER TABLE `site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `states`
--

LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` VALUES (1,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Andaman and Nicobar Islands'),(2,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Andhra Pradesh'),(3,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Arunachal Pradesh'),(4,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Assam'),(5,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Bihar'),(6,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Chattisgarh'),(7,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Chandigarh'),(8,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Daman and Diu'),(9,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Delhi'),(10,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Dadra and Nagar Haveli'),(11,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Goa'),(12,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Gujarat'),(13,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Himachal Pradesh'),(14,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Haryana'),(15,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Jammu and Kashmir'),(16,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Jharkhand'),(17,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Kerala'),(18,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Karnataka'),(19,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Lakshadweep'),(20,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Meghalaya'),(21,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Maharashtra'),(22,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Manipur'),(23,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Madhya Pradesh'),(24,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Mizoram'),(25,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Nagaland'),(26,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Orissa'),(27,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Punjab'),(28,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Pondicherry'),(29,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Rajasthan'),(30,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Sikkim'),(31,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Tamil Nadu'),(32,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Tripura'),(33,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Uttarakhand'),(34,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Uttar Pradesh'),(35,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','West Bengal'),(36,'2021-05-05 19:11:54','amit4343','2021-05-05 19:11:54','amit4343',_binary '','Telangana');
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,NULL,NULL,NULL,_binary '','$2a$10$eMBXgS74igoYB43txcbqRuG2X55kq2ogBK6d..rIoGDBMj/fdpbna','amit4343',1,2);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-05 19:14:27