/*================================== ENABLE ELEMENTS ===========================*/
$(function() {
	hdrSdbrFootrFunc();
});
/*================================== AJAX ===========================*/

/*$(window).on('load', function() {
	var $msg = $("#msg").val();
	if($msg){
		alert($msg);
	}
});*/

$(window).on('load', function() {
    ajax();
});

$(window).on('popstate', function() {
    ajax();
});

function ajax() {
    //var hash = window.location.href.split('#');
    var ajax_Url = getCurrentPage();
    if (typeof ajax_Url === 'undefined') {
        switch($("#role").val())
    	{
    		case "ORDERER"	: 	ajaxLoad('/page/orderer/dashboard');
    							break;
    		case "MASTER" 	: 	ajaxLoad('/page/master/dashboard');
    							break;
    		case "CUSTOMER"	: 	alert('CUSTOMER !!!');
    							break;
    		case "ADMIN"	: 	alert('ADMIN !!!');
    							break;
    		default:			ajaxLoad('/page/homepage');
    	}
    } else {
        ajaxLoad(ajax_Url);
    }
}

function ajaxLoad(url) {
    $.ajax({
    	url: url,
    	type : 'GET',
    	async: false,
    	beforeSend: function() {
            NProgress.start();
            $('.content-loader').show();
        },
        success: function(result) {
            NProgress.done();
            $(".wrapper > .content").html(result);
        },
        error: function(status, error) {
            alert("page not found !!!");
        	//window.location.href = "#/widgets/page-not-found.html";
        	//ajaxLoad("/widgets/page-not-found");
        },
        complete: function(status) {
            allFunctions();
            $('.content-loader').fadeOut(500);

            setTimeout(function() {
                $("body,html").animate({
                    scrollTop: 0
                }, 500);
            }, 1000);
        },
    });
}

/*If selection supports to multiple then expCount should be 0 else 1(Single Selection)
Execute URL and received list will be append to the select box, Which will have key, value.
*/
function appendOptions(url, appendId, key, value, expCount) {
	var optionCount = $(appendId+' option').length;
	if(optionCount === expCount){
		$.ajax({
			type : 'GET',
			url : url,
			async: false,
			cache: true,
			success : function(response, status, xhr) {
				appendObjectOptions(response.data, appendId, key, value, expCount);
			},
			error : function(jqXhr, textStatus, errorMessage) {
				message('error', jqXhr.responseJSON.message);
			}
		});
    }
}

function persist(url, ObjectArray)  {
	var inputData = {};
	inputData = ObjectArray;
	var result = false;
	$.ajax({
		type: 'POST',
		url: url,
		data: JSON.stringify(inputData),
		contentType: 'application/json',
		async: false,
		success: function (response, status, xhr) {
			result = response.data;
		},
		error: function (jqXhr, textStatus, errorMessage) {
			message('error', jqXhr.responseJSON.message);
		}
	});
	return result;
}

function patchByJson(url, ObjectArray)  {
	var inputData = {};
	inputData = ObjectArray;
	var result = false;
	$.ajax({
		type: 'PATCH',
		url: url,
		data: JSON.stringify(inputData),
		contentType: 'application/json',
		async: false,
		success: function (response, status, xhr) {
			result = response.data;
		},
		error: function (jqXhr, textStatus, errorMessage) {
			message('error', jqXhr.responseJSON.message);
		}
	});
	return result;
}

function get(url, ...val)  {
	var result = {};
	$.each(val, function(index, value) {
		url = url + "/" +value;
	});
	
	$.ajax({
		type: 'GET',
		url: url,
		async: false,
		cache: true,
		success: function (response, status, xhr) {
			result = response.data;
		},
		error: function (jqXhr, textStatus, errorMessage) {
			message('error', jqXhr.responseJSON.message);
		}
	});
	
	return result;
}

function getEntity(url, ...val)  {
	var result = null;
	$.each(val, function(index, value) {
		url = url + "/" +value;
	});
	$.ajax({
		type: 'GET',
		url: url,
		async: false,
		cache: true,
		success: function (response, status, xhr) {
			result = response.data;
		},
		error: function (jqXhr, textStatus, errorMessage) {
			message('error', jqXhr.responseJSON.message);
		}
	});
	return result;
}

function getByJson(url, json)  {
	var inputData = {};
	jsonData = json;
	var result = null;
	$.ajax({
		type: 'POST',
		url: url,
		data: JSON.stringify(jsonData),
		contentType: 'application/json',
		//dataType: 'json',
		async: false,
		cache: true,
		success: function (response, status, xhr) {
			result = response.data;
		},
		error: function (jqXhr, textStatus, errorMessage) {
			message('error', jqXhr.responseJSON.message);
		}
	});
	
	return result;
}

function patch(url, ...val)  {
	var result = null;
	$.each(val, function(index, value) {
		url = url + "/" +value;
	});
	
	$.ajax({
		type: 'PATCH',
		url: url,
		async: false,
		success: function (response, status, xhr) {
			result = response.data;
		},
		error: function (jqXhr, textStatus, errorMessage) {
			message('error', jqXhr.responseJSON.message);
		}
	});
	
	return result;
}

jQuery.loadScript = function (url, callback) {
    jQuery.ajax({
        url: url,
        dataType: 'script',
        cache: true,
        success: callback,
        async: true
    });
}
