$(function() {
	//single selection pass 1
	appendOptions('/list/active/Vehicle', '#vehicle', 'id', 'number', 1);
	appendOptions('/list/active/Material', '#material', 'id', 'name', 1);
	appendOptions('/list/active/Site', '#site', 'id', 'name', 1);
	
	applyDateTimePicker("#orderDate");
	
	/*------Customer table Configuration Start-------*/
	var custTable = $('#search-customer-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "order": [[ 0, "desc" ]],
        "lengthMenu" : [ [ 5, 10 ], [ 5, 10 ] ],
	    "ajax": {
	    	"url": '/dtble/customer',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	"columns": [
    		{
	            data: 'createdDate'
	        },{
	            data: 'name'
	        }, {
	            data: 'phone'
	        }, {
	            data: 'isActive',
	            render : function (data) {
	            	if(data){
	            		return 'Yes';
	            	}
	            	
	            	return 'No';
				}
	        }
	    ],
	    "rowId": 'id',
	    "columnDefs" : [
	    	{
				"targets" : [ 0 ],
				"visible" : false,
				"searchable" : false,
			}],
		initComplete: function(){
		      var api = this.api();
		      $('.dataTables_filter input')
		          .off('.DT')
		          .on('keyup.DT', function (e) {
		              // If the length is 3 or more characters, or the user pressed ENTER, search
		              if(this.value.length > 2 || e.keyCode == 13) {
						if($.isNumeric(this.value)){
							custTable
							.column(2).search(this.value).draw();
						}else{
							custTable
							.column(1).search(this.value).draw();
						}
		              } else if(this.value.length < 2) {// Ensure we clear the search if they backspace far enough
		            	  //cTable.column(1).search("true", true, false).draw();
		              }
		          });
		   }
    });
	
	var addressTable = $('#address-selection-tbl').DataTable({
		"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"order": [],
		"retrieve": true,
		"columnDefs": [{
			"orderable": false,
			"targets": 'no-sort',
		}]
	});
	
	$("#customer").focus(function(){
		$('#search-customer-modal').modal('show');
	});
	
	$('#search-customer-tbl tbody').on( 'click', 'tr', function () {
		var customerId = custTable.row(this).node().id;
        var customerName = custTable.row(this).data()['name'];
		
        $('#search-customer-modal').modal('hide');
        custTable.clear().draw();
        var customer = get("/customer", customerId);
        $("#customer").attr("value", customer.name);
		$("#customer").attr("id2", customer.id);
		addressSelection(customer.address);
	});
	
	function addressSelection(addrList) {
		if(addrList.length == 1){
			$("#address-id").attr("value", addrList[0].id);
		}else{
			addressTable.clear().draw();
	    	$.each(addrList, function(index, rowData) {
	    		addressTable.row.add([rowData.street, rowData.city, rowData.type]).node().id = rowData.id;
	    		addressTable.draw(true);
	    	});
			
			$("#address-selection-modal").modal("show");
		}
	}
	
	$('#address-selection-tbl tbody').on( 'click', 'tr', function () {
		var addressId = addressTable.row(this).node().id;
		$("#address-id").attr("value", addressId);
		addressTable.clear().draw();
		$('#address-selection-modal').modal('hide');
	});
		
	/*------Customer table Configuration End-------*/
	
	$("#submitDC").click(function() {
		var dc = {};
		dc.orderDate = $("#orderDate").val();
		dc.material = $("#material").val();
		dc.customer = ($("#customer").attr('id2'));
		dc.address = $("#address-id").val();
		dc.vehicle = $("#vehicle").val();
		dc.qtyKg = $("#qtyKg").val();
		dc.qtyBrass = $("#qtyBrass").val();
		dc.site = $("#site").val();
		dc.remarks = $("#remarks").val();
		
		if(isNullOrUndef(dc.material) || dc.customer || dc.address || dc.vehicle || dc.site){
			message('error', 'Incomplete details !!!');
		} else{
			var result = persist('/deliveryChallan', dc)
			if(isNotNullOrUndef(result)){
				message('success', 'DC successfully placed : ' + result.sid);
				window.open("/pdf/dc/"+result.id, "Invoice");
				ajax();
			}
		}
	});
	
	$("#resetDC").click(function() {
		ajax();
	});
	
	$("#dcInvoice").click(function() {
		var id = prompt("Please Enter DC Id.");
		if(id !== null){
			if(!jQuery.isNumeric(id) || isNullOrUndef(id)){
				message('error', 'Please provide Id in valid format !!!');
			} else {
				getInvoice("dc", id);
			}
		}
	});
});