$(function() {
	appendOptions('/list/active/Site', '#site', 'id', 'name', 1);
	
    var recentDCTbl = $('#recentDC-Tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "order" : [ [ 0, "desc" ] ],
        "lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ],
	    "ajax": {
	    	"url": '/dtble/filter/deliveryChallan',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
            "data": function (d) {
            	// NOTE: you also need to stringify for POST payload
				return JSON.stringify({
					draw: d.draw,
					start: d.start,
					length: d.length,
					order: d.order[0],
					customer: $("#customer").attr("id2"),
					site: $("#site").val(),
					isBillable: $("#isBillable").val(),
					startDate: $("#dateRange").val().split("-")[0].trim(),
					endDate: $("#dateRange").val().split("-")[1].trim()
				});
				//return JSON.stringify(d);
				 // NOTE: you also need to stringify for POST payload
            }
	    },
    	columns: [
	        {
	            data: 'orderDate'
	        },
	        {
	            data: 'sid'
	        },
	        {
	            data: 'material.name'
	        },
	        {
	            data: 'qtyBrass'
	        },
	        {
	            data: 'customer.name'
	        },
	        {
	        	data: 'address',
	            mRender: function (data) {
	            	return getAddrString(data)
	            }
	        },
	        {
	            data: 'vehicle.number'
	        },
	        {
	        	data: 'id',
	            mRender: function (data, type, row) {
	            	return '<a href="javascript:void(0);" class="modify-material" id="' + row.id + '">modify</a>'
	            }
	        }
	    ],
    });
    
    $("#search").click(function() {	
    	recentDCTbl.clear;
    	recentDCTbl.draw();
	});
    
	/*------Customer table Configuration Start-------*/
	var custTable = $('#search-customer-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "order": [[ 0, "desc" ]],
        "lengthMenu" : [ [ 5, 10 ], [ 5, 10 ] ],
	    "ajax": {
	    	"url": '/dtble/customer',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	"columns": [
    		{
	            data: 'createdDate'
	        },{
	            data: 'name'
	        }, {
	            data: 'phone'
	        }, {
	            data: 'isActive',
	            render : function (data) {
	            	if(data){
	            		return 'Yes';
	            	}
	            	
	            	return 'No';
				}
	        }
	    ],
	    "rowId": 'id',
	    "columnDefs" : [
	    	{
				"targets" : [ 0 ],
				"visible" : false,
				"searchable" : false,
			}],
		initComplete: function(){
		      $('.dataTables_filter input')
		          .off('.DT')
		          .on('keyup.DT', function (e) {
		              // If the length is 3 or more characters, or the user pressed ENTER, search
		              if(this.value.length > 2 || e.keyCode == 13) {
						if($.isNumeric(this.value)){
							custTable
							.column(2).search(this.value).draw();
						}else{
							custTable
							.column(1).search(this.value).draw();
						}
		              } else if(this.value.length < 2) {// Ensure we clear the search if they backspace far enough
		            	  //cTable.column(1).search("true", true, false).draw();
		              }
		          });
		   }
    });
	
	$("#customer").focus(function(){
		$('#search-customer-modal').modal('show');
	});
	
	$('#search-customer-tbl tbody').on( 'click', 'tr', function () {
		var customerId = custTable.row(this).node().id;
        var customerName = custTable.row(this).data()['name'];
		
        $('#search-customer-modal').modal('hide');
        custTable.clear().draw();
        $("#customer").attr("value", customerName);
		$("#customer").attr("id2", customerId);
	});
		
	/*------Customer table Configuration End-------*/
});