$(function() {
	var gstAmt = 0, subTotal = 0, total = 0;
	var dcs = [];

	$("#orderDate").val(moment().format('YYYY-MM-DD HH:mm:ss'));
	appendOptions('/list/active/Site', '#site', 'id', 'name', 1);

	var dcTable = $('#dc-tbl')
			.DataTable(
					{
						"dom" : '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
						"order" : [],
						"retrieve" : true,
						"columnDefs" : [ {
							'targets' : [ 0, 6, 8 ], // column index
							'orderable' : false, // true or false
						} ],
					});

	var odcTable = $('#odc-tbl')
			.DataTable(
					{
						"dom" : '<"list-row-header"> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
						"order" : [],
						"retrieve" : true,
						"columnDefs" : [ {
							"orderable" : false,
							"targets" : 'no-sort',
						} ]
					});

	/*------Carousel Configuration-------*/
	$("#odc-carousel").carousel({
		interval : false
	});

	$("#nextToSlide1").click(function() {
		dcs = [];
		$.each($("input[name='dcCheckbox']:checked"), function() {
			dcs.push($(this).val());
		});

		if (dcs.length > 0) {
			var withGst = confirm("Bill with GST ???");
			gstAmt = 0, subTotal = 0, total = 0;

			if (withGst) {
				$("#gst").val("YES");
				handleODCTable(getByJson("/odcCalculation/YES", dcs));
				$("#odc-carousel").carousel(1);
			} else {
				$("#gst").val("NO");
				handleODCTable(getByJson("/odcCalculation/NO", dcs));
				$("#odc-carousel").carousel(1);
			}
		} else {
			message("error", "Please select first DC !!!");
		}
	});

	$("#backToSlide0").click(function() {
		$("#odc-carousel").carousel(0);
	});

	$("#backToSlide1").click(function() {
		$("#odc-carousel").carousel(1);
	});

	$("#nextToSlide2").click(function() {
		$("#gstAmt").val(gstAmt);
		$("#subTotal").val(subTotal);
		$("#total").val(total);
		$("#receivedAmt").val(total);

		$("#odc-carousel").carousel(2);
	});

	/*------Customer table Configuration Start-------*/
	var custTable = $('#search-customer-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "order": [[ 0, "desc" ]],
        "lengthMenu" : [ [ 5, 10 ], [ 5, 10 ] ],
	    "ajax": {
	    	"url": '/dtble/customer',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	"columns": [
    		{
	            data: 'createdDate'
	        },{
	            data: 'name'
	        }, {
	            data: 'phone'
	        }, {
	            data: 'isActive'
	        }
	    ],
	    "rowId": 'id',
	    "columnDefs" : [
	    	{
				"targets" : [ 0 ],
				"visible" : false,
				"searchable" : false,
			}],
		initComplete: function(){
		      var api = this.api();
		      $('.dataTables_filter input')
		          .off('.DT')
		          .on('keyup.DT', function (e) {
		              // If the length is 3 or more characters, or the user pressed ENTER, search
		              if(this.value.length > 2 || e.keyCode == 13) {
						if($.isNumeric(this.value)){
							custTable
							.column(2).search(this.value).draw();
						}else{
							custTable
							.column(1).search(this.value).draw();
						}
		              } else if(this.value.length < 2) {// Ensure we clear the search if they backspace far enough
		            	  //cTable.column(1).search("true", true, false).draw();
		              }
		          });
		   }
    });
	
	$("#search-customer-btn").click(function(){
		$('#search-customer-modal').modal('show');
	});
	
	$('#search-customer-tbl tbody').on( 'click', 'tr', function () {
		var customerId = custTable.row(this).node().id;
        var customerName = custTable.row(this).data()['name'];
		
        $('#search-customer-modal').modal('hide');
        custTable.clear().draw();
		
        $("#customer").attr("value", customerName);
		$("#customer").attr("id2", customerId);
		
		handleDCTable(customerId);
	});
		
	/*------Customer table Configuration End-------*/
	
	function handleDCTable(cId) {
		var dcList = get("/dc/customer", cId);
		dcTable.clear().draw(true);
		if(isNullOrUndef(dcList)){
			message('error', "Don't have pending delievery challan's !!!");
		}else{
			/*append checkbox infront of select column*/
			$('#checkboxhtml').html("<input type='checkbox' id='selectAllDcs'/>");
			
			$.each(dcList, function(index, dc) {
			var addr = dc.address;
			dcTable.row.add(
					[
							"<input type='checkbox' name='dcCheckbox' id='dcCheckbox' value='"
									+ dc.id + "'>",
							dc.sid,
							dc.orderDate,
							dc.material.name,
							dc.qtyBrass,
							dc.vehicle.number,
							addr.street + ", " + addr.city + ",<br>"
									+ addr.state.name + ", " + addr.pincode,
							dc.site.name, dc.remarks ]).node().id = dc.id;
			dcTable.draw(false);
			});
		}
	}

	$(document).on("click", '#selectAllDcs', function(event) {
		// $('[name="dcCheckbox"]').prop('checked', this.checked);
		$('[name="dcCheckbox"]').prop('checked', $(this).prop('checked'));
	});

	$("#proceed-order-btn")
			.click(
					function() {
						var order = {};
						order.orderDate = $("#orderDate").val();
						order.customer = ($("#customer").attr('id2'));
						order.dcs = dcs.join();
						order.gst = $("#gst").val();
						order.total = $("#total").val();
						order.site = $("#site").val();
						order.payMode = $("#payMode").val();
						order.remarks = $("#remarks").val();
						order.receivedAmt = $("#receivedAmt").val();

						if ($('#isPending').is(":checked")) {
							order.isPending = "true";
							order.pendingAmt = (parseInt($("#total").val()) - parseInt($(
									"#receivedAmt").val())).toString();
						} else {
							order.isPending = "false";
							order.pendingAmt = "0";
						}

						var result = persist("/order-dc", order);

						if (isNotNullOrUndef(result)) {
							message('success', 'Bill Successfully Generated : '
									+ result.sid);
							window.open("/pdf/od/" + result.id, "Invoice");
							ajax();
						}
					});

	function handleODCTable(list) {
		var oList = {};
		oList = list;
		odcTable.clear().draw(true);
		$
				.each(oList,
						function(index, order) {
							gstAmt = gstAmt + order.gstAmt;
							subTotal = subTotal + order.subTotal;
							total = total + order.total;

							odcTable.row.add(
									[ order.id, order.material.name,
											order.qtyBrass, order.rate,
											order.gst, order.gstAmt,
											order.subTotal, order.total ])
									.node().id = order.id;
						});
		odcTable.row.add([ "", "", "", "", "<b>Total</b>", gstAmt, subTotal,
				total ]);
		odcTable.draw(true);
	}

	$("#reset-btn").click(function() {
		$("#customer").val(null);
		$("#gst").val(null);
		dcs = [];
		dcTable.clear().draw(true);
	});

	$("#print-order-dcs").click(function() {
		var id = prompt("Please Enter Place Order Id.");
		if (id !== null) {
			if (!jQuery.isNumeric(id) || isNullOrUndef(id)) {
				message('error', 'Please provide Id in valid format !!!');
			} else {
				getInvoice("odc", id);
			}
		}
	});
});