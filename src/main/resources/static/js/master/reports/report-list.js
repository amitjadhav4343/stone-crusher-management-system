$(function() {
	appendOptions('/list/active/Site', '#site', 'id', 'name', 1);

	// jQuery DataTables initialization
	var ordersTbl = $('#ordersTbl')
			.DataTable(
					{
						"dom" : '<"list-row-header"<"page-length"l><"export-btns">B> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
						"processing" : true, // for show processing bar
						"serverSide" : true, // for process on server side
						"order" : [ [ 0, "desc" ] ],
						"lengthMenu" : [ [ 10, 25, 50, -1 ],
								[ 10, 25, 50, "All" ] ],
						"columnDefs" : [ {
									'targets' : [1, 3, 4, 5, 6, 7, 8, 9, 10,
											11, 12, 13, 14, 15, 16 ], // column
																		// index
									'orderable' : false, // true or false
								} ],
						"buttons" : [ {
							extend : 'collection',
							text : '<i class="fas fa-cloud-download-alt"></i>&nbsp;&nbsp;Export',
							className : 'btn btn-theme-secondary',
							"buttons" : [ 'copy', 'excel', 'csv', {
								extend : 'pdfHtml5',
								text : 'PDF',
								orientation : 'landscape',
								pageSize : 'LEGAL'
							}, 'print' ],
							"enabled" : true
						} ],
						"columns" : [ {
							data : 'orderDate'
						}, {
							data : 'customer.name'
						}, {
							data : 'id'
						}, {
							data : 'items',
							render : "[,<br/> ].material.name"
						}, {
							data : 'items',
							render : "[,<br/> ].qtyBrass"
						}, {
							data : 'items',
							render : "[,<br/> ].rate"
						}, {
							data : 'items',
							render : "[,<br/> ].subTotal"
						}, {
							data : 'items',
							render : "[,<br/> ].gst"
						}, {
							data : 'items',
							render : "[,<br/> ].gstAmt"
						}, {
							data : 'total',
						}, {
							data : 'discount',
						}, {
							data : 'receivedAmt',
						}, {
							data : 'isPending',
							render : function (data) {
				            	if(data){
				            		return 'Yes';
				            	}
				            	
				            	return 'No';
							}
						}, {
							data : 'pendingAmt',
						}, {
							data : 'payMode',
						}, {
							data : 'site.name',
						}, {
							data : 'remarks',
						} ],
						"ajax" : {
							"url" : '/dtble/filter/orders',
							"type" : "POST",
							"contentType" : "application/json; charset=utf-8",
							"data" : function(d) {
								// NOTE: you also need to stringify for POST payload
								return JSON.stringify({
									draw: d.draw,
									start: d.start,
      								length: d.length,
									order: d.order[0],
									customer: $("#customer").attr("id2"),
									site: $("#site").val(),
									pending: $("#isPending").val(),
									startDate: $("#dateRange").val().split("-")[0].trim(),
									endDate: $("#dateRange").val().split("-")[1].trim()
								});
								//return JSON.stringify(d);
								 // NOTE: you also need to stringify for POST payload
							}
						}
					});
	// reportTable.buttons().container().appendTo('.dataTables_wrapper
	// .export-btns');
	message("success", "default loaded daily report !!!");

	$("#search").click(function() {	
		ordersTbl.clear;
		ordersTbl.draw();
	});
	
	/*------Customer table Configuration Start-------*/
	var custTable = $('#search-customer-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "order": [[ 0, "desc" ]],
        "lengthMenu" : [ [ 5, 10 ], [ 5, 10 ] ],
	    "ajax": {
	    	"url": '/dtble/customer',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	"columns": [
    		{
	            data: 'createdDate'
	        },{
	            data: 'name'
	        }, {
	            data: 'phone'
	        }, {
	            data: 'isActive',
	            render : function (data) {
	            	if(data){
	            		return 'Yes';
	            	}
	            	
	            	return 'No';
				}
	        }
	    ],
	    "rowId": 'id',
	    "columnDefs" : [
	    	{
				"targets" : [ 0 ],
				"visible" : false,
				"searchable" : false,
			}],
		initComplete: function(){
		      $('.dataTables_filter input')
		          .off('.DT')
		          .on('keyup.DT', function (e) {
		              // If the length is 3 or more characters, or the user pressed ENTER, search
		              if(this.value.length > 2 || e.keyCode == 13) {
						if($.isNumeric(this.value)){
							custTable
							.column(2).search(this.value).draw();
						}else{
							custTable
							.column(1).search(this.value).draw();
						}
		              } else if(this.value.length < 2) {// Ensure we clear the search if they backspace far enough
		            	  //cTable.column(1).search("true", true, false).draw();
		              }
		          });
		   }
    });
	
	$("#customer").focus(function(){
		$('#search-customer-modal').modal('show');
	});
	
	$('#search-customer-tbl tbody').on( 'click', 'tr', function () {
		var customerId = custTable.row(this).node().id;
        var customerName = custTable.row(this).data()['name'];
		
        $('#search-customer-modal').modal('hide');
        custTable.clear().draw();
        $("#customer").attr("value", customerName);
		$("#customer").attr("id2", customerId);
	});
		
	/*------Customer table Configuration End-------*/
});