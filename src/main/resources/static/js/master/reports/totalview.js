$(function() {
	//getOptions('/list/active/Site', '#site', 'id', 'name', 1);
	var filter={};
	filter.startDate = getStartDateTime('YYYY-MM-DD');
	filter.endDate = getEndDateTime('YYYY-MM-DD');
	//loadTotalViewData();
	
	/*------Customer table Configuration Start-------*/
	var custTable = $('#search-customer-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "order": [[ 0, "desc" ]],
        "lengthMenu" : [ [ 5, 10 ], [ 5, 10 ] ],
	    "ajax": {
	    	"url": '/dtble/customer',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	"columns": [
    		{
	            data: 'createdDate'
	        },{
	            data: 'name'
	        }, {
	            data: 'phone'
	        }, {
	            data: 'isActive'
	        }
	    ],
	    "rowId": 'id',
	    "columnDefs" : [
	    	{
				"targets" : [ 0 ],
				"visible" : false,
				"searchable" : false,
			}],
		initComplete: function(){
		      var api = this.api();
		      $('.dataTables_filter input')
		          .off('.DT')
		          .on('keyup.DT', function (e) {
		              // If the length is 3 or more characters, or the user pressed ENTER, search
		              if(this.value.length > 2 || e.keyCode == 13) {
						if($.isNumeric(this.value)){
							custTable
							.column(2).search(this.value).draw();
						}else{
							custTable
							.column(1).search(this.value).draw();
						}
		              } else if(this.value.length < 2) {// Ensure we clear the search if they backspace far enough
		            	  //cTable.column(1).search("true", true, false).draw();
		              }
		          });
		   }
    });
		
	$("#customerName").focus(function(){
		$('#search-customer-modal').modal('show');
	});
	
	$('#search-customer-tbl tbody').on( 'click', 'tr', function () {
		var customerId = custTable.row(this).node().id;
        var customerName = custTable.row(this).data()['name'];
		
        $('#search-customer-modal').modal('hide');
        custTable.clear().draw();
        $("#customerName").attr("value", customerName);
		$("#customerName").attr("id2", customerId);
	});
		
	/*------Customer table Configuration End-------*/
	
	$("#search").click(function() {
		var dateRange = $("#dateRange").val().split("-");
		filter.customer = ($("#customerName").attr('id2'));
		filter.startDate = dateRange[0].trim();
		filter.endDate = dateRange[1].trim();
		filter.site = $("#site").val();
		loadTotalViewData();
	});
	
	function loadTotalViewData(){
		/*----- get and append values -----*/
		$('#total-sales-amount').text(getByJson("/total-sales-amount", filter));
		$('#total-received-amount').text(getByJson("/total-received-amount", filter));
		$('#total-pending-amount').text(getByJson("/total-pending-amount", filter));
		
		$('#total-orders').text(getByJson("/total-orders", filter));
		$('#total-delivery-challans').text(getByJson("/total-delivery-challans", filter));
		$('#total-discount-amount').text(getByJson("/total-discount-amount", filter));
	}
});