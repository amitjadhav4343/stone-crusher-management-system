var materialList = null;
var vehicleList = null;
var counter = 1;
var itemList = [];

$(function() {
	$("#total").val(parseInt(0).toFixed(2));
	
	vehicleList = get("/list/active/Vehicle");
	materialList = get("/list/active/Material");
	
	/* Multiple selection pass 0 */
	appendObjectOptions(vehicleList, '#vehicle0', 'id', 'number', 0);
	
	/* Single selection pass 1 */
	appendObjectOptions(materialList, '#material0', 'id', 'name', 1);
	appendOptions('/list/active/Site', '#site', 'id', 'name', 1);

	$("#nw-addr-type").append("<option value= 'BILLING'>BILLING</option>");
	$('#nw-addr-type').selectpicker('refresh');

	// Setting data picker to edit vehicle model
	setStartDatePicker("#nw-vehicle-regDate", moment());
	applyDatePicker("#nw-vehicle-regDate", 'YYYY-MM-DD');

	setEndDatePicker("#nw-vehicle-regUpto", moment());
	applyDatePicker("#nw-vehicle-regUpto", 'YYYY-MM-DD');
	
	applyDateTimePicker("#orderDate");

	$("#place-order-carousel").carousel({
		interval : false
	});

	$("#nw-back-vehicle, #nw-back-customer, #nw-back-material, #nw-back-address").click(function() {
		$("#place-order-carousel").carousel(0);
	});

	/*------Customer table Configuration Start-------*/
	var custTable = $('#search-customer-tbl')
			.DataTable(
					{
						"dom" : '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
						"processing" : true,
						"serverSide" : true,
						"order" : [ [ 0, "desc" ] ],
						"lengthMenu" : [ [ 5, 10 ], [ 5, 10 ] ],
						"ajax" : {
							"url" : '/dtble/customer',
							"type" : "POST",
							"contentType" : "application/json; charset=utf-8",
							"data" : function(d) {
								return JSON.stringify(d); // NOTE: you also
															// need to stringify
															// POST payload
							}
						},
						"columns" : [ {
							data : 'createdDate'
						}, {
							data : 'name'
						}, {
							data : 'phone'
						}, {
							data : 'isActive',
							render : function (data) {
				            	if(data){
				            		return 'Yes';
				            	}
				            	
				            	return 'No';
							}
						} ],
						"rowId" : 'id',
						"columnDefs" : [ {
							"targets" : [ 0 ],
							"visible" : false,
							"searchable" : false,
						} ],
						initComplete : function() {
							var api = this.api();
							$('.dataTables_filter input').off('.DT').on(
									'keyup.DT',
									function(e) {
										// If the length is 3 or more
										// characters, or the user pressed
										// ENTER, search
										if (this.value.length > 2
												|| e.keyCode == 13) {
											if ($.isNumeric(this.value)) {
												custTable.column(2).search(
														this.value).draw();
											} else {
												custTable.column(1).search(
														this.value).draw();
											}
										} else if (this.value.length < 2) {// Ensure
																			// we
																			// clear
																			// the
																			// search
																			// if
																			// they
																			// backspace
																			// far
																			// enough
											// cTable.column(1).search("true",
											// true, false).draw();
										}
									});
						}
					});

	$("#customer").focus(function() {
		$('#search-customer-modal').modal('show');
	});

	$('#search-customer-tbl tbody').on('click', 'tr', function() {
		var customerId = custTable.row(this).node().id;

		$('#search-customer-modal').modal('hide');
		custTable.clear().draw();
		var customer = get("/customer", customerId);
		$("#customer").attr("value", customer.name);
		$("#customer").attr("id2", customer.id);
		appendAddress(customer.address);
		
		//addressSelection(customer.address);
	});
	
	function appendAddress(addrList) {
		if (isNullOrUndef(addrList)) {
			$("#customer").removeAttr("value");
			$("#customer").removeAttr("id2");

			message("error", "Create address to customer !!!");

		} else {
			$('#address').empty();
			$.each(addrList, function(index, data) {
				$("#address").append($('<option>').val(data.id).text(getAddrString(data)));
			});
			$('#address').selectpicker('refresh');
		}
	}
	
	/*------Customer Script Start-------*/

	$("#nw-customer-carousel").carousel({
		interval : false
	});

	$("#new-customer-btn").click(
			function() {
				appendOptions('/list/active/States', '#nw-addr-state', 'id',
						'name', 1);
				$('#nw-addr-state').selectpicker('refresh');

				$("#place-order-carousel").carousel(2);
				$("#nw-customer-carousel").carousel(0);
			});

	$("#next-to-address").click(
			function() {
				if (isNullOrUndef($("#nw-customer-name").val())
						|| isNullOrUndef($("#nw-customer-phone").val())
						|| isNullOrUndef($("#nw-customer-email").val())
						|| isNullOrUndef($("#nw-customer-type").val())) {
					message('error', 'Incomplete details !!!');
				} else {
					$("#nw-customer-carousel").carousel(1);
				}
			});

	$("#nw-save-address").click(
			function() {
				var customer = {};
				customer.name = camelCase("#nw-customer-name");
				customer.email = $("#nw-customer-email").val();
				customer.gstNo = upperCase("#nw-customer-gst");
				customer.advance = $("#nw-customer-advance").val();
				customer.isActive = $("#nw-customer-status").val();
				customer.phone = $("#nw-customer-phone").val();
				customer.firm = $("#nw-customer-firm").val();
				customer.identity = upperCase("#nw-customer-identity");
				customer.customerType = $("#nw-customer-type").val();

				var states = {};
				states.id = $("#nw-addr-state").val();

				// customer address details
				var addressList = [];
				var address = {};
				address.street = camelCase("#nw-addr-street");
				address.city = camelCase("#nw-addr-city");
				address.pincode = $("#nw-addr-pincode").val();
				address.type = $("#nw-addr-type").val();
				address.isActive = $('#nw-addr-status').val();
				address.state = states;
				addressList.push(address);
				customer.address = addressList;

				if (isNullOrUndef(address.street) || isNullOrUndef(states.id)
						|| isNullOrUndef(address.type)) {
					message('error', 'Incomplete details !!!');
				} else {
					var res = persist('/customer', customer);
					if (isNotNullOrUndef(res)) {
						custTable.ajax.reload();
						$("#place-order-carousel").carousel(0);
						message('success', 'Customer saved successfully !!!');
					}
				}
			});

	/*------Customer Script End-------*/

	/*------Vehicle Script Start-------*/

	$("#new-vehicle-btn").click(function() {
		$("#nw-vehicle-number").val(null);
		$("#nw-vehicle-owner-name").val(null);
		$("#nw-vehicle-model").val(null);
		$("#nw-vehicle-regNo").val(null);
		$("#nw-vehicle-regDate").val(null);
		$("#nw-vehicle-regUpto").val(null);
		$("#nw-vehicle-status").val(null);

		$("#place-order-carousel").carousel(3);
	});

	$("#nw-save-vehicle").click(
			function() {
				var vehicleOwnerName = camelCase("#nw-vehicle-owner-name");
				var vehicleNumber = upperCase("#nw-vehicle-number");
				if (isNullOrUndef(vehicleOwnerName)
						|| isNullOrUndef(vehicleNumber)) {
					message('error', 'Incomplete details !!!');
				} else {
					var vehicle = {};
					vehicle.number = vehicleNumber;
					vehicle.ownerName = vehicleOwnerName;
					vehicle.model = camelCase("#nw-vehicle-model");
					vehicle.regNo = upperCase("#nw-vehicle-regNo");
					vehicle.regDate = $("#nw-vehicle-regDate").val();
					vehicle.regUpto = $("#nw-vehicle-regUpto").val();
					vehicle.isActive = $("#nw-vehicle-status").val();

					var result = persist('/vehicle', vehicle);
					if (isNotNullOrUndef(result)) {
						$('#vehicle').append(
								$('<option>').val(result.id)
										.text(result.number));
						$('#vehicle').selectpicker('refresh');
						$("#place-order-carousel").carousel(0);
						message('success', 'Vehicle Saved Successfully !!!');
					}
				}
			});

	/*------Vehicle Script End-------*/

	/*------Material Script Start-------*/

	$("#new-material-btn")
			.click(
					function() {
						appendOptions('/list/active/Gst', '#nw-material-gst',
								'id', 'value', 1);
						$('#nw-material-gst').selectpicker('refresh');

						$("#nw-material-name").val(null);
						$("#nw-material-gst option:selected").removeAttr(
								"selected");
						$("#nw-material-rate").val(null);
						$("#nw-material-hsn").val(null);
						$("#nw-material-status option:selected").removeAttr(
								"selected");

						$("#place-order-carousel").carousel(1);
					});

	$("#nw-save-material")
			.click(
					function() {
						var materialName = camelCase("#nw-material-name");
						var materialGst = $("#nw-material-gst").val();
						if (isNullOrUndef(materialName)
								|| isNullOrUndef(materialGst)) {
							message('error', 'Incomplete details !!!');
						} else {
							var material = {};
							material.name = materialName;
							material.rate = $("#nw-material-rate").val();
							material.hsn = camelCase("#nw-material-hsn");
							material.isActive = $("#nw-material-status").val();
							material.gstId = materialGst;

							var result = persist("/material", material);
							if (isNotNullOrUndef(result)) {
								$('#material').append(
										$('<option>').val(result.id).text(
												result.name));
								$('#material').selectpicker('refresh');
								$("#place-order-carousel").carousel(0);
								message('success',
										'Material saved successfully !!!');
							}
						}
					});

	$("#site").change(function() {
		$("#receivedAmt").val($("#total").val());
	});
	
	$("#payMode").change(function() {
		if(isNullOrUndef(itemList)){
			message('error', 'Please add item first !!!');
		}else{
			var payMode = $("#payMode").val();
			if(payMode == "ADVANCE"){
				var url = "/balanceCheck/customer/" + $("#customer").attr('id2') + "/amount/" + $("#total").val();
				var balanceCheck = get(url);
				if(!balanceCheck){
					message('error', 'Insufficient balance !!!');
				}
			}
			
			$("#receivedAmt").val($("#total").val());
		}
	});
	
	/*Order validation function*/
	function validateOrder(){
		if(isNullOrUndef($("#customer").val())) {
			message("error", "Customer field should not be blank !!!");
			return false;
	    } else if(isNullOrUndef($("#address").val())) {
			message("error", "Address field should not be blank !!!");
			return false;
	    } else if(isNullOrUndef($("#payMode").val())) {
			message("error", "Payment Mode field should not be blank !!!");
			return false;
	    } else if(isNullOrUndef($("#site").val())) {
			message("error", "Site field should not be blank !!!");
			return false;
	    } else if(isNullOrUndef($("#receivedAmt").val())) {
			message("error", "Received Amount field should not be blank !!!");
			return false;
	    } else{
	    	return true;
	    }
	}

	$("#save-order-btn").click(function() {
		if (validateOrder()){
			var order = {};
			order.orderDate = $("#orderDate").val();
			order.customer = ($("#customer").attr('id2'));
			order.total = $("#total").val();
			order.payMode = $("#payMode").val();
			order.site = $("#site").val();
			order.address = $("#address").val();
			order.receivedAmt = $("#receivedAmt").val();
			order.remarks = $("#remarks").val();
			order.items = itemList;

			if ($('#isPending').is(":checked")) {
				order.isPending = "true";
				order.pendingAmt = (parseInt($("#total").val()) - parseInt($(
						"#receivedAmt").val())).toString();
			} else {
				order.isPending = "false";
				order.pendingAmt = "0";
			}

			var result = persist('/order', order);
			if (isNotNullOrUndef(result)) {
				message('success',
						'Order successfully placed : '
								+ result.sid);
				window.open("/pdf/place-order/" + result.id, "Invoice");
				ajax();
			}
		}
	});

	$("#resetOrdersBtn").click(function() {
		ajax();
	});

	$("#duplicateInvoice").click(function() {
		var id = prompt("Please Enter Place Order Id.");
		if (id !== null) {
			if (!jQuery.isNumeric(id) || isNullOrUndef(id)) {
				message('error', 'Please provide Id in valid format !!!');
			} else {
				getInvoice("place-order", id);
			}
		}
	});
});

$("#add-new-row").click(function(){
	addNewItem();
});

function addNewItem(){
	$("#item-tbl").append("<tr id = "+ counter +"><td><select title=' Material ' name='"+ counter +"' class='form-control selectpicker' onchange='getGst(this.value, this.name)' id='material"+ counter +"'></select></td>" +
			"<td><input type='text' name='"+ counter +"' id='qtyKg"+ counter +"' class='form-control'></td>"+
			"<td><input type='text' name='"+ counter +"' id='qtyBrass"+ counter +"' class='form-control'></td>"+
			"<td><input type='text' name='"+ counter +"' id='rate"+ counter +"' onfocusout='itemCalc(this.name)' class='form-control'></td>"+
			"<td><select data-live-search='true' name='"+ counter +"' class='form-control selectpicker' title='Vehicle'id='vehicle"+ counter +"' multiple></select></td>"+
			"<td><select name='"+ counter +"' title=' GST ' id='gst"+ counter +"' class='form-control selectpicker' onchange='itemCalc(this.name)'></select></td>"+
			"<td><input type='text' name='"+ counter +"' id='subTotal"+ counter +"' readonly='readonly' class='form-control' value='0.00'></td>"+
			"<td><input type='text' name='"+ counter +"' id='gstAmt"+ counter +"' readonly='readonly' class='form-control value='0.00''></td>"+
			"<td><input type='text' name='"+ counter +"' id='iTotal"+ counter +"' readonly='readonly' class='form-control' value='0.00'></td>"+
			"<td><span title='Save Item' id='"+ counter +"' class='save-item' style='font-size:20px; cursor: pointer;'>&#x2714;</span></tr>");

	appendObjectOptions(materialList, "#material"+ counter, 'id', 'name', 0);
	$("#material"+ counter).selectpicker('refresh');
	
	appendObjectOptions(vehicleList, "#vehicle"+ counter, 'id', 'number', 0);
	$("#vehicle"+ counter).selectpicker('refresh');
	
	$("#gst"+ counter).selectpicker('refresh');
	
	counter++;
}

/*Item validation part*/
function validateItem(id){
	if(isNullOrUndef($("#material"+id).val())) {
		message("error", "Material field should not be blank !!!");
		return false;
    } else if(isNullOrUndef($("#qtyKg"+id).val())) {
		message("error", "Qty in KG field should not be blank !!!");
		return false;
    } else if(isNullOrUndef($("#qtyBrass"+id).val())) {
		message("error", "Qty in Brass field should not be blank !!!");
		return false;
    } else if(isNullOrUndef($("#rate"+id).val())) {
		message("error", "Rate field should not be blank !!!");
		return false;
    } else if(isNullOrUndef($("#vehicle"+id).val())) {
		message("error", "Vehicle field should not be blank !!!");
		return false;
    } else if(isNullOrUndef($("#iTotal"+id).val())) {
		message("error", "Item Total field should not be blank !!!");
		return false;
    } else{
    	return true;
    }
}

$("#item-tbl").on("click", ".delete-item", function() {
	   revertMaterial(this.id);
	   revertVehicle(this.id);
	   revertTotal(this.id);
	   $(this).closest("tr").remove();
	   message("success", "Item removed successfully !!!");
});

$("#item-tbl").on("click", ".save-item", function() {
	if(validateItem(this.id) && addToCart(this.id)){
		message("success", "Item added successfully !!!");
		$(this).replaceWith( ("<span title='Delete Item' id='" + this.id + "' class='delete-item' style='font-size:20px; cursor: pointer;'>&#x2718;</span>"));
	}
});

function revertTotal(id){
	var total = $("#total").val();
	var iTotal = $("#iTotal"+id).val();
	
	var rtotal = parseInt(total) - parseInt(iTotal);
	$("#total").val(parseFloat(rtotal).toFixed(2));
}

function addToCart(id){
	var item = {};
	item.material = $("#material"+id).val();
	item.qtyKg = $("#qtyKg"+id).val();
	//alert(id + ", " + item.material + ", " + item.qtyKg);
	item.qtyBrass = $("#qtyBrass"+id).val();
	item.rate = $("#rate"+id).val();
	
	var vehicles = [];
	$.each($("#vehicle"+id+" option:selected"), function() {
		var vehicleId = $(this).val();
		vehicles.push(vehicleId);

		// get index of object with id of vehicle
		const removeIndex = vehicleList.findIndex(v => v.id == vehicleId);
		vehicleList.splice(removeIndex, 1);
	});
	
	if (isNullOrUndef($("#gst"+id).val())) {
		item.withGst = "false";
		item.gst = "0";
		item.gstAmt = "0";
	} else {
		item.withGst = "true";
		item.gst = $("#gst"+id).val();
		item.gstAmt = $("#gstAmt"+id).val();
	}
	
	item.vehicle = vehicles.toString();
	item.subTotal = $("#subTotal"+id).val();
	itemList.push(item);
	
	var iTotal = parseFloat(item.subTotal) + parseFloat(item.gstAmt);
	$("#iTotal" + id).val(parseFloat(iTotal).toFixed(2));
	
	// adding order amount in existing order amount
	var ordersTotal = parseFloat(iTotal) + parseFloat($("#total").val());
	$("#total").val(parseFloat(ordersTotal).toFixed(2));
	
	// get index of object with id of item.material
	const removeIndex = materialList.findIndex(m => m.id == item.material);
	materialList.splice(removeIndex, 1);
	return true;
}

function getGst(materialId, counter){
	$("#gst" + counter).empty();
	var material = get("/material", materialId);
	if (isNotNullOrUndef(material.gst)) {
		$("#rate" + counter).val(parseInt(material.rate).toFixed(2));
		$("#gst" + counter).append(
				$('<option>').val("").text("NO GST"));
		$("#gst" + counter).append(
				$('<option>').val(material.gst.value).text(
						material.gst.value));
		$("#gst" + counter).selectpicker('refresh');
	} else {
		message('warning', "Material don't have GST");
	}
}

function revertMaterial(id){
	var material = {};
	material.id = $("#material"+id+" option:selected").val();
	material.name = $("#material"+id+" option:selected").text();
	materialList.push(material);
}

function revertVehicle(id){
	$.each($("#vehicle"+id+" option:selected"), function() {
		var vehicle = {};
		vehicle.id = $("#vehicle"+id+" option:selected").val();
		vehicle.number = $("#vehicle"+id+" option:selected").text();
		vehicleList.push(vehicle);
	});
}

function itemCalc(id){
	var qtyBrass = $("#qtyBrass" + id).val();
	var rate = $("#rate" + id).val();
	var gst = $("#gst" + id).val();
	
	var subTotal = qtyBrass * rate;
	$("#subTotal" + id).val(parseInt(subTotal).toFixed(2));
	
	var gstAmt = 0;
	if (isNotNullOrUndef(gst)) {
		gstAmt = (parseInt(subTotal) * parseInt(gst)) / 100;
	}
	$("#gstAmt" + id).val(parseInt(gstAmt).toFixed(2));
	
	var iTotal = parseInt(subTotal) + parseInt(gstAmt);
	$("#iTotal" + id).val(parseInt(iTotal).toFixed(2));
}