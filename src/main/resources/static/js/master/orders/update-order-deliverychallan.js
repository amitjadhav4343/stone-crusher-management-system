$(function() {
	var dcTable = $('#dcTable')
	.DataTable(
			{
				"dom" : '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
				//"order" : [],
				"ordering": false,
				"retrieve" : true,
				'rowsGroup': [1],
				/*"columnDefs" : [ {
					"orderable" : false,
					"targets" : 'no-sort',
				} ]*/
			});

	$("#placeOrderBtn").click(function() {
		var id = prompt("Please Enter Order Id");
		if (id !== null) {
			if (!jQuery.isNumeric(id) || isNullOrUndef(id)) {
				message('error', 'Please provide valid Order Id !!!');
			} else {
				handleDCTable(id);
			}
		}
	});
	
	function handleDCTable(oId) {
		var itemList = get("/items/order", oId);
		dcTable.clear().draw(true);
		$.each(itemList, function(index, item) {
			var count = 0;
			$.each(item.deliveryChallans, function(index, dc) {
				if(count == 0){
					dcTable.row.add([ item.material.name, item.qtyBrass, numberInString("DC", dc.id, 10), dc.vehicle.number, dc.vehicle.model, dc.qtyBrass, dc.qtyKg, "<a href='javascript:void(0);' class='edit' id='"+dc.id+"'>edit</a>" ]).node().id = dc.id;
					count++;
				} else{
					dcTable.row.add([ "", "", numberInString("DC", dc.id, 10), dc.vehicle.number, dc.vehicle.model, dc.qtyBrass, dc.qtyKg, "<a href='javascript:void(0);' class='edit' id='"+dc.id+"'>edit</a>" ]).node().id = dc.id;
				}
			});
		});
		dcTable.draw(true);
	}
	
	$('#dcTableBody').on( 'click', '.edit', function () {
		//$(this).closest('tr').index();
		var row =$(this).closest("tr");
		var rowIndex = row.index();
		
		$("#rowIndex").val(rowIndex);
		$("#dcId").val($(this).attr('id'));
		$("#dcNumber").val(row.find("td:eq(2)").text());
		$("#vehicleNo").val(row.find("td:eq(3)").text());
		$("#qtyBrass").val(row.find("td:eq(5)").text());
		$("#qtyKg").val(row.find("td:eq(6)").text());
		$('#updatePLDCModal').modal('show');
		
		//var dcId = $(this).attr('id');
		//alert("dcId : " + dcId + ", rowIndex : " + rowIndex);
		
		//alert('Row index: '+dcTable.row(this).index());
		//alert("DC id : "+());
		
		//var currentRow=$(this).closest("tr");
		//var col1=currentRow.find("td:eq(0)").text();
		
		//currentRow.find("td:eq(0)").text("Amit");
		//var userRow = $('#dcTable tr').eq(rowIndex);
		//console.log(userRow);
		
		//dcTable.cell(rowIndex, 0).data('Angelica Ramos (UPDATED)').draw(false);
		
		/*$('.vehicleModalTitle').html("Modify Vehicle");
		$("#vehicleAction").val("Modify");
		setVehicleProperties(($(this).attr('id')));
		$('#updatePLDCModal').modal('show');*/
	});
	
	$("#updateDeliveryChallan").click(function() {
		var rowIndex = $("#rowIndex").val();
		var dc = {};
		dc.id = $("#dcId").val();
		dc.qtyBrass = $("#qtyBrass").val();
		dc.qtyKg = $("#qtyKg").val();

		var result = patchByJson('/deliveryChallan', dc);
		if(isNotNullOrUndef(result)){
			dcTable.cell(rowIndex, 5).data(dc.qtyBrass).draw(false);
			dcTable.cell(rowIndex, 6).data(dc.qtyKg).draw(false);
			message('success', 'Delivery Challan updated !!!');
		}
	});
	
	$("#printPLDC").click(function() {
		var id = prompt("Please Enter Order Id");
		if(id !== null){
			if(!jQuery.isNumeric(id) || isNullOrUndef(id)){
				message('error', 'Please provide valid Order Id !!!');
			} else {
				getInvoice("odc", id);
			}
		}
	});
});