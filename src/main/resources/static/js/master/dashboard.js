$(function() {
	var filter={};
	filter.startDate = getCurrDate('YYYY-MM-DD');
	filter.endDate = getCurrDate('YYYY-MM-DD');
	var ordersChartJson = getByJson("/material-wise-count", filter);
	bindDataContainer();
	
	function bindDataContainer(){
		drawBarChart("container", "13px", "Material wise report "+filter.startDate+" to "+filter.endDate, ordersChartJson);
	}

	/*----- Updated Rates table logic -----*/
	var materialRateTbl = $('#materialRateTbl').DataTable({
		"retrieve": true,
		"paging":   false,
        "ordering": false,
        "info":     false,
        "searching": false
	});
	
	var dataList = get("/keyValueList", "Material", "name", "rate");
	materialRateTbl.clear().draw();
	$.each(dataList, function(index, rowData) {
		materialRateTbl.row.add([rowData.column1, rowData.column2]);
		materialRateTbl.draw(true);
	});
	
	/*----- get and append values -----*/
	$('#total-sales-amount').text(getByJson("/total-sales-amount", filter));
	$('#total-received-amount').text(getByJson("/total-received-amount", filter));
	$('#total-pending-amount').text(getByJson("/total-pending-amount", filter));
	$('#total-orders').text(getByJson("/total-orders", filter));
});