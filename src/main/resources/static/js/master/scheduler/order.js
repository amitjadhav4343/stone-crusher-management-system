$(function() {
	$("#nw-job-startDate").daterangepicker({
		locale: {
            format: "YYYY-MM-DD HH:mm:ss"
        },
        applyButtonClasses: "btn-theme-primary",
        cancelClass: "btn-theme-secondary",
        timePicker24Hour: true,
        startDate: moment(),
        open: "center",
        timePicker: true,
        autoApply: true,
        minDate: moment(),
        showDropdowns: true,
        singleDatePicker: true,
        parentEl: 'body'
	});
	
	var startDate = moment();
    var endDate = moment();
    var period = 'Daily';
	
    $("#nw-job-period").daterangepicker({
        startDate: startDate,
        endDate: endDate,
        autoApply: true,
        ranges: {
           'DAILY': [moment(), moment()],
           'WEEKLY': [moment().subtract(6, 'days'), moment()],
           'MONTHLY': [moment().subtract(29, 'days'), moment()],
        }
    }, function(start, end, label) {
    	startDate = start.format('MM-DD');
    	endDate = end.format('MM-DD');
    	period = label;
    });
	
	appendOptions('/list/active/Site', '#nw-job-site', 'id', 'name', 1);
	appendOptions('/list/active/Site', '#md-job-site', 'id', 'name', 1);
	
    var schedulerTable = $('#scheduler-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ],
	    "ajax": {
	    	"url": '/dtble/scms-jobs',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	columns: [
	        {
	            data: 'jobName'
	        },
	        {
	            data: 'cronDesc'
	        },
	        {
	            data: 'isActive',
	            render : function (data) {
		             return data == 'true' ? 'Yes' : 'No'
				}
	        },
	        {
	        	data: 'id',
	            mRender: function (data, type, row) {
	            	return '<a href="javascript:void(0);" class="modify-job" id="' + row.id + '">modify</a>'
	            }
	        }
	    ],
	    "columnDefs": [
	    	{ "orderable": false, "targets": 0 }
	    	]
    });
    
	/*------Customer table Configuration Start-------*/
	var custTable = $('#search-customer-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "order": [[ 0, "desc" ]],
        "lengthMenu" : [ [ 5, 10 ], [ 5, 10 ] ],
	    "ajax": {
	    	"url": '/dtble/customer',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	"columns": [
    		{
	            data: 'createdDate'
	        },{
	            data: 'name'
	        }, {
	            data: 'phone'
	        }, {
	            data: 'isActive',
	            render : function (data) {
	            	if(data){
	            		return 'Yes';
	            	}
	            	
	            	return 'No';
				}
	        }
	    ],
	    "rowId": 'id',
	    "columnDefs" : [
	    	{
				"targets" : [ 0 ],
				"visible" : false,
				"searchable" : false,
			}],
		initComplete: function(){
		      var api = this.api();
		      $('.dataTables_filter input')
		          .off('.DT')
		          .on('keyup.DT', function (e) {
		              // If the length is 3 or more characters, or the user pressed ENTER, search
		              if(this.value.length > 2 || e.keyCode == 13) {
						if($.isNumeric(this.value)){
							custTable
							.column(2).search(this.value).draw();
						}else{
							custTable
							.column(1).search(this.value).draw();
						}
		              } else if(this.value.length < 2) {// Ensure we clear the search if they backspace far enough
		            	  //cTable.column(1).search("true", true, false).draw();
		              }
		          });
		   }
    });
	
	$("#nw-job-customer").focus(function(){
		$('#search-customer-modal').modal('show');
	});
	
	$('#search-customer-tbl tbody').on( 'click', 'tr', function () {
		var customerId = custTable.row(this).node().id;
        var customerName = custTable.row(this).data()['name'];
		
        $('#search-customer-modal').modal('hide');
        custTable.clear().draw();
        $("#nw-job-customer").attr("id2", customerId);
        $("#nw-job-customer").attr("value", customerName);
		
	});
	/*------Customer Configuration-------*/
    
    /*------Carousel Configuration-------*/
	$("#scheduler-order-carousel").carousel({
		interval : false
	});
	
	$("#nw-back-order-job, #md-back-order-job").click(function() {
		$("#scheduler-order-carousel").carousel(0);
	});
	
	$("#new-order-job-btn").click(function() {
		$("#scheduler-order-carousel").carousel(1);
	});
	
	/*Job validation function*/
	function validateNewJob(){
		if(isNullOrUndef($("#nw-job-name").val())) {
			message("error", "Job name field should not be blank !!!");
			return false;
	    } else if(isNullOrUndef($("#nw-job-reportType").val())) {
			message("error", "Report Type field should not be blank !!!");
			return false;
	    } else if(isNullOrUndef($("#nw-job-fileType").val())) {
			message("error", "File Type field should not be blank !!!");
			return false;
	    } else if(isNullOrUndef(period)) {
			message("error", "Period field should not be blank !!!");
			return false;
	    } else{
	    	return true;
	    }
	}
	
	$("#nw-save-order-job").click(function() {
		var jobName = camelCase("#nw-job-name");
		var reportType = $("#nw-job-reportType").val();
		var fileType = $("#nw-job-fileType").val();
		
		if(validateNewJob()){
			var job = {};
			job.jobName = jobName;
			job.startDate = $("#nw-job-startDate").val();
			job.period = period;
			job.customerId = $("#nw-job-customer").attr('id2');
			job.isActive = $("#nw-job-status").val();
			
			//jobParm list
			var jobParms = [];
			
			var jobParm = {};
			jobParm.parmkey = "ReportType";
			jobParm.parmValue = reportType;
			jobParms.push(jobParm);
			
			jobParm = {};
			jobParm.parmkey = "FileType";
			jobParm.parmValue = fileType;
			jobParms.push(jobParm);
			
			jobParm = {};
			jobParm.parmkey = "Customer";
			jobParm.parmValue = $("#nw-job-customer").attr('id2');
			jobParms.push(jobParm);
			
			jobParm = {};
			jobParm.parmkey = "Site";
			jobParm.parmValue = $("#nw-job-site").val();
			jobParms.push(jobParm);
			
			jobParm = {};
			jobParm.parmkey = "Recipients";
			jobParm.parmValue = $("#nw-job-Recipients").val();
			jobParms.push(jobParm);
			
			if(period == "DoNotRepeat"){
				jobParm = {};
				jobParm.parmkey = "StartDate";
				jobParm.parmValue = startDate;
				jobParms.push(jobParm);
				
				jobParm = {};
				jobParm.parmkey = "EndDate";
				jobParm.parmValue = endDate;
				jobParms.push(jobParm);
			}
			
			period = period.toUpperCase();
			job.jobParms = jobParms;
			
			var res = persist('/scheduler-order', job);
    		if(isNotNullOrUndef(res)){
    			schedulerTable.ajax.reload();
    			$("#scheduler-order-carousel").carousel(0);
    			message('success', 'Job saved successfully !!!');
    		}
		} else{
			message('error', 'Please fill all details !!!');
		}
	});

	$('#scheduler-tbl-body').on( 'click', '.modify-job', function () {
		var job = get("/scmsJob", $(this).attr('id'));
		
		$("#md-job-id").val(job.id);
		$("#md-job-name").val(job.jobName);
		$('#md-job-period option[value='+job.period+']').prop('selected', 'selected').change();
		$("#md-job-startDate").val(job.startDate);
		$('#md-job-status option[value='+job.isActive+']').prop('selected', 'selected').change();
		
		var parmList = job.jobParms;
		//1.parm list	2.textbox-id	3.PARMKEY
		setJobParmsVal(parmList, "ReportType", "#md-job-reportType", "select");
		setJobParmsVal(parmList, "FileType", "#md-job-fileType", "select");
		setJobParmsVal(parmList, "Customer", "#md-job-customer", "text");
		setJobParmsVal(parmList, "Site", "#md-job-site", "select");
		setJobParmsVal(parmList, "Recipients", "#md-job-Recipients", "text");
		
		$("#scheduler-order-carousel").carousel(2);
	});
	
	$("#md-save-order-job").click(function() {
		var jobName = camelCase("#md-job-name");
		var reportType = $("#md-job-reportType").val();
		var period = $("#md-job-period").val();
		var fileType = $("#md-job-fileType").val();
		
		if(isNullOrUndef(jobName) || isNullOrUndef(reportType) || isNullOrUndef(period) || isNullOrUndef(fileType)){
			message('error', 'Please fill all details !!!');
		} else{
			var job = {};
			job.id = $("#md-job-id").val();
			job.jobName = jobName;
			job.startDate = $("#md-job-startDate").val();
			job.period = period;
			job.isActive = $("#md-job-status").val();
			
			//jobParm list
			var jobParms = [];
			
			var jobParm = {};
			jobParm.parmkey = "ReportType";
			jobParm.parmValue = reportType;
			jobParms.push(jobParm);
			
			jobParm = {};
			jobParm.parmkey = "FileType";
			jobParm.parmValue = fileType;
			jobParms.push(jobParm);
			
			jobParm = {};
			jobParm.parmkey = "Site";
			jobParm.parmValue = $("#md-job-site").val();
			jobParms.push(jobParm);
			
			jobParm = {};
			jobParm.parmkey = "Recipients";
			jobParm.parmValue = $("#md-job-recipients").val();
			jobParms.push(jobParm);

			job.jobParms = jobParms;
			
			var res = patchByJson('/scheduler-order', job);
    		if(isNotNullOrUndef(res)){
    			schedulerTable.ajax.reload();
    			$("#scheduler-order-carousel").carousel(0);
    			message('success', 'Job updated successfully !!!');
    		}
		}
	});
	
	function setJobParmsVal(parmList, parmKey, boxId, boxType){
		var list = {};
		list = parmList;
		
		$.each(list, function(index, parm) {
			if(parm.parmkey == parmKey && boxType == "select"){
				$(boxId + ' option[value='+parm.parmValue+']').prop('selected', 'selected').change();
			} else if(parm.parmkey == parmKey && boxType == "text"){
				$(boxId).val(parm.parmValue);
			}
		});
	}
	
	$("#check-customer-btn").click(function(){
		var customer = get("/customer", $("#md-job-customer").val());
		
		$("#md-job-customer").attr("id2", customer.id);
		$("#md-job-customer").val(customer.name);
	});
});