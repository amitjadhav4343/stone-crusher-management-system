$(function() {
	//jQuery DataTables initialization 
	var gTable = $('#gst-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true, // for show processing bar
        "serverSide" : true, // for process on server side
        "lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ],
    	"columns": [
	        {
	            "data": 'value',
	        },{
	            data: 'isActive',
	            render : function (data) {
	            	if(data){
	            		return 'Yes';
	            	}
	            	
	            	return 'No';
				}
	        },{
	        	data: 'id',
	            mRender: function (data, type, row) {
	            	return '<a href="javascript:void(0);" class="modify-gst" id="' + row.id + '">modify</a>'
	            }
	        }
	    ],
		"ajax": {
	    	"url": '/dtble/gst',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
	        "data": function ( d ) {
	            return JSON.stringify(d); // NOTE: you also need to stringify POST payload
	        }
	    }
    });
	
	$("#new-gst").click(function(){
		$("#nw-gst-value").val(null);
		$("#nw-gst-status").val(null);
		$('#new-gst-modal').modal('show');
	});
	
	$('#gst-tbl-body').on( 'click', '.modify-gst', function () {
		setGstProperties($(this).attr('id'));
		$('#modify-gst-modal').modal('show');
	});
	
	function setGstProperties(id){
		var gst = get("/gst", id);
		$("#md-gst-id").val(gst.id);
		$("#md-gst-value").val(gst.value);
		$('#md-gst-status option[value='+gst.isActive+']').prop('selected', 'selected').change();
	}
	
	$("#save-md-gst").click(function(){
		var gst = {};
		gst.id = $("#md-gst-id").val();
		gst.isActive = $("#md-gst-status").val();

		if(isNotNullOrUndef(patchByJson("/gst", gst))){
			gTable.clear().draw(true);
			gTable.ajax.reload();
			message('success', 'GST Updated Successfully !!!');
		}
	});
	
	$("#save-nw-gst").click(function(){
		var gst = {};
		gst.value = $("#nw-gst-value").val();
		gst.isActive = $("#nw-gst-status").val();

		gst = persist("/gst", gst);
		if(isNotNullOrUndef(gst)){
			gTable.ajax.reload();
			message('success', 'GST Saved Successfully !!!');
		}
	});
});