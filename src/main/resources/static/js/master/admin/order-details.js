$(function() {
	var itemsTbl = $('#itemsTbl').DataTable({
		"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"order": [],
		"retrieve": true,
		"ordering": false
	});
	
	var dcTbl = $('#dcTbl').DataTable({
		"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"order": [],
		"retrieve": true,
		"ordering": false
	});
	
	$("#searchOrder").click(function() {
		var id = prompt("Please Enter Order Id.");
		if(id !== null){
			if(!jQuery.isNumeric(id) || isNullOrUndef(id)){
				message('error', 'Please provide Id in valid format !!!');
			} else {
				var order = get("/order", id);
				if(isNotNullOrUndef(order)) {
					displayOrderInfo(order);
					displayItemsInfo(order.items);
				}
			}
		}
	});
	
	function displayOrderInfo(order){
		$("#id").text(order.sid);
		$("#customerName").text(order.customer.name);
		$("#total").text(order.total);
		$("#discount").text(order.discount);
		
		$("#orderAddress").html(getAddrString(order.address));

		$("#payMode").text(order.payMode);
		$("#dateType").text(order.dateType);
		$("#orderDate").text(order.orderDate);
		$("#customerPhone").text(order.customer.phone);
		$("#receivedAmt").text(order.receivedAmt);
		
		$("#pendingAmt").text(order.pendingAmt);
		$("#site").text(order.site.name);
		$("#orderType").text(order.orderType);
		$("#remarks").text(order.remarks);
		
		if(order.withGst){
			$('#withGst').prop('checked', true);
		}
		if(order.isDelivered){
			$('#isDelivered').prop('checked', true);
		}
		if(order.isPending){
			$('#isPending').prop('checked', true);
		}
		if(order.isSentMail){
			$('#isSentMail').prop('checked', true);
		}
		if(order.isSentSms){
			$('#isSentSms').prop('checked', true);
		}
	}
	
	function displayItemsInfo(itemList){
		var items = {};
		items = itemList;
		itemsTbl.clear().draw(true);
		dcTbl.clear().draw(true);
		
		$.each(items, function(index, item) {
			itemsTbl.row.add([item.material.name, item.qtyKg, item.qtyBrass, item.rate, item.subTotal, item.gst, item.gstAmt, item.total]).node().id = item.id;
			itemsTbl.draw(false);
			
			displayDCInfo(item.deliveryChallans);
		});
	}
	
	function displayDCInfo(dcList){
		var deliveryChallans = {};
		deliveryChallans = dcList;
		$.each(deliveryChallans, function(index, dc) {
			dcTbl.row.add([dc.sid, dc.orderDate, dc.material.name, dc.qtyKg, dc.qtyBrass, dc.vehicle.number, dc.site.name, getAddrString(dc.address), dc.isDelivered, dc.remarks]).node().id = dc.id;
			dcTbl.draw(false);
		});
	}
	
	function getCommaString(ll, column){
		var list = {};
		list = ll;
		var arr = [];
		$.each(list, function(index, data) {
			arr.push(eval('data.'+column));
			//alert(eval('data.'+column));
		});
		//alert(arr.join(','));
		return arr.join('<br>');
	}
});