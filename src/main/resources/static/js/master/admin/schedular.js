$(function() {
	var mTable = $('#materialTable').DataTable({
		"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"order": [],
		"retrieve": true,
		"columnDefs": [{
			"orderable": false,
			"targets": 'no-sort',
		}]
	});

	var gstList = get("/gst");
	appendObjectOptions(gstList, '#emGst', 'id', 'name');
	appendObjectOptions(gstList, '#amGst', 'id', 'name');
	handleMaterialTable(mTable, get("/material"));

	$('#materialTableBody').on( 'click', '.edit', function () {
		//clear selection of emGst
		$("#emGst option:selected").removeAttr("selected");
		
		//get data from row and append it to text box
		var data = mTable.row($(this).closest('tr')).data();
		$("#mId").val($(this).attr("id"));
		$("#emName").val(data[0]);
		$("#emRate").val(data[1]);
		$("#emStatus").val(data[3]).change();
		$.each(data[2].split(','), function(index, gst) {
			$("#emGst option").filter(function() {
			    return this.text == gst.trim(); 
			}).attr('selected', true);
		});
		$('#editMaterialModal').modal('show');
	});
	
	$("#updateMaterial").click(function(){
        if(isNullOrUndef($("#emName").val())){
        	message('error', 'Incomplete details...');
        }else{
        	var material = {};
    		var gsts = [];
    		
        	material["id"] = $("#mId").val();
    		material["name"] = $("#emName").val();
    		material["rate"] = $("#emRate").val();
    		material["status"] = $("#emStatus").val();
            $.each($("#emGst option:selected"), function(){    
            	gsts.push($(this).val());
            });
            
        	material["gsts"] = gsts.toString();
     		var result = patchByJson('/material', material);
    		if(result){
    			handleMaterialTable(mTable, get("/material"));
    			message('success', 'Material updated successfully');
    		}else{
    			message('error', 'Getting error while saving Material');
    		}
        }
	});
	
	$('#materialTableBody').on( 'click', '.delete', function () {
		//alert('delete operation Id: ' + $(this).attr("id"));
		patch("/toggleStatus", $(this).attr("id"), "INACTIVE", "Material");
		handleMaterialTable(mTable, get("/material"));
		message("success", "Material disabled successfully !!!");
	});
	
	function handleMaterialTable(mTable, list) {
		var mList = {};
		mList = list;
		mTable.clear().draw(true);
		$.each(mList, function(index, rowData) {
			var gsts = "";
			$.each(rowData.gst, function(index, gst) {
				gsts = gsts + gst.name + ", ";
			});
			
			gsts = gsts.substring(0, gsts.length-2);
			mTable.row.add([rowData.name, rowData.rate, gsts, rowData.status,"<a href='javascript:void(0);' class='edit' id='"+rowData.id+"'>edit</a>"]).node().id = rowData.id;
			mTable.draw(false);
		});
	}
	
	$("#newMaterialBtn").click(function(){
		alert("newMaterialBtn");
	});
});