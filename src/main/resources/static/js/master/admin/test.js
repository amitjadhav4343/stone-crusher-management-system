$(function() {
/*	var tTable = $('#testTable2').DataTable({
		"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"order": [],
		"retrieve": true,
		"columnDefs": [{
			"orderable": false,
			"targets": 'no-sort',
		}]
	});*/
    
/*	$('#testTblBdy').on( 'click', '.edit', function () {
		testTable.ajax.reload();
	});*/
	
	/*******************************************************************/
	
	getOptions('/list/active/Site', '#site', 'id', 'name', 1);
	var cTable = $('#cSearchedTable').DataTable({
		"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"order": [],
		"retrieve": true,
		"columnDefs": [{
			"orderable": false,
			"targets": 'no-sort',
		}]
	});
	
	var filter={};
	filter.startDate = getStartDateTime('YYYY-MM-DD');
	filter.endDate = getEndDateTime('YYYY-MM-DD');
	//var ordersDetailsList = getByJson("/report/list/odetails", filter);
	
/*	var reportTable = $('#ordersDetailsTable').DataTable( {
		"dom": '<"list-row-header"<"page-length"l><"export-btns">> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"ordering": false,
		"order": [2],
		"retrieve": true,
		"columnDefs": [{
			"orderable": false,
			"targets"  : 'no-sort'
		}],
		"buttons": [{
			extend: 'collection',
			text: '<i class="fas fa-cloud-download-alt"></i>&nbsp;&nbsp;Export',
			className: 'btn btn-theme-secondary',
			"buttons": [
				'copy',
				'excel',
				'csv',
				{
		            extend: 'pdfHtml5',
		            text: 'PDF',
		            orientation : 'landscape',
	                pageSize : 'LEGAL'
		        },
				'print'
			],
			"enabled": true
		}]
	});*/
	
    var reportTable = $('#ordersDetailsTable').DataTable({
    	"dom": '<"list-row-header"<"page-length"l>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ],
	    "ajax": {
	    	"url": '/dataTable/ordersDetails',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
            "data": function ( d ) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	columns: [
	        {
	            data: 'id'
	        },
	        {
	            data: 'customer.name'
	        }
	    ],
	    "columnDefs": [{
			"orderable": false,
			"targets"  : 'no-sort'
		}],
    });
	
	//reportTable.buttons().container().appendTo('.dataTables_wrapper .export-btns');
	//handleOrderDetailsReport(reportTable, ordersDetailsList);
	message("success", "default loaded daily report !!!");
	
	function handleOrderDetailsReport(tableObject, odList) {
		var inputData = {};
		inputData = odList;
		tableObject.clear().draw();
		$.each(inputData, function(index, od) {
			var count = 0;
			$.each(od.orders, function(index, o) {
				if(count == 0){
					tableObject.row.add([od.orderDate, od.customer.name, od.id, o.material.name, o.qtyBrass, 
						o.rate, o.subTotal, o.gst + "%", o.gstAmt, od.total, 
						od.payMode, od.site.name, od.isPending, od.receivedAmt, od.pendingAmt, od.remarks]).draw(true);
					count++;
				} else{
					tableObject.row.add([null, null, od.id, o.material.name, o.qtyBrass, 
						o.rate, o.subTotal, o.gst + "%", o.gstAmt, null, null, null, null, null,
						null, null]).draw(true);
				}
			});
		});
	}
	
	$("#customerName").focus(function(){
		$('#searchCustomerModal').modal('show');
	});
	
	/*------Search Customer -------*/
	$("#searchCustomerBy").click(function() {
		var searchBy = $("#customerSearchBy").val();
		var searchVal = $("#customerSearchValue").val();
		
		isNullOrUndef(searchBy) || isNullOrUndef(searchVal) ? message("error", "Incomplete details !!!") : searchCustomer(searchBy, searchVal);
	});
	
	function searchCustomer(searchBy, searchVal) {
		var cList = get("/customers", searchBy, searchVal);
		cTable.clear().draw();
		$.each(cList, function(index, rowData) {
			cTable.row.add([rowData.name, rowData.phone]).node().id = rowData.id;
			cTable.draw(true);
		});
	}
	
	$('#cSearchedTable tbody').on( 'click', 'tr', function () {
		var cId = cTable.row(this).node().id;
		var cName = cTable.row(this).data()[0];

		$("#customerName").attr("value", cName);
		$("#customerName").attr("id2", cId);
		cTable.clear().draw();
		$('#searchCustomerModal').modal('hide');
	});
	
	$("#search").click(function() {
		var filter={};
		var customer = null;
		var isPending = null;
		var site = null;
		var dateRange = $("#dateRange").val().split("-");
		startDate = dateRange[0].replace(/\//g, "-").trim();
		endDate = dateRange[1].replace(/\//g, "-").trim();
		filter.customer = ($("#customerName").attr('id2'));
		
		if($("#isPending").is(':checked')){
			filter.isPending = "YES";
		}
		filter.startDate = startDate + " 00:00:01";
		filter.endDate = endDate + " 23:59:59";
		filter.site = $("#site").val();
		ordersDetailsList = getByJson("/report/list/odetails", filter);
		handleOrderDetailsReport(reportTable, ordersDetailsList);
	});
});