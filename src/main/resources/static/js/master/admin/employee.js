$(function() {
	var employee = {};
	
	// Setting date picker to joining date of new employee
	setDatePicker("#nw-emp-joiningDate", moment());
	applyDatePicker("#nw-emp-joiningDate", 'YYYY-MM-DD');
	
	setDatePicker("#nw-emp-birthDate", moment());
	applyDatePicker("#nw-emp-birthDate", 'YYYY-MM-DD');
	
	setDatePicker("#md-emp-resignedDate", moment());
	applyDatePicker("#md-emp-resignedDate", 'YYYY-MM-DD');
	
	setDatePicker("#md-emp-endDate", moment());
	applyDatePicker("#md-emp-endDate", 'YYYY-MM-DD');
	
	/*get options and append to select box*/
	appendOptions("/list/active/States", "#addr-state", "id", "name", 1);
	appendListOptions(get("/employee/addressType"), "#addr-type", 1);
	var designationList = get("/emoloyee/designation");
	appendListOptions(designationList, '#nw-emp-designation', 1);
	appendListOptions(designationList, '#md-emp-designation', 1);
	
	var empTbl = $('#search-employee-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "order": [[ 0, "asc" ]],
        "lengthMenu" : [ [ 10, 25, 50], [ 10, 25, 50] ],
	    "ajax": {
	    	"url": '/dtble/employee',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	"columns": [
			{
	            data: 'name'
	        }, {
	            data: 'phone'
	        }, {
				data: 'designation'
			}, {
	            data: 'isActive'
	        }
	    ],
	    "rowId": 'id',
		"initComplete": function(){
		      $('.dataTables_filter input')
		          .off('.DT')
		          .on('keyup.DT', function (e) {
		              // If the length is 3 or more characters, or the user pressed ENTER, search
		              if(this.value.length > 2 || e.keyCode == 13) {
						if($.isNumeric(this.value)){
							empTbl
							.column(0).search(this.value).draw();
						}else{
							empTbl
							.column(1).search(this.value).draw();
						}
		              } else if(this.value.length < 2) {// Ensure we clear the search if they backspace far enough
		            	  //cTable.column(1).search("true", true, false).draw();
		              }
		          });
		   }
    });
	
	var addrsTbl = addrsTbl = $('#addrsTbl').DataTable({
		"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"order": [],
		"retrieve": true,
		"columnDefs": [{
			"orderable": false,
			"targets": 'no-sort',
		}]
	});
	
	/*------Carousel Configuration-------*/
	$("#employee-carousel").carousel({
		interval : false
	});
	
	$("#new-employee").click(function() {
		$("#employee-carousel").carousel(1);
	});
	
	$("#nw-back-employee").click(function() {
		$("#employee-carousel").carousel(0);
	});
	
	$("#md-back-employee").click(function() {
		$("#employee-carousel").carousel(0);
	});
	
	$("#md-save-employee").click(function(){
		$("#employee-carousel").carousel(0);
	});
	
	$("#nw-save-employee").click(function(){
		$("#employee-carousel").carousel(0);
	});
	
	$("#search-emp-btn").click(function(){
		$('#search-employee-modal').modal('show');
	});
	
	$('#search-employee-modal tbody').on( 'click', 'tr', function () {
		var empId = empTbl.row(this).node().id;
        var empName = empTbl.row(this).data()['name'];

        $('#search-employee-modal').modal('hide');
		empTbl.clear().draw();
		employee = get("/employee", empId);
		displayEmpInfo(employee);
	});

	function displayEmpInfo(emp){
		$("#emp-id").text(emp.id);
		$('#emp-name').text(emp.name);
		$('#emp-phone').text(emp.phone);
		$('#emp-identity').text(emp.identity);
		$('#emp-joiningDate').text(emp.joiningDate);
		$('#emp-isActive').text(emp.isActive);
		
		$('#emp-designation').text(emp.designation);
		$('#emp-email').text(emp.email);
		$('#emp-salary').text(emp.salary);
		$('#emp-endDate').text(emp.endDate);
		
		displayAddrInfo(emp.address);
	}
	
	function displayAddrInfo(addList){
		var list = {};
		list = addList;
		addrsTbl.clear().draw(true);
		$.each(list, function(index, add) {
			addrsTbl.row.add([add.street, add.city, add.pincode, add.state.name, add.type, 
				add.isActive, "<a href='javascript:void(0);' class='modify-addr' id2='Employee' id='"+add.id+"'>modify</a>"]).node().id = add.id;
			addrsTbl.draw(false);
		});
	}
	
	$("#nw-save-employee").click(function(){
		var emp = {};
		emp.name = $("#nw-emp-name").val();
		emp.phone = $("#nw-emp-phone").val();
		emp.email = $("#nw-emp-email").val();
		emp.designation = $("#nw-emp-designation").val();
		emp.birthDate = $("#nw-emp-birthDate").val();
		emp.joiningDate = $("#nw-emp-joiningDate").val();
		emp.identity = $("#nw-emp-identity").val();
		emp.salary = $("#nw-emp-salary").val();
		emp.isActive = $("#nw-emp-isActive").val();
		
		var employee = persist("/employee", emp);
		
		if(isNotNullOrUndef(employee)) {
			message("success", "Employee saved successfully !!!");
			displayEmpInfo(employee);
		}
	});
	
	$("#modify-employee").click(function() {
		if(isNullOrUndef(employee)){
			message("error", "Please select Employee first !!!");
		}else{
			//clear selection of gst
			//$("#materialGst option:selected").removeAttr("selected");
			
			$("#md-emp-name").val(employee.name);
			$("#md-emp-phone").val(employee.phone);
			$("#md-emp-email").val(employee.email);
			$('#md-emp-designation option[value='+employee.designation+']').prop('selected', 'selected').change();
			$("#md-emp-joiningDate").val(employee.joiningDate);
			if(isNotNullOrUndef(employee.resignedDate)){
				setDatePicker("#md-emp-resignedDate", employee.resignedDate);
				$("#md-emp-resignedDate").val(employee.resignedDate);
			}
			if(isNotNullOrUndef(employee.endDate)){
				setDatePicker("#md-emp-endDate", employee.endDate);
				$("#md-emp-endDate").val(employee.endDate);
			}
			$("#md-emp-identity").val(employee.identity);
			$("#md-emp-salary").val(employee.salary);
			$('#md-emp-isActive option[value='+employee.isActive+']').prop('selected', 'selected').change();
			
			$("#employee-carousel").carousel(2);
		}
	});
	
	$("#md-save-employee").click(function(){
		var emp = {};
		emp.id = $("#emp-id").val();
		emp.name = $("#md-emp-name").val();
		emp.phone = $("#md-emp-phone").val();
		emp.email = $("#md-emp-email").val();
		emp.designation = $("#md-emp-designation").val();
		emp.joiningDate = $("#md-emp-joiningDate").val();
		emp.resignedDate = $("#md-emp-resignedDate").val();
		emp.endDate = $("#md-emp-endDate").val();
		emp.identity = $("#md-emp-identity").val();
		emp.salary = $("#md-emp-salary").val();
		emp.isActive = $("#md-emp-isActive").val();
		
		employee = patchByJson("/employee", emp);
		
		if(isNotNullOrUndef(employee)) {
			message("success", "Employee saved successfully !!!");
			displayEmpInfo(employee);
		}
	});
	
	/*------Address Configuration Start-------*/
	$("#new-address-btn").click(function(){
		if(isNullOrUndef(employee)){
			message("error", "Please select Employee first !!!");
		}else{
			$('.addr-modal-title').html("New Address");
			$("#addr-action").val("New");
			
			//setting null
			$("#addr-against-id").val(employee.id);
			$("#addr-against-class").val($(this).attr('id2'));
			$("#addr-street").val(null);
			$("#addr-city").val(null);
			$("#addr-pincode").val(null);
			$("#addr-state option[selected]").removeAttr("selected");
			$("#addr-type option[selected]").removeAttr("selected");  
			$("#addr-status option[selected]").removeAttr("selected");  
			
			$('#new-modify-address-modal').modal('show');
		}
	});
	
	$(document).on("click", '.modify-addr', function() {
		$('.addr-modal-title').html("Modify Address");
		$("#addr-action").val("Modify");
		$("#addr-against-id").val(employee.id);
		$("#addr-against-class").val($(this).attr('id2'));
		setAddressModelProperties(($(this).attr('id')));
		$('#new-modify-address-modal').modal('show');
	});
	
	function setAddressModelProperties(id){
		var add = get("/address", id);
		$("#addr-id").val(add.id);
		$("#addr-street").val(add.street);
		$("#addr-city").val(add.city);
		$("#addr-pincode").val(add.pincode);
		$('#addr-state option[value='+add.state.id+']').prop('selected', 'selected').change();
		$('#addr-type option[value='+add.type+']').prop('selected', 'selected').change();
		$('#addr-status option[value='+add.isActive+']').prop('selected', 'selected').change();
	}
	
	$("#save-address").click(function(){
		var address = {};
		var state = {};
		address.id = $("#addr-id").val();
		address.addrAgainstId = $("#addr-against-id").val();
		address.addrAgainstClass = $("#addr-against-class").val();
		address.street = camelCase("#addr-street");
		address.city = camelCase("#addr-city");
		address.pincode = $("#addr-pincode").val();
		state.id = $("#addr-state").val();
		state.name = $("#addr-state option:selected").text();
		state.isActive = "true";
		address.state = state;
		address.type = $("#addr-type").val();
		address.isActive = $("#addr-status").val();
		
		if($("#addr-action").val() === "New"){
			var addr = persist("/address", address);
			if(isNotNullOrUndef(addr)){
				addressTbl.row.add([addr.street, addr.city, addr.pincode, addr.state.name, addr.type, addr.isActive, "<a href='javascript:void(0);' class='modify-addr' id2='Employee' id='"+addr.id+"'>modify</a>"]).node().id = addr.id;
				addressTbl.draw(false);
				message('success', 'Address Saved Successfully !!!');
			}
		}else {
			var addrr = patchByJson("/address", address);
			if(isNotNullOrUndef(addrr)){
				var url = "/address/clazz/"+address.addrAgainstClass+"/id/"+address.addrAgainstId;
				var object = get(url);
				displayAddrInfo(object.address);
				message('success', 'Address Updated Successfully !!!');
			}
		}
	});
	/*------Address Configuration End-------*/
	
});