$(function() {

	var gstList = get('/list/active/Gst');
	appendObjectOptions(gstList, '#nw-material-gst', 'id', 'value', 1);
	appendObjectOptions(gstList, '#md-material-gst', 'id', 'value', 1);
	
	//jQuery DataTables initialization 
	var mTable = $('#materialTable').DataTable({
    	"dom": '<"list-row-header"<"page-length"l>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true, // for show processing bar
        "serverSide" : true, // for process on server side
        "lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ],
    	"columns": [
	        {
	            "data": 'name',
	        },
	        {
	            data: 'rate'
	        },
	        {
	            data: 'hsn'
	        },
	        {
	            data: 'gst.value'
	        },
	        {
	            data: 'isActive',
	            render : function (data) {
	            	if(data){
	            		return 'Yes';
	            	}
	            	
	            	return 'No';
				}
	        },
	        {
	        	data: 'id',
	            mRender: function (data, type, row) {
	            	return '<a href="javascript:void(0);" class="modify-material" id="' + row.id + '">modify</a>'
	            }
	        }
	    ],
		"ajax": {
	    	"url": '/dtble/material',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
	        "data": function ( d ) {
	            return JSON.stringify(d); // NOTE: you also need to stringify POST payload
	        }
	    }
    });
	
	/*------Carousel Configuration-------*/
	$("#material-carousel").carousel({
		interval : false
	});
	
	$("#new-material").click(function() {
		$("#nw-material-name").val(null);
		$("#nw-material-gst option:selected").removeAttr("selected");
		$("#nw-material-rate").val(null);
		$("#nw-material-hsn").val(null);
		$("#nw-material-status option:selected").removeAttr("selected");
		$("#material-carousel").carousel(1);
	});
	
	$('#material-tbl-body').on( 'click', '.modify-material', function () {
		setMaterialProperties($(this).attr('id'));
		$("#material-carousel").carousel(2);
	});
	
	$("#nw-save-material").click(function(){
		var materialName = camelCase("#nw-material-name");
		var materialGst = $("#nw-material-gst").val();
        if(isNullOrUndef(materialName) || isNullOrUndef(materialGst)){
        	message('error', 'Incomplete details...');
        }else{
        	var material = {};
    		material.name = materialName;
    		material.rate = $("#nw-material-rate").val();
    		material.hsn = camelCase("#nw-material-hsn");
    		material.isActive = $("#nw-material-status").val();
    		material.gstId = materialGst;
    		
    		material = persist("/material", material);
			if(isNotNullOrUndef(material)) {
				mTable.ajax.reload();
				$("#material-carousel").carousel(0);
				message('success', 'Material saved successfully');
			}
        }
	});
	
	$("#nw-back-material, #md-back-material").click(function() {
		$("#material-carousel").carousel(0);
	});
	
	$("#md-save-material").click(function(){
		var materialName = camelCase("#md-material-name");
		var materialGst = $("#md-material-gst").val();
        if(isNullOrUndef(materialName) || isNullOrUndef(materialGst)){
        	message('error', 'Incomplete details...');
        }else{
        	var material = {};
        	material.id = $("#md-material-id").val();
    		material.name = materialName;
    		material.rate = $("#md-material-rate").val();
    		material.hsn = camelCase("#md-material-hsn");
    		material.isActive = $("#md-material-status").val();
    		material.gstId = materialGst;
    		
    		material = patchByJson("/material", material);
			if(isNotNullOrUndef(material)) {
				mTable.ajax.reload();
				$("#material-carousel").carousel(0);
				message('success', 'Material updated successfully !!!');
			}
        }
	});

	function setMaterialProperties(id){
		var material = get("/material", id);
		
		$("#md-material-id").val(material.id);
		$("#md-material-name").val(material.name);
		$("#md-material-rate").val(material.rate);
		$("#md-material-hsn").val(material.hsn);
		$('#md-material-gst option[value='+material.gst.id+']').prop('selected', 'selected').change();
		$('#md-material-status option[value='+material.isActive+']').prop('selected', 'selected').change();
	}
});