$(function() {
	var siteTbl = $('#siteTbl').DataTable({
		"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"order": [],
		"retrieve": true,
		"columnDefs": [{
			"orderable": false,
			"targets": 'no-sort',
		}]
	});
	
	var company = get("/company/user", $("#user").val());
	displayCompanyInfo(company);
	displayBankInfo(company.bank);
	displaySiteInfo(company.site);

	appendOptions('/list/active/States', '#addr-state', 'id', 'name', 1);
	appendListOptions(get("/company/addressType"), "#addr-type", 1);
	
	//Set Date picker Site modify form
	setStartDatePicker("#siteRegDate");
	setEndDatePicker("#siteRegUpto");
	applyDatePicker("#siteRegDate", 'YYYY-MM-DD');
	applyDatePicker("#siteRegUpto", 'YYYY-MM-DD');
	
	//Set Date picker Company modify form
	setStartDatePicker("#mdCompanyRegDate");
	applyDatePicker("#mdCompanyRegDate", 'YYYY-MM-DD');

	function displayCompanyInfo(company){
		$("#companyRegNo").text(company.regNo);
		$("#companyName").text(company.name);
		$("#companyEmail").text(company.email);
		$("#companyGstNo").text(company.gstNo);
		
		if(isNotNullOrUndef(company.address)){
			$("#companyAddress").html(getAddrString(company.address) + "<br> <a href='javascript:void(0);' class='modify-addr'  id2='Company' id='"+company.address.id+"'>modify</a>");
		} else{
			$("#companyAddress").html("<a href='javascript:void(0);' class='new-address-btn' id='"+company.id+"' id2='Company'> New </a>");
		}

		$("#companyRegDate").text(company.regDate);
		$("#companyPhone").text(company.phone);
		$("#companyWebsite").text(company.website);
		$("#companyPan").text(company.pan);
		$("#companyFax").text(company.fax);
	}
	
	function displayBankInfo(bank){
		$("#bankName").text(bank.name);
		$("#bankAccName").text(bank.accName);
		$("#bankIfsc").text(bank.ifsc);
		
		$("#bankBranch").text(bank.branch);
		$("#bankAccNo").text(bank.accNo);
		$("#bankMicr").text(bank.micr);
	}
	
	function displaySiteInfo(siteList){
		var list = {};
		list = siteList;
		siteTbl.clear().draw(true);
		$.each(list, function(index, site) {
			siteTbl.row.add([site.name, site.regNo, site.regDate, site.regUpto, site.phone, 
				site.email, isNotNullOrUndef(site.address) ? getAddrString(site.address) + "<br> <a href='javascript:void(0);' class='modify-addr'  id2='Site' id='"+site.address.id+"'>modify</a>" : "<a href='javascript:void(0);' class='new-address-btn' id2='Site' id='"+site.id+"'>new</a>", site.isActive, "<a href='javascript:void(0);' class='modifySiteBtn' id='"+site.id+"'>modify</a>"]).node().id = site.id;
			siteTbl.draw(false);
		});
	}
	
	$("#modifyCompanyBtn").click(function() {
		setCompanyModelAttributes(company);
		$('#modifyCompanyModal').modal('show');
	});
	
	function setCompanyModelAttributes(company){
		$("#mdCompanyId").val(company.id);
		$("#mdCompanyRegNo").val(company.regNo);
		$("#mdCompanyName").val(company.name);
		$("#mdCompanyEmail").val(company.email);
		$("#mdCompanyGstNo").val(company.gstNo);
		
		$("#mdCompanyRegDate").val(company.regDate);
		$("#mdCompanyPhone").val(company.phone);
		$("#mdCompanyWebsite").val(company.website);
		$("#mdCompanyPan").val(company.pan);
		$("#mdCompanyFax").val(company.fax);
	}
	
	$("#modifyCompany").click(function(){
		var company = {};
		company.id = $("#mdCompanyId").val();
		company.regNo = upperCase("#mdCompanyRegNo");
		company.regDate = $("#mdCompanyRegDate").val();
		company.name = camelCase("#mdCompanyName");
		company.phone = $("#mdCompanyPhone").val();
		company.email = $("#mdCompanyEmail").val();
		company.fax = upperCase("#mdCompanyFax");
		company.website = $("#mdCompanyWebsite").val();
		company.gstNo = upperCase("#mdCompanyGstNo");
		company.pan = upperCase("#mdCompanyPan");
		
		company = patchByJson("/company", company);
		
		if(isNotNullOrUndef(company)){
			displayCompanyInfo(company);
			message('success', 'Details Updated Successfully !!!');
		}else{
			message('error', 'Getting error while modifing company details');
		}
	});
	
	$(document).on("click", '#modifyBankBtn', function(event) {
		$('.bankTitle').text("Modify Bank Details");
		$("#mdBankAction").val("modify");
		setBankModelAttributes(company.bank);
		$('#newOrModifyBankModal').modal('show');
	});
	
	function setBankModelAttributes(bank){
		$("#mdBankId").val(bank.id);
		$("#mdBankName").val(bank.name);
		$("#mdBankAccName").val(bank.accName);
		$("#mdBankIfsc").val(bank.ifsc);
		
		$("#mdBankBranch").val(bank.branch);
		$("#mdBankAccNo").val(bank.accNo);
		$("#mdBankMicr").val(bank.micr);
	}
	
	$("#saveBank").click(function(){
		var bank = {};
		bank.id = $("#mdBankId").val();
		bank.name = upperCase("#mdBankName");
		bank.branch = camelCase("#mdBankBranch");
		bank.accName = camelCase("#mdBankAccName");
		bank.accNo = upperCase("#mdBankAccNo");
		bank.ifsc = upperCase("#mdBankIfsc");
		bank.micr = upperCase("#mdBankMicr");
		
		if($("#mdBankAction").val() === "New"){
			alert("new bank");
		} else{
			company.bank = patchByJson("/bank", bank);
			if(isNotNullOrUndef(company.bank)){
				displayBankInfo(company.bank);
				message('success', 'Details Updated Successfully !!!');
			}else{
				message('error', 'Getting error while modifing company details');
			}
		}
	});
	
	$(document).on("click", '#newSiteBtn', function(event) {
		$('.siteTitle').text("New Site");
		$("#siteAction").val("New");
		$("#siteAgainstId").val(company.id);
		$("#siteId").val(null);
		$("#siteName").val(null);
		$("#siteRegNo").val(null);
		$("#siteRegDate").val(null);
		$("#siteRegUpto").val(null);
		$("#sitePhone").val(null);
		$("#siteEmail").val(null);

		$('#newOrModifySiteModal').modal('show');
	});

	$(document).on("click", '.modifySiteBtn', function(event) {
		$('.siteTitle').text("Modify Site Details");
		$("#siteAction").val("modify");
		setSiteProperties($(this).attr('id'));
		$('#newOrModifySiteModal').modal('show');
	});
	
	function setSiteProperties(id){
		var site = get("/site", id);
		$("#siteId").val(site.id);
		$("#siteName").val(site.name);
		$("#siteRegNo").val(site.regNo);
		$("#siteRegDate").val(site.regDate);
		$("#siteRegUpto").val(site.regUpto);
		$("#sitePhone").val(site.phone);
		$("#siteEmail").val(site.email);
	}
	
	$("#saveSite").click(function(){
		var site = {};
		site.id = $("#siteId").val();
		site.siteAgainstId = $("#siteAgainstId").val();
		site.name = camelCase("#siteName");
		site.regNo = upperCase("#siteRegNo");
		site.regDate = $("#siteRegDate").val();
		site.regUpto = $("#siteRegUpto").val();
		site.phone = $("#sitePhone").val();
		site.email = $("#siteEmail").val();
		
		if($("#siteAction").val() === "New"){
			var st = persist("/site", site);
			if(isNotNullOrUndef(st)){
				company.site.push(st);
				displaySiteInfo(company.site);
				message('success', 'successfully saved !!!');
			}else{
				message('error', 'Getting error while creating site');
			}
		}else {
			if(patchByJson("/site", site)){
				var cust = get("/company/user", $("#user").val());
				company.site = cust.site; 
				displaySiteInfo(company.site);
				message('success', 'successfully updated !!!');
			}else{
				message('error', 'Getting error while modifing site');
			}
		}
	});

	//-----------------------------------------------------------------------------------------------

	$(".disableSite").click(function(){
		var flag = patch("/session/toggleStatus", $(this).attr("id"), "INACTIVE", "Site");
		if(flag){
			location.reload();
		}else{
			message('error', 'Getting error while changing status of site');
		}
	});
	
	$(".enableSite").click(function(){
		var flag = patch("/session/toggleStatus", $(this).attr("id"), "ACTIVE", "Site");
		if(flag){
			location.reload();
		}else{
			message('error', 'Getting error while changing status of site');
		}
	});
	
	/*------Address Configuration Start-------*/
	$(".new-address-btn").click(function(){
		$('.addr-modal-title').html("New Address");
		$("#addr-action").val("New");
		
		//setting null
		$("#addr-against-id").val($(this).attr('id'));
		$("#addr-against-class").val($(this).attr('id2'));
		$("#addr-street").val(null);
		$("#addr-city").val(null);
		$("#addr-pincode").val(null);
		$("#addr-state option[selected]").removeAttr("selected");
		$("#addr-type option[selected]").removeAttr("selected");  
		$("#addr-status option[selected]").removeAttr("selected");  
		
		$('#new-modify-address-modal').modal('show');
	});
	
	$(document).on("click", '.modify-addr', function() {
		$('.addr-modal-title').html("Modify Address");
		$("#addr-action").val("Modify");
		$("#addr-against-class").val($(this).attr('id2'));
		setAddressModelProperties($(this).attr('id'));
		$('#new-modify-address-modal').modal('show');
	});
	
	function setAddressModelProperties(id){
		var add = get("/address", id);
		$("#addr-id").val(add.id);
		$("#addr-street").val(add.street);
		$("#addr-city").val(add.city);
		$("#addr-pincode").val(add.pincode);
		$('#addr-state option[value='+add.state.id+']').prop('selected', 'selected').change();
		$('#addr-type option[value='+add.type+']').prop('selected', 'selected').change();
		$('#addr-status option[value='+add.isActive+']').prop('selected', 'selected').change();
	}
	
	$("#save-address").click(function(){
		var address = {};
		var state = {};
		address.id = $("#addr-id").val();
		address.addrAgainstId = $("#addr-against-id").val();
		address.addrAgainstClass = $("#addr-against-class").val();
		address.street = camelCase("#addr-street");
		address.city = camelCase("#addr-city");
		address.pincode = $("#addr-pincode").val();
		state.id = $("#addr-state").val();
		state.name = $("#addr-state option:selected").text();
		state.isActive = "true";
		address.state = state;
		address.type = $("#addr-type").val();
		address.isActive = $("#addr-status").val();
		
		if($("#addr-action").val() === "New"){
			var addr = persist("/address", address);
			if(isNotNullOrUndef(addr)){
				console.log(address.addrAgainstClass);
				if(address.addrAgainstClass == 'Company'){
					$("#companyAddress").html(getAddrString(addr) + "<br> <a href='javascript:void(0);' class='modify-addr'  id2='Company' id='"+addr.id+"'>modify</a>");
					message('success', 'Address Saved Successfully !!!');
				} else if(address.addrAgainstClass == 'Site'){
					var sites = get("/site/company", company.id);
					company.site = sites;
					displaySiteInfo(company.site);
					message('success', 'Address Saved Successfully !!!');
				} else{
					message('error', 'Error while appending address !!!');
				}
			}
		}else {
			var addr = patchByJson("/address", address);
			console.log(address.addrAgainstClass);
			if(isNotNullOrUndef(addr)){
				if(address.addrAgainstClass == 'Company'){
					$("#companyAddress").html(getAddrString(addr) + "<br> <a href='javascript:void(0);' class='modify-addr'  id2='Company' id='"+addr.id+"'>modify</a>");
					message('success', 'Address Updated Successfully !!!');
				} else if(address.addrAgainstClass == 'Site'){
					var sites = get("/site/company", company.id);
					company.site = sites;
					displaySiteInfo(company.site);
					message('success', 'Address Updated Successfully !!!');
				} else{
					message('error', 'Error while appending address !!!');
				}
			}
		}
	});
	/*------Address Configuration End-------*/
});