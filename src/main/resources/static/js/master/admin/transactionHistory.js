$(function() {
    var thTbl = $('#transaction-history-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ],
        "order": [[ 0, "desc" ]],
	    "ajax": {
	    	"url": '/dtble/transactionHistory',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	columns: [
    		{
	            data: 'id'
	        },
    		{
	            data: 'customerId'
	        },
	        {
	            data: 'transactionDate'
	        },
	        {
	            data: 'remarks'
	        },
	        {
	            data: 'amount'
	        },
	        {
	            data: 'type'
	        },
	        {
	            data: 'closingBalance'
	        },
	        {
	            data: 'createdUserId'
	        }
	    ],
	    "columnDefs": [
            {
                "targets": [ 0, 1 ],
                "visible": false,
                "searchable": true
            }],
    });
    
    /*------Customer table Configuration Start-------*/
	var custTable = $('#search-customer-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "order": [[ 0, "desc" ]],
        "lengthMenu" : [ [ 5, 10 ], [ 15, 10 ] ],
	    "ajax": {
	    	"url": '/dtble/customer',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	"columns": [
    		{
	            data: 'createdDate'
	        },{
	            data: 'name'
	        }, {
	            data: 'phone'
	        }, {
	            data: 'isActive'
	        }
	    ],
	    "rowId": 'id',
	    "columnDefs" : [
	    	{
				"targets" : [ 0 ],
				"visible" : false,
				"searchable" : false,
			}],
		initComplete: function(){
		      var api = this.api();
		      $('.dataTables_filter input')
		          .off('.DT')
		          .on('keyup.DT', function (e) {
		              // If the length is 3 or more characters, or the user pressed ENTER, search
		              if(this.value.length > 2 || e.keyCode == 13) {
						if($.isNumeric(this.value)){
							custTable
							.column(2).search(this.value).draw();
						}else{
							custTable
							.column(1).search(this.value).draw();
						}
		              } else if(this.value.length < 2) {// Ensure we clear the search if they backspace far enough
		            	  //cTable.column(1).search("true", true, false).draw();
		              }
		          });
		   }
    });
    
	$("#search-customer-btn").click(function(){
		$('#search-customer-modal').modal('show');
	});
	
	$('#search-customer-tbl tbody').on( 'click', 'tr', function () {
		var customerId = custTable.row(this).node().id;
        
		$('#search-customer-modal').modal('hide');
		custTable.clear().draw();
		thTbl.column(1).search(customerId).draw();
	});
});