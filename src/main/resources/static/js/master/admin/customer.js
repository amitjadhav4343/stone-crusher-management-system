$(function() {
	var customer = {};
	appendOptions('/list/active/States', "#nw-addr-state", 'id', 'name', 1);
	$("#nw-addr-type").append("<option value= 'BILLING'>BILLING</option>");
	
	var addressTbl = $('#address-tbl').DataTable({
		"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"order": [],
		"retrieve": true,
		"columnDefs": [{
			"orderable": false,
			"targets": 'no-sort',
		}]
	});
	
	/*------Customer table Configuration Start-------*/
	var custTable = $('#search-customer-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "order": [[ 0, "desc" ]],
        "lengthMenu" : [ [ 5, 10 ], [ 5, 10 ] ],
	    "ajax": {
	    	"url": '/dtble/customer',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	"columns": [
    		{
	            data: 'createdDate'
	        },{
	            data: 'name'
	        }, {
	            data: 'phone'
	        }, {
	            data: 'isActive',
	            render : function (data) {
	            	if(data){
	            		return 'Yes';
	            	}
	            	
	            	return 'No';
				}
	        }
	    ],
	    "rowId": 'id',
	    "columnDefs" : [
	    	{
				"targets" : [ 0 ],
				"visible" : false,
				"searchable" : false,
			}],
		initComplete: function(){
		      $('.dataTables_filter input')
		          .off('.DT')
		          .on('keyup.DT', function (e) {
		              // If the length is 3 or more characters, or the user pressed ENTER, search
		              if(this.value.length > 2 || e.keyCode == 13) {
						if($.isNumeric(this.value)){
							custTable
							.column(2).search(this.value).draw();
						}else{
							custTable
							.column(1).search(this.value).draw();
						}
		              } else if(this.value.length < 2) {// Ensure we clear the search if they backspace far enough
		            	  //cTable.column(1).search("true", true, false).draw();
		              }
		          });
		   }
    });
	
	$("#search-customer-btn").click(function(){
		$('#search-customer-modal').modal('show');
	});
	
	$('#search-customer-tbl tbody').on( 'click', 'tr', function () {
		var customerId = custTable.row(this).node().id;
		
        $('#search-customer-modal').modal('hide');
        custTable.clear().draw();
		customer = get("/customer", customerId);
		displayCustInfo(customer);
	});
		
	/*------Customer table Configuration End-------*/
	
	function displayCustInfo(customer){
		$("#customer-id").val(customer.id);
		$('#customer-name').text(customer.name);
		$('#customer-email').text(customer.email);
		$('#customer-gst').text(customer.gstNo);
		$('#customer-advance').html(parseFloat(customer.advance).toFixed(2) + "&nbsp; <a href='javascript:void(0);' class='modify-advance' id='"+customer.id+"'>modify</a>");
		$('#customer-status').text(customer.isActive);
		$('#customer-phone').text(customer.phone);
		$('#customer-firm').text(customer.firm);
		$('#customer-identity').text(customer.identity);
		$('#customer-type').text(customer.customerType);
		displayAddrInfo(customer.address);
	}
	
	function displayAddrInfo(addList){
		var list = {};
		list = addList;
		addressTbl.clear().draw(true);
		$.each(list, function(index, add) {
			addressTbl.row.add([add.street, add.city, add.pincode, add.state.name, add.type, 
				add.isActive, "<a href='javascript:void(0);' class='modify-addr' id2='Customer' id='"+add.id+"'>modify</a>"]).node().id = add.id;
			addressTbl.draw(false);
		});
	}
	
	/*------Carousel Configuration-------*/
	$("#customer-carousel, #nw-customer-carousel").carousel({
		interval : false
	});
	
	$("#admin-new-customer-btn").click(function() {
		clearNewCustomerForm();
		$("#customer-carousel").carousel(1);
		$("#nw-customer-carousel").carousel(0);
	});
	
	$("#modify-customer-btn").click(function() {
		if(isNullOrUndef(customer)){
			message("error", "Please select Customer first !!!");
		}else{
			//setCustomerProperties(customer);
			$("#customer-carousel").carousel(2);
		}
	});
	
	function setCustomerProperties(customer){
		$('#md-customer-id').val(customer.id);
		$('#md-customer-name').val(customer.name);
		$('#md-customer-email').val(customer.email);
		$('#md-customer-gst').val(customer.gstNo);
		$('#md-customer-advance').val(customer.advance);
		$('#md-customer-phone').val(customer.phone);
		$('#md-customer-firm').val(customer.firm);
		$('#md-customer-identity').val(customer.identity);		
		$('#md-customer-type option[value='+customer.customerType+']').prop('selected', 'selected').change();
		$('#md-customer-status option[value='+customer.isActive+']').prop('selected', 'selected').change();
	}
	
	$("#nw-back-customer, #md-back-customer, #nw-back-address").click(function() {
		$("#customer-carousel").carousel(0);
	});
	
	$("#next-to-address").click(function(){
		if(isNullOrUndef($("#nw-customer-name").val()) || isNullOrUndef($("#nw-customer-phone").val()) || isNullOrUndef($("#nw-customer-email").val()) || isNullOrUndef($("#nw-customer-type").val())){
        	message('error', 'Incomplete details !!!');
        }else{
        	$("#nw-customer-carousel").carousel(1);
        }
	});
	
	$("#nw-save-address").click(function(){
		var customer = {};
		customer.name = camelCase("#nw-customer-name");
		customer.email = $("#nw-customer-email").val();
		customer.gstNo = upperCase("#nw-customer-gst");
		customer.advance = $("#nw-customer-advance").val();
		customer.isActive = $("#nw-customer-status").val();
		customer.phone = $("#nw-customer-phone").val();
		customer.firm = $("#nw-customer-firm").val();
		customer.identity = upperCase("#nw-customer-identity");
		customer.customerType = $("#nw-customer-type").val();
		
		var states = {};
		states.id = $("#nw-addr-state").val();
		
		//customer address details
		var addressList = [];
		var address = {};
		address.street = camelCase("#nw-addr-street");
		address.city = camelCase("#nw-addr-city");
		address.pincode = $("#nw-addr-pincode").val();
		address.type = $("#nw-addr-type").val();
		address.isActive = $('#nw-addr-status').val();
		address.state = states;
		addressList.push(address);
		customer.address = addressList;
		
		if(isNullOrUndef(address.street) || isNullOrUndef(states.id) || isNullOrUndef(address.type)){
        	message('error', 'Incomplete details !!!');
        }else{
        	var res = persist('/customer', customer);
    		if(isNotNullOrUndef(res)){
    			custTable.ajax.reload();
    			displayCustInfo(res);
    			$("#customer-carousel").carousel(0);
    			message('success', 'Customer saved successfully !!!');
    		}	
        }
	});
	
	$("#md-save-customer").click(function(){
		var cust = {};
		cust.id = $("#md-customer-id").val();
		cust.name = camelCase("#md-customer-name");
		cust.email = $("#md-customer-email").val();
		cust.gstNo = upperCase("#md-customer-gst");
		cust.advance = $("#md-customer-advance").val();
		cust.isActive = $("#md-customer-status").val();
		cust.phone = $("#md-customer-phone").val();
		cust.firm = $("#md-customer-firm").val();
		cust.identity = upperCase("#md-customer-identity");
		cust.customerType = $("#md-customer-type").val();
		
		customer = patchByJson("/customer", cust);
		if(isNotNullOrUndef(customer)) {
			displayCustInfo(customer);
			$("#customer-carousel").carousel(0);
			message('success', 'Customer updated successfully !!!');
		}
	});
	
	function clearNewCustomerForm(){
		$("#nw-customer-name").val(null);
		$("#nw-customer-email").val(null);
		$("#nw-customer-gst").val(null);
		$("#nw-customer-advance").val(null);
		$("#nw-customer-phone").val(null);
		$("#nw-customer-firm").val(null);
		$("#nw-customer-identity").val(null);
		$("#nw-customer-type option:selected").removeAttr("selected");
		$("#nw-customer-status option:selected").removeAttr("selected");
	}
	
	/*------Address Configuration Start-------*/
	$("#new-address-btn").click(function(){
		if(isNullOrUndef(customer)){
			message("error", "Please select Customer first !!!");
		}else{
			$('.addr-modal-title').html("New Address");
			$("#addr-action").val("New");
			
			//setting null
			$("#addr-against-id").val(customer.id);
			$("#addr-against-class").val($(this).attr('id2'));
			$("#addr-street").val(null);
			$("#addr-city").val(null);
			$("#addr-pincode").val(null);
			$("#addr-state option[selected]").removeAttr("selected");
			$("#addr-type option[selected]").removeAttr("selected");  
			$("#addr-status option[selected]").removeAttr("selected");  
			
			$('#new-modify-address-modal').modal('show');
		}
	});
	
	$(document).on("click", '.modify-addr', function() {
		$('.addr-modal-title').html("Modify Address");
		$("#addr-action").val("Modify");
		$("#addr-against-id").val(customer.id);
		$("#addr-against-class").val($(this).attr('id2'));
		setAddressModelProperties(($(this).attr('id')));
		$('#new-modify-address-modal').modal('show');
	});
	
	function setAddressModelProperties(id){
		var add = get("/address", id);
		$("#addr-id").val(add.id);
		$("#addr-street").val(add.street);
		$("#addr-city").val(add.city);
		$("#addr-pincode").val(add.pincode);
		$('#addr-state option[value='+add.state.id+']').prop('selected', 'selected').change();
		$('#addr-type option[value='+add.type+']').prop('selected', 'selected').change();
		$('#addr-status option[value='+add.isActive+']').prop('selected', 'selected').change();
	}
	
	$("#save-address").click(function(){
		var address = {};
		var state = {};
		address.id = $("#addr-id").val();
		address.addrAgainstId = $("#addr-against-id").val();
		address.addrAgainstClass = $("#addr-against-class").val();
		address.street = camelCase("#addr-street");
		address.city = camelCase("#addr-city");
		address.pincode = $("#addr-pincode").val();
		state.id = $("#addr-state").val();
		state.name = $("#addr-state option:selected").text();
		state.isActive = "true";
		address.state = state;
		address.type = $("#addr-type").val();
		address.isActive = $("#addr-status").val();
		
		if($("#addr-action").val() === "New"){
			var addr = persist("/address", address);
			if(isNotNullOrUndef(addr)){
				addressTbl.row.add([addr.street, addr.city, addr.pincode, addr.state.name, addr.type, addr.isActive, "<a href='javascript:void(0);' class='modify-addr' id2='Customer' id='"+addr.id+"'>modify</a>"]).node().id = addr.id;
				addressTbl.draw(false);
				message('success', 'Address Saved Successfully !!!');
			}
		}else {
			var addrr = patchByJson("/address", address);
			if(isNotNullOrUndef(addrr)){
				var url = "/address/clazz/"+address.addrAgainstClass+"/id/"+address.addrAgainstId;
				var object = get(url);
				displayAddrInfo(object.address);
				message('success', 'Address Updated Successfully !!!');
			}
		}
	});
	/*------Address Configuration End-------*/
	
	$(document).on("click", '.modify-advance', function() {
		$("#advance-amount").val(null);
		$("#advance-remarks").val(null);
		$('#modify-advance-modal').modal('show');
	});
	
	$("#save-advance").click(function(){
		var th = {};
		th.amount = $("#advance-amount").val();
		th.remarks = $("#advance-remarks").val();
		th.customerId = $(".modify-advance").attr('id');
		th.type = "Cr";
		
		var updatedAdvance = patchByJson("/modifyAdvance", th);
		if(updatedAdvance != null){
			$('#customer-advance').html(parseFloat(updatedAdvance).toFixed(2) + "&nbsp; <a href='javascript:void(0);' class='modify-advance' id='"+th.customerId+"'>modify</a>");
			message("success", "Successfully advance updated !!!");
		}
	});
});