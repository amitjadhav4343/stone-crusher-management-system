$(function() {
	// Setting data picker to edit vehicle model
	setStartDatePicker("#nw-vehicle-regDate", moment());
	applyDatePicker("#nw-vehicle-regDate", 'YYYY-MM-DD');
	
	setEndDatePicker("#nw-vehicle-regUpto", moment());
	applyDatePicker("#nw-vehicle-regUpto", 'YYYY-MM-DD');
	
    var vTable = $('#vehicle-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ],
	    "ajax": {
	    	"url": '/dtble/vehicle',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",			    
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	columns: [
	        {
	            data: 'number'
	        },
	        {
	            data: 'ownerName'
	        },
	        {
	            data: 'regNo'
	        },
	        {
	            data: 'model'
	        },
	        {
	            data: 'regDate'
	        },
	        {
	            data: 'regUpto'
	        },
	        {
	            data: 'isActive',
	            render : function (data) {
	            	if(data){
	            		return 'Yes';
	            	}
	            	
	            	return 'No';
				}
	        },
	        {
	        	data: 'id',
	            mRender: function (data, type, row) {
	            	return '<a href="javascript:void(0);" class="modify-material" id="' + row.id + '">modify</a>'
	            }
	        }
	    ],
	    "columnDefs": [
	    	{ "orderable": false, "targets": 0 }
	    	]
    });
    
    /*------Carousel Configuration-------*/
	$("#vehicle-carousel").carousel({
		interval : false
	});
	
	$("#nw-back-vehicle, #md-back-vehicle").click(function() {
		$("#vehicle-carousel").carousel(0);
	});
	
	$("#new-vehicle").click(function() {
		$("#nw-vehicle-number").val(null);
		$("#nw-vehicle-owner-name").val(null);
		$("#nw-vehicle-model").val(null);
		$("#nw-vehicle-regNo").val(null);
		$("#nw-vehicle-regDate").val(null);
		$("#nw-vehicle-regUpto").val(null);
		$("#nw-vehicle-status").val(null);
		
		$("#vehicle-carousel").carousel(1);
	});

	$('#vehicle-tbl-body').on( 'click', '.modify-material', function () {
		setVehicleProperties(($(this).attr('id')));
		$("#vehicle-carousel").carousel(2);
	});
	
	function setVehicleProperties(id){
		var vehicle = get("/vehicle", id);
		
		$("#md-vehicle-id").val(vehicle.id);
		$("#md-vehicle-number").val(vehicle.number);
		$("#md-vehicle-owner-name").val(vehicle.ownerName);
		$("#md-vehicle-model").val(vehicle.model);
		$("#md-vehicle-regNo").val(vehicle.regNo);
		
		if(isNotNullOrUndef(vehicle.regDate)){
			setDatePicker("#md-vehicle-regDate", vehicle.regDate);
			$("#md-vehicle-regDate").val(vehicle.regDate);
		}
		if(isNotNullOrUndef(vehicle.regUpto)){
			setDatePicker("#md-vehicle-regUpto", vehicle.regUpto);
			$("#md-vehicle-regUpto").val(vehicle.regUpto);
		}

		$('#md-vehicle-status option[value='+vehicle.isActive+']').prop('selected', 'selected').change();
		$("#vehicle-carousel").carousel(2);
	}
	
	$("#nw-save-vehicle").click(function(){		
		var vehicleOwnerName = camelCase("#nw-vehicle-owner-name");
		var vehicleNumber = upperCase("#nw-vehicle-number");
        if(isNullOrUndef(vehicleOwnerName) || isNullOrUndef(vehicleNumber)){
        	message('error', 'Incomplete details...');
        }else{
        	var vehicle = {};
    		vehicle.number = vehicleNumber;
    		vehicle.ownerName = vehicleOwnerName;
    		vehicle.model = camelCase("#nw-vehicle-model");
    		vehicle.regNo = upperCase("#nw-vehicle-regNo");
    		vehicle.regDate = $("#nw-vehicle-regDate").val();
    		vehicle.regUpto = $("#nw-vehicle-regUpto").val();
    		vehicle.isActive = $("#nw-vehicle-status").val();
    		
    		vehicle = persist("/vehicle", vehicle);
			if(isNotNullOrUndef(vehicle)) {
				vTable.ajax.reload();
				$("#vehicle-carousel").carousel(0);
				message('success', 'vehicle saved successfully !!!');
			}
        }
	});
	
	$("#md-save-vehicle").click(function(){		
		var vehicleOwnerName = camelCase("#md-vehicle-owner-name");
		var vehicleNumber = upperCase("#md-vehicle-number");
        if(isNullOrUndef(vehicleOwnerName) || isNullOrUndef(vehicleNumber)){
        	message('error', 'Incomplete details...');
        }else{
        	var vehicle = {};
    		vehicle.number = vehicleNumber;
    		vehicle.ownerName = vehicleOwnerName;
    		vehicle.id = $("#md-vehicle-id").val();
    		vehicle.model = camelCase("#md-vehicle-model");
    		vehicle.regNo = upperCase("#md-vehicle-regNo");
    		vehicle.regDate = $("#md-vehicle-regDate").val();
    		vehicle.regUpto = $("#md-vehicle-regUpto").val();
    		vehicle.isActive = $("#md-vehicle-status").val();
    		
    		vehicle = patchByJson("/vehicle", vehicle);
			if(isNotNullOrUndef(vehicle)) {
				vTable.ajax.reload();
				$("#vehicle-carousel").carousel(0);
				message('success', 'vehicle updated successfully !!!');
			}
        }
	});
});