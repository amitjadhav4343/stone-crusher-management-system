$(function() {

	var roles = get("/list/active/Role");

	appendObjectOptions(roles, '#nw-user-role', 'id', 'name', 1);
	appendObjectOptions(roles, '#md-user-role', 'id', 'name', 1);

	var uTable = $('#user-tbl').DataTable({
		"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"processing": true,
		"serverSide": true,
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"ajax": {
			"url": '/dtble/user',
			"type": "POST",
			"contentType": "application/json; charset=utf-8",
			"data": function(d) {
				return JSON.stringify(d); // NOTE: you also need to stringify POST payload
			}
		},
		columns: [
			{
				data: 'employee.name'
			},
			{
				data: 'employee.email'
			},
			{
				data: 'employee.phone'
			},
			{
				data: 'username'
			},
			{
				data: 'role.name'
			},
			{
				data: 'isActive',
				render : function (data) {
	            	if(data){
	            		return 'Yes';
	            	}
	            	
	            	return 'No';
				}
			},
			{
				data: 'id',
				mRender: function(data, type, row) {
					return '<a href="javascript:void(0);" class="modify-user-btn" id="' + row.id + '">modify</a>'
				}
			}
		],
		"columnDefs": [
			{ "orderable": false, "targets": 0 }
		]
	});

	var empTbl = $('#search-employee-tbl').DataTable({
		"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"processing": true,
		"serverSide": true,
		"order": [[0, "asc"]],
		"lengthMenu": [[10, 25, 50], [10, 25, 50]],
		"ajax": {
			"url": '/dtble/employee',
			"type": "POST",
			"contentType": "application/json; charset=utf-8",
			"data": function(d) {
				return JSON.stringify(d); // NOTE: you also need to stringify POST payload
			}
		},
		"columns": [
			{
				data: 'name'
			}, {
				data: 'phone'
			}, {
				data: 'designation'
			}, {
				data: 'isActive'
			}
		],
		"rowId": 'id',
		"initComplete": function() {
			$('.dataTables_filter input')
				.off('.DT')
				.on('keyup.DT', function(e) {
					// If the length is 3 or more characters, or the user pressed ENTER, search
					if (this.value.length > 2 || e.keyCode == 13) {
						if ($.isNumeric(this.value)) {
							empTbl
								.column(0).search(this.value).draw();
						} else {
							empTbl
								.column(1).search(this.value).draw();
						}
					} else if (this.value.length < 2) {// Ensure we clear the search if they backspace far enough
						//cTable.column(1).search("true", true, false).draw();
					}
				});
		}
	});

	/*------Carousel Configuration Start-------*/
	$("#user-carousel").carousel({
		interval: false
	});

	$("#new-user-btn").click(function() {
		$("#user-carousel").carousel(1);
	});
	
	$('#user-tbl-body').on( 'click', '.modify-user-btn', function () {
		var user = get("/user", $(this).attr('id'));
		
		$("#md-user-id").val(user.id);
		$("#md-user-name").val(user.employee.name);
		$("#md-user-designation").val(user.employee.designation);
		$("#md-user-phone").val(user.employee.phone);
		$("#md-user-username").val(user.username);
		$('#md-user-role option[value='+user.role.id+']').prop('selected', 'selected').change();
		$('#md-user-status option[value='+user.isActive+']').prop('selected', 'selected').change();
		
		$("#user-carousel").carousel(2);
	});

	$("#nw-back-user, #md-back-user").click(function() {
		$("#user-carousel").carousel(0);
	});

	$("#nw-save-user").click(function() {
		var userRoleId = $("#nw-user-role").val();
		var userStatus = $("#nw-user-status").val();
		
		if(isNotNullOrUndef(userRoleId) && isNotNullOrUndef(userStatus)){
			var usr = {};
			usr.username = $("#nw-user-username").val();
			usr.employeeId = $("#nw-user-emp-id").val();
			var role = {};
			role.id = userRoleId;
			role.name = $("#nw-user-role option:selected").text();
			role.isActive = "true";
			usr.role = role;
			usr.isActive = userStatus;
			
			var user = persist("/user", usr);
			
			if(isNotNullOrUndef(user)) {
				message("success", "User saved successfully !!!");
				uTable.ajax.reload();
				$("#user-carousel").carousel(0);
			}
		} else{
			message("error", "Incomplete details !!!");
		}
	});
	
	$("#md-save-user").click(function() {
		var userRoleId = $("#md-user-role").val();
		var userStatus = $("#md-user-status").val();
		
		if(isNotNullOrUndef(userRoleId) && isNotNullOrUndef(userStatus)){
			var usr = {};
			usr.username = $("#md-user-username").val();
			usr.employeeId = $("#md-user-emp-id").val();
			var role = {};
			role.id = userRoleId;
			role.name = $("#md-user-role option:selected").text();
			role.isActive = "true";
			usr.role = role;
			usr.isActive = userStatus;
			
			var user = persist("/user", usr);
			
			if(isNotNullOrUndef(user)) {
				message("success", "User saved successfully !!!");
				uTable.ajax.reload();
				$("#user-carousel").carousel(0);
			}
		} else{
			message("error", "Incomplete details !!!");
		}
	});

	/*------Carousel Configuration End-------*/

	$("#nw-user-name").focus(function() {
		$('#search-employee-modal').modal('show');
	});

	$('#search-employee-modal tbody').on('click', 'tr', function() {
		var empId = empTbl.row(this).node().id;
		$('#search-employee-modal').modal('hide');
		empTbl.clear().draw();
		
		var employee = get("/employee", empId);
		
		$('#nw-user-emp-id').val(employee.id);
		$('#nw-user-name').val(employee.name);
		$('#nw-user-phone').val(employee.phone);
		$('#nw-user-username').val(employee.phone);
		$('#nw-user-designation').val(employee.designation);
	});
	
	$('#user-tbl-body').on( 'click', '.reset-password-btn', function () {
		var userId = $(this).attr('id');
		message("success", "Password sent successfully to user !!!" + userId);
	});
});