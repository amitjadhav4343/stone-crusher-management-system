/*===PLUGINS===*/
include("/plugins/jquery/jquery.min.js");
include("/plugins/bootstrap-daterangepicker/js/moment.min.js");
include("/plugins/popper/popper.js");
include("/plugins/bootstrap/js/bootstrap.min.js");
include("/plugins/nprogress-master/nprogress.js");
include("/plugins/bootstrap-select/js/bootstrap-select.js");
include("/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js");
include("/plugins/bootstrap-daterangepicker/js/daterangepicker.js");
include("/plugins/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js");
include("/plugins/typeahead/js/typeahead.js");
include("/plugins/toastr/jquery.toast.js");

/*Data-table*/
include("/plugins/bootstrap-datatable/js/jquery.dataTables.min.js");
include("/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js");
include("/plugins/bootstrap-datatable/js/dataTables.select.min.js");
include("/plugins/bootstrap-datatable/js/dataTables.editor.min.js");
include("/plugins/others/jquery.spring-friendly.js");

/*Data-table-Export-Buttons*/
include("/plugins/bootstrap-datatable/js/dataTables.buttons.min.js");
include("/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js");
include("/plugins/bootstrap-datatable/js/jszip.min.js");
include("/plugins/bootstrap-datatable/js/pdfmake.min.js");
include("/plugins/bootstrap-datatable/js/vfs_fonts.js");
include("/plugins/bootstrap-datatable/js/buttons.html5.min.js");
include("/plugins/bootstrap-datatable/js/buttons.print.min.js");

/* High-chart */
include("/plugins/highchart/highcharts.js");
include("/plugins/highchart/exporting.js");
include("/plugins/highchart/export-data.js");
include("/plugins/highchart/accessibility.js");

/* html2canvas */
include("/plugins/html2canvas/html2canvas.js");
include("/js/print.min.js");

/*===SP-ADMIN COMMON SCRIPTS===*/
include("/js/common.js");

/*===SP-ADMIN AJAX===*/
include("/js/ajax.js");

function include(url) {
	document.write('<script type="text/javascript" src="' + url + '" ></script>');
}