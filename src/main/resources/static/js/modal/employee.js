$(function() {
	var employee = {};
	var addrsTbl = null;
	
	// Setting date picker to joining date of new employee
	setDatePicker("#nw-emp-joiningDate", moment());
	applyDatePicker("#nw-emp-joiningDate", 'YYYY-MM-DD');
	
	setDatePicker("#md-emp-resignedDate", moment());
	applyDatePicker("#md-emp-resignedDate", 'YYYY-MM-DD');
	
	setDatePicker("#md-emp-endDate", moment());
	applyDatePicker("#md-emp-endDate", 'YYYY-MM-DD');
	
	var page = getCurrentPage();
	var empTbl = $('#search-employee-tbl').DataTable({
    	"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
    	"processing" : true,
        "serverSide" : true,
        "order": [[ 0, "asc" ]],
        "lengthMenu" : [ [ 10, 25, 50], [ 10, 25, 50] ],
	    "ajax": {
	    	"url": '/dtble/employee',
	    	"type": "POST",
	    	"contentType" : "application/json; charset=utf-8",
            "data": function (d) {
                return JSON.stringify(d); // NOTE: you also need to stringify POST payload
            }
	    },
    	"columns": [
			{
	            data: 'name'
	        }, {
	            data: 'phone'
	        }, {
				data: 'designation'
			}, {
	            data: 'isActive'
	        }
	    ],
	    "rowId": 'id',
		"initComplete": function(){
		      var api = this.api();
		      $('.dataTables_filter input')
		          .off('.DT')
		          .on('keyup.DT', function (e) {
		              // If the length is 3 or more characters, or the user pressed ENTER, search
		              if(this.value.length > 2 || e.keyCode == 13) {
						if($.isNumeric(this.value)){
							empTbl
							.column(0).search(this.value).draw();
						}else{
							empTbl
							.column(1).search(this.value).draw();
						}
		              } else if(this.value.length < 2) {// Ensure we clear the search if they backspace far enough
		            	  //cTable.column(1).search("true", true, false).draw();
		              }
		          });
		   }
    });

	switch(page) {
		case "/page/admin/employee":
			addrsTbl = $('#addrsTbl').DataTable({
				"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
				"order": [],
				"retrieve": true,
				"columnDefs": [{
					"orderable": false,
					"targets": 'no-sort',
				}]
			});
		    break;
	}

	$("#search-emp-btn").click(function(){
		$('#search-employee-modal').modal('show');
	});
	
	$('#search-employee-modal tbody').on( 'click', 'tr', function () {
		var empId = empTbl.row(this).node().id;
        var empName = empTbl.row(this).data()['name'];

        $('#search-employee-modal').modal('hide');
		empTbl.clear().draw();
		employee = get("/employee", empId);
		displayEmpInfo(employee);
	});

	function displayEmpInfo(emp){
		$("#emp-id").val(emp.id);
		$('#emp-name').text(emp.name);
		$('#emp-phone').text(emp.phone);
		$('#emp-identity').text(emp.identity);
		$('#emp-joiningDate').text(emp.joiningDate);
		$('#emp-isActive').text(emp.isActive);
		
		$('#emp-designation').text(emp.designation);
		$('#emp-email').text(emp.email);
		$('#emp-salary').text(emp.salary);
		$('#emp-endDate').text(emp.endDate);
		
		displayAddrInfo(emp.address);
	}
	
	function displayAddrInfo(addList){
		var list = {};
		list = addList;
		addrsTbl.clear().draw(true);
		$.each(list, function(index, add) {
			addrsTbl.row.add([add.street, add.city, add.pincode, add.state.name, add.type, 
				add.isActive, "<a href='javascript:void(0);' class='modify-address' id='"+add.id+"'>edit</a>"]).node().id = add.id;
			addrsTbl.draw(false);
		});
	}
	
	$("#nw-save-employee").click(function(){
		employee.name = $("#nw-emp-name").val();
		employee.phone = $("#nw-emp-phone").val();
		employee.email = $("#nw-emp-email").val();
		employee.designation = $("#nw-emp-designation").val();
		
		employee.joiningDate = $("#nw-emp-joiningDate").val();
		employee.identity = $("#nw-emp-identity").val();
		employee.salary = $("#nw-emp-salary").val();
		employee.isActive = $("#nw-emp-isActive").val();
		
		var res = persist("/employee", employee);
		
		if(isNotNullOrUndef(res)) {
			message("success", "Employee saved successfully !!!");
			displayEmpInfo(employee);
		}
	});
	
	$("#modify-employee").click(function() {
		if(isNotNullOrUndef(employee)) {
			//clear selection of gst
			//$("#materialGst option:selected").removeAttr("selected");
			
			$("#md-emp-name").val(employee.name);
			$("#md-emp-phone").val(employee.phone);
			$("#md-emp-email").val(employee.email);
			$('#md-emp-designation option[value='+employee.designation+']').prop('selected', 'selected').change();
			$("#md-emp-joiningDate").val(employee.joiningDate);
			if(isNotNullOrUndef(employee.resignedDate)){
				setDatePicker("#md-emp-resignedDate", employee.resignedDate);
				$("#md-emp-resignedDate").val(employee.resignedDate);
			}
			if(isNotNullOrUndef(employee.endDate)){
				setDatePicker("#md-emp-endDate", employee.endDate);
				$("#md-emp-endDate").val(employee.endDate);
			}
			$("#md-emp-identity").val(employee.identity);
			$("#md-emp-salary").val(employee.salary);
			$('#md-emp-isActive option[value='+employee.isActive+']').prop('selected', 'selected').change();
		}
	});
	
	$("#md-save-employee").click(function(){
		var emp = {};
		emp.id = $("#emp-id").val();
		emp.name = $("#md-emp-name").val();
		emp.phone = $("#md-emp-phone").val();
		emp.email = $("#md-emp-email").val();
		emp.designation = $("#md-emp-designation").val();
		emp.joiningDate = $("#md-emp-joiningDate").val();
		emp.resignedDate = $("#md-emp-resignedDate").val();
		emp.endDate = $("#md-emp-endDate").val();
		emp.identity = $("#md-emp-identity").val();
		emp.salary = $("#md-emp-salary").val();
		emp.isActive = $("#md-emp-isActive").val();
		
		var res = patchByJson("/employee", emp);
		
		if(isNotNullOrUndef(res)) {
			message("success", "Employee saved successfully !!!");
			displayEmpInfo(res);
		}
	});
	
	$("#save-address").click(function(){
		var address = {};
		var state = {};
		address.id = $("#addrId").val();
		address.addrAgainstId = $("#addrAgainstId").val();
		address.addrAgainstClass = $("#addrAgainstClass").val();
		address.street = camelCase("#addrStreet");
		address.city = camelCase("#addrCity");
		address.pincode = $("#addrPincode").val();
		state.id = $("#addrState").val();
		state.name = $("#addrState option:selected").text();
		state.isActive = "true";
		address.state = state;
		address.type = $("#addrType").val();
		address.isActive = $("#addrStatus").val();
		
		if($("#addrAction").val() === "New"){
			var addr = persist("/address", address);
			if(isNotNullOrUndef(addr)){
				addrsTbl.row.add([addr.street, addr.city, addr.pincode, addr.state.name, addr.type, addr.isActive, "<a href='javascript:void(0);' class='modify-address' id='"+addr.id+"'>edit</a>"]).node().id = addr.id;
				addrsTbl.draw(false);
			}
		}else {
			var addrr = patchByJson("/address", address);
			if(isNotNullOrUndef(addrr)){
				var url = "/address/clazz/"+$("#addrAgainstClass").val()+"/id/"+$("#addrAgainstId").val();
				var object = get(url);
				addrsTbl.clear().draw(true);
				$.each(object.address, function(index, addr) {
					addrsTbl.row.add([addr.street, addr.city, addr.pincode, addr.state.name, addr.type, addr.isActive, "<a href='javascript:void(0);' class='modify-address' id='"+addr.id+"'>edit</a>"]).node().id = addr.id;
					addrsTbl.draw(false);
				});
			}
		}
	});
});