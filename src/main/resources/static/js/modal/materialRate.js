$(function() {
	var mrTable = $('#material-rate-tbl').DataTable({
		"dom": '<"list-row-header"<"page-length"l><"keyword-search"f>> <"list-row-body"<"page-list "<"table-responsive"rt>>><"list-row-footer"<"show-entry"i><"list-pagination"p>>',
		"order": [],
		"retrieve": true,
		"columnDefs": [{
			"orderable": false,
			"targets": 'no-sort',
		}]
	});
	
	var materialList = get("/list/active/Material");
	
	/*------Open Material Rate Modal -------*/
	$("#modify-rate").click(function(){
		$('#material-rate-modal').modal('show');
		handleMaterialRateTable(mrTable, materialList);
	});

	function handleMaterialRateTable(tbl, params) {
		var list = {};
		list = params;
		tbl.clear().draw();
		$.each(list, function(index, obj) {
			tbl.row.add([obj.name, obj.rate, obj.gst.value, "<a href='javascript:void(0);' class='modify-material-rate' id='"+obj.id+"'>Modify Rate</a>"]).node().id = obj.id;
			tbl.draw(false);
		});
	}
	
	$('#material-rate-tbl-body').on( 'click', '.modify-material-rate', function () {
		var uRate = prompt("Please enter updated rate!!!");
		if(uRate !== null){
			if(!jQuery.isNumeric(uRate) || isNullOrUndef(uRate)){
				message('error', 'Please provide rate in valid format !!!');
			} else {
				var materialId = $(this).attr('id');
				var material = {};
				material.id= materialId;
				material.rate= uRate;
				
				if(isNotNullOrUndef(patchByJson('/modify-material-rate', material))){
					materialList = get("/list/active/Material");
					handleMaterialRateTable(mrTable, materialList);
        			message('success', 'Material updated successfully');
    			}
			}
		}
	});
});