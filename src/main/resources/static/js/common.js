/*================================== HEADER / SIDEBAR Functions ===========================*/
function hdrSdbrFootrFunc() {
    /*SIDEBAR MENU CLICK*/
    $('.sidebar-menu > li > a').on('click', function() {
        $(this).parent('li').toggleClass('active').siblings('li').removeClass('active');
    });
    $('.submenu > li > a').on('click', function() {
        $(this).parent('li').addClass('active').siblings('li').removeClass('active');
    });
    /*SIDEBAR DESKTOP TOGGLER CLICK*/
    $('.desktop-toggler-click').on('click', function() {
        $('body').toggleClass('desktop-sidebar');
        $(this).toggleClass('active');
    });
    /*SIDEBAR MOBILE-TAB TOGGLER CLICK*/
    $('.mobile-tab-toggler-click').on('click', function() {
        $('.sidebar-overlay').fadeIn(300);
        $('body').toggleClass('mobile-tab-sidebar');
        $(this).toggleClass('active');
        $(document).ajaxComplete(function() {
            $('.sidebar-overlay').fadeOut(300);
            $('body').removeClass('mobile-tab-sidebar');
            $('.mobile-tab-toggler-click').removeClass('active');
        });
    });
    $('.mobile-tab-close-sidebar,.sidebar-overlay').on('click', function() {
        $('.sidebar-overlay').fadeOut(300);
        $('body').removeClass('mobile-tab-sidebar');
        $('.mobile-tab-toggler-click').removeClass('active');
    });
    /*SIDEBAR HOVER*/
    $(".sidebar").hover(function() {
        $(".desktop-sidebar").addClass('hover-sidebar');
    }, function() {
        $(".desktop-sidebar").removeClass('hover-sidebar');
    });
    /*SIDEBAR MENU SCROLL*/
    $(".sidebar-menu").mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });
    
    // NOTIFICATION DROPDOWN CLICK
    $('.notification-menu-clk').on('click', function() {
        $('.notification-dropdown').toggleClass('active');
        $('.user-dropdown').removeClass('active');
    });
    
    // USER DROPDOWN CLICK
    $('.user-menu-clk').on('click', function() {
        $('.user-dropdown').toggleClass('active');
        $('.notification-dropdown').removeClass('active');
    });
    
/*    $('.toggle-header-links-clk').on('click', function() {
        $('.header-links').toggleClass('active');
    });
    // HEADER LINKS ACTION
    $('.header-links').on('click', function(e) {
        e.stopPropagation();
    });
    $(document).on('click', function(e) {
        if ($(e.target).is('.user-dropdown') == false) {
            $('.user-dropdown').removeClass('active');
        }
        if ($(e.target).is('.notification-dropdown') == false) {
            $('.notification-dropdown').removeClass('active');
        }
    })
    $(document).ajaxComplete(function() {
        $('.notification-dropdown').removeClass('active');
        $('.user-dropdown').removeClass('active');
        $('.header-links').removeClass('active');
    });
    // USER DROPDOWN CLICK
    $('.user-menu-clk').on('click', function() {
        $('.user-dropdown').toggleClass('active');
        $('.notification-dropdown').removeClass('active');
    });
    // NOTIFICATION DROPDOWN CLICK
    $('.notification-menu-clk').on('click', function() {
        $('.notification-dropdown').toggleClass('active');
        $('.user-dropdown').removeClass('active');
    });

    $(".notification-dropdown").mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark"
    });


    setTimeout(function() {
        $('body').addClass('session-expired');
    }, 500000);

    $('.log-btn').on('click', function() {
        $('body').removeClass('session-expired');
    })*/

}
/*================================== AJAX LOAD COMMON Functions ===========================*/
function allFunctions() {

    //content div Height 100% Assign 
    if ($(window).width() > 767) {
        var WindowHt = $(window).outerHeight();
        var contentHt = WindowHt - $('.header').outerHeight() - $('.footer').outerHeight();
        $('.content').css('min-height', contentHt);
    }


    /*=========== M-CustomScrollbar ===========*/
    $('.slide-body').mCustomScrollbar({
        axis: "y",
        theme: "minimal-dark",
        autoHideScrollbar: true,
    });



    /*=========== Sticky Content Header ===========*/

    $(window).scroll(function() {

        if ($(window).width() > 992) {

            var sticky = $('.sticky-header'),
                scroll = $(window).scrollTop();

            if (scroll >= 150) {
                sticky.addClass('active');
            } else {
                sticky.removeClass('active');
            }

        }

    });


    /*=====Action Switch Use in Form Header====*/

    var chklength = $('.chk-status').length;

    if (chklength > 0) {

        $('.chk-status').each(function(i) {

            if ($('.chk-status').eq(i).prop("checked") == true) {

                $('.chk-status').eq(i).parents('.actn-btn').removeClass('inactive').addClass('active');

            } else if ($('.chk-status').eq(i).prop("checked") == false) {

                $('.chk-status').eq(i).parents('.actn-btn').removeClass('active').addClass('inactive');
            }

        })

    }

    $('.chk-status').on('click', function() {


        if ($(this).prop("checked") == true) {

            $(this).parents('.actn-btn').addClass('active').removeClass('inactive');

        } else if ($(this).prop("checked") == false) {

            $(this).parents('.actn-btn').addClass('inactive').removeClass('active');

        }

    });

    /*=========== Page Slide Section ===========*/

    var sliderFooterHt = $('.slide-footer').outerHeight();

    $('[data-toggle="slide"]').on('click', function() {
        $('body').addClass('slide');
    });

    $('[data-dismiss="slide"],.slide-overlay').on('click', function() {
        $('body').removeClass('slide');
    });

    if ($('.slide-footer').length > 0) {
        $('.slide-section').css('padding-bottom', sliderFooterHt);
    }

    /*=========== POPOVER ===========*/

    $('[data-toggle="popover"]').popover({
        html: true,
    });

    $('body').on('click', function(e) {
        if ($(e.target).data('toggle') !== 'popover' && $(e.target).parents('.popover.in').length === 0) {
            $('[data-toggle="popover"]').popover('hide');
        }
    });

    /*===PAGE ACTIVE TAB===*/
    $('.tabs-link:first-child').addClass('active');
    $('.tabs-data:first-child').addClass('active');

    /*===PAGE TABS===*/
    $('.tabs-link').on('click', function() {

        var targetId = $(this).attr('rel');

        if (!$(this).hasClass('active')) {

            $('.tabs-link').removeClass('active');
            $(this).addClass('active');

            $('.tabs-data').removeClass('active');
            $(targetId).addClass('active');

        }
    });

    /*===SELECTPICKER===*/
    $('.selectpicker').selectpicker({
        windowPadding: [60, 0, 60, 0],
        dropupAuto: true,
        container: 'body',
        size: 5,
    });

    /*=========== BUTTON-CUSTOM-SELECTPICKER ===========*/
    $('.button-custom-selectpicker .dropdown-item').on('click', function() {
        $(this).parents('.dropdown-menu').siblings('.dropdown-toggle').find('.sel-val').text($(this).text());
    });

    /*===SEARCH WITH SELECT===*/
    $('.input-group .dropdown-item').on('click', function() {
        var selText = $(this).html();
        $(this).parent('.dropdown-menu').siblings('button.dropdown-toggle').html(selText);
    });

    /*===FILE-INPUT===*/
    var FILE = FILE || {};
    FILE.fileInputs = function() {
        var $this = $(this),
            $val = $this.val(),
            valArray = $val.split('\\'),
            newVal = valArray[valArray.length - 1],
            $label = $this.siblings('.custom-file-label');
        if (newVal !== '') {
            $label.text(newVal);
        }
    };
    $('input[type=file]').bind('change focus click', FILE.fileInputs);

    /*===TAGS INPUT===*/
    $('input[name="tagsinput"]').tagsinput({
        confirmKeys: [13, 32, 44]
    });

    $('.bootstrap-tagsinput + input').css({
        visibility: 'hidden',
        position: 'absolute',
        display: 'initial',
        top: "0",
        left: "0",
        bottom: "0",
        right: "0",
        height: "0",
        width: "0",
        zIndex: "-99999",
    });

    /*===DATE-RANGE-PICKER===*/

    var currDt = moment();

    $('input[name="daterange"]').daterangepicker({
        applyButtonClasses: "btn-theme-primary",
        cancelClass: "btn-theme-secondary",
        showDropdowns: true,
        autoApply: true,
        locale: {
            format: 'YYYY/MM/DD'
        },
        maxDate: currDt,
        parentEl: 'body',
    });

    $('.modal input[name="daterange"]').daterangepicker({
        parentEl: '.modal',
        autoApply: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY/MM/DD'
        },
        maxDate: currDt,
    });

    /*===DATE-RANGE-TIME-PICKER===*/
    $('input[name="daterangetime"]').daterangepicker({
        applyButtonClasses: "btn-theme-primary",
        cancelClass: "btn-theme-secondary",
        showDropdowns: true,
        timePicker: true,
        autoApply: true,
        locale: {
            format: 'YYYY/MM/DD hh:mm'
        },
        maxDate: currDt,
        parentEl: 'body',
    });

    $('.modal input[name="daterangetime"]').daterangepicker({
        parentEl: '.modal',
        autoApply: true,
        timePicker: true,
        locale: {
            format: 'YYYY/MM/DD hh:mm A'
        },
        maxDate: currDt,
        showDropdowns: true,
    });

    
    /*===SINGLE DATE-PICKER===*/
    $('input[name="singledate"]').daterangepicker({
        locale: {
            format: 'YYYY/MM/DD'
        },
        applyButtonClasses: "btn-theme-primary",
        cancelClass: "btn-theme-secondary",
        maxDate: currDt,
        autoApply: true,
        showDropdowns: true,
        singleDatePicker: true,
        parentEl: 'body',
    });

    $('.modal input[name="singledate"]').daterangepicker({
        parentEl: '.modal',
        locale: {
            format: 'YYYY/MM/DD'
        },
        maxDate: currDt,
        autoApply: true,
        singleDatePicker: true,
        showDropdowns: true,
    });

    /*===SINGLE DATE-TIME PICKER===*/
    $('input[name="singledatetime"]').daterangepicker({
        timePicker: true,
        locale: {
            format: 'YYYY/MM/DD hh:mm A'
        },
        applyButtonClasses: "btn-theme-primary",
        cancelClass: "btn-theme-secondary",
        maxDate: currDt,
        autoApply: true,
        showDropdowns: true,
        singleDatePicker: true,
        parentEl: 'body',
    });

    $('.modal input[name="singledatetime"]').daterangepicker({
        parentEl: '.modal',
        timePicker: true,
        locale: {
            format: 'YYYY/MM/DD hh:mm A'
        },
        maxDate: currDt,
        autoApply: true,
        singleDatePicker: true,
        showDropdowns: true,
    });

    /*===LISTING SEARCH SECTION===*/

    //content overlay add in content div 
    $('.content').append("<div class='content-overlay'></div>");

    $('.search-content').hide();

    $('.search-heading').on('click', function() {
        if ($(window).width() < 767.98) {

            $(this).parents('.search-section').toggleClass('active');
            $(this).next('.search-content').slideToggle(300);
            $('.content-overlay').toggleClass('active');

        } else {

            $(this).parents('.search-section').addClass('active');
            $(this).next('.search-content').slideDown(300);
            $('.content-overlay').addClass('active');
        }
    });

    if ($(window).width() < 767.98) {
        $(".search-heading .btns").detach().appendTo(".search-content");
    }

    $('.submit-search').on('click', function() {
        $('.search-heading').find('.title span').text('Modify your search');
        $('.search-section').removeClass('active');
        $('.search-heading').next('.search-content').slideToggle(300);
        $('.search-section').next('.listing-section').show();
        $('.search-section').next('.listing-section').next('.no-records-listing').hide();
        $('.content-overlay').removeClass('active');
    });

    $('.cancel-search').on('click', function() {
        $('.search-section').removeClass('active');
        $('.search-heading').next('.search-content').slideToggle(300);
        $('.content-overlay').removeClass('active');
    });


    if ($('.search-section').hasClass('active')) {
        $('.search-heading .title span').text('Advanced Search');
        $('.search-content').slideDown(300);
        $('.content-overlay').addClass('active');

    } else {
        $('.search-section').removeClass('active');
        $('.search-content').slideUp(300);
        $('.content-overlay').removeClass('active');
    }

    $(".search-heading .btn").click(function(e) {
        e.stopPropagation();
    });


    $('.content-overlay').on('click', function() {
        $('.search-content').slideUp(300);
        $('.search-section').removeClass('active');
        $(this).removeClass('active');
    })


    /*==  (For Listing table) chkAll / chkOne  ==*/
    $(".chkAll").on('change', function() {
        $(this).parents('.table').find('.chkOne').prop("checked", this.checked);
    });

    $(".chkOne").on('change', function() {

        if ($(".chkOne").length == $(".chkOne:checked").length) {

            $(this).parents('.table').find(".chkAll").prop("checked", true);

        } else {

            $(this).parents('.table').find(".chkAll").prop("checked", false);
        }

    });
    
    $('#openChangePassModal').on('click', function() {
    	$('#changePasswordModal').modal('show');
    });
    
	$('#getFragment').click(function (event) {
	    event.preventDefault();
	    $('#replaceDiv').replaceWith(get($(this).attr('id2')));
		$('.selectpicker').selectpicker('refresh');
    });
}

/*! common function written in this file all rights are reserved!!!! */

function getInvoice(type, id) {
	var windowProperties = "location=yes,height=900,width=1000,scrollbars=yes,status=yes";
	var url = "/pdf/" + type + "/" + id;
	window.open(url, "Duplicate Invoice", windowProperties);
}

function message(type, message) {
	var heading = null;

	switch (type) {
	case "info":
		heading = "Information";
		break;
	case "success":
		heading = "Success";
		break;
	case "warning":
		heading = "Warning";
		break;
	case "error":
		heading = "Error";
		break;
	}
	
	$.toast({
	    text: message, // Text that is to be shown in the toast
	    heading: heading, // Optional heading to be shown on the toast
	    icon: type, // Type of toast icon
	    showHideTransition: 'fade', // fade, slide or plain
	    allowToastClose: true, // Boolean value true or false
	    hideAfter: 4000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
	    stack: 1, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
	    position: 'top-center', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values

	    textAlign: 'left',  // Text alignment i.e. left, right or center
	    loader: true,  // Whether to show loader or not. True by default
	    loaderBg: '#9EC600',  // Background color of the toast loader
	    beforeShow: function () {}, // will be triggered before the toast is shown
	    afterShown: function () {}, // will be triggered after the toat has been shown
	    beforeHide: function () {}, // will be triggered before the toast gets hidden
	    afterHidden: function () {}  // will be triggered after the toast has been hidden
	});
}

function closeButtonMessage(type, message) {
	toastr.options = {
		"closeButton" : true,
		"debug" : false,
		"newestOnTop" : false,
		"progressBar" : true,
		"positionClass" : "toast-top-center",
		"preventDuplicates" : true,
		"onclick" : null,
		"showDuration" : "300",
		"hideDuration" : "10000",
		"timeOut" : "10000",
		"extendedTimeOut" : "1000",
		"showEasing" : "swing",
		"hideEasing" : "linear",
		"showMethod" : "fadeIn",
		"hideMethod" : "fadeOut"
	}

	switch (type) {
	case "info":
		toastr.info(message);
		break;
	case "success":
		toastr.success(message);
		break;
	case "warning":
		toastr.warning(message);
		break;
	case "error":
		toastr.error(message);
		break;
	}
}

function isNullOrUndef(variable) {
    return jQuery.isEmptyObject(variable);
}

function isNotNullOrUndef(variable) {
    return !jQuery.isEmptyObject(variable);
}

function applyDateTimePicker(id){	
	$(id).daterangepicker({
		locale: {
            format: "YYYY-MM-DD HH:mm:ss"
        },
        applyButtonClasses: "btn-theme-primary",
        cancelClass: "btn-theme-secondary",
        timePicker24Hour: true,
        startDate: moment(),
        open: "center",
        timePicker: true,
        autoApply: true,
        showDropdowns: true,
        singleDatePicker: true,
        parentEl: 'body'
	});
}

function setDateTimeToPicker(id, dt){	
	$(id).daterangepicker({
		locale: {
            format: "YYYY-MM-DD HH:mm:ss"
        },
        applyButtonClasses: "btn-theme-primary",
        cancelClass: "btn-theme-secondary",
        timePicker24Hour: true,
        startDate: dt,
        open: "center",
        timePicker: true,
        autoApply: true,
        showDropdowns: true,
        singleDatePicker: true,
        parentEl: 'body'
	});
}

function setDatePicker(id, dt){
	$(id).daterangepicker({
	    "singleDatePicker": true,
	    "autoUpdateInput" : false,
		"showDropdowns" : true,
		"startDate": dt,
		"autoApply": true,
		"opens" : "center",
	    minYear: parseInt(moment().format('YYYY'),0)-1,
	});
}

function setStartDatePicker(id, dt){
	$(id).daterangepicker({
	    "singleDatePicker": true,
	    "autoUpdateInput" : false,
		"showDropdowns" : true,
		"startDate": dt,
		"autoApply": true,
		"opens" : "center",
	    maxDate: moment(),
	    minYear: parseInt(moment().format('YYYY'),0)-15,
	});
}

function setEndDatePicker(id, dt){
	$(id).daterangepicker({
		"singleDatePicker": true,
	    "autoUpdateInput" : false,
		"showDropdowns" : true,
		"startDate": dt,
		"autoApply": true,
		"opens" : "center",
	    minDate: moment(),
	    maxYear: parseInt(moment().format('YYYY'),0)+15,
	});
}

function applyDatePicker(id, format){
	$(id).on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format(format));
	});
}

function drawBarChart(divId, titleSize, title, chartData){
	/* daily chart for orderer page */
	Highcharts.chart(divId, {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: title,
	        style: {
                fontSize: titleSize,
                fontFamily: 'Verdana, sans-serif'
            }
	    },
	    xAxis: {
	        type: 'category',
	        title: {
	            text: 'Material'
	        },
	        labels: {
	            rotation: -45,
	            style: {
	                fontSize: '13px',
	                fontFamily: 'Verdana, sans-serif'
	            }
	        }
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Quantity in brass'
	        }
	    },
	    legend: {
	        enabled: false
	    },
	    tooltip: {
	        pointFormat: '<b>{point.y:.1f} brass</b>'
	    },
	    series: [{
	        name: 'Material',
	        data: chartData,
	        dataLabels: {
	            enabled: true,
	            rotation: -90,
	            color: '#FFFFFF',
	            align: 'right',
	            format: '{point.y:.2f}', // one decimal
	            y: 10, // 10 pixels down from the top
	            style: {
	                fontSize: '12px',
	                fontFamily: 'Verdana, sans-serif'
	            }
	        }
	    }]
	});
}

function getCurrDate(dateFormat){
	return moment().format(dateFormat);
}

function getStartDateTime(dateFormat){
	return moment().format(dateFormat) + " 00:00:01";
}

function getEndDateTime(dateFormat){
	return moment().format(dateFormat) + " 23:59:59";
}

function numberInString(prefix, number, length) {
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return prefix + str;
}

function getAddrString(address){
	return address.street + ", " + address.city + ", " + address.state.name + ", " + address.pincode;
}

function getCurrentPage(){
	return window.location.href.split('#')[1];
}

function isDecimal(txt, evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46) {
      //Check if the text already contains the . character
      if (txt.value.indexOf('.') === -1) {
        return true;
      } else {
        return false;
      }
    } else {
      if (charCode > 31 &&
        (charCode < 48 || charCode > 57))
        return false;
    }
    return true;
}

function isNumeric(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

function allowNumberOnly(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

function isAlphaNumeric(evt){
	if (evt.shiftKey || evt.ctrlKey || evt.altKey) {
		evt.preventDefault();
     } 
     else {
         var key = evt.keyCode;
         if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
        	 evt.preventDefault();
         }
     }
}

function camelCase(id) {
	var str = $(id).val();
	if(!jQuery.isEmptyObject(str)){
		return str.replace(/(?:^|\s)\w/g, function(match) {
        	return match.toUpperCase();
    	});
	}
	return str;
}

function upperCase(id) {
	var str = $(id).val();
	if(!jQuery.isEmptyObject(str)){
	  return str.toUpperCase();
	}
	return str;
}

function lowerCase(id) {
	var str = $(id).val();
	if(!jQuery.isEmptyObject(str)){
	  return str.toLowerCase();
	}
	return str;
}

/*If selection supports to multiple then expCount should be 0 else 1(Single Selection)
appending object list which will have key, value.
*/
function appendObjectOptions(list, appendId, key, value, expCount){
	var optionCount = $(appendId+' option').length;
	if(optionCount === expCount){
		$.each(list, function(index, data) {
			$(appendId).append(
					$('<option>').val(eval('data.'+key)).text(eval('data.'+value)));
		});
	}
}

/*If selection supports to multiple then expCount should be 0 else 1(Single Selection)
appending list which don't have any key value pair. It's plain list
*/
function appendListOptions(list, appendId, expCount){
	var optionCount = $(appendId+' option').length;
	if(optionCount === expCount){
		$.each(list, function(index, data) {
			$(appendId).append(
					$('<option>').val(data).text(data));
		});
	}
}