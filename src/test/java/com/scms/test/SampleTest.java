package com.scms.test;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;

import org.junit.Test;

public class SampleTest extends ScmsApplicationTest {
	
	@Test
	public void sampleTest() {
		// Get epoch timestamp using System.currentTimeMillis()
        long currentTimestamp = Instant.now().getEpochSecond();
        System.out.println("Current epoch timestamp in millis: " + currentTimestamp);
        
        String md5Str = getMd5(getMd5("12345678") + currentTimestamp);
        System.out.println("md5Str : " + md5Str);
	}
	
	public static String getMd5(String input)
    {
        try {
  
            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
  
            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());
  
            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);
  
            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } 
  
        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
