package com.scms.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EncryptionTest extends ScmsApplicationTest {
	
	@Test
	public void encryptPassword() {
		String encPass = new BCryptPasswordEncoder().encode("yzaheska");
		log.debug("Encrypted Password: {}", encPass);
		
		assertNotNull("Encrypted password should not be null", encPass);
	}
}
