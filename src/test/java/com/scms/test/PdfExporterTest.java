package com.scms.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.scms.constants.Constants;
import com.scms.model.Item;
import com.scms.model.Orders;
import com.scms.util.Utility;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PdfExporterTest extends ScmsApplicationTest {

	PdfPTable mainTable = null;
	static Font blackFont10Bold = FontFactory.getFont("Helvetica", 10.0F, 1, new CMYKColor(59, 14, 0, 220));
	static Font blackFont10 = FontFactory.getFont("Helvetica", 10.0F, 0, new CMYKColor(59, 14, 0, 220));
	static Font blackFont15 = FontFactory.getFont("Helvetica", 12.0F, 0, new CMYKColor(59, 14, 0, 220));
	static Font whiteFont10 = FontFactory.getFont("Helvetica", 10.0F, 0, new CMYKColor(0, 0, 0, 0));
       
    private void writeHeaderLine() {
        String[] headers = {"Date", "Customer", "Order Id", "Material", "Quantity", "Rate", "Sub Total", "Gst %", "Gst Amt", "Total(ST+GA)", "Discount", "Received Amount", "Pending", "Pending Amount", "Payment Mode", "Site", "Remarks"};
        /* Used below loop to append CSV header part */
        for (String header : headers) {
			PdfPCell mtCell = new PdfPCell(new Paragraph(header, whiteFont10));
			mtCell.setBackgroundColor(new BaseColor(60, 124, 145));
			mtCell.setUseVariableBorders(true);
			mtCell.setBorderColor(BaseColor.BLACK);
			this.mainTable.addCell(mtCell);
        }
    }
    
    private void writeDataLines(List<Orders> orderList) {
    	for (Orders order : orderList) {
    		for (Item item : order.getItems()) {
    			this.mainTable.addCell(this.createDataRow(order.getOrderDate().toString()));
    			this.mainTable.addCell(this.createDataRow(order.getOrderDate().toString()));      
        		this.mainTable.addCell(this.createDataRow(order.getCustomer().getName()));
        		this.mainTable.addCell(this.createDataRow(order.getSid()));   
        		this.mainTable.addCell(this.createDataRow(item.getMaterial().getName()));
        		this.mainTable.addCell(this.createDataRow(item.getQtyBrass().toString()));
        		this.mainTable.addCell(this.createDataRow(item.getRate().toString()));
        		this.mainTable.addCell(this.createDataRow(item.getSubTotal().toString()));
        		this.mainTable.addCell(this.createDataRow(item.getGst().toString()));
        		this.mainTable.addCell(this.createDataRow(item.getGstAmt().toString()));
        		this.mainTable.addCell(this.createDataRow(order.getTotal().toString()));
        		this.mainTable.addCell(this.createDataRow(order.getDiscount().toString()));
        		this.mainTable.addCell(this.createDataRow(order.getReceivedAmt().toString()));
        		this.mainTable.addCell(this.createDataRow(String.valueOf(order.isPending())));
        		this.mainTable.addCell(this.createDataRow(order.getPendingAmt().toString()));
        		this.mainTable.addCell(this.createDataRow(order.getPayMode().name()));
        		this.mainTable.addCell(this.createDataRow(order.getSite().getName()));
        		this.mainTable.addCell(this.createDataRow(order.getRemarks()));
    		}
        }
    }
    
    private PdfPCell createDataRow(String cellValue) {
    	PdfPCell mtCell = new PdfPCell(new Paragraph(cellValue, blackFont10Bold));
		mtCell.setBackgroundColor(new BaseColor(60, 124, 145));
		mtCell.setHorizontalAlignment(1);
		mtCell.setVerticalAlignment(1);
		mtCell.setPaddingBottom(5.0F);
		mtCell.setPaddingTop(15.0F);
		//mtCell.setColspan(9);
		mtCell.setUseVariableBorders(true);
		mtCell.setBorderColor(new BaseColor(60, 124, 145));

		return mtCell;
    }

	public String export(List<Orders> orderList, String fileName) {
		String filePath = null;
        try {
        	filePath = Constants.PATH_SRC_FILES + File.separator + "scheduler/pdf" + File.separator + fileName + Constants.DOT_PDF_EXT;
        	log.debug("CSV created : " + filePath);
        	
        	/* Before creating tmp file check the if it already exists */
            Utility.deleteFileIfExists(filePath);
        	
        	File file = new File(filePath);
    		Document document = new Document(PageSize.TABLOID);

    		FileOutputStream outputFile = new FileOutputStream(file);
			log.debug("file path : {}", file.getAbsolutePath());
			
			final PdfWriter writer = PdfWriter.getInstance(document, outputFile);
			document.open();
			
			this.mainTable = new PdfPTable(17);
			this.mainTable.setWidthPercentage(100.0F);
			this.mainTable.setSpacingBefore(10.0F);
			this.mainTable.setSpacingAfter(10.0F);
			final float[] mainTableWidth = { 1.0F, 1.2F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 2.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F };
			this.mainTable.setWidths(mainTableWidth);
			
            writeHeaderLine();
            //writeDataLines(orderList);
            document.add(this.mainTable);
            
            document.close();
			writer.close();
            log.info("pdf created successfully !!!");
		} catch (IOException | DocumentException e) {
			e.printStackTrace();
		}
        return filePath;
	}
	
	@Test
	public void pdfExporterTest() {
		export(null, "12");
	}
}
