package com.scms.test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import com.scms.constants.Constants;
import com.scms.model.Item;
import com.scms.model.Orders;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExcelExporterTest extends ScmsApplicationTest {
	
	@Test
	public void excelExporterTest2() {
		export(null, "12");
	}

	private XSSFWorkbook workbook;
    private XSSFSheet sheet;
       
    private void writeHeaderLine() {
        sheet = workbook.createSheet("Order sheet");
        String[] headers = {"DateTime", "Customer", "Order Id", "Material", "Quantity", "Rate", "Sub Total", "Gst %", "Gst Amt", "Total(ST+GA)", "Discount", "Received Amount", "Pending", "Pending Amount", "Payment Mode", "Site", "Remarks"};
        XSSFRow row = sheet.createRow(0);
        int cellNum = 0;
        
        for(String header : headers) {
        	createCell(row, cellNum++, header);
        }
    }
    
    private void createCell(XSSFRow row, int cellNum, Object value) {
        sheet.autoSizeColumn(cellNum);
        XSSFCell cell = row.createCell(cellNum);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }
    }
    
    private void writeDataLines(List<Orders> orderList) {
        int rowNum = 1;
        for (Orders order : orderList) {
        	XSSFRow row = sheet.createRow(rowNum);
            int cellNum = 0;
            
            for (Item item : order.getItems()) {
                createCell(row, cellNum++, order.getOrderDate());      
                createCell(row, cellNum++, order.getCustomer());       
                createCell(row, cellNum++, order.getSid());    
                createCell(row, cellNum++, item.getMaterial().getName());
                createCell(row, cellNum++, item.getQtyBrass());
                createCell(row, cellNum++, item.getRate());
                createCell(row, cellNum++, item.getSubTotal());
                createCell(row, cellNum++, item.getGst());
                createCell(row, cellNum++, item.getGstAmt());
                createCell(row, cellNum++, order.getTotal());
                createCell(row, cellNum++, order.getDiscount());
                createCell(row, cellNum++, order.getReceivedAmt());
                createCell(row, cellNum++, order.isPending());
                createCell(row, cellNum++, order.getPendingAmt());
                createCell(row, cellNum++, order.getPayMode());
                createCell(row, cellNum++, order.getSite());
                createCell(row, cellNum++, order.getRemarks());
            }
            
            rowNum++;
        }
    }

	public String export(List<Orders> orderList, String fileName) {
		String filePath = null;
        try {
        	filePath = Constants.PATH_SRC_FILES + "/scheduler/"+ fileName + ".xlsx";
        	
			/* An output stream accepts output bytes and sends them to sink */
            //OutputStream outFile = new FileOutputStream(filePath);
            
            /* Creating Workbook instances */
        	workbook = new XSSFWorkbook();
        	log.debug("workbook created");
    		
            writeHeaderLine();
            //writeDataLines(orderList);
            
			/* Write the output to a file */
            FileOutputStream fileOut = new FileOutputStream(filePath);
            workbook.write(fileOut);
            
            /* Closing the output stream */
            fileOut.close();

            // Closing the workbook
            workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        return filePath;
	}

}
